import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4Textfield from "./kode4-textfield";
let Kode4Textarea = class Kode4Textarea extends Kode4Textfield {
    constructor() {
        super();
        this.resize = 'vertical';
        this.confirmOnEnter = false;
    }
    get typeCssClass() {
        return 'kode4-textarea';
    }
    get inputCssSelector() {
        return 'textarea';
    }
    ;
    // --- RENDER -----------------
    renderInput() {
        return html `
            <textarea
                class="field-input ${this.resize}"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${(e) => e.stopPropagation()}"
                >${this.displayValue}</textarea>
        `;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            ${super.styles}

            textarea.field-input {
                min-height: 3em;
                height: 3em;
            }

            textarea.field-input.both {
                resize: both;
            }

            textarea.field-input.vertical {
                resize: vertical;
            }

            textarea.field-input.horizontal {
                resize: horizontal;
            }

            textarea.field-input.none {
                resize: none;
            }

        `;
    }
};
__decorate([
    property()
], Kode4Textarea.prototype, "resize", void 0);
Kode4Textarea = __decorate([
    customElement('kode4-textarea')
], Kode4Textarea);
export default Kode4Textarea;
