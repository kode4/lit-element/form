import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import debounce from "lodash-es/debounce";
// const kode4FieldTypes = [
//     'kode4-field',
//     'kode4-textfield',
//     'kode4-textarea',
//     'kode4-check',
//     'kode4-select',
//     'kode4-datetime-picker',
//     'kode4-custom-select',
// ];
let Kode4Form = class Kode4Form extends LitElement {
    constructor() {
        super(...arguments);
        this.action = '';
        this.method = 'get';
        this.isValid = false;
        this.formElementsInitialized = false;
        this.formElements = [];
        this.fieldsDirty = new FormData();
        this.dirty = false;
        this.debounce = 150;
        this.fieldsToAdd = [];
        this.fieldsToRemove = [];
        this.onFieldsUpdatedDebounced = debounce(this.onFieldsUpdated, this.debounce);
    }
    // protected async updated(changedProperties:any) {
    //     super.updated(changedProperties);
    //     await this.initFormElements();
    // }
    // protected async initFormElements() {
    //     const slt = <HTMLSlotElement>this.shadowRoot?.querySelector('slot');
    //     this.formElements = [];
    //     if (slt?.assignedNodes()) {
    //         Object.values(slt?.assignedNodes()).forEach(el => {
    //             if (el instanceof Kode4Field && !(el instanceof Kode4Button)) {
    //                 this.formElements.push(el);
    //             } else if (el instanceof HTMLElement) {
    //                 this.formElements = [
    //                     ...this.formElements,
    //                     ...<NodeListOf<Kode4Field>>el.querySelectorAll(kode4FieldTypes.join(','))
    //                 ];
    //             }
    //         });
    //     }
    //     for (let i=0; i<this.formElements.length; i+=1) {
    //         const el = this.formElements[i]
    //         await el.isInitialized;
    //         if (!el.classList.contains('kode4-form-initialized')) {
    //             el.classList.add('kode4-form-initialized');
    //         }
    //     }
    //     this.formElementsInitialized = true;
    //     await this.applyConditions();
    //     return true;
    // }
    connectedCallback() {
        this.addEventListener('kode4-field-destroyed', (e) => {
            this.fieldDestroyed(e);
        });
        super.connectedCallback();
    }
    disconnectedCallback() {
        super.disconnectedCallback();
    }
    checkDirty(e) {
        // filter all fields that are not in the list
        const dirtyBefore = this.dirty;
        for (let key of [...this.fieldsDirty.keys()]) {
            if (!this.formElements.find((el) => el.name === key)) {
                this.fieldsDirty.delete(key);
            }
        }
        this.dirty = [...this.fieldsDirty.keys()].length > 0;
        this.dirty ? this.classList.add('kode4-dirty') : this.classList.remove('kode4-dirty');
        if (dirtyBefore !== this.dirty) {
            const detail = {
                dirty: this.dirty,
            };
            if (e) {
                detail.originalEvent = e;
            }
            let event = new CustomEvent("stateChanged", {
                detail: detail
            });
            this.dispatchEvent(event);
        }
    }
    async onChange(e) {
        const { name, dirty } = e.detail;
        if (dirty && !this.fieldsDirty.has(name)) {
            this.fieldsDirty.set(name, dirty);
        }
        else if (!dirty && this.fieldsDirty.has(name)) {
            this.fieldsDirty.delete(name);
        }
        this.checkDirty();
        await this.applyConditions();
        return;
    }
    async applyConditions() {
        for (let i = 0; i < this.formElements.length; i += 1) {
            await this.formElements[i].applyConditions(this);
        }
        return;
    }
    getField(name) {
        return this.formElements.reduce((result, el) => {
            if (el.name === name) {
                result = el;
            }
            return result;
        }, null);
    }
    // @property({ type: Boolean })
    // public dirty:boolean = false;
    // public get onDirtyChange():boolean {
    //     let dirty = false;
    //     this.formElements.forEach((el:Kode4Field) => {
    //         dirty = dirty || el.dirty;
    //     });
    //     return dirty;
    // }
    validate() {
        let valid = true;
        this.formElements.forEach(el => {
            try {
                if (el.canValidate && !el.valid) {
                    valid = false;
                }
            }
            catch (e) {
                console.warn('unexpected error while validating', e);
            }
        });
        return valid;
    }
    get formData() {
        const formData = new FormData();
        this.formElements.map(({ name, stringValue, hidden, disabled, hasValue, manageValue }) => {
            if (manageValue && hasValue && name && !hidden && !disabled) {
                formData.append(name, stringValue);
            }
        });
        // this.formElements.map(({name, value, stringValue, hidden, disabled, hasValue, manageValue}) => {
        //     if (manageValue && hasValue && name && !hidden && !disabled) {
        //         formData.append(<string>name, <string>stringValue);
        //     }
        // });
        return formData;
    }
    onResetForm(e) {
        e.preventDefault();
        this.reset();
    }
    onClearForm(e) {
        e.preventDefault();
        this.clear();
    }
    // protected onUpdateFields(e:Event) {
    //     e.preventDefault();
    //     this.updateFields();
    // }
    onUpdateValues(e) {
        e.preventDefault();
        this.updateValues();
    }
    onRespawnForm(e) {
        e.preventDefault();
        this.respawn();
    }
    onSubmit(e) {
        var _a, _b;
        console.log('GO SUBMIT!');
        e.preventDefault();
        let event;
        const formData = this.formData;
        if (e.detail.name && e.detail.value) {
            formData.append(e.detail.name, e.detail.value);
        }
        if (this.validate()) {
            event = new CustomEvent("submit", {
                bubbles: true,
                detail: {
                    formData,
                    form: this,
                }
            });
            console.log('SUBMIT:');
            console.log([...(_a = event.detail.formData) === null || _a === void 0 ? void 0 : _a.entries()]);
        }
        else {
            event = new CustomEvent("submit-error", {
                bubbles: true,
                detail: {
                    formData,
                    form: this,
                }
            });
            console.log('SUBMIT-ERROR:');
            console.log([...(_b = event.detail.formData) === null || _b === void 0 ? void 0 : _b.entries()]);
        }
        this.dispatchEvent(event);
    }
    reset() {
        this.formElements.forEach(el => {
            el.reset();
        });
    }
    clear() {
        this.formElements.forEach(el => {
            el.clear();
        });
    }
    // public async updateFields() {
    //     await this.initFormElements();
    //     this.updateValues();
    // }
    updateValues() {
        this.formElements.forEach(el => {
            el.updateValue();
        });
    }
    respawn() {
        this.formElements.forEach(el => {
            el.respawn();
        });
    }
    fieldCreated(e) {
        var _a, _b, _c;
        if (!((_b = (_a = e.detail) === null || _a === void 0 ? void 0 : _a.field) === null || _b === void 0 ? void 0 : _b.name)) {
            return;
        }
        this.fieldsToAdd.push((_c = e.detail) === null || _c === void 0 ? void 0 : _c.field);
        this.fieldsToRemove.filter((fieldid) => { var _a, _b; return fieldid !== ((_b = (_a = e.detail) === null || _a === void 0 ? void 0 : _a.field) === null || _b === void 0 ? void 0 : _b.id); });
        this.onFieldsUpdatedDebounced();
    }
    fieldDestroyed(e) {
        var _a, _b;
        if (!((_a = e.detail) === null || _a === void 0 ? void 0 : _a.fieldid)) {
            return;
        }
        this.fieldsToRemove.push((_b = e.detail) === null || _b === void 0 ? void 0 : _b.fieldid);
        this.fieldsToAdd.filter((el) => { var _a; return el.id !== ((_a = e.detail) === null || _a === void 0 ? void 0 : _a.fieldid); });
        this.onFieldsUpdatedDebounced();
    }
    onFieldsUpdated() {
        const formElements = [
            ...this.formElements.filter((fe) => !this.fieldsToRemove.find((fieldid) => fieldid === fe.id))
        ];
        this.fieldsToAdd.forEach((el) => {
            if (!formElements.find((fe) => fe.id === el.id)) {
                formElements.push(el);
            }
        });
        this.fieldsToAdd = [];
        this.fieldsToRemove = [];
        this.formElements = [
            ...formElements,
        ];
        this.applyConditions();
        this.checkDirty();
        this.formElementsInitialized = true;
    }
    render() {
        // @updateFields="${this.onUpdateFields}"
        return html `
        <form
            class="kode4-form ${this.formElementsInitialized ? 'initialized' : ''}"
            action="${this.action}"
            method="${this.method}"
            @submitForm="${this.onSubmit}"
            @change="${this.onChange}"
            @resetForm="${this.onResetForm}"
            @clearForm="${this.onClearForm}"
            @updateValues="${this.onUpdateValues}"
            @respawnForm="${this.onRespawnForm}"
            @kode4-field-created="${this.fieldCreated}"
            >
            <slot></slot>
        </form>
        `;
        // @managedValueChanged="${this.onManagedValueChanged}"
    }
    static get styles() {
        return css `
            form:not(.initialized) {
                visibility: hidden;
                opacity: 0;
            }

            form {
                transition: opacity 0.4s ease-in-out;
            }
        `;
    }
};
__decorate([
    property()
], Kode4Form.prototype, "action", void 0);
__decorate([
    property()
], Kode4Form.prototype, "method", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Form.prototype, "formElementsInitialized", void 0);
__decorate([
    property()
], Kode4Form.prototype, "dirty", void 0);
__decorate([
    property({ type: Number })
], Kode4Form.prototype, "debounce", void 0);
Kode4Form = __decorate([
    customElement('kode4-form')
], Kode4Form);
export default Kode4Form;
