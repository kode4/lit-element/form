import Kode4Field from "./kode4-field";
export default class Kode4Formblock extends Kode4Field {
    protected get typeCssClass(): string;
    constructor();
    get value(): String | Number | Boolean | undefined;
    set value(v: String | Number | Boolean | undefined);
    protected render(): import("lit-html").TemplateResult<1>;
}
