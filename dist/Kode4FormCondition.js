export const kode4CustomConditionFieldEquals = (fieldname, value) => (form /*, field:Kode4Field*/) => {
    const fld = form.getField(fieldname);
    return Boolean((fld === null || fld === void 0 ? void 0 : fld.value) == value);
};
export const kode4CustomConditionFieldEqualsNot = (fieldname, value) => (form /*, field:Kode4Field*/) => {
    const fld = form.getField(fieldname);
    return Boolean((fld === null || fld === void 0 ? void 0 : fld.value) != value);
};
/* _form is prefixed with an underscore for typescript */
export const kode4FormCustomConditionHandlerActivate = (options = ['hidden', 'disabled']) => (_form, field) => {
    if (options.includes('hidden')) {
        field.hidden = false;
    }
    if (options.includes('disabled')) {
        field.disabled = false;
    }
};
/* _form is prefixed with an underscore for typescript */
export const kode4FormCustomConditionHandlerDeactivate = (options = ['hidden', 'disabled']) => (_form, field) => {
    if (options.includes('hidden')) {
        field.hidden = true;
    }
    if (options.includes('disabled')) {
        field.disabled = true;
    }
};
export const showIfValue = (fieldname, value) => {
    return {
        conditions: kode4CustomConditionFieldEquals(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerActivate(),
        handleElse: kode4FormCustomConditionHandlerDeactivate(),
    };
};
export const hideIfValue = (fieldname, value) => {
    return {
        conditions: kode4CustomConditionFieldEquals(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerDeactivate(),
        handleElse: kode4FormCustomConditionHandlerActivate(),
    };
};
export const showIfNotValue = (fieldname, value) => {
    return {
        conditions: kode4CustomConditionFieldEqualsNot(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerActivate(),
        handleElse: kode4FormCustomConditionHandlerDeactivate(),
    };
};
export const hideIfNotValue = (fieldname, value) => {
    return {
        conditions: kode4CustomConditionFieldEqualsNot(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerDeactivate(),
        handleElse: kode4FormCustomConditionHandlerActivate(),
    };
};
