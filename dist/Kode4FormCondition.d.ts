import Kode4Field from "./kode4-field";
import Kode4Form from "./kode4-form";
export interface Kode4FormConditionExpression {
    conditions: Kode4FormCustomCondition | Array<Kode4FormCustomCondition>;
    handleThen?: Kode4FormCustomConditionThenHandler;
    handleElse?: Kode4FormCustomConditionElseHandler;
}
export declare type Kode4FormCustomCondition = (form: Kode4Form, field: Kode4Field) => boolean;
export declare type Kode4FormCustomConditionThenHandler = (form: Kode4Form, field: Kode4Field) => void;
export declare type Kode4FormCustomConditionElseHandler = (form: Kode4Form, field: Kode4Field) => void;
export declare const kode4CustomConditionFieldEquals: (fieldname: string, value: any) => (form: Kode4Form) => boolean;
export declare const kode4CustomConditionFieldEqualsNot: (fieldname: string, value: any) => (form: Kode4Form) => boolean;
export declare const kode4FormCustomConditionHandlerActivate: (options?: Array<string>) => (_form: Kode4Form, field: Kode4Field) => void;
export declare const kode4FormCustomConditionHandlerDeactivate: (options?: Array<string>) => (_form: Kode4Form, field: Kode4Field) => void;
export declare const showIfValue: (fieldname: string, value: any) => Kode4FormConditionExpression;
export declare const hideIfValue: (fieldname: string, value: any) => Kode4FormConditionExpression;
export declare const showIfNotValue: (fieldname: string, value: any) => Kode4FormConditionExpression;
export declare const hideIfNotValue: (fieldname: string, value: any) => Kode4FormConditionExpression;
