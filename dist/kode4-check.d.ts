import Kode4Field from "./kode4-field";
import '@kode4/ui/kode4-fa-icon';
export default class Kode4Check extends Kode4Field {
    protected trueValue: String | Number | Boolean | undefined;
    protected falseValue: String | Number | Boolean | undefined;
    protected get typeCssClass(): string;
    protected firstUpdated(changedProperties: any): void;
    protected get displayValue(): String | Number | Boolean | undefined;
    get stringValue(): string;
    protected sanititzeValue(v: any): String | Number | Boolean | undefined;
    constructor();
    handleEsc(): void;
    protected onInputChange(e: Event): void;
    protected render(): import("lit-html").TemplateResult<1>;
    protected renderInput(): import("lit-html").TemplateResult<1>;
    renderLabel(text: String): import("lit-html").TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
