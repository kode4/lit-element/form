import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { unsafeHTML } from "lit/directives/unsafe-html.js";
import Kode4Textfield from "./kode4-textfield";
import { Kode4StaticOptionsProvider } from "./Kode4OptionsProvider";
import debounce from "lodash-es/debounce";
let Kode4Select = class Kode4Select extends Kode4Textfield {
    // --- INSTANTIATION -----------------
    constructor() {
        super();
        this._options = [];
        this.optionHighlighted = -1;
        this.searchTerm = undefined;
        this.filters = new FormData();
        this.forceSelection = false;
        this.onSearchTermChangeDebounced = debounce(this.onSearchTermChange, this.debounce);
        this.type = 'select';
        this.action = true;
        this.hasDialog = true;
        this.openDialogOnFocus = this.type === 'select';
        this.changeOnType = true;
        document.addEventListener("click", this.onOutsideClick.bind(this));
    }
    get typeCssClass() {
        return `kode4-${this.type}`;
    }
    get value() {
        return super.value;
    }
    set value(v) {
        var _a;
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }
        super.value = v;
        if (v === undefined) {
            if (this.type === 'autocomplete') {
                this.searchTerm = undefined;
            }
            this._selectedOption = undefined;
            this.onChange();
        }
        else {
            if (((_a = this._selectedOption) === null || _a === void 0 ? void 0 : _a.value) != v) {
                (async () => {
                    var _a;
                    try {
                        if (!v || v == 'undefined' || v == 'null') {
                            throw new Error('Skip getItem on optionsProvider, value is falsy');
                        }
                        if (this.type === 'autocomplete') {
                            this.searchTerm = undefined;
                        }
                        let selectedOption = await ((_a = this.optionsProvider) === null || _a === void 0 ? void 0 : _a.getItem(v));
                        this._selectedOption = selectedOption;
                        this.onChange();
                    }
                    catch (e) {
                        super.value = undefined;
                        this._selectedOption = undefined;
                    }
                })();
            }
        }
    }
    get displayValue() {
        var _a;
        if (this.type === 'autocomplete' && this.searchTerm !== undefined && this.searchTerm !== ((_a = this.selectedOption) === null || _a === void 0 ? void 0 : _a.value)) {
            return this.searchTerm;
        }
        if (!this.selectedOption) {
            return '';
        }
        return this.selectedOption.unsafe ? html `${unsafeHTML(String(this.selectedOption.label))}` : this.selectedOption.label;
    }
    get options() {
        return this._options;
    }
    set options(options) {
        if (!this.initialized && !this.initializing) {
            this._optionsCandidate = options;
            return;
        }
        this._options = options.map(el => {
            if (typeof el === 'object') {
                if (this.valueField) {
                    el.value = el[this.valueField];
                }
                if (this.labelField) {
                    el.label = el[this.labelField];
                }
            }
            return typeof el === 'object' ? {
                ...el,
                value: el.value || undefined,
                label: el.label || '',
                unsafe: el.unsafe || false
            } : {
                value: el,
                label: el,
                unsafe: false
            };
        });
    }
    get optionsProvider() {
        return this._optionsProvider;
    }
    set optionsProvider(optionsProvider) {
        if (this._optionsProvider) {
            this._optionsProvider.unsubscribe(this);
        }
        this._optionsProvider = optionsProvider;
        this.initOptionsProvider();
    }
    get selectedOption() {
        if (this._selectedOption) {
            return this._selectedOption;
        }
        return undefined;
    }
    set selectedOption(selectedOption) {
        this._selectedOption = selectedOption;
        this.value = (selectedOption === null || selectedOption === void 0 ? void 0 : selectedOption.value) || undefined;
    }
    getOptionByValue(v) {
        if (!v) {
            return undefined;
        }
        return this.options.reduce((result, el) => {
            return (el === null || el === void 0 ? void 0 : el.value) == v ? el : result;
        }, undefined);
    }
    get busy() {
        if (super.busy) {
            return true;
        }
        if (!this.optionsProvider) {
            return false;
        }
        return this.optionsProvider.busy;
    }
    async firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        if (this.openDialogOnFocus === undefined) {
            this.openDialogOnFocus = true;
        }
    }
    // protected async updated(changedProperties: Map<PropertyKey, unknown>) {
    //     super.updated(changedProperties);
    //     if (changedProperties.has('_options') && this.type !== 'autocomplete' && !this.optionsProvider) {
    //         this.value = this.value;
    //     }
    // }
    async doInit(updateValueFromAttribute = false) {
        this.initOptionsProvider();
        // if (!this.optionsProvider) {
        //     const op = new Kode4StaticOptionsProvider();
        //     op.list = this._optionsCandidate || [];
        //     this.optionsProvider = op;            
        // }
        // this.optionsProvider.subscribe(this);
        // this.updateOptionsFromOptionsProvider();
        if (updateValueFromAttribute) {
            this.updateValue();
        }
        else {
            this.value = this._valueCandidate;
        }
        this.initialValue = this.value;
        if (this.outline) {
            this.busyStyle = 'icon';
        }
    }
    async initOptionsProvider() {
        if (!this.optionsProvider) {
            const op = new Kode4StaticOptionsProvider();
            op.list = this._optionsCandidate || [];
            this.optionsProvider = op;
        }
        this.optionsProvider.subscribe(this);
        this.updateOptionsFromOptionsProvider();
    }
    handleListUpdated() {
        this.updateOptionsFromOptionsProvider();
    }
    async updateOptionsFromOptionsProvider() {
        var _a;
        try {
            this.handleOptionsProviderListResponse(await ((_a = this.optionsProvider) === null || _a === void 0 ? void 0 : _a.getList(this.filters)));
        }
        catch (e) {
            this.handleOptionsProviderListResponseError(e);
        }
    }
    async handleOptionsProviderListResponse(lr) {
        var _a;
        try {
            if (lr.error !== 0) {
                throw new Error('Errorcode');
            }
            this.options = lr.list;
            this.dialogErrorMessage = undefined;
            if (!this.value) {
                this.updateValue();
            }
            const changedSelectedOption = await ((_a = this.optionsProvider) === null || _a === void 0 ? void 0 : _a.checkItemChangedAfterListUpdate(String(this.value), this._selectedOption));
            if (changedSelectedOption !== false) {
                this._selectedOption = changedSelectedOption;
            }
        }
        catch (e) {
            this.handleOptionsProviderListResponseError(e, lr);
        }
    }
    handleOptionsProviderListResponseError(e, lr) {
        this.options = [];
        this.dialogErrorMessage = (lr === null || lr === void 0 ? void 0 : lr.errorMessage) || 'Beim Laden ist ein Fehler aufgetreten';
        throw e;
    }
    // --- UI MANIPULATON -----------------
    addFocusClass() {
    }
    removeFocusClass() {
    }
    handleOnFocus() {
    }
    handleOnBlur() {
        this.optionHighlighted = -1;
        this.searchTerm = undefined;
        // console.log('SELECT BLUR', this.value, this.selectedOption?.value, this.searchTerm, this.input?.value, this.displayValue);
        // if (this.value !== this.selectedOption?.value) {
        //     console.log('Value not synced :(');
        //     this.searchTerm = undefined;
        //     // this.value = this.resetValue;
        // } else {
        //     console.log('Value synced :)');
        // }
        // this.requestUpdate();
    }
    highlightPrevOption() {
        if (!this.options) {
            this.optionHighlighted = -1;
            return;
        }
        if (this.optionHighlighted === -1) {
            this.optionHighlighted = this.options.length - 1;
            return;
        }
        this.optionHighlighted -= 1;
        if (this.optionHighlighted < 0) {
            this.optionHighlighted = 0;
        }
    }
    highlightNextOption() {
        if (!this.options) {
            this.optionHighlighted = -1;
            return;
        }
        if (this.optionHighlighted === -1) {
            this.optionHighlighted = 0;
            return;
        }
        this.optionHighlighted += 1;
        if (this.optionHighlighted > this.options.length - 1) {
            this.optionHighlighted = this.options.length - 1;
        }
    }
    selectHighlightedOption() {
        var _a, _b, _c, _d;
        if (this.optionHighlighted !== -1) {
            this.value = (_a = this.options[this.optionHighlighted]) === null || _a === void 0 ? void 0 : _a.value;
            this.searchTerm = undefined;
        }
        else if (((_b = this.searchTerm) === null || _b === void 0 ? void 0 : _b.length) && this.options.length) {
            this.value = (_c = this.options[0]) === null || _c === void 0 ? void 0 : _c.value;
            this.searchTerm = undefined;
        }
        else if (this.forceSelection && this.options.length && !this.value) {
            this.value = (_d = this.options[0]) === null || _d === void 0 ? void 0 : _d.value;
            this.searchTerm = undefined;
        }
        else if (!this.options.length) {
            this.value = undefined;
            this.searchTerm = undefined;
        }
        // this.selectOption(this.options[this.optionHighlighted]);
        // this.selectedOption = this.options[this.optionHighlighted];
    }
    // --- EVENTS -----------------
    onChange(originalEvent) {
        this.touched = true;
        let event = new CustomEvent("change", {
            bubbles: true,
            composed: true,
            detail: {
                disabled: this.disabled,
                manageValue: this.manageValue,
                value: this.value,
                valueObject: this._selectedOption,
                name: this.name,
                originalEvent: originalEvent,
                dirty: this.dirty,
            }
        });
        if (originalEvent) {
            originalEvent.preventDefault();
        }
        this.dispatchEvent(event);
    }
    onKeydown(e) {
        switch (e.key) {
            case 'Tab':
                if (this.dialogOpen) {
                    this.selectHighlightedOption();
                    this.closeDialog();
                }
                return;
            default:
                if (this.type !== 'autocomplete') {
                    e.preventDefault();
                }
        }
    }
    onKeyup(e) {
        var _a;
        switch (e.key) {
            case 'Escape':
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                else {
                    this.handleEsc();
                }
                return;
            case 'ArrowDown':
                if (!this.dialogOpen) {
                    this.openDialog();
                }
                else {
                    this.highlightNextOption();
                }
                return;
            case 'ArrowUp':
                if (!this.dialogOpen) {
                    this.openDialog();
                }
                else {
                    this.highlightPrevOption();
                }
                return;
            case 'ArrowRight':
                if (this.dialogOpen) {
                    // this can be nice if typing is not allowed.
                    // but I think moving the cursor left an right
                    // while typing is more convenient than
                    // making a selection with ArrowRight.
                    // this.selectHighlightedOption();
                }
                else {
                    this.openDialog();
                }
                return;
            case 'ArrowLeft':
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                return;
            case 'Enter':
                if (this.dialogOpen) {
                    this.selectHighlightedOption();
                    this.closeDialog();
                }
                else {
                    this.onSubmit(e);
                }
                return;
            case 'Tab':
                // action handled in onKeydown
                return;
            default:
                if (this.type === 'autocomplete') {
                    if (!this.dialogOpen) {
                        this.openDialog();
                    }
                    this.onSearchTermChangeDebounced((_a = this.input) === null || _a === void 0 ? void 0 : _a.value);
                }
                e.preventDefault();
        }
    }
    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    onInputChange() {
        // do nothing
    }
    onSearchTermChange(searchTerm) {
        if (!this.isFocused) {
            this.searchTerm = undefined;
        }
        else if (this.searchTerm !== searchTerm) {
            this.searchTerm = searchTerm;
            if (this.optionsProvider) {
                this.filters.set('searchterm', searchTerm);
                this.updateOptionsFromOptionsProvider();
            }
        }
    }
    onAction() {
        if (this.dialogActionBlocked !== undefined) {
            this.dialogOpen = this.dialogActionBlocked;
        }
    }
    onOptionClick(e) {
        var _a, _b, _c;
        e.preventDefault();
        const selectedOption = (_a = e.target) === null || _a === void 0 ? void 0 : _a.closest(`[data-value]`);
        if (selectedOption === undefined) {
            return;
        }
        if (!((_b = this.shadowRoot) === null || _b === void 0 ? void 0 : _b.contains(selectedOption)) && !this.contains(selectedOption)) {
            return;
        }
        this.value = ((_c = selectedOption) === null || _c === void 0 ? void 0 : _c.dataset.value) || undefined;
        this.searchTerm = undefined;
        this.closeDialog();
    }
    onOutsideClick(e) {
        var _a;
        if (!((_a = e.target) === null || _a === void 0 ? void 0 : _a.closest(`#${this.id}`))) {
            this.closeDialog();
        }
    }
    renderInput() {
        return html `
            ${super.renderInput()}
            <label
                for="field"
                class="field-input field-input-display ${!this.displayValue ? 'field-input-display-placeholder' : ''}"
                >${this.displayValue || this.placeholder || html `&nbsp;`}</label>
        `;
    }
    renderDialog() {
        return html `
            ${this.dialogErrorMessage ? html `
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            ` : ''}
            ${this.dialogMessagePrepend ? html `
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            ` : ''}
            <div class="list" @click="${this.onOptionClick}">
                ${this.options.map(opt => this.renderOption(opt))}
            </div>
            ${this.dialogMessageAppend ? html `
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            ` : ''}
        `;
    }
    renderOption(opt) {
        return html `
            <div class="kode4-select-option ${this.optionHighlighted !== -1 && opt === this.options[this.optionHighlighted] ? 'kode4-select-option-highlight' : ''}" data-value="${opt.value}">
                ${opt.unsafe ? html `${unsafeHTML(String(opt.label))}` : opt.label}
            </div>
        `;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            ${super.styles}

            :host(.kode4-select) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            :host(.kode4-autocomplete) .field-input.field-input-display {
                display: none !important;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .kode4-select-option {
                display: block;
                padding-top: var(--kode4-select-option-padding-top);
                padding-right: var(--kode4-select-option-padding-right);
                padding-bottom: var(--kode4-select-option-padding-bottom);
                padding-left: var(--kode4-select-option-padding-left);
                margin-top: var(--kode4-select-option-margin-top);
                margin-right: var(--kode4-select-option-margin-right);
                margin-bottom: var(--kode4-select-option-margin-bottom);
                margin-left: var(--kode4-select-option-margin-left);
                cursor: pointer;

                color: var(--kode4-select-option-color);
                font-family: var(--kode4-select-option-font-family);
                font-size: var(--kode4-select-option-font-size);
                line-height: var(--kode4-select-option-line-height);
                font-weight: var(--kode4-select-option-font-weight);
                opacity: var(--kode4-select-option-opacity);
                background-color: var(--kode4-select-option-background-color);
            }

            .kode4-select-option:first-child {
                border-top-left-radius: var(--kode4-field-dialog-border-radius);
                border-top-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option:last-child {
                border-bottom-left-radius: var(--kode4-field-dialog-border-radius);
                border-bottom-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option.kode4-select-option-highlight,
            .kode4-select-option:hover {
                color: var(--kode4-select-option-selected-color);
                font-weight: var(--kode4-select-option-selected-font-weight);
                opacity: var(--kode4-select-option-selected-opacity);
                background-color: var(--kode4-select-option-selected-background-color);
            }

            .kode4-field-dialog {
                max-height: var(--kode4-select-dialog-max-height);
                overflow: auto;
            }
        `;
    }
};
__decorate([
    property()
], Kode4Select.prototype, "valueObject", void 0);
__decorate([
    property()
], Kode4Select.prototype, "value", null);
__decorate([
    property()
], Kode4Select.prototype, "displayValue", null);
__decorate([
    property({ type: Array })
], Kode4Select.prototype, "_options", void 0);
__decorate([
    property({ type: Array })
], Kode4Select.prototype, "_optionsCandidate", void 0);
__decorate([
    property()
], Kode4Select.prototype, "options", null);
__decorate([
    property({ type: String })
], Kode4Select.prototype, "valueField", void 0);
__decorate([
    property({ type: String })
], Kode4Select.prototype, "labelField", void 0);
__decorate([
    property()
], Kode4Select.prototype, "_optionsProvider", void 0);
__decorate([
    property()
], Kode4Select.prototype, "optionsProvider", null);
__decorate([
    property()
], Kode4Select.prototype, "_selectedOption", void 0);
__decorate([
    property()
], Kode4Select.prototype, "selectedOption", null);
__decorate([
    property()
], Kode4Select.prototype, "optionHighlighted", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Select.prototype, "busy", null);
__decorate([
    property()
], Kode4Select.prototype, "searchTerm", void 0);
__decorate([
    property()
], Kode4Select.prototype, "filters", void 0);
__decorate([
    property()
], Kode4Select.prototype, "dialogMessagePrepend", void 0);
__decorate([
    property()
], Kode4Select.prototype, "dialogMessageAppend", void 0);
__decorate([
    property()
], Kode4Select.prototype, "dialogErrorMessage", void 0);
__decorate([
    property({ type: Boolean, attribute: 'force-selection' })
], Kode4Select.prototype, "forceSelection", void 0);
Kode4Select = __decorate([
    customElement("kode4-select")
], Kode4Select);
export default Kode4Select;
