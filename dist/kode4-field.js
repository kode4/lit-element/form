import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, state } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import { styleMap } from "lit/directives/style-map.js";
import { v4 as uuidv4 } from "uuid";
let Kode4Field = class Kode4Field extends LitElement {
    // --- INIT -----------------
    constructor() {
        super();
        this.block = false;
        this.submitEmpty = false;
        this.initialized = false;
        this.initializing = false;
        this.touched = false;
        this.type = 'text';
        this.name = '';
        this.required = false;
        this.floatingLabel = false;
        this.xxs = false;
        this.xs = false;
        this.s = false;
        this.m = false;
        this.l = false;
        this.xl = false;
        this.xxl = false;
        this.requiredSuffix = '';
        this._value = '';
        this.manageValue = true;
        this.slotsReady = false;
        this.initialValue = undefined;
        this.resetValue = undefined;
        this.initialDisabled = false;
        this._disabled = false;
        this.readonly = false;
        this.controlReadonly = false;
        this.label = '';
        this.hint = '';
        this.error = '';
        this.debounce = 350;
        this.canValidate = true;
        this.inputFocused = false;
        this.busyStyle = 'linear';
        this.inputFocusEventBlocked = false;
        this.preventBlur = false;
        this.forceBlur = false;
        this.inputBlurEventBlocked = false;
        this.dirty = false;
        this.lastFocusedState = false;
        this.isInitialized = new Promise(resolve => {
            this.resolveInitialized = resolve;
        });
        this.setAttribute('id', this.id);
    }
    get typeCssClass() {
        return `kode4-hidden`;
    }
    get inputCssSelector() {
        return 'input';
    }
    ;
    get value() {
        return this._value;
    }
    set value(v) {
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }
        this._value = this.sanititzeValue(v);
        if (this.initialized) {
            this.dirty = this.getDirty();
        }
    }
    getDirty() {
        if (this.disabled !== this.initialDisabled) {
            return true;
        }
        if (!this.getValueIsDefined(this.initialValue)) {
            return this.getValueIsDefined(this._value);
        }
        return this.initialValue != this._value;
    }
    get stringValue() {
        if (!this.getValueIsDefined(this.value)) {
            return '';
        }
        return String(this.value);
    }
    sanititzeValue(v) {
        return v;
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(disabled) {
        this._disabled = disabled;
        if (this.initialized) {
            this.dirty = this.getDirty();
            // this.onChange();
        }
    }
    get kode4Form() {
        if (!this._kode4Form) {
            this._kode4Form = this.closest('kode4-form');
        }
        return this._kode4Form;
    }
    ;
    applyConditions(form) {
        if (!this.conditions) {
            return;
        }
        if (Array.isArray(this.conditions)) {
            for (let condition of this.conditions) {
                this.applyCondition(condition, form);
            }
        }
        else {
            this.applyCondition(this.conditions, form);
        }
    }
    applyCondition(condition, form) {
        const success = Array.isArray(condition.conditions)
            ? condition.conditions.reduce((result, condition) => {
                result = result && this.evalCondition(condition, form);
                return result;
            }, true)
            : this.evalCondition(condition.conditions, form);
        if (success) {
            if (condition.handleThen) {
                condition.handleThen(form, this);
                this.requestUpdate();
            }
        }
        else {
            if (condition.handleElse) {
                condition.handleElse(form, this);
                this.requestUpdate();
            }
        }
    }
    evalCondition(condition, form) {
        if (typeof condition === 'function') {
            return Boolean(condition(form, this));
        }
        return false;
    }
    get displayValue() {
        if (!this.getValueIsDefined(this.value)) {
            return '';
        }
        return this.value;
    }
    get hasValue() {
        return this.submitEmpty || this.getValueIsDefined(this.value);
    }
    getValueIsDefined(value) {
        return !(typeof value === 'undefined' || !value || value === 'undefined' || value === 'null');
    }
    get isFocused() {
        return this.inputFocused || this.preventBlur;
    }
    get busy() {
        return false;
    }
    initDom() {
        this.classList.add("kode4-field");
        this.classList.add(this.typeCssClass);
    }
    connectedCallback() {
        if (!this.id) {
            this.id = `kode4field_${uuidv4()}`;
        }
        if (!this.name) {
            this.name = `kode4field_${uuidv4()}`;
        }
        if (this.block) {
            this.classList.add("kode4-block");
        }
        if (this.canValidate) {
            this.classList.add("kode4-canValidate");
        }
        if (this.xxs) {
            this.classList.add("kode4-field-xxs");
        }
        if (this.xs) {
            this.classList.add("kode4-field-xs");
        }
        if (this.s) {
            this.classList.add("kode4-field-s");
        }
        if (this.m) {
            this.classList.add("kode4-field-m");
        }
        if (this.l) {
            this.classList.add("kode4-field-l");
        }
        if (this.xl) {
            this.classList.add("kode4-field-xl");
        }
        if (this.xxl) {
            this.classList.add("kode4-field-xxl");
        }
        super.connectedCallback();
    }
    disconnectedCallback() {
        var _a;
        let event = new CustomEvent("kode4-field-destroyed", {
            bubbles: false,
            composed: true,
            detail: {
                fieldname: this.name,
                fieldid: this.id,
            }
        });
        (_a = this.kode4Form) === null || _a === void 0 ? void 0 : _a.dispatchEvent(event);
        super.disconnectedCallback();
    }
    firstUpdated(changedProperties) {
        var _a;
        super.firstUpdated(changedProperties);
        this.initDom();
        this.slotsReady = true;
        this.input = ((_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector(this.inputCssSelector)) || undefined;
        this.init();
    }
    updated(_changedProperties) {
        if (this.lastFocusedState !== this.isFocused) {
            this.isFocused ? this.handleOnFocus() : this.handleOnBlur();
        }
        this.lastFocusedState = this.isFocused;
    }
    handleOnFocus() {
    }
    handleOnBlur() {
    }
    async init(updateValueFromAttribute = false) {
        this.beforeInit();
        await this.doInit(updateValueFromAttribute);
        this.afterInit();
    }
    beforeInit() {
        this.initialized = false;
        this.initializing = true;
        this.dirty = false;
        this.initialValue = undefined;
        this.initialDisabled = false;
        this.error = '';
    }
    async doInit(updateValueFromAttribute = false) {
        if (updateValueFromAttribute) {
            this.updateValue();
        }
        else {
            this.value = this._valueCandidate;
        }
        this.initialValue = this.getValueIsDefined(this.value) ? this.value : undefined;
        this.initialDisabled = this.disabled;
    }
    afterInit() {
        this.initializing = false;
        this.initialized = true;
        if (this.resolveInitialized) {
            this.resolveInitialized();
        }
        this.fireRegisterEvent();
    }
    // --- LOGIC -----------------
    reset() {
        this.value = this.initialValue;
        this.disabled = this.initialDisabled;
        this.requestUpdate();
        this.onChange();
    }
    clear() {
        this.value = this.resetValue;
        this.requestUpdate();
        this.onChange();
    }
    updateValue() {
        this.value = this.getAttribute('value');
    }
    async respawn() {
        await this.init(true);
        this.onChange();
    }
    resetState() {
        this.touched = false;
        this.error = '';
    }
    // --- VALIDATE -----------------
    get valid() {
        if (!this.canValidate) {
            return true;
        }
        return this.validate();
    }
    /**
     * @return Boolean
     */
    validate() {
        this.error = '';
        if (this.required && !this.disabled && !this.hidden && !this.hasValue) {
            this.error = 'Dieses Feld wird benötigt';
            return false;
        }
        return true;
    }
    // --- ACTIONS -----------------
    focus() {
        this.inputFocusEventBlocked = true;
        this.focusInput();
        this.inputFocusEventBlocked = false;
    }
    blur() {
    }
    focusInput() {
        var _a;
        (_a = this.input) === null || _a === void 0 ? void 0 : _a.focus();
    }
    blurInput() {
        var _a;
        (_a = this.input) === null || _a === void 0 ? void 0 : _a.blur();
    }
    sanitizeInputFocused() {
        var _a;
        this.inputFocused = ((_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.activeElement) === this.input;
    }
    // --- EVENTS -----------------
    // protected hasFocus() {
    //     return this.field?.classList.contains("kode4-focus")
    // }
    fireRegisterEvent() {
        let event = new CustomEvent("kode4-field-created", {
            bubbles: true,
            composed: true,
            detail: {
                field: this,
            }
        });
        this.dispatchEvent(event);
    }
    fireUnregisterEvent() {
        let event = new CustomEvent("kode4-field-destroyed", {
            bubbles: true,
            composed: true,
            detail: {
                fieldname: this.name,
                fieldid: this.id,
            }
        });
        this.dispatchEvent(event);
    }
    onFocusInput() {
        this.inputFocused = !this.forceBlur;
    }
    onBlurInput() {
        if (this.preventBlur) {
            this.sanitizeInputFocused();
            this.preventBlur = false;
            this.focusInput();
        }
        else {
            this.inputFocused = false;
        }
    }
    onChange(originalEvent) {
        this.touched = true;
        let event = new CustomEvent("change", {
            bubbles: true,
            composed: true,
            detail: {
                disabled: this.disabled,
                readonly: this.readonly,
                manageValue: this.manageValue,
                value: this.value,
                name: this.name,
                originalEvent: originalEvent,
                dirty: this.dirty,
            }
        });
        if (originalEvent) {
            originalEvent.preventDefault();
        }
        this.dispatchEvent(event);
    }
    onSubmit(originalEvent, submitValue = true) {
        const detail = {
            originalEvent: originalEvent
        };
        if (submitValue) {
            detail.value = this.value,
                detail.name = this.name;
        }
        ;
        let event = new CustomEvent("submitForm", {
            bubbles: true,
            composed: true,
            detail
        });
        if (originalEvent) {
            originalEvent.preventDefault();
        }
        this.dispatchEvent(event);
    }
    onMousedownPreventBlur() {
        if (this.isFocused) {
            this.preventBlur = true;
        }
    }
    onMousedownPreventBlurOrForceBlur() {
        if (this.isFocused) {
            this.preventBlur = true;
        }
        else {
            this.forceBlur = true;
        }
    }
    onClear() {
        this.clear();
    }
    onReset() {
        this.reset();
    }
    // --- RENDER -----------------
    get cssClasses() {
        const cssClasses = {
            'kode4-focus': this.isFocused,
            'busy': this.busy,
            'hidden': this.hidden,
            'disabled': this.disabled,
            'readonly': this.readonly,
            'kode4-field-hasValue': this.hasValue,
            'kode4-field-empty': !this.hasValue,
        };
        return cssClasses;
    }
    render() {
        return html `
            <div class="component ${classMap(this.cssClasses)}">
                ${this.renderInput()}
            </div>
        `;
    }
    renderInput() {
        return html `
            <input
                id="field"
                type="hidden"
                class="field-control-input"
                ?name="${this.name}"
                .value="${this.displayValue}"
                />
        `;
    }
    get isLabelFloating() {
        return this.floatingLabel && this.label && !this.hasValue && !this.isFocused;
    }
    get legendWidth() {
        var _a;
        if (this.floatingLabel && this.isLabelFloating) {
            return '0px';
        }
        if (!this.labelWidth) {
            const el = ((_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('.field-label')) || null;
            if (!el) {
                return 'auto';
            }
            this.labelWidth = el.clientWidth;
        }
        return this.labelWidth ? `${this.labelWidth}px` : 'auto';
    }
    renderLabel(text) {
        return html `
            <legend class="field-legend" style="${styleMap({ 'width': this.legendWidth })}">${text}</legend>
            <label class="field-label" for="field">${text}${this.required && this.requiredSuffix ? html ` ${this.requiredSuffix}` : ''}</label>
        `;
    }
    renderMessages() {
        return html `
            <div class="kode4-field-messages">
                <div class="kode4-field-messages-container">
                    ${this.renderHint()}
                    ${this.renderError()}
                </div>

                ${this.renderMessagesAddon()}
            </div>
        `;
    }
    renderHint() {
        if (!this.hint) {
            return '';
        }
        return html `
            <div class="kode4-field-hint">${this.hint}</div>
        `;
    }
    renderError() {
        if (!this.error) {
            return '';
        }
        return html `
            <div class="kode4-field-error">${this.error}</div>
        `;
    }
    renderMessagesAddon() {
        return html ``;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
        
            * {
                box-sizing: border-box;
                text-overflow: ellipsis;
            }

            :host {
                display: inline-block;
                font-size: 1em;
                line-height: 1.0em;
                width: auto;
                max-width: 100%;
                box-sizing: border-box;

                margin-top: var(--kode4-field-padding-top, 0px);
                margin-right: var(--kode4-field-margin-right, 0px);
                margin-bottom: var(--kode4-field-padding-bottom, 0px);
                margin-left: var(--kode4-field-margin-left, 0px);
            }

            :host(.kode4-block) {
                display: block;
                width: 100%;
            }

            .component {
                transition: opacity 0.4s ease-in-out;
                display: flex;
                flex-direction: row;
            }

            .component.hidden {
                position: absolute;
                width: 0;
                overflow: hidden;
                visibility: hidden;
                opacity: 0;
            }

            .component.disabled,
            .component.disabled > *,
            .component.disabled .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .component.readonly,
            .component.readonly > *,
            .component.readonly .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .field-container {
                flex: 1 0 1px;
                /* flex: 0 1 100%; */
                position: relative;
            }

            .field-input-container-layout {
                display: flex;
                flex-direction: row;
                /* max-width: 100%; */
                overflow: hidden;
            }

            .field-input {
                /* display: block; */
                flex: 1 0 1px;
                width: 100%;
                min-width: 10px;
                /* flex: 0 1 100%; */
                background: transparent;
                border: none;
                outline: none;
                box-shadow: none; /* Firefox adds a red shadow to required fields */
            }

            ${this.getLabelStyles()}
            ${this.getOutlineStyles()}
            ${this.getFilledStyles()}


            .kode4-field-messages {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;

                color: var(--kode4-field-messages-color);
                font-family: var(--kode4-field-messages-font-family);
                font-size: var(--kode4-field-messages-font-size);
                line-height: var(--kode4-field-messages-line-height);
                font-weight: var(--kode4-field-messages-font-weight);

                margin-right: var(--kode4-field-messages-margin-right);
                margin-left: var(--kode4-field-messages-margin-left);
                padding-right: var(--kode4-field-messages-padding-right);
                padding-left: var(--kode4-field-messages-padding-left);
            }
            
            .kode4-field-messages-container {
                flex: 1 0 1px;
            }

            .kode4-field-messages-container > *:first-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-top: var(--kode4-field-messages-padding-top);
            }
            
            .kode4-field-messages-container > *:last-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-bottom: var(--kode4-field-messages-padding-bottom);
            }
                
            .kode4-field-messages-container > *:not(:first-child) {
                margin-top: var(--kode4-field-messages-spacing);
            }
                
            .outline .kode4-field-messages {
                padding-top: var(--kode4-field-outline-messages-padding-top);
                padding-right: var(--kode4-field-outline-messages-padding-right);
                padding-bottom: var(--kode4-field-outline-messages-padding-bottom);
                padding-left: var(--kode4-field-outline-messages-padding-left);
            }

            .kode4-field-hint {
                font-family: var(--kode4-field-hint-font-family, inherit);
                font-size: var(--kode4-field-hint-font-size, inherit);
                line-height: var(--kode4-field-hint-line-height, inherit);
                font-weight: var(--kode4-field-hint-font-weight, inherit);
                color: var(--kode4-field-hint-color, inherit);
            }

            .kode4-field-error {
                font-family: var(--kode4-field-error-font-family, inherit);
                font-size: var(--kode4-field-error-font-size, inherit);
                line-height: var(--kode4-field-error-line-height, inherit);
                font-weight: var(--kode4-field-error-font-weight, inherit);
                color: var(--kode4-field-error-color, inherit);
            }


            /*
            .field-hint-content {
                display: inline-block;
                font-family: var(--kode4-field-hint-font-family);
                font-size: var(--kode4-field-hint-font-size);
                line-height: var(--kode4-field-hint-line-height);
                font-weight: var(--kode4-field-hint-font-weight);
                color: var(--kode4-field-hint-color);
                padding-top: var(--kode4-field-hint-padding-top);
                padding-right: var(--kode4-field-hint-padding-right);
                padding-bottom: var(--kode4-field-hint-padding-bottom);
                padding-left: var(--kode4-field-hint-padding-left);
            }

            .field-error-content {
                display: inline-block;
                font-family: var(--kode4-field-error-font-family);
                font-size: var(--kode4-field-error-font-size);
                line-height: var(--kode4-field-error-line-height);
                font-weight: var(--kode4-field-error-font-weight);
                color: var(--kode4-field-error-color);
                padding-top: var(--kode4-field-error-padding-top);
                padding-right: var(--kode4-field-error-padding-right);
                padding-bottom: var(--kode4-field-error-padding-bottom);
                padding-left: var(--kode4-field-error-padding-left);
            }

            .outline .field-hint-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-hint-padding-top);
                padding-right: var(--kode4-field-outline-hint-padding-right);
                padding-bottom: var(--kode4-field-outline-hint-padding-bottom);
                padding-left: var(--kode4-field-outline-hint-padding-left);
            }

            .outline .field-error-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-error-padding-top);
                padding-right: var(--kode4-field-outline-error-padding-right);
                padding-bottom: var(--kode4-field-outline-error-padding-bottom);
                padding-left: var(--kode4-field-outline-error-padding-left);
            }
            */
        `;
    }
    static getLabelStyles() {
        return css `
            .field-label {
                display: block;
                text-overflow: ellipsis;
                max-width: 75%;
                overflow: hidden;
                white-space: nowrap;
            }

            .component {
            }

            .component .field-label {
                transition: all 0.3s ease-out;
                position: absolute;
                z-index: 10;
            }

            .field-input-container {
                margin-top: var(--kode4-field-input-container-margin-top);
                margin-right: var(--kode4-field-input-container-margin-right);
                margin-bottom: var(--kode4-field-input-container-margin-bottom);
                margin-left: var(--kode4-field-input-container-margin-left);
                padding-top: var(--kode4-field-input-container-padding-top);
                padding-right: var(--kode4-field-input-container-padding-right);
                padding-bottom: var(--kode4-field-input-container-padding-bottom);
                padding-left: var(--kode4-field-input-container-padding-left);
            }

            .field-label {
                transform: translate(var(--kode4-field-label-translate-x), var(--kode4-field-label-translate-y)) scale(var(--kode4-field-label-scale));
            }

            legend.field-legend {
                width: 0;
                opacity: 0;
                height: 0;
                margin: 0;
                padding: 0;
                visibility: hidden;
                transition: width 0.05s ease-out 0.15s;
            }

            .floatinglabel .field-label {
                transform: translate(var(--kode4-field-floatinglabel-label-translate-x), var(--kode4-field-floatinglabel-label-translate-y)) ;
            }
        `;
    }
    static getOutlineStyles() {
        return css `
            .component:not(.outline) .field-input-container {
                border: none;
            }

            .outline .field-input-container {
                border: var(--kode4-field-outline-border-style) var(--kode4-field-outline-border-width) var(--kode4-field-outline-border-color);
                margin-top: var(--kode4-field-outline-input-container-margin-top);
                margin-right: var(--kode4-field-outline-input-container-margin-right);
                margin-bottom: var(--kode4-field-outline-input-container-margin-bottom);
                margin-left: var(--kode4-field-outline-input-container-margin-left);
                padding-top: var(--kode4-field-outline-input-container-padding-top);
                padding-right: var(--kode4-field-outline-input-container-padding-right);
                padding-bottom: var(--kode4-field-outline-input-container-padding-bottom);
                padding-left: var(--kode4-field-outline-input-container-padding-left);
                border-radius: var(--kode4-field-outline-border-radius);
            }
            
            .outline .field-label {
                transform: translate(var(--kode4-field-outline-label-translate-x), var(--kode4-field-outline-label-translate-y)) scale(var(--kode4-field-outline-label-scale));
            }

            @-moz-document url-prefix() {
                .outline.floatinglabel .field-legend {
                    /* Fix. Firefox creates an overflow without this */
                    display: none;
                }
            }

            .outline.floatinglabel .field-label {
                transform: translate(var(--kode4-field-outline-floatinglabel-label-translate-x), var(--kode4-field-outline-floatinglabel-label-translate-y));
            }
        `;
    }
    static getFilledStyles() {
        return css `
            .component:not(.filled) .field-input-container {
                background-color: transparent;
            }

            .filled .field-input-container {
                background-color: var(--kode4-field-filled-background);
            }
        `;
    }
};
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "block", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "submitEmpty", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "initialized", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "initializing", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "touched", void 0);
__decorate([
    property()
], Kode4Field.prototype, "type", void 0);
__decorate([
    property()
], Kode4Field.prototype, "name", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "required", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "floatingLabel", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "xxs", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "xs", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "s", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "m", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "l", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "xl", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "xxl", void 0);
__decorate([
    property({ type: String, attribute: 'required-suffix' })
], Kode4Field.prototype, "requiredSuffix", void 0);
__decorate([
    property()
], Kode4Field.prototype, "_value", void 0);
__decorate([
    property()
], Kode4Field.prototype, "_valueCandidate", void 0);
__decorate([
    property()
], Kode4Field.prototype, "value", null);
__decorate([
    property()
], Kode4Field.prototype, "stringValue", null);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "manageValue", void 0);
__decorate([
    property()
], Kode4Field.prototype, "slotsReady", void 0);
__decorate([
    property()
], Kode4Field.prototype, "field", void 0);
__decorate([
    property()
], Kode4Field.prototype, "input", void 0);
__decorate([
    property()
], Kode4Field.prototype, "initialValue", void 0);
__decorate([
    property()
], Kode4Field.prototype, "resetValue", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "initialDisabled", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "_disabled", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "disabled", null);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "readonly", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "controlReadonly", void 0);
__decorate([
    property()
], Kode4Field.prototype, "label", void 0);
__decorate([
    property()
], Kode4Field.prototype, "hint", void 0);
__decorate([
    property()
], Kode4Field.prototype, "error", void 0);
__decorate([
    property({ type: Number, attribute: 'debounce', reflect: true })
], Kode4Field.prototype, "debounce", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "canValidate", void 0);
__decorate([
    property()
], Kode4Field.prototype, "conditions", void 0);
__decorate([
    state()
], Kode4Field.prototype, "_kode4Form", void 0);
__decorate([
    state()
], Kode4Field.prototype, "kode4Form", null);
__decorate([
    property()
], Kode4Field.prototype, "displayValue", null);
__decorate([
    property()
], Kode4Field.prototype, "hasValue", null);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "inputFocused", void 0);
__decorate([
    property()
], Kode4Field.prototype, "isFocused", null);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "busy", null);
__decorate([
    property({ type: String })
], Kode4Field.prototype, "busyStyle", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "dirty", void 0);
__decorate([
    property()
], Kode4Field.prototype, "cssClasses", null);
__decorate([
    property({ type: Number })
], Kode4Field.prototype, "labelWidth", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Field.prototype, "isLabelFloating", null);
__decorate([
    property({ type: String })
], Kode4Field.prototype, "legendWidth", null);
Kode4Field = __decorate([
    customElement('kode4-field')
], Kode4Field);
export default Kode4Field;
