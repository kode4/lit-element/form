/// <reference types="lodash" />
import { LitElement } from "lit";
import Kode4Field from "./kode4-field";
export default class Kode4Form extends LitElement {
    action: String;
    method: String;
    isValid: Boolean;
    private formElementsInitialized;
    private formElements;
    private fieldsDirty;
    dirty: boolean;
    debounce: number;
    connectedCallback(): void;
    disconnectedCallback(): void;
    protected checkDirty(e?: Event): void;
    protected onChange(e: Event): Promise<void>;
    protected applyConditions(): Promise<void>;
    getField(name: string): Kode4Field | null;
    validate(): boolean;
    get formData(): FormData;
    protected onResetForm(e: Event): void;
    protected onClearForm(e: Event): void;
    protected onUpdateValues(e: Event): void;
    protected onRespawnForm(e: Event): void;
    protected onSubmit(e: CustomEvent): void;
    reset(): void;
    clear(): void;
    updateValues(): void;
    respawn(): void;
    private fieldsToAdd;
    private fieldsToRemove;
    protected fieldCreated(e: CustomEvent): void;
    protected fieldDestroyed(e: CustomEvent): void;
    protected onFieldsUpdatedDebounced: import("lodash").DebouncedFunc<() => void>;
    protected onFieldsUpdated(): void;
    render(): import("lit-html").TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
