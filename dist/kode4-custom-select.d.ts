/// <reference types="lodash" />
import { ClassInfo } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
export default class Kode4CustomSelect extends Kode4Field {
    private formElements;
    column: boolean;
    get value(): String | Number | Boolean | undefined;
    set value(v: String | Number | Boolean | undefined);
    protected get typeCssClass(): string;
    private formElementsToAdd;
    private formElementsToRemove;
    debounce: number;
    connectedCallback(): void;
    protected formElementCreated(e: CustomEvent): void;
    protected formElementDestroyed(e: CustomEvent): void;
    protected onFieldsUpdatedDebounced: import("lodash").DebouncedFunc<() => void>;
    protected onFieldsUpdated(): void;
    protected initFormElements(): void;
    protected doInit(updateValueFromAttribute?: boolean): Promise<void>;
    protected onCustomOptionSelected(e: CustomEvent): void;
    protected updateCustomOptionsValue(): void;
    protected onChange(originalEvent?: Event): void;
    protected render(): import("lit-html").TemplateResult<1>;
    protected renderInput(): import("lit-html").TemplateResult<1>;
    protected get cssClassesOptionsContainer(): ClassInfo;
    static get styles(): import("lit").CSSResult;
}
