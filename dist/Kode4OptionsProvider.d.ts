export interface Kode4OptionsProviderSubscriber {
    handleListUpdated: Function;
}
export interface Kode4Option {
    value: String | Number;
    label: String | Element;
    unsafe?: Boolean;
}
export interface Kode4ListResponse {
    total?: Number;
    list?: Array<any>;
    infoMessage?: String;
    errorMessage?: String;
    error: Number;
}
export interface Kode4OptionsProvider {
    busy: boolean;
    noSelection?: String;
    filters?: FormData;
    subscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    unsubscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    getList(filters?: FormData): Promise<any>;
    checkItemChangedAfterListUpdate(primaryKey: String, item: Kode4Option): Promise<Kode4Option | boolean>;
    getItem(primaryKey: String): Promise<any>;
}
export declare class Kode4StaticOptionsProvider implements Kode4OptionsProvider {
    busy: boolean;
    noSelection?: String;
    valueField?: string;
    labelField?: string;
    list: Array<Kode4Option>;
    private subscribers;
    constructor(list?: Array<Kode4Option>);
    subscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    unsubscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    fireListChanged(): void;
    clearList(): void;
    setList(list: Array<Kode4Option>): void;
    applyListFilters(list: Array<Kode4Option>): Kode4Option[];
    getList(filters?: FormData): Promise<any>;
    checkItemChangedAfterListUpdate(primaryKey: String, item?: Kode4Option): Promise<any>;
    getItem(primaryKey: String): Promise<any>;
}
export declare class Kode4RestOptionsProvider implements Kode4OptionsProvider {
    httpConfigBase: string;
    baseUrl: String;
    listCacheEnabled: boolean;
    listCache: Array<Kode4Option>;
    listUrl: String;
    itemUrl: String;
    valueField?: string;
    labelField?: string;
    noSelection?: String;
    get busy(): boolean;
    protected busyList: boolean;
    protected busyItem: boolean;
    protected lastRequestHash?: string;
    private subscribers;
    constructor(httpConfigBase?: string);
    subscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    unsubscribe(subscriber: Kode4OptionsProviderSubscriber): void;
    fireListChanged(): void;
    applyRequestFilters(params: any): any;
    applyListFilters(list: Array<Kode4Option>): Kode4Option[];
    getList(filters?: FormData): Promise<any>;
    checkItemChangedAfterListUpdate(): Promise<boolean>;
    protected getItemFromListCache(primaryKey: String): Kode4Option | undefined;
    clearListCache(): void;
    getItem(primaryKey: String): Promise<any>;
}
