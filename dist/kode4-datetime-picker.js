var Kode4DatetimePicker_1;
import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import Kode4Textfield from "./kode4-textfield";
import { faChevronRight } from '@fortawesome/free-solid-svg-icons/faChevronRight';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons/faChevronLeft';
import '@kode4/ui/kode4-fa-icon';
let Kode4DatetimePicker = Kode4DatetimePicker_1 = class Kode4DatetimePicker extends Kode4Textfield {
    // --- INSTANTIATION -----------------
    constructor() {
        super();
        // all dates are handles as UTC timezone
        this._timezoneOffset = 0;
        this.nodialog = false;
        this.dialogonly = false;
        this.noyear = false;
        this.nomonth = false;
        this.noday = false;
        this.nohours = false;
        this.nominutes = false;
        this.noseconds = false;
        this.noweek = false;
        this.nozeropadding = false;
        this.timestamp = false;
        this.dateTimeValue = new Date();
        this.calendarDateTime = new Date();
        this.searchTerm = undefined;
        this.type = 'date';
        this.dateTimeValue = new Date(); // which is now
        // this.dateTimeValue = new Date(`${dateNow.getFullYear()}-${this.padZeros(dateNow.getMonth()+1)}-${this.padZeros(dateNow.getDate())}T${this.padZeros(dateNow.getHours())}:${this.padZeros(dateNow.getMinutes())}:${this.padZeros(dateNow.getSeconds())}${this.timezoneOffset}`);
        this.action = true;
        this.hasDialog = true;
        this.openDialogOnFocus = true;
        this.changeOnType = true;
        document.addEventListener("click", this.onOutsideClick.bind(this));
    }
    get typeCssClass() {
        return `kode4-${this.type}`;
    }
    // protected _timezoneOffset:number = Math.round(new Date().getTimezoneOffset() / 60);
    // protected timezoneOffset:any = Intl.DateTimeFormat().resolvedOptions().timeZone;
    get timezoneOffset() {
        return `${this._timezoneOffset <= 0 ? '+' : '-'}${this.padZeros(Math.abs(this._timezoneOffset))}:00`;
    }
    get value() {
        // console.log('POSSIBLE DETECTED TIMEZONES: ', Math.round(new Date().getTimezoneOffset() / 60), Intl.DateTimeFormat().resolvedOptions().timeZone);
        // protected _timezoneOffset:number = Math.round(new Date().getTimezoneOffset() / 60);
        // protected timezoneOffset:any = Intl.DateTimeFormat().resolvedOptions().timeZone;
        const myTimezoneName = "Europe/Berlin";
        // Generating the formatted text
        let dateText = Intl.DateTimeFormat([], { timeZone: myTimezoneName, timeZoneName: "short" }).format(new Date());
        // Scraping the numbers we want from the text
        let timezoneString = dateText.split(" ")[1].slice(3);
        // Getting the offset
        var timezoneOffset = parseInt(timezoneString.split(':')[0]) * 60;
        // Checking for a minutes offset and adding if appropriate
        if (timezoneString.includes(":")) {
            var timezoneOffset = timezoneOffset + parseInt(timezoneString.split(':')[1]);
        }
        if (!this.getValueIsDefined(this._value) || !this.getValueIsDefined(this.dateTimeValue)) {
            return undefined;
        }
        if (this.output === 'isodate') {
            if (this.type === 'date') {
                // this.dateTimeValue.setHours(0);
                // this.dateTimeValue.setMinutes(0);
                // this.dateTimeValue.setSeconds(0);
                // console.log('ISO TIME: ', this.dateTimeValue.toISOString(), this.timezoneOffset);
                // return this.dateTimeValue.toISOString();
                return `${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth() + 1)}-${this.padZeros(this.dateTimeValue.getDate())}T00:00:00${this.timezoneOffset}`;
            }
            else {
                // console.log('ISO TIME: ', this.dateTimeValue.toISOString(), this.timezoneOffset);
                // return this.dateTimeValue.toISOString();
                return `${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth() + 1)}-${this.padZeros(this.dateTimeValue.getDate())}T${this.padZeros(this.dateTimeValue.getHours())}:${this.padZeros(this.dateTimeValue.getMinutes())}:${this.padZeros(this.dateTimeValue.getSeconds())}${this.timezoneOffset}`;
            }
        }
        else if (this.output === 'timestamp') {
            return String(Math.round(this.dateTimeValue.getTime() / 1000));
        }
        throw new Error('Date is neither isodate nor timestamp');
    }
    set value(v) {
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }
        try {
            this.dateTimeValue = this.convertParameterToValue(String(v), this.timestamp);
            this._value = this.dateTimeValue.getTime();
        }
        catch (e) {
            this._value = undefined;
        }
        // try {
        //     switch (this.type) {
        //         case 'date':
        //             if (this.timestamp) {
        //                 const dt = new Date(Number(v) * 1000);
        //                 this.dateTimeValue = new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T00:00:00${this.timezoneOffset}`);
        //             } else {
        //                 const date:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}/);
        //                 if (!date) {
        //                     throw new Error('Type mismatch for date');
        //                 }
        //                 this.dateTimeValue = new Date(`${date[0]}T00:00:00${this.timezoneOffset}`);
        //             }
        //             this._value = this.dateTimeValue.getTime();
        //             break;
        //         case 'datetime':
        //             if (this.timestamp) {
        //                 const dt = new Date(Number(v) * 1000);
        //                 this.dateTimeValue = new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T${this.padZeros(dt.getHours())}:${this.padZeros(dt.getMinutes())}:00${this.timezoneOffset}`);
        //             } else {
        //                 const datetime:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);
        //                 if (!datetime) {
        //                     throw new Error('Type mismatch for date');
        //                 }
        //                 this.dateTimeValue = new Date(`${datetime[0]}:00${this.timezoneOffset}`);
        //             }
        //             this._value = this.dateTimeValue.getTime();
        //             break;
        //         default :
        //             throw new Error('Undefined type');
        //     }
        // } catch(e) {
        //     this._value = undefined;
        // }
        if (this.initialized) {
            this.dirty = this.getDirty();
        }
    }
    get mind() {
        return this._min;
    }
    set mind(v) {
        if (v instanceof Date) {
            this._min = v;
        }
        else if (typeof v === 'string') {
            try {
                this._min = this.convertParameterToValue(v, Boolean(v.match(/^\d+$/)));
            }
            catch (e) {
                this._min = undefined;
            }
        }
        else {
            this._min = undefined;
        }
    }
    convertParameterToDateValue(v, timestamp = false) {
        if (timestamp) {
            const dt = new Date(Number(v) * 1000);
            return new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth() + 1)}-${this.padZeros(dt.getDate())}T00:00:00${this.timezoneOffset}`);
        }
        else {
            const date = v.match(/^\d{4}-\d{2}-\d{2}/);
            if (!date) {
                throw new Error('Type mismatch for date');
            }
            return new Date(`${date[0]}T00:00:00${this.timezoneOffset}`);
        }
    }
    convertParameterToDateTimeValue(v, timestamp = false) {
        if (timestamp) {
            const dt = new Date(Number(v) * 1000);
            return new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth() + 1)}-${this.padZeros(dt.getDate())}T${this.padZeros(dt.getHours())}:${this.padZeros(dt.getMinutes())}:${this.padZeros(dt.getSeconds())}${this.timezoneOffset}`);
        }
        else {
            const datetime = v.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);
            if (!datetime) {
                throw new Error('Type mismatch for date');
            }
            return new Date(`${datetime[0]}:00${this.timezoneOffset}`);
        }
    }
    convertParameterToValue(v, timestamp = false) {
        switch (this.type) {
            case 'date':
                return this.convertParameterToDateValue(v, timestamp);
                break;
            case 'datetime':
                return this.convertParameterToDateTimeValue(v, timestamp);
                break;
            default:
                throw new Error('Undefined type');
        }
    }
    /**
     * @return Boolean
     */
    validate() {
        if (!super.validate()) {
            return false;
        }
        this.error = '';
        if (this.mind instanceof Date && this.dateTimeValue instanceof Date && this.mind.getTime() > this.dateTimeValue.getTime()) {
            this.error = 'Bitte beachten Sie Mindestdatum';
            return false;
        }
        return true;
    }
    get displayValue() {
        if (!this.getValueIsDefined(this._value) || !this.getValueIsDefined(this.dateTimeValue)) {
            return '';
        }
        let output = String(this.format);
        output = output.replace(/D/, this.day);
        output = output.replace(/M/, this.month);
        output = output.replace(/Y/, this.year);
        output = output.replace(/h/, this.hours);
        output = output.replace(/m/, this.minutes);
        output = output.replace(/s/, this.seconds);
        return output;
    }
    async firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        if (this.nodialog && this.dialogonly) {
            this.nodialog = false;
        }
        // if (this.openDialogOnFocus === undefined) {
        //     this.openDialogOnFocus = true;
        // }
    }
    // protected async updated(changedProperties: Map<PropertyKey, unknown>) {
    //     super.updated(changedProperties);
    //     if (changedProperties.has('_options') && this.type !== 'autocomplete' && !this.optionsProvider) {
    //         this.value = this.value;
    //     }
    // }
    async doInit( /*updateValueFromAttribute:boolean = false*/) {
        // if (!this.optionsProvider) {
        //     const op = new Kode4StaticOptionsProvider();
        //     op.list = this._optionsCandidate || [];
        //     this.optionsProvider = op;
        // }
        if (!this.output) {
            this.output = this.timestamp ? 'timestamp' : 'isodate';
        }
        if (!this.format) {
            switch (this.type) {
                case 'date':
                    this.format = 'D.M.Y';
                    break;
                case 'datetime':
                    this.format = 'D.M.Y h:m:s';
                    break;
            }
        }
        this.value = this._valueCandidate;
        if (this.getValueIsDefined(this._valueCandidate)) {
            this.initialValue = this._value;
            this.calendarDateTime = new Date(this.dateTimeValue.getTime());
            this.calendarDateTime.setDate(1);
        }
        else {
            this.initialValue = undefined;
        }
        // this.updateOptionsFromOptionsProvider();
        // if (updateValueFromAttribute) {
        //     this.updateValue();
        // } else {
        //     this.value = this._valueCandidate;
        // }
    }
    // --- LOGIC -----------------
    reset() {
        if (this.getValueIsDefined(this.initialValue)) {
            this.dateTimeValue = new Date(Number(this.initialValue));
            this._value = this.dateTimeValue.getTime();
        }
        else {
            this.dateTimeValue = new Date();
            this._value = undefined;
        }
        this.dirty = this.getDirty();
        this.requestUpdate();
        this.onChange();
    }
    // --- UI MANIPULATON -----------------
    addFocusClass() {
    }
    removeFocusClass() {
    }
    // --- EVENTS -----------------
    onKeydown(e) {
        switch (e.key) {
            case 'Tab':
                if (this.dialogOpen) {
                    // this.selectHighlightedOption();
                    this.closeDialog();
                }
                return;
            default:
                if (this.type !== 'autocomplete') {
                    e.preventDefault();
                }
        }
    }
    onKeyup(e) {
        switch (e.key) {
            case 'Escape':
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                else {
                    this.handleEsc();
                }
                return;
            case 'ArrowDown':
                if (!this.dialogOpen) {
                    this.openDialog();
                }
                else {
                    // this.highlightNextOption();
                }
                return;
            // case 'ArrowUp' :
            //     if (!this.dialogOpen) {
            //         this.openDialog();
            //     } else {
            //         this.highlightPrevOption();
            //     }
            //     return;
            case 'ArrowRight':
                if (this.dialogOpen) {
                    // this can be nice if typing is not allowed.
                    // but I think moving the cursor left an right
                    // while typing is more convenient than
                    // making a selection with ArrowRight.
                    // this.selectHighlightedOption();
                }
                else {
                    this.openDialog();
                }
                return;
            case 'ArrowLeft':
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                return;
            case 'Enter':
                if (this.dialogOpen) {
                    // this.selectHighlightedOption();
                    this.closeDialog();
                }
                else {
                    this.onSubmit(e);
                }
                return;
            case 'Tab':
                // action handled in onKeydown
                return;
            default:
                if (this.type === 'autocomplete') {
                    if (!this.dialogOpen) {
                        this.openDialog();
                    }
                    // this.onSearchTermChangeDebounced(<String>this.input?.value);
                }
                e.preventDefault();
        }
    }
    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    onInputChange() {
        // do nothing
    }
    // protected onSearchTermChangeDebounced = debounce(this.onSearchTermChange, this.debounce);
    // protected onSearchTermChange(searchTerm:String) {
    //     if (this.searchTerm !== searchTerm) {
    //         this.searchTerm = searchTerm;
    //         // if (this.optionsProvider) {
    //         //     this.filters.set('searchterm', <string>searchTerm);
    //         //     this.updateOptionsFromOptionsProvider();
    //         // }
    //     }
    // }
    onAction() {
        if (this.readonly || this.disabled) {
            return;
        }
        if (this.dialogActionBlocked !== undefined) {
            this.dialogOpen = this.dialogActionBlocked;
        }
    }
    onOptionClick(e) {
        e.preventDefault();
        //     const selectedOption = (<Element>e.target)?.closest(`[data-value]`);
        //     if (selectedOption === undefined) {
        //         return;
    }
    //     if (!this.shadowRoot?.contains(selectedOption) && !this.contains(selectedOption)) {
    //         return;
    //     }
    //     this.value = (<String|Number>(<HTMLElement>selectedOption)?.dataset.value) || undefined;
    //     this.closeDialog();
    // }
    onOutsideClick(e) {
        var _a;
        if (!((_a = e.target) === null || _a === void 0 ? void 0 : _a.closest(`#${this.id}`))) {
            this.closeDialog();
        }
    }
    get year() {
        var _a;
        return String((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getFullYear());
    }
    padZeros(val) {
        if (this.nozeropadding) {
            return String(val);
        }
        else if (val < 10) {
            return `0${val}`;
        }
        return String(val);
    }
    get month() {
        var _a;
        return this.padZeros(((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getMonth()) + 1);
    }
    get monthLabel() {
        var _a;
        return Kode4DatetimePicker_1.labels.months[(_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getMonth()];
    }
    get day() {
        var _a;
        return this.padZeros((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getDate());
    }
    get weekDay() {
        var _a, _b;
        if (((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getDay()) === 0) {
            return '7';
        }
        return String((_b = this.dateTimeValue) === null || _b === void 0 ? void 0 : _b.getDay());
    }
    get weekDayLabel() {
        var _a;
        return Kode4DatetimePicker_1.labels.days[(_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getDay()];
    }
    get hours() {
        var _a;
        return this.padZeros((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getHours());
    }
    get minutes() {
        var _a;
        return this.padZeros((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getMinutes());
    }
    get seconds() {
        var _a;
        return this.padZeros((_a = this.dateTimeValue) === null || _a === void 0 ? void 0 : _a.getSeconds());
    }
    renderInput() {
        return html `
            <input
                class="field-input"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="text"
                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${(e) => e.stopPropagation()}"
                />
            <label
                for="field"
                class="field-input field-input-display ${!this.displayValue ? 'field-input-display-placeholder' : ''}"
                >${this.displayValue || this.placeholder || html `&nbsp;`}</label>
        `;
    }
    renderDialog() {
        if (!this.initialized) {
            return html ``;
        }
        return html `
            ${this.dialogErrorMessage ? html `
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            ` : ''}
            ${this.dialogMessagePrepend ? html `
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            ` : ''}
            <div class="calendar">
                ${this.renderMonthPagination()}
                ${this.renderCalendarMonthContainer()}
            </div>
            <div class="time">
                ${this.type === 'datetime' ? this.renderTimeEditor() : ''}
            </div>
            ${this.dialogMessageAppend ? html `
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            ` : ''}
        `;
    }
    renderCalendarYearContainer() {
        return html `
            <div class="calendar-year-container">
                <input
                    id="calendarYear"
                    type="number"
                    step="1"
                    value="${this.calendarDateTime.getFullYear()}"
                    class="calendar-year-input"
                    @input="${(e) => { var _a; return this.selectYear(Number((_a = e.target) === null || _a === void 0 ? void 0 : _a.value)); }}"
                    tabindex="-1" 
                    />
            </div>
        `;
    }
    renderCalendarMonthContainer() {
        return html `
            <div class="calendar-month-container">
                ${this.renderCalendarWeekContainer()}
            </div>
        `;
    }
    renderMonthPagination() {
        return html `
            <div class="calendar-month-pagination">
                ${this.renderCalendarMonthPaginationPrev()}
                <div class="spacer"></div>
                <div class="calendar-month-container-monthlabel">
                    ${Kode4DatetimePicker_1.labels.months[this.calendarDateTime.getMonth()]}
                </div>
                ${this.renderCalendarYearContainer()}
                <div class="spacer"></div>
                ${this.renderCalendarMonthPaginationNext()}
            </div>
        `;
    }
    renderCalendarMonthPaginationPrev() {
        return html `
            <div class="calendar-month-container-monthPaginationButton" @click="${() => this.selectMonth(this.calendarDateTime.getMonth() - 1)}">
                <kode4-fa-icon
                    .icon="${faChevronLeft}"
                ></kode4-fa-icon>
            </div>
        `;
    }
    renderCalendarMonthPaginationNext() {
        return html `
            <div class="calendar-month-container-monthPaginationButton" @click="${() => this.selectMonth(this.calendarDateTime.getMonth() + 1)}">
                <kode4-fa-icon
                    .icon="${faChevronRight}"
                ></kode4-fa-icon>
            </div>
        `;
    }
    renderCalendarWeekContainer() {
        return html `
            <div class="calendar-week-container">
                ${this.renderCalendarWeekContent()}
            </div>
        `;
    }
    renderCalendarWeekContent() {
        const weeks = [];
        let currentWeek = [];
        const monthDate = new Date(this.calendarDateTime.getTime());
        monthDate.setDate(1);
        const currentMonth = monthDate.getMonth();
        const blankDays = monthDate.getDay() === 0 ? 6 : monthDate.getDay() - 1;
        for (let i = 0; i < blankDays; i += 1) {
            currentWeek.push(0);
        }
        let countX = 0;
        do {
            countX += 1;
            let count = 0;
            do {
                count += 1;
                currentWeek.push(monthDate.getDate());
                monthDate.setDate(monthDate.getDate() + 1);
            } while (count < 50 && monthDate.getDay() !== 1 && monthDate.getMonth() === currentMonth);
            for (let i = currentWeek.length; i < 7; i += 1) {
                currentWeek.push(0);
            }
            weeks.push(currentWeek);
            currentWeek = [];
        } while (countX < 50 && monthDate.getMonth() === currentMonth);
        return html `
            <table class="calendarTable">
                <thead>
                    <tr>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[1]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[2]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[3]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[4]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[5]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[6]}</th>
                        <th>${Kode4DatetimePicker_1.labels.daysShort[0]}</th>
                    </tr>
                </thead>
                <tbody>
                    ${weeks.map((week) => this.renderCalendarWeekRow(week))}
                </tbody>
            </table>
        `;
    }
    renderCalendarWeekRow(week) {
        return html `
            <tr>
                ${week.map((day) => this.renderCalendarDayCell(day))}
            </tr>
        `;
    }
    renderCalendarDayCell(day) {
        if (day === 0) {
            return html `
                <td>
                </td>
            `;
        }
        return html `
            <td @click="${() => this.selectDay(day)}">
                ${day}
            </td>
        `;
    }
    selectYear(year) {
        var _a;
        const newDate = new Date(this.calendarDateTime.getTime());
        this.calendarDateTime.setFullYear(year);
        newDate.setFullYear(year);
        if (this.yearSelector && ((_a = this.yearSelector) === null || _a === void 0 ? void 0 : _a.value) !== String(this.calendarDateTime.getFullYear())) {
            this.yearSelector.value = String(this.calendarDateTime.getFullYear());
        }
        this.requestUpdate();
    }
    selectMonth(month) {
        var _a;
        const newDate = new Date(this.calendarDateTime.getTime());
        this.calendarDateTime.setMonth(month);
        newDate.setMonth(month);
        if (this.yearSelector && ((_a = this.yearSelector) === null || _a === void 0 ? void 0 : _a.value) !== String(this.calendarDateTime.getFullYear())) {
            this.yearSelector.value = String(this.calendarDateTime.getFullYear());
        }
        this.requestUpdate();
        // this.dateTimeValue = newDate;
    }
    selectDay(day) {
        const newDate = new Date(this.calendarDateTime.getTime());
        newDate.setDate(day);
        // if (!this.getValueIsDefined(this.dateTimeValue)) {
        //     this.dateTimeValue = new Date(this.calendarDateTime.getTime());
        //     console.log('SELECT DAY - undefined case dateTimeValue', this.name, this.dateTimeValue);
        // }
        this.dateTimeValue = newDate;
        this._value = this.dateTimeValue.getTime();
        if (this.initialized) {
            this.dirty = this.getDirty();
        }
        this.onChange();
        this.closeDialog();
    }
    renderTimeEditor() {
        return html `
            <div class="time-editor">
                Uhrzeit:
                <div class="time-editor-body">
                    <input
                        id="timeHours"
                        type="number"
                        min="0"
                        max="24"
                        step="1"
                        value="${this.calendarDateTime.getHours()}"
                        class="time-hours-input"
                        @input="${(e) => { var _a; return this.selectHours(Number((_a = e.target) === null || _a === void 0 ? void 0 : _a.value)); }}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeMinutes"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getMinutes()}"
                        class="time-minutes-input"
                        @input="${(e) => { var _a; return this.selectMinutes(Number((_a = e.target) === null || _a === void 0 ? void 0 : _a.value)); }}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeSeconds"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getSeconds()}"
                        class="time-seconds-input"
                        @input="${(e) => { var _a; return this.selectSeconds(Number((_a = e.target) === null || _a === void 0 ? void 0 : _a.value)); }}"
                        tabindex="-1" 
                        />
                    </div>
            </div>
        `;
    }
    selectHours(hours) {
        this.calendarDateTime.setHours(hours);
        this.dateTimeValue.setHours(hours);
        this.requestUpdate();
        this.onChange();
    }
    selectMinutes(minutes) {
        this.calendarDateTime.setMinutes(minutes);
        this.dateTimeValue.setMinutes(minutes);
        this.requestUpdate();
        this.onChange();
    }
    selectSeconds(minutes) {
        this.calendarDateTime.setSeconds(minutes);
        this.dateTimeValue.setSeconds(minutes);
        this.requestUpdate();
        this.onChange();
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            ${super.styles}

            :host(.kode4-date) #field.field-input,
            :host(.kode4-datetime) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            .calendar {
                padding-top: var(--kode4-datetime-dialog-calendar-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            .calendar-month-pagination {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: var(--kode4-datetime-dialog-calendar-month-pagination-margin-top);
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-margin-right);
                margin-bottom: var(--kode4-datetime-dialog-calendar-month-pagination-margin-bottom);
                margin-left: var(--kode4-datetime-dialog-calendar-month-pagination-margin-left);
            }

            .calendar-month-pagination .spacer {
                flex: 1 0 1px;
            }

            .calendar-month-container-monthPaginationButton {
                width: var(--kode4-datetime-dialog-calendar-month-pagination-button-width);
                text-align: center;
                cursor: pointer;
            }

            .calendar-month-container-monthlabel {
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-label-spacer);
                vertical-align: middle;
            }

            .calendarTable {
                width: 100%;
                table-layout: fixed;
                border: none;
                border-collapse: collapse;
                border-spacing: 0;
            }

            .calendarTable th,
            .calendarTable td {
                text-align: center;
                padding-top: var(--kode4-datetime-dialog-calendar-tablecell-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-tablecell-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-tablecell-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-tablecell-padding-left);
                cursor: pointer;
            }

            .calendarTable th {
                font-weight: bold;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .calendar-year-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-calendar-year-input-width);
                color: var(--kode4-datetime-dialog-calendar-year-input-color);
                font-family: var(--kode4-datetime-dialog-calendar-year-input-font-family);
                font-size: var(--kode4-datetime-dialog-calendar-year-input-font-size);
                line-height: var(--kode4-datetime-dialog-calendar-year-input-line-height);
                font-weight: var(--kode4-datetime-dialog-calendar-year-input-font-weight);
                -moz-appearance: textfield;
            }

            .calendar-year-input::-webkit-outer-spin-button,
            .calendar-year-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }


            .time-editor {
                padding-top: var(--kode4-datetime-dialog-time-padding-top);
                padding-right: var(--kode4-datetime-dialog-time-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-time-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-time-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            
            .time-hours-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-hours-input-width);
                color: var(--kode4-datetime-dialog-time-hours-input-color);
                font-family: var(--kode4-datetime-dialog-time-hours-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-hours-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-hours-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-hours-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-hours-input-text-align);
                -moz-appearance: textfield;
            }

            .time-hours-input::-webkit-outer-spin-button,
            .time-hours-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            .time-minutes-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-minutes-input::-webkit-outer-spin-button,
            .time-minutes-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .time-seconds-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-seconds-input::-webkit-outer-spin-button,
            .time-seconds-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

        `;
    }
};
Kode4DatetimePicker.labels = {
    months: [
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember',
    ],
    days: [
        'Sonntag',
        'Montag',
        'Dienstag',
        'Mittwoch',
        'Donnerstag',
        'Freitag',
        'Samstag',
    ],
    daysShort: [
        'So',
        'Mo',
        'Di',
        'Mi',
        'Do',
        'Fr',
        'Sa',
    ]
};
__decorate([
    property()
], Kode4DatetimePicker.prototype, "_timezoneOffset", void 0);
__decorate([
    property({ type: String })
], Kode4DatetimePicker.prototype, "timezoneOffset", null);
__decorate([
    query('#calendarYear')
], Kode4DatetimePicker.prototype, "yearSelector", void 0);
__decorate([
    property({ type: String })
], Kode4DatetimePicker.prototype, "format", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "nodialog", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "dialogonly", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "noyear", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "nomonth", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "noday", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "nohours", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "nominutes", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "noseconds", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "noweek", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "nozeropadding", void 0);
__decorate([
    property({ type: Boolean })
], Kode4DatetimePicker.prototype, "timestamp", void 0);
__decorate([
    property({ type: String })
], Kode4DatetimePicker.prototype, "output", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "dateTimeValue", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "calendarDateTime", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "value", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "_min", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "mind", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "displayValue", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "searchTerm", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "dialogMessagePrepend", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "dialogMessageAppend", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "dialogErrorMessage", void 0);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "year", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "month", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "monthLabel", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "day", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "weekDay", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "weekDayLabel", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "hours", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "minutes", null);
__decorate([
    property()
], Kode4DatetimePicker.prototype, "seconds", null);
Kode4DatetimePicker = Kode4DatetimePicker_1 = __decorate([
    customElement("kode4-datetime-picker")
], Kode4DatetimePicker);
export default Kode4DatetimePicker;
