export class AbstractKode4FileUploader {
    constructor() {
        this.subscribers = [];
    }
    subscribe(subscriber) {
        if (!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }
    unsubscribe(subscriber) {
        if (this.subscribers.includes(subscriber)) {
            const i = this.subscribers.indexOf(subscriber);
            this.subscribers.splice(i, 1);
        }
    }
}
export class Kode4FileUploaderSimulatorError extends AbstractKode4FileUploader {
    constructor() {
        super(...arguments);
        this.uploading = false;
        this.progress = 0;
    }
    upload( /*fileList?:FileList*/) {
        return new Promise(() => {
            throw new Error('An error occurred while uploading.');
        });
    }
    cancel() {
    }
}
export class Kode4FileUploaderSimulator extends AbstractKode4FileUploader {
    constructor() {
        super(...arguments);
        this.fileCount = 0;
        this.uploading = false;
        this.progress = 0;
    }
    upload(fileList) {
        return new Promise((resolve) => {
            if (this.uploading) {
                throw new Error('An upload is already in progress. Please wait fopr this Upload to finish.');
            }
            if (!fileList) {
                throw new Error('Filelist is empty, nothing.');
            }
            this.progress = 0;
            this.uploading = true;
            this.subscribers.map((el) => {
                el.handleOnBeginUpload();
            });
            const i = setInterval(() => {
                this.progress = Math.min(this.progress + Math.floor(Math.random() * 10), 250);
                if (this.progress < 100) {
                    this.subscribers.map((el) => {
                        el.handleOnUploadProgress(this.progress);
                    });
                }
                else {
                    clearInterval(i);
                    this.progress = 0;
                    this.uploading = false;
                    this.subscribers.map((el) => {
                        el.handleOnUploadComplete();
                    });
                    const result = [];
                    Array.from(fileList).forEach((file) => {
                        result.push({
                            file,
                            filename: `uploaded_filename_${this.fileCount}`
                        });
                        this.fileCount += 1;
                    });
                    resolve(result);
                }
            }, 200);
        });
    }
    cancel() {
    }
}
// export class Kode4PostFileUploader implements Kode4FileUploader {
//     public baseUrl:String = '';
//     public listCacheEnabled:boolean = true;
//     public listCache:Array<Kode4Option> = [];
//     public listUrl:String = '';
//     public itemUrl:String = '';
//     public valueField?:string;
//     public labelField?:string;
//     public noSelection?:String;
//     public get uploading():boolean {
//         return this.busyList || this.busyItem;
//     }
//     protected busyList:boolean = false;
//     protected busyItem:boolean = false;
//     private subscribers:Array<Kode4OptionsProviderSubscriber> = [];
//     public subscribe(subscriber:Kode4OptionsProviderSubscriber) {
//         if (!this.subscribers.includes(subscriber)) {
//             this.subscribers.push(subscriber);
//         }
//     }
//     public unsubscribe(subscriber:Kode4OptionsProviderSubscriber) {
//         if (this.subscribers.includes(subscriber)) {
//             const i = this.subscribers.indexOf(subscriber);
//             this.subscribers.splice(i, 1);
//         }
//     }
//     public fireListChanged() {
//         this.subscribers.map((el:Kode4OptionsProviderSubscriber) => {
//             el.handleListUpdated();
//         });
//     }
//     public applyRequestFilters(params:any) {
//         return params;
//     }
//     public applyListFilters(list:Array<Kode4Option>) {
//         return list;
//     }
//     public async getList(filters?:FormData):Promise<any> {
//         try {
//             this.busyList = true;
//             // if (filters) {
//             //     const queryString = (new URLSearchParams(filters) ).toString()
//             // }
//             const params = {
//                 searchterm: filters?.get('searchterm')
//             }
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}`, {
//                 params: this.applyRequestFilters(params)
//             });
//             if (!data.data || typeof data.data !== 'object') {
//                 throw Error('Invalid result: '+typeof data);
//             }
//             const lr:Kode4ListResponse = <Kode4ListResponse>data.data;
//             if (!lr.list) {
//                 lr.list = [];
//             }
//             if (!Array.isArray(lr.list)) {
//                 throw Error('Invalid list result: '+typeof lr.list);
//             }
//             if (this.valueField || this.labelField) {
//                 lr.list = lr.list.map((el:any) => {
//                     if (this.valueField) {
//                         el['value'] = el[this.valueField];
//                     }
//                     if (this.labelField) {
//                         el['label'] = el[this.labelField];
//                     }
//                     return el;
//                 });
//             }
//             lr.list = this.applyListFilters(lr.list);
//             if (this.listCacheEnabled) {
//                 this.listCache = this.listCache.concat(lr.list);
//             }
//             if (this.noSelection) {
//                 (<Array<any>>lr.list).unshift({
//                     label: this.noSelection,
//                     unsafe: false
//                 });
//             }
//             this.busyList = false;
//             return lr;
//         } catch (e) {
//             this.busyList = false;
//             throw e;
//             return [];
//         }
//     }
//     public async checkItemChangedAfterListUpdate(primaryKey:String, item:Kode4Option) {
//         // TODO:
//         //      - check if item is in list
//         //          - YES: return newItem != listItem
//         //          - NO: return false
//         return false;
//     }
//     protected getItemFromListCache(primaryKey:String) {
//         return this.listCache.find((el:Kode4Option) => String(el.value) === primaryKey);
//     }
//     public clearListCache() {
//         this.listCache = [];
//     }
//     public async getItem(primaryKey:String):Promise<any> {
//         try {
//             if (!primaryKey) {
//                 throw new Error('primary Key is required');
//             }
//             if (this.listCacheEnabled) {
//                 const cachedItem = this.getItemFromListCache(primaryKey);
//                 if (cachedItem !== undefined) {
//                     return cachedItem;
//                 }
//             }
//             this.busyItem = true;
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}/${primaryKey}`);
//             const item:Kode4Option = data.data;
//             if (this.valueField) {
//                 (<any>item)['value'] = (<any>item)[this.valueField];
//             }
//             if (this.labelField) {
//                 (<any>item)['label'] = (<any>item)[this.labelField];
//             }
//             this.busyItem = false;
//             this.listCache.push(item);
//             return item;
//         } catch (e) {
//             this.busyItem = false;
//             throw e;
//         }
//     }
// }
// export class Kode4ChunkFileUploader implements Kode4FileUploader {
//     public uploading = false;
//     public noSelection?:String;
//     public valueField?:string;
//     public labelField?:string;
//     public list:Array<Kode4Option> = [];
//     private subscribers:Array<Kode4OptionsProviderSubscriber> = [];
//     public subscribe(subscriber:Kode4OptionsProviderSubscriber) {
//         if (!this.subscribers.includes(subscriber)) {
//             this.subscribers.push(subscriber);
//         }
//     }
//     public unsubscribe(subscriber:Kode4OptionsProviderSubscriber) {
//         if (this.subscribers.includes(subscriber)) {
//             const i = this.subscribers.indexOf(subscriber);
//             this.subscribers.splice(i, 1);
//         }
//     }
//     public fireListChanged() {
//         this.subscribers.map((el:Kode4OptionsProviderSubscriber) => {
//             el.handleListUpdated();
//         });
//     }
//     public clearList() {
//         this.list = [];
//         this.fireListChanged();
// }
//     public setList(list:Array<Kode4Option>) {
//         if (list.length && <any>list !== <any>this.list) {
//             this.list = list;
//             this.fireListChanged();
//         }
//     }
//     public applyListFilters(list:Array<Kode4Option>) {
//         return list;
//     }
//     public async getList(filters?:FormData):Promise<any> {
//         let list;
//         if (this.valueField || this.labelField) {
//             list = this.list.map((el:any) => {
//                 if (this.valueField) {
//                     el['value'] = el[this.valueField];
//                 }
//                 if (this.labelField) {
//                     el['label'] = el[this.labelField];
//                 }
//                 return el;
//             });
//         } else {
//             list = this.list;
//         }
//         list = filters && filters.has('searchterm') ? list.filter((el:Kode4Option) => el.label === filters.get('searchterm')) : list;
//         list = this.applyListFilters(list);
//         if (this.noSelection) {
//             (<Array<any>>list).unshift({
//                 label: this.noSelection,
//                 unsafe: false
//             });
//         }
//         return {
//             error: 0,
//             list
//         };
//     }
//     public async checkItemChangedAfterListUpdate(primaryKey:String, item?:Kode4Option) {
//         try {
//             const newItem = await this.getItem(primaryKey);
//             if ((newItem && !item) || (newItem?.label != item?.label)) {
//                 return newItem;
//             }
//             return false;
//         } catch (e) {
//             // item is not in list. return false? or unset value?
//             return false;
//         }
//     }
//     public async getItem(primaryKey:String):Promise<any> {
//         const item = this.list.reduce((result:Kode4Option|undefined, el:Kode4Option) => {
//             if (el.value == primaryKey) {
//                 result = el;
//             }
//             return result;
//         }, undefined);
//         if (item === undefined) {
//             throw new Error('Item not found for '+primaryKey);
//         }
//         if (this.valueField) {
//             (<any>item)['value'] = (<any>item)[this.valueField];
//         }
//         if (this.labelField) {
//             (<any>item)['label'] = (<any>item)[this.labelField];
//         }
//         return item;
//     }
// }
