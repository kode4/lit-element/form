import { TemplateResult } from "lit";
import Kode4Textfield from "./kode4-textfield";
import '@kode4/ui/kode4-fa-icon';
export default class Kode4DatetimePicker extends Kode4Textfield {
    protected get typeCssClass(): string;
    protected _timezoneOffset: number;
    protected get timezoneOffset(): string;
    protected yearSelector?: HTMLInputElement;
    protected format?: string;
    protected nodialog: Boolean;
    protected dialogonly: Boolean;
    protected noyear: Boolean;
    protected nomonth: Boolean;
    protected noday: Boolean;
    protected nohours: Boolean;
    protected nominutes: Boolean;
    protected noseconds: Boolean;
    protected noweek: Boolean;
    protected nozeropadding: Boolean;
    protected timestamp: boolean;
    protected output?: 'isodate' | 'timestamp';
    protected dateTimeValue: Date;
    protected calendarDateTime: Date;
    get value(): String | Number | Boolean | undefined;
    set value(v: String | Number | Boolean | undefined);
    private _min?;
    get mind(): Date | string | undefined;
    set mind(v: Date | string | undefined);
    private convertParameterToDateValue;
    private convertParameterToDateTimeValue;
    private convertParameterToValue;
    /**
     * @return Boolean
     */
    protected validate(): boolean;
    protected get displayValue(): TemplateResult | String | Number | Boolean | undefined;
    searchTerm: String | undefined;
    dialogMessagePrepend?: String;
    dialogMessageAppend?: String;
    dialogErrorMessage?: String;
    constructor();
    protected firstUpdated(changedProperties: Map<PropertyKey, unknown>): Promise<void>;
    protected doInit(): Promise<void>;
    reset(): void;
    protected addFocusClass(): void;
    protected removeFocusClass(): void;
    protected onKeydown(e: KeyboardEvent): void;
    protected onKeyup(e: KeyboardEvent): void;
    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    protected onInputChange(): void;
    protected onAction(): void;
    protected onOptionClick(e: Event): void;
    protected onOutsideClick(e: Event): void;
    static labels: {
        months: string[];
        days: string[];
        daysShort: string[];
    };
    protected get year(): string;
    protected padZeros(val: number): string;
    protected get month(): string;
    protected get monthLabel(): string;
    protected get day(): string;
    protected get weekDay(): string;
    protected get weekDayLabel(): string;
    protected get hours(): string;
    protected get minutes(): string;
    protected get seconds(): string;
    protected renderInput(): TemplateResult<1>;
    protected renderDialog(): TemplateResult<1>;
    protected renderCalendarYearContainer(): TemplateResult<1>;
    protected renderCalendarMonthContainer(): TemplateResult<1>;
    protected renderMonthPagination(): TemplateResult<1>;
    protected renderCalendarMonthPaginationPrev(): TemplateResult<1>;
    protected renderCalendarMonthPaginationNext(): TemplateResult<1>;
    protected renderCalendarWeekContainer(): TemplateResult<1>;
    protected renderCalendarWeekContent(): TemplateResult<1>;
    protected renderCalendarWeekRow(week: Array<number>): TemplateResult<1>;
    protected renderCalendarDayCell(day: number): TemplateResult<1>;
    protected selectYear(year: number): void;
    protected selectMonth(month: number): void;
    protected selectDay(day: number): void;
    protected renderTimeEditor(): TemplateResult<1>;
    protected selectHours(hours: number): void;
    protected selectMinutes(minutes: number): void;
    protected selectSeconds(minutes: number): void;
    static get styles(): import("lit").CSSResult;
}
