/// <reference types="lodash" />
import { ClassInfo } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import '@kode4/ui/kode4-fa-icon';
export default class Kode4Textfield extends Kode4Field {
    protected get typeCssClass(): string;
    min?: any;
    size?: number;
    maxlength?: number;
    max?: any;
    step?: number;
    outline: boolean;
    filled: boolean;
    placeholder: String;
    prefixOutside: string;
    prefix: string;
    suffix: string;
    suffixOutside: string;
    iconPrependOutside: IconDefinition | undefined;
    iconPrepend: IconDefinition | undefined;
    iconAppend: IconDefinition | undefined;
    iconAppendOutside: IconDefinition | undefined;
    iconReset: IconDefinition;
    iconClear: IconDefinition;
    iconAction: IconDefinition;
    resettable: boolean;
    action: boolean;
    clearable: boolean;
    changeOnType: boolean;
    counter: boolean;
    counterMax?: number;
    counterDivider?: string;
    counterLabel?: string;
    get charCount(): Number;
    /**
     * @return Boolean
     */
    protected validate(): boolean;
    hasDialog: boolean;
    dialogOpen: boolean;
    get isDialogOpen(): boolean;
    openDialogOnFocus: boolean;
    confirmOnEnter: boolean;
    constructor();
    protected doInit(updateValueFromAttribute?: boolean): Promise<void>;
    reset(): void;
    clear(): void;
    protected handleEsc(): void;
    openDialog(): void;
    closeDialog(): void;
    toggleDialog(): void;
    protected onFocusInput(): void;
    protected onInputChangeDebounced: import("lodash").DebouncedFunc<(e: Event) => void>;
    protected onInputChange(e: Event): void;
    protected onKeydown(_e: KeyboardEvent): void;
    protected onKeyup(e: KeyboardEvent): void;
    protected dialogActionBlocked: boolean | undefined;
    protected onActionMousedown(): void;
    protected onAction(): void;
    protected get isLabelFloating(): boolean;
    protected get showPlaceholder(): boolean;
    protected get cssClasses(): ClassInfo;
    protected render(): import("lit-html").TemplateResult<1>;
    protected renderIcon(icon: IconDefinition, classes?: String, eventName?: string): import("lit-html").TemplateResult<1>;
    protected fireIconEvent(e: Event, eventName: string): void;
    protected renderInlinePrefix(text: String): import("lit-html").TemplateResult<1>;
    protected renderPrefixOutside(text: String): import("lit-html").TemplateResult<1>;
    protected renderSuffixOutside(text: String): import("lit-html").TemplateResult<1>;
    protected renderInput(): import("lit-html").TemplateResult<1>;
    protected renderDecoratorBar(): import("lit-html").TemplateResult<1>;
    protected renderProgressBar(): "" | import("lit-html").TemplateResult<1>;
    protected renderProgressIcon(): "" | import("lit-html").TemplateResult<1>;
    protected renderInlineSuffix(text: String): import("lit-html").TemplateResult<1>;
    protected renderResetButton(): import("lit-html").TemplateResult<1>;
    protected renderActionButton(): import("lit-html").TemplateResult<1>;
    protected renderClearButton(): import("lit-html").TemplateResult<1>;
    protected renderMessagesAddon(): import("lit-html").TemplateResult<1>;
    protected renderCounter(): import("lit-html").TemplateResult<1>;
    protected renderDialogContainer(): "" | import("lit-html").TemplateResult<1>;
    protected renderDialog(): import("lit-html").TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
