import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { v4 as uuidv4 } from "uuid";
let Kode4CustomSelectOption = class Kode4CustomSelectOption extends LitElement {
    constructor() {
        super();
        this.selected = false;
        this.setAttribute('id', this.id);
    }
    connectedCallback() {
        if (!this.id) {
            this.id = `kode4field_${uuidv4()}`;
        }
        super.connectedCallback();
    }
    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        this.kode4CustomSelect = this.closest('kode4-custom-select');
        this.addEventListener('click', this.onSelect.bind(this));
        this.classList.remove('kode4-custom-option-selected');
        this.fireRegisterEvent();
    }
    fireRegisterEvent() {
        let event = new CustomEvent("kode4-custom-select-option-created", {
            bubbles: true,
            composed: true,
            detail: {
                field: this,
            }
        });
        this.dispatchEvent(event);
    }
    disconnectedCallback() {
        var _a;
        let event = new CustomEvent("kode4-custom-select-option-destroyed", {
            bubbles: false,
            composed: true,
            detail: {
                fieldid: this.id,
            }
        });
        (_a = this.kode4CustomSelect) === null || _a === void 0 ? void 0 : _a.dispatchEvent(event);
        super.disconnectedCallback();
    }
    onSelect() {
        this.selected = true;
        this.classList.add('kode4-custom-option-selected');
        this.dispatchEvent(new CustomEvent("customOptionSelected", {
            bubbles: true,
            composed: true,
            detail: {
                value: this.value,
            }
        }));
    }
    onOptionSelected(value) {
        const wasSelected = this.selected;
        this.selected = this.value == value;
        if (wasSelected !== this.selected) {
            this.selected ? this.classList.add('kode4-custom-option-selected') : this.classList.remove('kode4-custom-option-selected');
        }
    }
    render() {
        return html `
            <slot></slot>
        `;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            :host {
                flex: 1 0 1px;
                overflow: hidden;
                cursor: pointer;
                transition: all 0.3s ease-in-out;
                box-sizing: border-box;
            }
            
            :host(.kode4-custom-option-grayscale:not(.kode4-custom-option-selected)) {
                filter: grayscale(100%);
            }

            :host(.kode4-custom-option-grayscale:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-grayscale.kode4-custom-option-selected) {
                filter: grayscale(0);
            }

            :host(.kode4-custom-option-opacity:not(.kode4-custom-option-selected)) {                
                opacity: var(--kode4-customselect-option-deselected-opacity);
            }
            
            :host(.kode4-custom-option-opacity:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-opacity.kode4-custom-option-selected) {
                opacity: var(--kode4-customselect-option-selected-opacity);
            }
        `;
    }
};
__decorate([
    property({ type: String })
], Kode4CustomSelectOption.prototype, "value", void 0);
__decorate([
    property({ type: Boolean })
], Kode4CustomSelectOption.prototype, "selected", void 0);
Kode4CustomSelectOption = __decorate([
    customElement('kode4-custom-select-option')
], Kode4CustomSelectOption);
export default Kode4CustomSelectOption;
