import Kode4Textfield from "./kode4-textfield";
export default class Kode4Textarea extends Kode4Textfield {
    protected get typeCssClass(): string;
    protected get inputCssSelector(): string;
    resize: String;
    constructor();
    protected renderInput(): import("lit-html").TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
