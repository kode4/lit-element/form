import { LitElement } from "lit";
export default class Kode4CustomSelectOption extends LitElement {
    /**
     * This field is required because apparently we cannot
     * dispatch event on "this" in disconnectedCallback.
     * But we can on an element outside "this", so we save
     * a reference to the parent element.
     */
    private kode4CustomSelect?;
    protected value?: string;
    protected selected: boolean;
    constructor();
    connectedCallback(): void;
    protected firstUpdated(changedProperties: any): void;
    protected fireRegisterEvent(): void;
    disconnectedCallback(): void;
    protected onSelect(): void;
    onOptionSelected(value: any): void;
    render(): import("lit-html").TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
