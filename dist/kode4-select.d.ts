/// <reference types="lodash" />
import { TemplateResult } from "lit";
import Kode4Textfield from "./kode4-textfield";
import { Kode4OptionsProviderSubscriber, Kode4Option, Kode4ListResponse, Kode4OptionsProvider } from "./Kode4OptionsProvider";
export default class Kode4Select extends Kode4Textfield implements Kode4OptionsProviderSubscriber {
    protected get typeCssClass(): string;
    protected valueObject?: any;
    get value(): String | Number | Boolean | undefined;
    set value(v: String | Number | Boolean | undefined);
    protected get displayValue(): TemplateResult | String | Number | Boolean | undefined;
    protected _options: Array<Kode4Option>;
    protected _optionsCandidate?: Array<Kode4Option>;
    get options(): Array<Kode4Option>;
    set options(options: Array<Kode4Option>);
    valueField?: string;
    labelField?: string;
    _optionsProvider: Kode4OptionsProvider | undefined;
    get optionsProvider(): Kode4OptionsProvider | undefined;
    set optionsProvider(optionsProvider: Kode4OptionsProvider | undefined);
    protected _selectedOption: Kode4Option | undefined;
    protected get selectedOption(): Kode4Option | undefined;
    protected set selectedOption(selectedOption: Kode4Option | undefined);
    protected getOptionByValue(v: String | Number | Boolean | undefined): Kode4Option | undefined;
    optionHighlighted: number;
    get busy(): boolean;
    searchTerm: String | undefined;
    filters: FormData;
    dialogMessagePrepend?: String;
    dialogMessageAppend?: String;
    dialogErrorMessage?: String;
    forceSelection: Boolean;
    constructor();
    protected firstUpdated(changedProperties: Map<PropertyKey, unknown>): Promise<void>;
    protected doInit(updateValueFromAttribute?: boolean): Promise<void>;
    protected initOptionsProvider(): Promise<void>;
    handleListUpdated(): void;
    updateOptionsFromOptionsProvider(): Promise<void>;
    protected handleOptionsProviderListResponse(lr: Kode4ListResponse): Promise<void>;
    protected handleOptionsProviderListResponseError(e: Error, lr?: Kode4ListResponse): void;
    protected addFocusClass(): void;
    protected removeFocusClass(): void;
    protected handleOnFocus(): void;
    protected handleOnBlur(): void;
    protected highlightPrevOption(): void;
    protected highlightNextOption(): void;
    protected selectHighlightedOption(): void;
    protected onChange(originalEvent?: Event): void;
    protected onKeydown(e: KeyboardEvent): void;
    protected onKeyup(e: KeyboardEvent): void;
    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    protected onInputChange(): void;
    protected onSearchTermChangeDebounced: import("lodash").DebouncedFunc<(searchTerm: String) => void>;
    protected onSearchTermChange(searchTerm: String): void;
    protected onAction(): void;
    protected onOptionClick(e: Event): void;
    protected onOutsideClick(e: Event): void;
    protected renderInput(): TemplateResult<1>;
    protected renderDialog(): TemplateResult<1>;
    protected renderOption(opt: Kode4Option): TemplateResult<1>;
    static get styles(): import("lit").CSSResult;
}
