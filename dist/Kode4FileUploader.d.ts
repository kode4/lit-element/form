export interface Kode4FileUploaderProgressSubscriber {
    handleOnBeginUpload(): void;
    handleOnUploadProgress(progress: number): void;
    handleOnUploadComplete(): void;
}
export interface Kode4FileUploaderResultItem {
    file: File;
    filename: string;
}
export interface Kode4FileUploader {
    uploading: boolean;
    progress: number;
    fileList?: FileList;
    progressHandler?: string;
    subscribe(subscriber: Kode4FileUploaderProgressSubscriber): void;
    unsubscribe(subscriber: Kode4FileUploaderProgressSubscriber): void;
    upload(files?: FileList): Promise<any>;
    cancel(): void;
}
export declare abstract class AbstractKode4FileUploader {
    protected subscribers: Array<Kode4FileUploaderProgressSubscriber>;
    subscribe(subscriber: Kode4FileUploaderProgressSubscriber): void;
    unsubscribe(subscriber: Kode4FileUploaderProgressSubscriber): void;
}
export declare class Kode4FileUploaderSimulatorError extends AbstractKode4FileUploader implements Kode4FileUploader {
    uploading: boolean;
    progress: number;
    fileList?: FileList;
    progressHandler?: string;
    upload(): Promise<any>;
    cancel(): void;
}
export declare class Kode4FileUploaderSimulator extends AbstractKode4FileUploader implements Kode4FileUploader {
    private fileCount;
    uploading: boolean;
    progress: number;
    fileList?: FileList;
    progressHandler?: string;
    upload(fileList?: FileList): Promise<any>;
    cancel(): void;
}
