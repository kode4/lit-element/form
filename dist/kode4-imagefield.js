import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import { styleMap } from "lit/directives/style-map.js";
import Kode4Field from "./kode4-field";
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faUpload } from '@fortawesome/free-solid-svg-icons/faUpload';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons/faCaretDown';
import { faUndoAlt } from '@fortawesome/free-solid-svg-icons/faUndoAlt';
import { dragover, dragleave, drop } from "@kode4/ui/kode4-draggable";
import '@kode4/ui/kode4-fa-icon';
// const stringToBoolean = (str:string, force:boolean = true):boolean|string => {
//     switch(str.toLowerCase().trim()){
//         case 'true':
//         case 'yes':
//         case 'on':
//         case '1':
//             return true;
//
//         case 'false':
//         case 'no':
//         case 'off':
//         case '0':
//         case 'null':
//              return false;
//
//         default:
//             return force ? Boolean(str) : str;
//     }
// }
let Kode4Imagefield = class Kode4Imagefield extends Kode4Field {
    // --- INIT -----------------
    constructor() {
        super();
        this.previewImageMode = 'contain';
        this.mimeTypes = ['image/*'];
        this.placeholderAspectRatio = false;
        this.uploading = false;
        this.progress = 0;
        // we don't need a property here
        this.imageDragoverOptions = {
            acceptsFiles: true
        };
        this.showFilename = false;
        this.outline = false;
        this.filled = false;
        this.placeholder = '';
        this.placeholderImage = 'dummy';
        this.prefixOutside = '';
        this.prefix = '';
        this.suffix = '';
        this.suffixOutside = '';
        this.iconReset = faUndoAlt;
        this.iconClear = faTimes;
        this.iconAction = faCaretDown;
        this.resettable = false;
        this.action = false;
        this.clearable = false;
        this.hasDialog = false;
        this.dialogOpen = false;
        this.openDialogOnFocus = false;
        this.dialogActionBlocked = undefined;
    }
    get typeCssClass() {
        return 'kode4-imagefield';
    }
    get value() {
        return this._value;
    }
    set value(v) {
        super.value = v;
        if (this.hasValue && typeof this.value === 'string' && this.value.length) {
            try {
                JSON.parse(String(this.value));
                this.previewThumb = undefined;
            }
            catch (e) {
                if (this.previewThumb !== String(this.value)) {
                    this.previewThumb = String(this.value);
                }
            }
        }
        // if (!this.initialized && !this.initializing) {
        //     this._valueCandidate = v;
        //     return;
        // }
        // this._value = this.sanititzeValue(v);
        // if (this.initialized) {
        //     this.dirty = this.getDirty();
        // }
    }
    get fileUploader() {
        return this._fileUploader;
    }
    ;
    set fileUploader(fileUploader) {
        if (this._fileUploader) {
            this._fileUploader.unsubscribe(this);
        }
        this._fileUploader = fileUploader;
        if (this._fileUploader) {
            this._fileUploader.subscribe(this);
        }
    }
    ;
    get isDialogOpen() {
        // if (this.openDialogOnFocus && this.isFocused) {
        //     return true;
        // }
        return this.dialogOpen;
    }
    get isLabelFloating() {
        return this.floatingLabel && this.label && !this.hasValue && !this.showPlaceholder && !this.prefix && !this.iconPrepend;
    }
    // protected set isLabelFloating(floating:boolean) {
    // }
    get showPlaceholder() {
        return !this.hasValue && (Boolean(this.placeholder) || Boolean(this.placeholderIcon));
    }
    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
    }
    async doInit(updateValueFromAttribute = false) {
        super.doInit(updateValueFromAttribute);
        // if (this.outline) {
        //     this.busyStyle = 'icon';
        // }
    }
    // --- LOGIC -----------------
    reset() {
        super.reset();
        // if (!this.changeOnType && this.input) {
        //     this.input.value = <string>this.displayValue;
        // }
    }
    clear() {
        super.clear();
        // if (!this.changeOnType && this.input) {
        //     this.input.value = <string>this.displayValue;
        // }
    }
    handleEsc() {
        this.onClear();
    }
    // --- UI LOGIC -----------------
    openDialog() {
        this.dialogOpen = true;
    }
    closeDialog() {
        this.dialogOpen = false;
    }
    toggleDialog() {
        this.dialogOpen ? this.closeDialog() : this.openDialog();
    }
    // --- EVENTS -----------------
    onFocusInput() {
        super.onFocusInput();
        if (this.openDialogOnFocus && !this.forceBlur) {
            this.openDialog();
        }
        this.inputBlurEventBlocked = true;
        this.forceBlur = false;
    }
    // protected onInputChangeDebounced = debounce(this.onInputChange, this.debounce);
    async uploadFiles(files) {
        try {
            if (!this.fileUploader) {
                throw new Error('FileUploader is not defined.');
            }
            this.updateValueAfterUpload(await this.fileUploader.upload(files));
        }
        catch (e) {
            this.handleOnUploadError(e);
            // fire upload error event with error:Error
        }
    }
    updateValueAfterUpload(result) {
        const value = [];
        result.forEach((file) => {
            value.push({
                uploadedFilename: file.filename,
                originalFilename: file.file.name,
                originalFilesize: file.file.size,
            });
        });
        this.value = JSON.stringify(value);
        this.onChange();
        const thumb = result[0];
        var reader = new FileReader();
        reader.onload = (e) => {
            this.previewThumb = String(e.target.result);
        };
        reader.readAsDataURL(thumb.file);
    }
    handleOnBeginUpload() {
        this.error = '';
        this.uploading = true;
        this.progress = 0;
    }
    handleOnUploadProgress(progress) {
        this.progress = progress;
    }
    handleOnUploadComplete() {
        this.uploading = false;
        this.progress = 0;
    }
    handleOnUploadError(e) {
        this.error = e.message;
        this.uploading = false;
        this.progress = 0;
    }
    onInputChange(e) {
        this.uploadFiles(e.target.files || undefined);
        // this.value = <String>this.input?.value;
        // this.onChange(e);
    }
    onActionMousedown(e) {
        e.preventDefault();
        this.onMousedownPreventBlur();
        this.dialogActionBlocked = !this.dialogOpen;
    }
    onAction() {
        this.dialogActionBlocked = undefined;
    }
    // --- RENDER -----------------
    get cssClasses() {
        const cssClasses = {
            ...super.cssClasses,
            'kode4-dialog-open': this.isDialogOpen,
            // 'kode4-focus': this.isFocused,
            // 'busy': this.busy,
            // 'kode4-field-hasValue': this.valueIsDefined,
            // 'kode4-field-empty': !this.valueIsDefined,
            'outline': this.outline,
            'filled': this.filled,
            'floatinglabel': this.isLabelFloating,
        };
        return cssClasses;
    }
    onDropOnAssets(e) {
        var _a;
        this.uploadFiles((_a = e.dataTransfer) === null || _a === void 0 ? void 0 : _a.files);
    }
    render() {
        return html `
            <div class="component ${classMap(this.cssClasses)}">

                ${this.iconPrependOutside ? this.renderIcon(this.iconPrependOutside, "icon-prepend-outside", 'click-icon-prepend-outside') : ""}

                ${this.prefixOutside ? this.renderPrefixOutside(this.prefixOutside) : ""}

                <div class="field-container"
                
                    @dragover="${(e) => dragover(e, this.imageDragoverOptions)}"
                    @dragleave="${dragleave}"
                    @drop="${(e) => drop(e, this.onDropOnAssets.bind(this))}"
                    >
                
                    <fieldset class="field-input-container">
                        ${this.label ? this.renderLabel(this.label) : ""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend ? this.renderIcon(this.iconPrepend, "icon-prepend", 'click-icon-prepend') : ""}
                            ${this.prefix ? this.renderInlinePrefix(this.prefix) : ""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix ? this.renderInlineSuffix(this.suffix) : ""}
                            ${this.action ? this.renderActionButton() : ""}
                            ${this.resettable && this.initialValue && this.initialValue !== this.value ? this.renderResetButton() : ""}
                            ${this.clearable && this.displayValue !== '' ? this.renderClearButton() : ""}
                            ${this.iconAppend ? this.renderIcon(this.iconAppend, "icon-append", 'click-icon-append') : ""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}

                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside ? this.renderSuffixOutside(this.suffixOutside) : ""}

                ${this.iconAppendOutside ? this.renderIcon(this.iconAppendOutside, "icon-append-outside", 'click-icon-append-outside') : ""}

            </div>
        `;
    }
    renderIcon(icon, classes, eventName) {
        return html `
            <label for="field" class="${classes}" @mousedown="${this.onMousedownPreventBlur}" @click="${(e) => this.fireIconEvent(e, eventName || 'click-icon')}"><kode4-fa-icon .icon="${icon}"></kode4-fa-icon></label>
        `;
    }
    renderInlinePrefix(text) {
        return html `
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e) => this.fireIconEvent(e, 'click-prefix')}">${text}</label>
        `;
    }
    renderPrefixOutside(text) {
        return html `
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e) => this.fireIconEvent(e, 'click-prefix-outside')}">${text}</label>
        `;
    }
    renderSuffixOutside(text) {
        return html `
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e) => this.fireIconEvent(e, 'click-suffix-outside')}">${text}</label>
        `;
    }
    fireIconEvent(e, eventName) {
        let event = new CustomEvent(eventName, {
            bubbles: true,
            composed: true,
            detail: {
                aspectRatio: this.aspectRatio,
                element: this,
                originalEvent: e,
            },
        });
        this.dispatchEvent(event);
    }
    renderPlaceholder() {
        if (!this.showPlaceholder && this.hasValue) {
            return '';
        }
        const stylesContainer = {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            alignItems: 'center',
            margin: '0',
            padding: '0',
        };
        const stylesImage = {
            flex: '1 0 1px',
            textAlign: 'center',
        };
        return html `
            <div style="${styleMap(stylesContainer)}">
                ${this.placeholderAspectRatio && this.aspectRatio ? html `
                    <div style="${styleMap({ paddingTop: `calc(${this.aspectRatio.height} / ${this.aspectRatio.width} * 100%)`, width: '0' })}"></div>
                ` : ''}
                <div style="${styleMap(stylesImage)}" class="imagePreviewAspectRatio">
                    <div class="placeholder-container">
                        ${this.placeholderIcon ? html `<div class="placeholder-icon"><kode4-fa-icon .icon="${this.placeholderIcon}"></kode4-fa-icon></div>` : ''}
                        ${this.placeholder ? html `<div class="placeholder">${this.placeholder}</div>` : html `<div class="placeholder">&nbsp;</div>`}
                    </div>
                </div>
            </div>
        `;
    }
    renderPreviewThumb() {
        if (!this.previewThumb) {
            return '';
        }
        const stylesContainer = {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            alignItems: 'stretch',
        };
        const stylesImage = {
            flex: '1 0 1px',
            textAlign: 'center',
            background: 'center center no-repeat transparent',
            backgroundSize: this.previewImageMode === 'cover' ? 'cover' : 'contain',
            backgroundImage: this.previewThumb ? `url("${this.previewThumb}")` : ''
        };
        return html `
            <div style="${styleMap(stylesContainer)}">
                ${this.aspectRatio ? html `
                    <div style="${styleMap({ paddingTop: `calc(${this.aspectRatio.height} / ${this.aspectRatio.width} * 100%)`, width: '0' })}"></div>
                    <div style="${styleMap(stylesImage)}" class="imagePreviewAspectRatio"></div>
                ` : html `
                    <img src="${this.previewThumb}" style="max-width: 100%; display: block;">
                `}
            </div>
        `;
    }
    renderInput() {
        return html `
            <div
                class="field-input">
                ${this.renderPlaceholder()}
                ${this.renderPreviewThumb()}
                <div
                    style="overflow: hidden !important; width: 0 !important; height: 0 !important; padding: 0 !important; margin: 0 !important; border: none !important; display: block !important;">
                    <input 
                        id="field"
                        type="file" 
                        accept="${this.mimeTypes.join(',')}"
                        @change="${this.onInputChange}"
                        @focus="${this.onFocusInput}"
                        @blur="${this.onBlurInput}"
                        >
                </div>
            </div>
            <input
                type="hidden"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                type="text"

                .value="${this.displayValue}"
                autocomplete="off"
                />
        `;
    }
    renderDecoratorBar() {
        if (!this.uploading || this.busyStyle !== 'linear') {
            return '';
        }
        return html `
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `;
    }
    renderProgressBar() {
        return this.uploading && this.busyStyle === 'linear' ? html `
            <kode4-progress progress="${this.progress}"></kode4-progress>
        ` : '';
    }
    renderProgressIcon() {
        return this.uploading && this.busyStyle === 'icon' ? html `
            <kode4-progress
                type="icon"
                progress="${this.progress}"
                class="icon-busy"
                @mousedown="${this.onActionMousedown}"
                ></kode4-progress>
        ` : '';
    }
    renderInlineSuffix(text) {
        return html `
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e) => this.fireIconEvent(e, 'click-suffix')}">${text}</label>
        `;
    }
    renderResetButton() {
        return html `
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `;
    }
    renderActionButton() {
        return html `
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `;
    }
    renderClearButton() {
        return html `
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `;
    }
    renderDialogContainer() {
        if (!this.hasDialog) {
            return '';
        }
        return html `
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `;
    }
    renderDialog() {
        return html `
        `;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            ${super.styles}

            .field-input-container {
                overflow: hidden;
            }





            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                margin-left: -9px;
                margin-right: -9px;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }




            .placeholder-container {
                dispplay: block;
            }

            .placeholder-icon {
                dispplay: block;
                font-size: 2.0em;
            }
            
            .placeholder {
                dispplay: block;
            }

            .placeholder-icon + .placeholder {
                margin-top: 0.5em;
            }

        `;
    }
};
__decorate([
    property({ type: String, attribute: 'preview-image-mode', reflect: true, converter: {
            fromAttribute: (value) => {
                if ((value === null || value === void 0 ? void 0 : value.toLowerCase()) !== 'cover') {
                    return 'contain';
                }
                return 'cover';
            }
        } })
], Kode4Imagefield.prototype, "previewImageMode", void 0);
__decorate([
    property({ type: String, attribute: 'mime-types', reflect: true, converter: {
            fromAttribute: (value) => {
                if (!value || !value.length) {
                    return undefined;
                }
                return value.split(',');
            },
            toAttribute: (value) => {
                if (!value) {
                    return null;
                }
                return value.join(',');
            }
        } })
], Kode4Imagefield.prototype, "mimeTypes", void 0);
__decorate([
    property({ type: String, attribute: 'aspect-ratio', reflect: true, converter: {
            fromAttribute: (value) => {
                if (!value) {
                    return undefined;
                }
                if (!value.length) {
                    return undefined;
                }
                const parts = value.split(':');
                if (parts.length !== 2) {
                    return undefined;
                }
                return {
                    width: Number(parts[0]),
                    height: Number(parts[1]),
                };
            },
            toAttribute: (value) => {
                if (!value) {
                    return null;
                }
                return String(value.width + ':' + value.height);
            }
        } })
], Kode4Imagefield.prototype, "aspectRatio", void 0);
__decorate([
    property({ type: Boolean, attribute: 'placeholder-aspect-ratio' })
], Kode4Imagefield.prototype, "placeholderAspectRatio", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "uploading", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "progress", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "previewThumb", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "value", null);
__decorate([
    property()
], Kode4Imagefield.prototype, "_fileUploader", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "fileUploader", null);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "showFilename", void 0);
__decorate([
    property({ type: Number })
], Kode4Imagefield.prototype, "size", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "outline", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "filled", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "placeholder", void 0);
__decorate([
    property({ type: String, attribute: 'placeholder-icon', reflect: true, converter: {
            fromAttribute: () => {
                return faUpload;
            }
        } })
], Kode4Imagefield.prototype, "placeholderIcon", void 0);
__decorate([
    property({ type: Boolean, attribute: 'placeholder-image', reflect: true, converter: {
            fromAttribute: (value) => {
                if (!value) {
                    return false;
                }
                switch (value.toLowerCase().trim()) {
                    case 'true':
                    case 'yes':
                    case 'on':
                    case '1':
                        return true;
                    case 'false':
                    case 'no':
                    case 'off':
                    case '0':
                    case 'null':
                        return false;
                    default:
                        return Boolean(value);
                }
            },
            toAttribute: (value) => {
                return String(value);
            }
        } })
], Kode4Imagefield.prototype, "placeholderImage", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "prefixOutside", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "prefix", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "suffix", void 0);
__decorate([
    property({ type: String })
], Kode4Imagefield.prototype, "suffixOutside", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconPrependOutside", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconPrepend", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconAppend", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconAppendOutside", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconReset", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconClear", void 0);
__decorate([
    property()
], Kode4Imagefield.prototype, "iconAction", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "resettable", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "action", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "clearable", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "hasDialog", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "dialogOpen", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "isDialogOpen", null);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "openDialogOnFocus", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "isLabelFloating", null);
__decorate([
    property({ type: Boolean })
], Kode4Imagefield.prototype, "showPlaceholder", null);
__decorate([
    property()
], Kode4Imagefield.prototype, "cssClasses", null);
Kode4Imagefield = __decorate([
    customElement('kode4-imagefield')
], Kode4Imagefield);
export default Kode4Imagefield;
