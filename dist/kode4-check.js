import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import { faSquare } from '@fortawesome/free-regular-svg-icons/faSquare';
import { faCheckSquare } from '@fortawesome/free-regular-svg-icons/faCheckSquare';
import '@kode4/ui/kode4-fa-icon';
let Kode4Check = class Kode4Check extends Kode4Field {
    // --- INIT -----------------
    constructor() {
        super();
        this.trueValue = true;
        this.falseValue = false;
        this.type = 'checkbox';
        this.value = false;
        // this.submitEmpty = true;
    }
    get typeCssClass() {
        return 'kode4-check';
    }
    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        this.input.autofocus = true;
    }
    get displayValue() {
        return this.value;
    }
    get stringValue() {
        return String(this.value ? this.trueValue : this.falseValue);
    }
    sanititzeValue(v) {
        const tp = (typeof v);
        if (tp === 'string') {
            const vlc = v.toLowerCase();
            const c = vlc !== 'false' && vlc !== 'off' && vlc !== 'null' && vlc !== 'undefined' && vlc !== '0';
            return c;
        }
        else if (tp === 'number') {
            return v > 0;
        }
        return v;
    }
    // --- LOGIC -----------------
    handleEsc() {
        this.onClear();
    }
    // --- EVENTS -----------------
    onInputChange(e) {
        var _a;
        this.value = (_a = this.input) === null || _a === void 0 ? void 0 : _a.checked;
        this.onChange(e);
    }
    // --- RENDER -----------------
    render() {
        return html `
            <div class="component ${classMap(this.cssClasses)}">
                ${this.renderInput()}
                <label class="icon-unchecked" for="field">
                    <kode4-fa-icon .icon="${faSquare}"></kode4-fa-icon>
                </label>
                <label class="icon-checked" for="field">
                    <kode4-fa-icon .icon="${faCheckSquare}"></kode4-fa-icon>
                </label>
                <div class="kode4-field-box">
                    ${this.label ? this.renderLabel(this.label) : ""}



                    ${this.renderMessages()}

                </div>
            </div>
        `;
    }
    renderInput() {
        return html `
            <input
                class="field-input"
                value="1"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="checkbox"
                .checked="${this.displayValue}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                />
        `;
    }
    renderLabel(text) {
        return html `
            <label class="field-label-block" for="field">${text}</label>
        `;
    }
    // --- STYLES -----------------
    static get styles() {
        return css `
            ${super.styles}

            .field-input {
                display: inline-block;
                margin: 0;
                padding: 0;
                width: 0;
                min-width: 0;
                width: 0;
                min-height: 0;
                opacity: 0;
                flex: 0 1 0px;
            }

            .field-input:not(:checked) ~ .icon-checked {
                display: none;
            }

            .field-input:checked ~ .icon-unchecked {
                display: none;
            }

            .kode4-field {
                display: flex;
                flex-direction: row;
            }

            .kode4-field-box {
                flex: 1 0 1px;
                margin-left: 10px;
            }

            .kode4-focus .icon-checked,
            .kode4-focus .icon-unchecked,
            .kode4-focus .field-label {
                color: var(--kode4-field-decorator-color);
            }

            .icon-checked,
            .icon-unchecked {
                margin-top: var(--kode4-field-check-icon-spacing);
            }

            .field-label {
                transform: translate(0, 0) scale(1);
            }

            .field-label-block {
                display: block;
                line-height: var(--kode4-field-check-line-height, inherit);
            }
        `;
    }
};
__decorate([
    property()
], Kode4Check.prototype, "trueValue", void 0);
__decorate([
    property()
], Kode4Check.prototype, "falseValue", void 0);
__decorate([
    property()
], Kode4Check.prototype, "displayValue", null);
__decorate([
    property()
], Kode4Check.prototype, "stringValue", null);
Kode4Check = __decorate([
    customElement('kode4-check')
], Kode4Check);
export default Kode4Check;
