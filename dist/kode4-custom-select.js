import { __decorate } from "tslib";
import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import Kode4CustomSelectOption from "./kode4-custom-select-option";
import debounce from "lodash-es/debounce";
let Kode4CustomSelect = class Kode4CustomSelect extends Kode4Field {
    constructor() {
        super(...arguments);
        this.formElements = [];
        this.column = false;
        this.formElementsToAdd = [];
        this.formElementsToRemove = [];
        this.debounce = 150;
        this.onFieldsUpdatedDebounced = debounce(this.onFieldsUpdated, this.debounce);
    }
    get value() {
        return super.value;
    }
    set value(v) {
        super.value = v;
        this.updateCustomOptionsValue();
    }
    get typeCssClass() {
        return 'kode4-custom-select';
    }
    connectedCallback() {
        this.addEventListener('kode4-custom-select-option-destroyed', (e) => {
            this.formElementDestroyed(e);
        });
        super.connectedCallback();
    }
    formElementCreated(e) {
        var _a, _b;
        if (!((_a = e.detail) === null || _a === void 0 ? void 0 : _a.field)) {
            return;
        }
        this.formElementsToAdd.push((_b = e.detail) === null || _b === void 0 ? void 0 : _b.field);
        this.formElementsToRemove.filter((fieldid) => { var _a; return fieldid !== ((_a = e.detail) === null || _a === void 0 ? void 0 : _a.field.id); });
        this.onFieldsUpdatedDebounced();
    }
    formElementDestroyed(e) {
        var _a, _b;
        if (!((_a = e.detail) === null || _a === void 0 ? void 0 : _a.fieldid)) {
            return;
        }
        this.formElementsToRemove.push((_b = e.detail) === null || _b === void 0 ? void 0 : _b.fieldid);
        this.formElementsToAdd.filter((el) => { var _a; return el.id !== ((_a = e.detail) === null || _a === void 0 ? void 0 : _a.fieldid); });
        this.onFieldsUpdatedDebounced();
    }
    onFieldsUpdated() {
        const formElements = [
            ...this.formElements.filter((fe) => !this.formElementsToRemove.find((fieldid) => fieldid === fe.id))
        ];
        this.formElementsToAdd.forEach((el) => {
            if (!formElements.find((fe) => fe.id === el.id)) {
                formElements.push(el);
            }
        });
        this.formElementsToAdd = [];
        this.formElementsToRemove = [];
        this.formElements = [
            ...formElements,
        ];
        this.updateCustomOptionsValue();
        // this.applyConditions();
        // this.checkDirty();
        // this.formElementsInitialized = true;
    }
    initFormElements() {
        var _a;
        const slt = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('slot#options');
        this.formElements = [];
        Object.values(slt === null || slt === void 0 ? void 0 : slt.assignedElements()).forEach(el => {
            if (el instanceof Kode4CustomSelectOption) {
                this.formElements.push(el);
            }
            else {
                this.formElements = [
                    ...this.formElements,
                    ...el.querySelectorAll('kode4-custom-select-option')
                ];
            }
        });
    }
    async doInit(updateValueFromAttribute = false) {
        await super.doInit(updateValueFromAttribute);
        this.initFormElements();
        this.updateCustomOptionsValue();
    }
    // --- EVENTS -----------------
    onCustomOptionSelected(e) {
        this.value = e.detail.value;
        // this.updateCustomOptionsValue();
        this.onChange();
    }
    updateCustomOptionsValue() {
        this.formElements.map((el) => {
            el.onOptionSelected(this.value);
        });
    }
    onChange(originalEvent) {
        this.updateCustomOptionsValue();
        super.onChange(originalEvent);
    }
    // --- RENDER -----------------
    render() {
        return html `
            <div 
                class="component ${classMap(this.cssClasses)}"
                @kode4-custom-select-option-created="${this.formElementCreated}"
                >
                <slot name="label">
                    <div>
                        ${this.label}
                    </div>
                </slot>
                ${this.renderInput()}
                <div class="optionsContainer ${classMap(this.cssClassesOptionsContainer)}" @customOptionSelected="${this.onCustomOptionSelected}">
                    <slot id="options"></slot>
                </div>
                <div class="kode4-field-messages">
                    <div class="kode4-field-hint">
                        <div class="kode4-field-hint-left">
                            ${this.hint ? this.renderHint() : ""}
                        </div>
                    </div>
                    <div class="kode4-field-error">
                        ${this.error ? this.renderError() : ""}
                    </div>
                </div>
            </div>
        `;
    }
    renderInput() {
        return html `
            <input
                class="field-input"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="hidden"
                value="${this.value}"
                />
        `;
    }
    // --- STYLES -----------------
    get cssClassesOptionsContainer() {
        const cssClasses = {
            'layout-row': !this.column,
            'layout-column': this.column,
            // 'kode4-dialog-open': this.isDialogOpen,
            'kode4-focus': this.isFocused,
            // 'busy': this.busy,
            'kode4-field-hasValue': this.hasValue,
            'kode4-field-empty': !this.hasValue,
            // 'outline': this.outline,
            // 'filled': this.filled,
            // 'floatinglabel': this.floatingLabel && this.label && !this.valueIsDefined && !this.placeholder && !this.prefix && !this.iconPrepend && !this.isFocused,
        };
        return cssClasses;
    }
    static get styles() {
        return css `
            ${super.styles}

            :host {
                display: block;
            }

            .component {
                display: block;
            }

            .layout-row {
                display: flex;
                flex-direction: row;
                justify-items: stretch;
            }
            
            .layout-column {
                display: flex;
                flex-direction: column;
                justify-items: stretch;
            }
        `;
    }
};
__decorate([
    property({ type: Boolean })
], Kode4CustomSelect.prototype, "column", void 0);
__decorate([
    property()
], Kode4CustomSelect.prototype, "value", null);
__decorate([
    property()
], Kode4CustomSelect.prototype, "debounce", void 0);
__decorate([
    property()
], Kode4CustomSelect.prototype, "cssClassesOptionsContainer", null);
Kode4CustomSelect = __decorate([
    customElement('kode4-custom-select')
], Kode4CustomSelect);
export default Kode4CustomSelect;
