import { __decorate } from "tslib";
import { html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
let Kode4Formblock = class Kode4Formblock extends Kode4Field {
    get typeCssClass() {
        return 'kode4-formblock';
    }
    constructor() {
        super();
        this.canValidate = false;
    }
    get value() {
        return undefined;
    }
    // @ts-ignore
    set value(v) { }
    // --- RENDER -----------------
    render() {
        return html `
            <div class="component ${classMap(this.cssClasses)}">
                <slot style="border: solid 2px #ccc;"></slot>
            </div>
        `;
    }
};
__decorate([
    property()
], Kode4Formblock.prototype, "value", null);
Kode4Formblock = __decorate([
    customElement('kode4-formblock')
], Kode4Formblock);
export default Kode4Formblock;
