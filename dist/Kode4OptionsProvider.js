// import axios from "axios";
import { httpServiceFactory } from "@kode4/core/http/HttpService";
export class Kode4StaticOptionsProvider {
    constructor(list) {
        this.busy = false;
        this.list = [];
        this.subscribers = [];
        if (list) {
            this.setList(list);
        }
    }
    subscribe(subscriber) {
        if (!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }
    unsubscribe(subscriber) {
        if (this.subscribers.includes(subscriber)) {
            const i = this.subscribers.indexOf(subscriber);
            this.subscribers.splice(i, 1);
        }
    }
    fireListChanged() {
        this.subscribers.map((el) => {
            el.handleListUpdated();
        });
    }
    clearList() {
        this.list = [];
        this.fireListChanged();
    }
    setList(list) {
        if (list.length && list !== this.list) {
            this.list = list;
            this.fireListChanged();
        }
    }
    applyListFilters(list) {
        return list;
    }
    async getList(filters) {
        let list;
        if (this.valueField || this.labelField) {
            list = this.list.map((el) => {
                if (this.valueField) {
                    el['value'] = el[this.valueField];
                }
                if (this.labelField) {
                    el['label'] = el[this.labelField];
                }
                return el;
            });
        }
        else {
            list = this.list;
        }
        list = filters && filters.has('searchterm') ? list.filter((el) => el.label === filters.get('searchterm')) : list;
        list = this.applyListFilters(list);
        if (this.noSelection) {
            list.unshift({
                label: this.noSelection,
                unsafe: false
            });
        }
        return {
            error: 0,
            list
        };
    }
    async checkItemChangedAfterListUpdate(primaryKey, item) {
        try {
            const newItem = await this.getItem(primaryKey);
            if ((newItem && !item) || ((newItem === null || newItem === void 0 ? void 0 : newItem.label) != (item === null || item === void 0 ? void 0 : item.label))) {
                return newItem;
            }
            return false;
        }
        catch (e) {
            // item is not in list. return false? or unset value?
            return false;
        }
    }
    async getItem(primaryKey) {
        const item = this.list.reduce((result, el) => {
            if (el.value == primaryKey) {
                result = el;
            }
            return result;
        }, undefined);
        if (item === undefined) {
            throw new Error('Item not found for ' + primaryKey);
        }
        if (this.valueField) {
            item['value'] = item[this.valueField];
        }
        if (this.labelField) {
            item['label'] = item[this.labelField];
        }
        return item;
    }
}
export class Kode4RestOptionsProvider {
    constructor(httpConfigBase = 'default') {
        this.httpConfigBase = 'default';
        this.baseUrl = '';
        this.listCacheEnabled = true;
        this.listCache = [];
        this.listUrl = '';
        this.itemUrl = '';
        this.busyList = false;
        this.busyItem = false;
        this.subscribers = [];
        this.httpConfigBase = httpConfigBase;
    }
    get busy() {
        return this.busyList || this.busyItem;
    }
    subscribe(subscriber) {
        if (!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }
    unsubscribe(subscriber) {
        if (this.subscribers.includes(subscriber)) {
            const i = this.subscribers.indexOf(subscriber);
            this.subscribers.splice(i, 1);
        }
    }
    fireListChanged() {
        this.subscribers.map((el) => {
            el.handleListUpdated();
        });
    }
    applyRequestFilters(params) {
        return params;
    }
    applyListFilters(list) {
        return list;
    }
    async getList(filters) {
        var _a;
        try {
            // if (filters) {
            //     const queryString = (new URLSearchParams(filters) ).toString()
            // }
            const params = {
                searchterm: String(filters === null || filters === void 0 ? void 0 : filters.get('searchterm'))
            };
            const newRequestHash = JSON.stringify(params);
            if (this.lastRequestHash === newRequestHash && this.listCache) {
                return {
                    total: this.listCache.length,
                    list: this.listCache,
                    error: 0,
                };
            }
            this.lastRequestHash = newRequestHash;
            this.busyList = true;
            const response = (await httpServiceFactory()
                .config(this.httpConfigBase)
                .url(`${this.baseUrl}${this.listUrl}`)
                .param(params)
                .get());
            console.log('[Kode4RestOptionsProvider] response', response);
            const lr = {
                list: ((_a = response.data) === null || _a === void 0 ? void 0 : _a.list) || response.data,
                error: 0,
            };
            // // let data = await axios.get(`${this.baseUrl}${this.itemUrl}`, {
            // //     params: this.applyRequestFilters(params)
            // // });
            // let data = {
            //     data: {},
            // };
            // if (!data.data || typeof data.data !== 'object') {
            //     throw Error('Invalid result: '+typeof data);
            // }
            //
            // const lr:Kode4ListResponse = <Kode4ListResponse>data.data;
            // if (!lr.list) {
            //     lr.list = [];
            // }
            if (!Array.isArray(lr.list)) {
                throw Error('Invalid list result: ' + typeof lr.list);
            }
            if (this.valueField || this.labelField) {
                lr.list = lr.list.map((el) => {
                    if (this.valueField) {
                        el['value'] = el[this.valueField];
                    }
                    if (this.labelField) {
                        el['label'] = el[this.labelField];
                    }
                    return el;
                });
            }
            lr.list = this.applyListFilters(lr.list);
            if (this.listCacheEnabled) {
                this.listCache = this.listCache.concat(lr.list);
            }
            if (this.noSelection) {
                lr.list.unshift({
                    label: this.noSelection,
                    unsafe: false
                });
            }
            this.busyList = false;
            return lr;
        }
        finally {
            this.busyList = false;
        }
    }
    async checkItemChangedAfterListUpdate( /*primaryKey:String, item:Kode4Option*/) {
        // TODO:
        //      - check if item is in list
        //          - YES: return newItem != listItem
        //          - NO: return false
        return false;
    }
    getItemFromListCache(primaryKey) {
        return this.listCache.find((el) => String(el.value) === primaryKey);
    }
    clearListCache() {
        this.listCache = [];
    }
    async getItem(primaryKey) {
        try {
            if (!primaryKey) {
                throw new Error('primary Key is required');
            }
            if (this.listCacheEnabled) {
                const cachedItem = this.getItemFromListCache(primaryKey);
                if (cachedItem !== undefined) {
                    return cachedItem;
                }
            }
            this.busyItem = true;
            const data = (await httpServiceFactory()
                .config(this.httpConfigBase)
                .url(`${this.baseUrl}${this.itemUrl}/${primaryKey}`)
                .get()).data;
            // let data = await axios.get(`${this.baseUrl}${this.itemUrl}/${primaryKey}`);
            // let data = {
            //     data: {
            //         value: '',
            //         label: '',
            //     },
            // };
            const item = data.data;
            if (this.valueField) {
                item['value'] = item[this.valueField];
            }
            if (this.labelField) {
                item['label'] = item[this.labelField];
            }
            this.busyItem = false;
            this.listCache.push(item);
            return item;
        }
        finally {
            this.busyList = false;
        }
    }
}
// export class Kode4SelectOptionsProvider implements Kode4optionsProvider {
//     public baseUrl:String = '';
//     public listUrl:String = '';
//     public itemUrl:String = '';
//     public noSelection?:String;
//     public get busy():boolean {
//         return this.busyList || this.busyItem;
//     }
//     protected busyList:boolean = false;
//     protected busyItem:boolean = false;
//     public async getList(filters?:FormData):Promise<any> {
//         try {
//             this.busyList = true;
//             // if (filters) {
//             //     const queryString = (new URLSearchParams(filters) ).toString()
//             // }
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}`, {
//                 params: {
//                     searchterm: filters?.get('searchterm')
//                 }
//             });
//             if (!data.data || typeof data.data !== 'object') {
//                 throw Error('Invalid result: '+typeof data);
//             }
//             const lr:Kode4ListResponse = <Kode4ListResponse>data.data;
//             if (!lr.list) {
//                 lr.list = [];
//             }
//             if (!Array.isArray(lr.list)) {
//                 throw Error('Invalid list result: '+typeof lr.list);
//             }
//             if (this.noSelection) {
//                 (<Array<any>>lr.list).unshift({
//                     label: this.noSelection,
//                     unsafe: false
//                 });
//             }
//             this.busyList = false;
//             return lr;
//         } catch (e) {
//             this.busyList = false;
//             throw e;
//             return [];
//         }
//     }
//     public async getItem(primaryKey:String):Promise<any> {
//         try {
//             if (!primaryKey) {
//                 throw new Error('primary Key is required');
//             }
//             this.busyItem = true;
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}/${primaryKey}`);
//             data = data.data;
//             this.busyItem = false;
//             return data;
//         } catch (e) {
//             this.busyItem = false;
//             throw e;
//         }
//     }
// }
