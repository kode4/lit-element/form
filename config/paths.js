const path = require('path')

module.exports = {
  // Source files
  src: path.resolve(__dirname, '../src'),

  // Production build files
  build: path.resolve(__dirname, '../dist-demo'),

  // Static files that get copied to build folder
  public: path.resolve(__dirname, '../public'),

  // Static files that get copied to build folder
  env: process.env.NODE_ENV === 'production' ? path.resolve(__dirname, '.env.prod') : path.resolve(__dirname, '.env'),

}
