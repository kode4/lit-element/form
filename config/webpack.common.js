const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const paths = require('./paths')
const Dotenv = require("dotenv-webpack");
const path = require("path");

module.exports = {
  // Where webpack looks to start building the bundle
  entry: [paths.src + '/demo/index.ts'],

  // Where webpack outputs the assets and bundles
  output: {
    path: paths.build,
    filename: '[name].bundle.js',
    publicPath: '/',
  },

  // Customize the webpack build process
  plugins: [
    new Dotenv({
      path: paths.env, // load this now instead of the ones in '.env'
      safe: false, // true // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
      systemvars: true, // true // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      silent: false, // true // hide any errors
      defaults: false // false // load '.env.defaults' as the default values if empty.
    }),

    // Copies files from target to destination folder
    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.public,
          to: 'assets',
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),

    new CopyWebpackPlugin({
      patterns: [
        {
          context: "node_modules/@webcomponents/webcomponentsjs",
          from: "**/*.js",
          to: "webcomponents",
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),

    // Generates an HTML file from a template
    // Generates deprecation warning: https://github.com/jantimon/html-webpack-plugin/issues/1501
    new HtmlWebpackPlugin({
      title: 'KODE4 Form Demo',
      // favicon: paths.src + '/images/favicon.png',
      template: paths.src + '/demo/index.html', // template file
      filename: 'index.html', // output file,
      ignoreCustomComments: [/^!/],
    }),
  ],

  // Determine how modules within the project are treated
  module: {
    rules: [
      // // JavaScript: Use Babel to transpile JavaScript files
      // { test: /\.js$/, use: ['babel-loader'] },

      {
        test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/
      },

      // Images: Copy image files to build folder
      { test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },

      // Fonts and SVGs: Inline files
      { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },
    ],
  },

  resolve: {
    modules: [paths.src, 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    alias: {
      '@': paths.src,
      assets: paths.public,
    },
  },
}
