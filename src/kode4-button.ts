import { html, css } from "lit";
import {customElement, property, state, queryAssignedElements} from "lit/decorators.js";
import { ifDefined } from "lit/directives/if-defined.js";
import { classMap, ClassInfo } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import {faCaretDown} from "@fortawesome/free-solid-svg-icons/faCaretDown";
import {kode4Dialogs, kode4Drawers, kode4Menus} from "@kode4/ui";

@customElement('kode4-button')
export default class Kode4Button extends Kode4Field {

    @state()
    private hasMenuItems:boolean = false;

    @queryAssignedElements({slot: 'menu'})
    protected menuItems?:Array<HTMLElement>;

    private handleSlotchange() {
        this.hasMenuItems = this.menuItems !== undefined && this.menuItems.length > 0;
    }


    protected get typeCssClass():string {
        return 'kode4-button';
    }

    @property({ type: String })
    public href:String|undefined;

    @property({ type: String })
    public target:String|undefined = undefined;

    @property({ type: String })
    public type:String = 'button';

    @property({ type: String })
    public color?:string;

    @property({ type: Boolean })
    public primary:boolean = false;

    @property({ type: Boolean })
    public secondary:boolean = false;

    @property({ type: Boolean })
    public success:boolean = false;

    @property({ type: Boolean })
    public danger:boolean = false;

    @property({ type: Boolean })
    public warning:boolean = false;

    @property({ type: Boolean })
    public info:boolean = false;

    @property({ type: Boolean })
    public outline:boolean = false;

    @property({ type: Boolean })
    public filled:boolean = false;

    @property({ type: Boolean })
    public flat:boolean = false;

    @property({ type: Boolean })
    public icon:boolean = false;

    @property({ type: Boolean })
    public iconSquare:boolean = false;

    @property({ type: String, attribute: 'menu' })
    public menu?:string;

    @property({ type: String, attribute: 'drawer' })
    public drawer?:string;

    @property({ type: String, attribute: 'dialog' })
    public dialog?:string;

    @property({ type: String, attribute: 'caller-params' })
    public callerParams?:any;

    // protected firstUpdated(changedProperties:any) {
    //     super.firstUpdated(changedProperties);

    //     if (this.primary) {
    //         this.classList.add("kode4-primary");
    //     }
        
    //     if (this.secondary) {
    //         this.classList.add("kode4-secondary");
    //     }

    //     if (this.success) {
    //         this.classList.add("kode4-success");
    //     }

    //     if (this.danger) {
    //         this.classList.add("kode4-danger");
    //     }

    //     if (this.warning) {
    //         this.classList.add("kode4-warning");
    //     }

    //     if (this.info) {
    //         this.classList.add("kode4-info");
    //     }

    //     if (this.outline) {
    //         this.classList.add("kode4-button-outline");
    //     }

    //     if (this.filled) {
    //         this.classList.add("kode4-button-filled");
    //     }

    //     if (this.flat) {
    //         this.classList.add("kode4-button-flat");
    //     }

    //     if (this.icon) {
    //         this.classList.add("kode4-button-icon");
    //     }

    //     if (this.iconSquare) {
    //         this.classList.add("kode4-button-icon");
    //         this.classList.add("kode4-button-iconSquare");
    //     }
    // }

    private updateCssClass(changedProperties:Map<PropertyKey, unknown>, prop:string, value:boolean, cssClass:string) {
        if (changedProperties.has(prop)) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }
    }

    protected updated(changedProperties:Map<PropertyKey, unknown>) {
        super.updated(changedProperties);

        this.updateCssClass(changedProperties, 'primary', this.primary, 'kode4-primary');
        this.updateCssClass(changedProperties, 'secondary', this.secondary, 'kode4-secondary');
        this.updateCssClass(changedProperties, 'success', this.success, 'kode4-success');
        this.updateCssClass(changedProperties, 'danger', this.danger, 'kode4-danger');
        this.updateCssClass(changedProperties, 'warning', this.warning, 'kode4-warning');
        this.updateCssClass(changedProperties, 'info', this.info, 'kode4-info');
        this.updateCssClass(changedProperties, 'outline', this.outline, 'kode4-button-outline');
        this.updateCssClass(changedProperties, 'filled', this.filled, 'kode4-button-filled');
        this.updateCssClass(changedProperties, 'flat', this.flat, 'kode4-button-flat');

        if (changedProperties.has('icon') || changedProperties.has('iconSquare')) {
            if (!this.icon && !this.iconSquare)  {
                this.classList.remove("kode4-button-icon");
                this.classList.remove("kode4-button-iconSquare");
            
            } else if (this.icon)  {
                this.classList.add("kode4-button-icon");
            
            } else if (this.iconSquare)  {
                this.classList.add("kode4-button-icon");
                this.classList.add("kode4-button-iconSquare");
            }
        }
    }

    public constructor() {
        super();
        this.manageValue = false;
    }

    // --- LOGIC -----------------
    handleEsc() {
        this.onClear();
    }



    // --- EVENTS -----------------
    onClick(e:Event) {
        if (this.disabled) {
            e.stopPropagation();
            e.preventDefault();
            return;
        }

        if (this.menu && kode4Menus.has(this.menu)) {
            e.preventDefault();
            const menu = kode4Menus.get(this.menu)!;
            menu.show({
                alignToElement: this.parentElement!,
                callerParams: this.callerParams,
            });
            return;
        }

        if (this.drawer && kode4Drawers.has(this.drawer)) {
            e.preventDefault();
            const drawer = kode4Drawers.get(this.drawer)!;
            drawer.show();
            return;
        }

        if (this.dialog && kode4Dialogs.has(this.dialog)) {
            e.preventDefault();
            const dialog = kode4Dialogs.get(this.dialog)!;
            dialog.show();
            return;
        }

        this.onChange();

        switch (this.type) {
            case 'submit' :
                this.onSubmit(e);
                break;

            case 'reset' :
                this.onFormControlEvent(e, 'resetForm');
                break;

            case 'clear' :
                this.onFormControlEvent(e, 'clearForm');
                break;

            case 'updateFields' :
                this.onFormControlEvent(e, 'updateFields');
                break;

            case 'updateValues' :
                this.onFormControlEvent(e, 'updateValues');
                break;

            case 'respawn' :
                this.onFormControlEvent(e, 'respawnForm');
                break;
        }
    }

    onFormControlEvent(originalEvent:Event, eventName:string) {
        let event = new CustomEvent(eventName, {
            bubbles: true,
            composed: true,
            detail: {
                originalEvent: originalEvent
            }
        });

        if (originalEvent) {
            originalEvent.preventDefault();
        }

        this.dispatchEvent(event);
    }

    // --- RENDER -----------------
    @property()
    protected get cssClasses():ClassInfo {
        const cssClasses = {
            ...super.cssClasses,
            'kode4-button': true,
        };

        return cssClasses;
    }

    protected render() {
        return this.href ? this.renderLink() : this.renderButton();
    }

    protected renderButton() {
        return html`
            <button
                type="${this.type}"
                class="component ${classMap(this.cssClasses)}"
                ?required="${this.required}"
                @click="${this.onClick}">
                <span class="kode4-button-content">
                    <slot></slot>
                </span>
                ${this.hasMenuItems ? html`
                    <span class="kode4-button-icon-dropdown">
                        <kode4-fa-icon .icon="${faCaretDown}"></kode4-fa-icon>
                    </span>
                ` : undefined}
            </button>
            <slot name="menu" @slotchange=${this.handleSlotchange}></slot>
        `;
    }

    protected renderLink() {
        return html`
            <a
                href="${this.href}"
                target="${ifDefined(this.target)}"
                class="component ${this.typeCssClass}"
                @click="${this.onClick}">
                <span class="kode4-button-content">
                    <slot></slot>
                </span>
            </a>
        `;
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            :host {
                background-color: transparent;
            }

            :host(:hover) {
                --main-color: var(--main-color-hover);
                --main-color-filled: var(--main-color-filled-hover);
                --background-color-filled: var(--background-color-filled-hover);
            }

            .kode4-button {
                color: var(--main-color);
                font-size: var(--kode4-button-font-size);
                font-family: var(--kode4-button-font-family);
                line-height: var(--kode4-button-line-height);
                font-weight: var(--kode4-button-font-weight);
                border-radius: var(--kode4-button-border-radius);
                //display: block;
                width: 100%;
                height: 100%;
                text-align: center;
                cursor: pointer;
                outline: none;
                padding: 0;
                margin: 0;
                border: none;
                text-decoration: none;
                /* color: inherit; */
                background-color: transparent;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }
          
          .component.disabled,
          .component.disabled > *,
          .component.disabled .field-input-display {
                filter: grayscale(50%) brightness(102%);
                opacity: 0.85;
          }
          
            :host(.kode4-field-xxs) .kode4-button {
                font-size: var(--kode4-button-font-size-xxs);
            }
            
            :host(.kode4-field-xs) .kode4-button {
                font-size: var(--kode4-button-font-size-xs);
            }
            
            :host(.kode4-field-s) .kode4-button {
                font-size: var(--kode4-button-font-size-s);
            }
            
            :host(.kode4-field-m) .kode4-button {
                font-size: var(--kode4-button-font-size-m);
            }
            
            :host(.kode4-field-l) .kode4-button {
                font-size: var(--kode4-button-font-size-l);
            }
            
            :host(.kode4-field-xl) .kode4-button {
                font-size: var(--kode4-button-font-size-xl);
            }
            
            :host(.kode4-field-xxl) .kode4-button {
                font-size: var(--kode4-button-font-size-xxl);
            }
            
            :host(.kode4-primary) {
                --main-color: var(--main-color-primary);
                --main-color-filled: var(--main-color-filled-primary);
                --background-color-filled: var(--background-color-filled-primary);
            }

            :host(.kode4-primary:not(.disabled)):hover {
                --main-color: var(--main-color-hover-primary);
                --main-color-filled: var(--main-color-filled-hover-primary);
                --background-color-filled: var(--background-color-filled-hover-primary);
            }

            :host(.kode4-secondary) {
                --main-color: var(--main-color-secondary);
                /* --background-color: transparent; */
                --main-color-filled: var(--main-color-filled-secondary);
                --background-color-filled: var(--background-color-filled-secondary);
            }

            :host(.kode4-secondary:not(.disabled)):hover {
                --main-color: var(--main-color-hover-secondary);
                /* --background-color: transparent; */
                --main-color-filled: var(--main-color-filled-hover-secondary);
                --background-color-filled: var(--background-color-filled-hover-secondary);
            }

            :host(.kode4-success) {
                --main-color: var(--main-color-success);
                --main-color-filled: var(--main-color-filled-success);
                --background-color-filled: var(--background-color-filled-success);
            }

            :host(.kode4-success:not(.disabled)):hover {
                --main-color: var(--main-color-hover-success);
                --main-color-filled: var(--main-color-filled-hover-success);
                --background-color-filled: var(--background-color-filled-hover-success);
            }

            :host(.kode4-danger) {
                --main-color: var(--main-color-danger);
                --main-color-filled: var(--main-color-filled-danger);
                --background-color-filled: var(--background-color-filled-danger);
            }

            :host(.kode4-danger:not(.disabled)):hover {
                --main-color: var(--main-color-hover-danger);
                --main-color-filled: var(--main-color-filled-hover-danger);
                --background-color-filled: var(--background-color-filled-hover-danger);
            }

            :host(.kode4-warning) {
                --main-color: var(--main-color-warning);
                --main-color-filled: var(--main-color-filled-warning);
                --background-color-filled: var(--background-color-filled-warning);
            }

            :host(.kode4-warning:not(.disabled)):hover {
                --main-color: var(--main-color-hover-warning);
                --main-color-filled: var(--main-color-filled-hover-warning);
                --background-color-filled: var(--background-color-filled-hover-warning);
            }

            :host(.kode4-info) {
                --main-color: var(--main-color-info);
                --main-color-filled: var(--main-color-filled-info);
                --background-color-filled: var(--background-color-filled-info);
            }

            :host(.kode4-info:not(.disabled)):hover {
                --main-color: var(--main-color-hover-info);
                --main-color-filled: var(--main-color-filled-hover-info);
                --background-color-filled: var(--background-color-filled-hover-info);
            }

            :host(.kode4-button-outline) .kode4-button {
                border: var(--kode4-button-outline-border-width) var(--kode4-button-outline-border-style) var(--main-color);
                background: transparent;
            }
            
            :host(.kode4-button-filled) .kode4-button {
                color: var(--main-color-filled);
                background: var(--background-color-filled);
            }

            :host(:not(.kode4-button-flat):not([disabled]):hover) .kode4-button {
                box-shadow: var(--main-color) 0px 0px 5px;
            }

            .kode4-button.disabled {
                cursor: default;
            }

            :host(:not(.kode4-button-icon)) .kode4-button-content {
                flex: 1 0 1px;
                padding-top: var(--kode4-button-padding-top);
                padding-bottom: var(--kode4-button-padding-bottom);
                padding-left: var(--kode4-button-padding-left);
                padding-right: var(--kode4-button-padding-right);
            }

            :host(.kode4-button-icon) .kode4-button {
                border-radius: 50%;
                line-height: 1.0em;
                width: 3.05em;
                height: 3em;
                padding: 0;
                margin: 0;
                text-align: center;
            }
            
            :host(.kode4-button-icon.kode4-button-iconSquare) .kode4-button {
                border-radius: 0;
            }
            
            :host(.kode4-button-icon) .kode4-button-content {
                display: inline-block;
                padding: 0;
                margin: 0;
                font-size: 1.5em;
            }

            :host(.kode4-block) .kode4-button {
                display: block;
                width: 100%;
            }

            :host([no-menu-icon]) .kode4-button-icon-dropdown {
                display: none;
            }
            
            .kode4-button-icon-dropdown {
                padding-left: .5em;
                padding-right: .5em;
                //border-left-style: dotted;
                //border-left-width: 1px;
            }
        `;
    }

}
