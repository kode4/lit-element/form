import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { ifDefined } from "lit/directives/if-defined.js";
import { classMap, ClassInfo } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import debounce from "lodash-es/debounce";
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons/faCaretDown';
import { faUndoAlt } from '@fortawesome/free-solid-svg-icons/faUndoAlt';
import '@kode4/ui/kode4-fa-icon'

@customElement('kode4-textfield')
export default class Kode4Textfield extends Kode4Field {

    protected get typeCssClass():string {
        return 'kode4-textfield';
    }
   
    @property()
    public min?:any;

    @property({ type: Number })
    public size?:number;

    @property({ type: Number })
    public maxlength?:number;

    @property()
    public max?:any;

    @property({ type: Number })
    public step?:number;

    @property({ type: Boolean })
    public outline:boolean = false;

    @property({ type: Boolean })
    public filled:boolean = false;

    @property({ type: String })
    public placeholder:String = '';

    @property({ type: String })
    public prefixOutside:string = '';

    @property({ type: String })
    public prefix:string = '';

    @property({ type: String })
    public suffix:string = '';

    @property({ type: String })
    public suffixOutside:string = '';

    @property()
    public iconPrependOutside:IconDefinition|undefined;

    @property()
    public iconPrepend:IconDefinition|undefined;

    @property()
    public iconAppend:IconDefinition|undefined;

    @property()
    public iconAppendOutside:IconDefinition|undefined;

    @property()
    public iconReset:IconDefinition = faUndoAlt;

    @property()
    public iconClear:IconDefinition = faTimes;

    @property()
    public iconAction:IconDefinition = faCaretDown;

    @property({ type: Boolean })
    public resettable:boolean = false;

    @property({ type: Boolean })
    public action:boolean = false;

    @property({ type: Boolean })
    public clearable:boolean = false;

    @property({ type: Boolean })
    public changeOnType:boolean = false;

    @property({ type: Boolean })
    public counter:boolean = false;

    @property({ type: Number, attribute: 'counter-max' })
    public counterMax?:number;

    @property({ type: String, attribute: 'counter-divider' })
    public counterDivider?:string = '/';

    @property({ type: String, attribute: 'counter-label' })
    public counterLabel?:string;

    @property()
    public get charCount():Number {
        if (this.isFocused && !this.changeOnType && this.input) {
            return String(this.input.value).length;
        }

        if (!this.hasValue) {
            return 0;
        }
        return (<String>this.value).length;
    }

    /**
     * @return Boolean
     */
    protected validate() {
        if (!super.validate()) {
            return false;
        }

        if (this.counter && this.counterMax && this.charCount > this.counterMax) {
            this.error = `Die maximale Zeichenanzahl beträgt ${this.counterMax}`;
            return false;
        }

        return true;
    }


    @property({ type: Boolean })
    public hasDialog:boolean = false;

    @property({ type: Boolean })
    public dialogOpen:boolean = false;

    @property({ type: Boolean })
    public get isDialogOpen():boolean {
        // if (this.openDialogOnFocus && this.isFocused) {
        //     return true;
        // }
        return this.dialogOpen;
    }

    @property({ type: Boolean })
    public openDialogOnFocus:boolean = false;

    @property({ type: Boolean })
    public confirmOnEnter:boolean = true;


    // --- INIT -----------------
    constructor() {
        super();
    }

    protected async doInit(updateValueFromAttribute:boolean = false) {
        super.doInit(updateValueFromAttribute);
        if (this.outline) {
            this.busyStyle = 'icon';
        }
    }


    // --- LOGIC -----------------
    public reset() {
        super.reset();
        if (!this.changeOnType && this.input) {
            this.input.value = <string>this.displayValue;
        }
    }

    public clear() {
        super.clear();
        if (!this.changeOnType && this.input) {
            this.input.value = <string>this.displayValue;
        }
    }

    protected handleEsc() {
        this.onClear();
    }



    // --- UI LOGIC -----------------
    public openDialog() {
        if (this.readonly || this.disabled) {
            return;
        }
        this.dialogOpen = true;

    }

    public closeDialog() {
        this.dialogOpen = false;
    }

    public toggleDialog() {
        if (this.readonly || this.disabled) {
            return;
        }
        this.dialogOpen ? this.closeDialog() : this.openDialog();
    }



    // --- EVENTS -----------------
    protected onFocusInput() {
        super.onFocusInput();

        if (this.openDialogOnFocus && !this.forceBlur) {
            this.openDialog();
        }
        
        this.forceBlur = false;
    }

    protected onInputChangeDebounced = debounce(this.onInputChange, this.debounce);

    protected onInputChange(e:Event) {
        this.value = <String>this.input?.value;
        this.onChange(e);
    }

    protected onKeydown(_e:KeyboardEvent) {
    }

    protected onKeyup(e:KeyboardEvent) {
        if (e.key === "Escape") {
            this.handleEsc();

        } else if (e.key === "Enter" && this.confirmOnEnter) {
            this.onSubmit(e, false);

        } else if (this.changeOnType) {
            this.onInputChangeDebounced(e);

        } else if (this.counter) {
            this.requestUpdate();
        }
    }

    protected dialogActionBlocked:boolean|undefined = undefined;

    protected onActionMousedown() {
        this.onMousedownPreventBlur();
        this.dialogActionBlocked = !this.dialogOpen;
    }

    protected onAction() {
        this.dialogActionBlocked = undefined;
    }

    // @property({ type: Boolean })
    // protected get isLabelFloating():boolean {
    //     return super.isLabelFloating && !this.placeholder && !this.prefix && !this.iconPrepend;
    // }

    @property({ type: Boolean })
    protected get isLabelFloating():boolean {
        return this.floatingLabel && this.label && !this.hasValue && !this.showPlaceholder && !this.prefix && !this.iconPrepend && !this.isFocused;
    }

    // protected set isLabelFloating(floating:boolean) {

    // }

    @property({ type: Boolean })
    protected get showPlaceholder():boolean {
        return !this.hasValue && Boolean(this.placeholder);
    }

    // --- RENDER -----------------
    @property()
    protected get cssClasses():ClassInfo {
        const cssClasses = {
            ...super.cssClasses,
            'kode4-dialog-open': this.isDialogOpen,
            // 'kode4-focus': this.isFocused,
            // 'busy': this.busy,
            // 'kode4-field-hasValue': this.valueIsDefined,
            // 'kode4-field-empty': !this.valueIsDefined,
            'outline': this.outline,
            'filled': this.filled,
            // 'floatinglabel': this.floatingLabel && this.label && !this.hasValue && !this.placeholder && !this.prefix && !this.iconPrepend && !this.isFocused,
            'floatinglabel': this.isLabelFloating,
        };

        return cssClasses;
    }

    protected render() {
        return html`
            <div class="component ${classMap(this.cssClasses)}">

                ${this.iconPrependOutside ? this.renderIcon(this.iconPrependOutside, "icon-prepend-outside", 'click-icon-prepend-outside') : ""}

                ${this.prefixOutside ? this.renderPrefixOutside(this.prefixOutside) : ""}

                <div class="field-container">
                    <fieldset class="field-input-container">
                        ${this.label ? this.renderLabel(this.label) : ""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend ? this.renderIcon(this.iconPrepend, "icon-prepend", 'click-icon-prepend') : ""}
                            ${this.prefix ? this.renderInlinePrefix(this.prefix) : ""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix ? this.renderInlineSuffix(this.suffix) : ""}
                            ${this.action ? this.renderActionButton() : ""}
                            ${this.resettable && this.initialValue && this.initialValue !== this.value ? this.renderResetButton() : ""}
                            ${this.clearable && this.displayValue !== '' ? this.renderClearButton() : ""}
                            ${this.iconAppend ? this.renderIcon(this.iconAppend, "icon-append", 'click-icon-append') : ""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}
                    
                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside ? this.renderSuffixOutside(this.suffixOutside) : ""}

                ${this.iconAppendOutside ? this.renderIcon(this.iconAppendOutside, "icon-append-outside", 'click-icon-append-outside') : ""}

            </div>
        `;
    }

    protected renderIcon(icon:IconDefinition, classes?:String, eventName?:string) {
        return html`
            <label for="field" class="${classes}" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, eventName || 'click-icon')}"><kode4-fa-icon .icon="${icon}"></kode4-fa-icon></label>
        `;
    }

    protected fireIconEvent(e:Event, eventName:string) {
        let event = new CustomEvent(eventName, {
            bubbles: true,
            composed: true,
            detail: {
                element: this,
                originalEvent: e,
            },
        });
        this.dispatchEvent(event);
    }

    protected renderInlinePrefix(text:String) {
        return html`
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-prefix')}">${text}</label>
        `;
    }

    protected renderPrefixOutside(text:String) {
        return html`
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-prefix-outside')}">${text}</label>
        `;
    }

    protected renderSuffixOutside(text:String) {
        return html`
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-suffix-outside')}">${text}</label>
        `;
    }

    protected renderInput() {
        return html`
            <input
                class="field-input"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"

                min="${ifDefined(this.min)}"
                max="${ifDefined(this.max)}"
                step="${ifDefined(this.step)}"
                size="${ifDefined(this.size)}"
                maxlength="${ifDefined(this.maxlength)}"

                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${(e:MouseEvent) => e.stopPropagation()}"
                autocomplete="off"
                />
        `;
    }

    protected renderDecoratorBar() {
        return html`
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `;
    }

    protected renderProgressBar() {
        return this.busy && this.busyStyle === 'linear' ? html`
            <kode4-progress indeterminate></kode4-progress>
        ` : '';
    }

    protected renderProgressIcon() {
        return this.busy && this.busyStyle === 'icon' ? html`
                <kode4-progress
                    type="icon"
                    indeterminate
                    class="icon-busy"
                    @mousedown="${this.onActionMousedown}"
                    ></kode4-progress>
        ` : '';
    }

    protected renderInlineSuffix(text:String) {
        return html`
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-suffix')}">${text}</label>
        `;
    }

    protected renderResetButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `;
    }

    protected renderActionButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `;
    }

    protected renderClearButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `;
    }

    protected renderMessagesAddon() {
        return this.renderCounter();
    }

    protected renderCounter() {
        if (!this.counter) {
            return html``;
        }
        return html`
            <span class="field-counter">
                <span class="field-counter-chars">
                    ${this.charCount}
                </span>
            ${this.counterMax ? html`
                <span class="field-counter-divider">
                    ${this.counterDivider}
                </span>
                <span class="field-counter-max">
                    ${this.counterMax}
                </span>
            `: ''}
            ${this.counterLabel ? html`
                <span class="field-counter-label">
                    ${this.counterLabel}
                </span>
            `: ''}
            </span>
        `;
    }

    protected renderDialogContainer() {
        if (!this.hasDialog) {
            return '';
        }
        return html`
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `;
    }
    protected renderDialog() {
        return html`
        `;
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}
            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                display: none;
                visibility: hidden;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }

            .field-counter {
                margin-top: var(--kode4-field-messages-margin-top);
                margin-bottom: var(--kode4-field-messages-margin-bottom);
                padding-top: var(--kode4-field-messages-padding-top);
                padding-bottom: var(--kode4-field-messages-padding-bottom);
                padding-left: var(--kode4-field-counter-padding-left);
            }



        `;
    }
    
}
