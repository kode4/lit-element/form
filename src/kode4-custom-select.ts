import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap, ClassInfo } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import Kode4CustomSelectOption from "./kode4-custom-select-option";
import debounce from "lodash-es/debounce";

@customElement('kode4-custom-select')
export default class Kode4CustomSelect extends Kode4Field {

    private formElements:Array<Kode4CustomSelectOption> = [];

    @property({ type: Boolean })
    public column:boolean = false;

    @property()
    public get value():String|Number|Boolean|undefined {
        return super.value;
    }
    
    public set value(v:String|Number|Boolean|undefined) {
        super.value = v;
        this.updateCustomOptionsValue();
    }

    protected get typeCssClass():string {
        return 'kode4-custom-select';
    }

    private formElementsToAdd:Array<Kode4CustomSelectOption> = [];
    
    private formElementsToRemove:Array<string> = [];

    @property()
    public debounce:number = 150;

    connectedCallback() {
        this.addEventListener('kode4-custom-select-option-destroyed', (e:Event) => {
            this.formElementDestroyed(<CustomEvent>e);
        });
        super.connectedCallback();
    }

    protected formElementCreated(e:CustomEvent) {
        if (!e.detail?.field) {
            return;
        }

        this.formElementsToAdd.push(e.detail?.field);
        this.formElementsToRemove.filter((fieldid:string) => fieldid !== e.detail?.field.id);

        this.onFieldsUpdatedDebounced();
    }

    protected formElementDestroyed(e:CustomEvent) {
        if (!e.detail?.fieldid) {
            return;
        }

        this.formElementsToRemove.push(e.detail?.fieldid);
        this.formElementsToAdd.filter((el:Kode4CustomSelectOption) => el.id !== e.detail?.fieldid);

        this.onFieldsUpdatedDebounced();
    }

    protected onFieldsUpdatedDebounced = debounce(this.onFieldsUpdated, this.debounce);

    protected onFieldsUpdated() {
        const formElements:Array<Kode4CustomSelectOption> = [
            ...this.formElements.filter((fe:Kode4CustomSelectOption) => !this.formElementsToRemove.find((fieldid:string) => fieldid === fe.id))        
        ];

        this.formElementsToAdd.forEach((el:Kode4CustomSelectOption) => {
            if (!formElements.find((fe:Kode4CustomSelectOption) => fe.id === el.id)) {
                formElements.push(el);
            }
        });
        
        this.formElementsToAdd = [];
        this.formElementsToRemove = [];

        this.formElements = [
            ...formElements,
        ]

        this.updateCustomOptionsValue();
        // this.applyConditions();
        // this.checkDirty();
        // this.formElementsInitialized = true;
    }

    protected initFormElements() {
        const slt = <HTMLSlotElement>this.shadowRoot?.querySelector('slot#options');

        this.formElements = [];
        Object.values(slt?.assignedElements()).forEach(el => {
            if (el instanceof Kode4CustomSelectOption) {
                this.formElements.push(el);

            } else {
                this.formElements = [
                    ...this.formElements,
                    ...<NodeListOf<Kode4CustomSelectOption>>el.querySelectorAll('kode4-custom-select-option')
                ];
            }
        });
    }

    protected async doInit(updateValueFromAttribute:boolean = false) {
        await super.doInit(updateValueFromAttribute);
        this.initFormElements();
        this.updateCustomOptionsValue();
    }

    // --- EVENTS -----------------
    protected onCustomOptionSelected(e:CustomEvent) {
        this.value = e.detail.value;
        // this.updateCustomOptionsValue();
        this.onChange();
    }

    protected updateCustomOptionsValue() {
        this.formElements.map((el:Kode4CustomSelectOption) => {
            el.onOptionSelected(this.value);
        });
    }

    protected onChange(originalEvent?:Event) {
        this.updateCustomOptionsValue();
        super.onChange(originalEvent);
    }


    // --- RENDER -----------------
    protected render() {
        return html`
            <div 
                class="component ${classMap(this.cssClasses)}"
                @kode4-custom-select-option-created="${this.formElementCreated}"
                >
                <slot name="label">
                    <div>
                        ${this.label}
                    </div>
                </slot>
                ${this.renderInput()}
                <div class="optionsContainer ${classMap(this.cssClassesOptionsContainer)}" @customOptionSelected="${this.onCustomOptionSelected}">
                    <slot id="options"></slot>
                </div>
                <div class="kode4-field-messages">
                    <div class="kode4-field-hint">
                        <div class="kode4-field-hint-left">
                            ${this.hint ? this.renderHint() : ""}
                        </div>
                    </div>
                    <div class="kode4-field-error">
                        ${this.error ? this.renderError() : ""}
                    </div>
                </div>
            </div>
        `;
    }

    protected renderInput() {
        return html`
            <input
                class="field-input"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="hidden"
                value="${this.value}"
                />
        `;
    }



    // --- STYLES -----------------
    @property()
    protected get cssClassesOptionsContainer():ClassInfo {
        const cssClasses = {
            'layout-row': !this.column,
            'layout-column': this.column,
            // 'kode4-dialog-open': this.isDialogOpen,
            'kode4-focus': this.isFocused,
            // 'busy': this.busy,
            'kode4-field-hasValue': this.hasValue,
            'kode4-field-empty': !this.hasValue,
            // 'outline': this.outline,
            // 'filled': this.filled,
            // 'floatinglabel': this.floatingLabel && this.label && !this.valueIsDefined && !this.placeholder && !this.prefix && !this.iconPrepend && !this.isFocused,
        };

        return cssClasses;
    }

    static get styles() {
        return css`
            ${super.styles}

            :host {
                display: block;
            }

            .component {
                display: block;
            }

            .layout-row {
                display: flex;
                flex-direction: row;
                justify-items: stretch;
            }
            
            .layout-column {
                display: flex;
                flex-direction: column;
                justify-items: stretch;
            }
        `;
    }

}
