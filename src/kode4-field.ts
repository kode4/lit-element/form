import { LitElement, html, css, TemplateResult } from "lit";
import {customElement, property, state} from "lit/decorators.js";
import { classMap, ClassInfo } from "lit/directives/class-map.js";
import { styleMap, StyleInfo } from "lit/directives/style-map.js";
import { Kode4FormConditionExpression, Kode4FormCustomCondition } from "./Kode4FormCondition";
import { v4 as uuidv4 } from "uuid";
import Kode4Form from "./kode4-form";

@customElement('kode4-field')
export default class Kode4Field extends LitElement {

    protected get typeCssClass():string {
        return `kode4-hidden`;
    }

    protected get inputCssSelector():string {
        return 'input';
    };

    @property({ type: Boolean })
    public block:Boolean = false;

    @property({ type: Boolean })
    protected submitEmpty:boolean = false;

    @property({ type: Boolean })
    protected initialized:boolean = false;

    @property({ type: Boolean })
    protected initializing:boolean = false;

    @property({ type: Boolean })
    public touched:boolean = false;

    @property()
    public type:String = 'text';

    @property()
    public name:String = '';

    @property({ type: Boolean })
    public required:Boolean = false;

    @property({ type: Boolean })
    public floatingLabel:boolean = false;

    @property({ type: Boolean })
    public xxs:Boolean = false;

    @property({ type: Boolean })
    public xs:Boolean = false;

    @property({ type: Boolean })
    public s:Boolean = false;

    @property({ type: Boolean })
    public m:Boolean = false;

    @property({ type: Boolean })
    public l:Boolean = false;

    @property({ type: Boolean })
    public xl:Boolean = false;

    @property({ type: Boolean })
    public xxl:Boolean = false;

    @property({ type: String, attribute: 'required-suffix' })
    public requiredSuffix:String = '';

    @property()
    protected _value:String|Number|Boolean|undefined = '';

    @property()
    protected _valueCandidate?:any;

    @property()
    public get value():String|Number|Boolean|undefined {
        return this._value;
    }
    
    public set value(v:String|Number|Boolean|undefined) {
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }
        this._value = this.sanititzeValue(v);

        if (this.initialized) {
            this.dirty = this.getDirty();
        }
    }
    
    protected getDirty():boolean {
        if (this.disabled !== this.initialDisabled) {
            return true;
        }
        if (!this.getValueIsDefined(this.initialValue)) {
            return this.getValueIsDefined(this._value);
        }
        return this.initialValue != this._value;
    }

    @property()
    public get stringValue() {  
        if (!this.getValueIsDefined(this.value)) {
            return '';
        }
        return String(this.value);
    }

    @property({ type: Boolean })
    public manageValue:boolean = true;
    
    @property()
    protected slotsReady:Boolean = false;

    protected sanititzeValue(v:any):String|Number|Boolean|undefined {
        return <String>v;
    }

    @property()
    protected field:Element|undefined;

    @property()
    protected input:HTMLInputElement|undefined;

    @property()
    public initialValue:String|Number|Boolean|undefined = undefined;

    @property()
    public resetValue:String|Number|Boolean|undefined = undefined;

    @property({ type: Boolean })
    protected initialDisabled:boolean = false;

    @property({ type: Boolean })
    protected _disabled:boolean = false;

    @property({ type: Boolean })
    public get disabled():boolean {
        return this._disabled;
    }

    public set disabled(disabled:boolean) {
        this._disabled = disabled;
        if (this.initialized) {
            this.dirty = this.getDirty();
            // this.onChange();
        }
    }

    @property({ type: Boolean })
    public readonly:boolean = false;

    @property({ type: Boolean })
    protected controlReadonly:Boolean = false;

    @property()
    public label:String = '';

    @property()
    public hint:String = '';

    @property()
    public error:String = '';

    @property({ type: Number, attribute: 'debounce', reflect: true })
    public debounce:number = 350;

    @property({ type: Boolean })
    public canValidate:Boolean = true;

    @property()
    public conditions?:Kode4FormConditionExpression|Array<Kode4FormConditionExpression>;

    /**
     * This field is required because apparently we cannot
     * dispatch event on "this" in disconnectedCallback.
     * But we can on an element outside "this", so we save
     * a reference to the parent element.
     */
    @state()
    private _kode4Form?:Kode4Form;

    @state()
    private get kode4Form():Kode4Form {
        if (!this._kode4Form) {
            this._kode4Form = <Kode4Form>this.closest('kode4-form');
        }
        return this._kode4Form;
    };

    public applyConditions(form:Kode4Form) {
        if (!this.conditions) {
            return;
        }

        if (Array.isArray(this.conditions)) {
            for (let condition of this.conditions) {
                this.applyCondition(condition, form);
            }
        } else {
            this.applyCondition(this.conditions, form);
        }
    }

    public applyCondition(condition:Kode4FormConditionExpression, form:Kode4Form) {
        const success:boolean = Array.isArray(condition.conditions) 
            ? condition.conditions.reduce((result:boolean, condition:Kode4FormCustomCondition) => {
                result = result && this.evalCondition(condition, form);
                return result;
            }, true)
            : this.evalCondition(condition.conditions, form);

        if (success) {
            if (condition.handleThen) {
                condition.handleThen(form, this);
                this.requestUpdate();
            }
        } else {
            if (condition.handleElse) {
                condition.handleElse(form, this);
                this.requestUpdate();
            }
        }
    }

    public evalCondition(condition:Kode4FormCustomCondition, form:Kode4Form):boolean {
        if (typeof condition === 'function') {
            return Boolean(condition(form, this));
        }
        return false;
    }

    @property()
    protected get displayValue():TemplateResult|String|Number|Boolean|undefined {
        if (!this.getValueIsDefined(this.value)) {
            return '';
        }
        return this.value;
    }

    @property() 
    public get hasValue():boolean {
        return this.submitEmpty || this.getValueIsDefined(this.value);
    }

    protected getValueIsDefined(value:any) {
        return !(typeof value === 'undefined' || !value || value === 'undefined' || value === 'null');
    }

    @property({ type: Boolean })
    protected inputFocused:boolean = false;

    @property()
    public get isFocused():boolean {
        return this.inputFocused || this.preventBlur;
    }

    @property({ type: Boolean })
    public get busy():boolean {
        return false;
    }
    
    @property({ type: String })
    public busyStyle:string = 'linear';

    protected inputFocusEventBlocked:boolean = false;

    protected preventBlur:boolean = false;
    
    protected forceBlur:boolean = false;

    protected inputBlurEventBlocked:boolean = false;
    

    @property({ type: Boolean })
    public dirty:boolean = false;


    // --- INIT -----------------
    constructor() {
        super();
        this.setAttribute('id', this.id);
    }

    protected initDom() {
        this.classList.add("kode4-field");
        this.classList.add(this.typeCssClass);
    }

    public connectedCallback() {
        if (!this.id) {
            this.id = `kode4field_${uuidv4()}`;
        }

        if (!this.name) {
            this.name = `kode4field_${uuidv4()}`;
        }

        if (this.block) {
            this.classList.add("kode4-block");
        }

        if (this.canValidate) {
            this.classList.add("kode4-canValidate");
        }

        if (this.xxs) {
            this.classList.add("kode4-field-xxs");
        }

        if (this.xs) {
            this.classList.add("kode4-field-xs");
        }

        if (this.s) {
            this.classList.add("kode4-field-s");
        }

        if (this.m) {
            this.classList.add("kode4-field-m");
        }

        if (this.l) {
            this.classList.add("kode4-field-l");
        }

        if (this.xl) {
            this.classList.add("kode4-field-xl");
        }

        if (this.xxl) {
            this.classList.add("kode4-field-xxl");
        }

        super.connectedCallback()
    }
    
    public disconnectedCallback() {
        let event = new CustomEvent("kode4-field-destroyed", {
            bubbles: false,
            composed: true,
            detail: {
                fieldname: this.name,
                fieldid: this.id,
            }
        });
        this.kode4Form?.dispatchEvent(event);

        super.disconnectedCallback();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);

        this.initDom();
        this.slotsReady = true;

        this.input = this.shadowRoot?.querySelector(this.inputCssSelector) || undefined;

        this.init();
    }

    private lastFocusedState:boolean = false;

    protected updated(_changedProperties:any) {
        if (this.lastFocusedState !== this.isFocused) {
            this.isFocused ? this.handleOnFocus() : this.handleOnBlur();
        }
        this.lastFocusedState = this.isFocused;
    }

    protected handleOnFocus() {
    }

    protected handleOnBlur() {
    }

    protected async init(updateValueFromAttribute:boolean = false) {
        this.beforeInit();
        await this.doInit(updateValueFromAttribute);
        this.afterInit();
    }
    
    protected beforeInit() {
        this.initialized = false;
        this.initializing = true;
        this.dirty = false;
        this.initialValue = undefined;
        this.initialDisabled = false;
        this.error = '';
    }

    protected resolveInitialized?:Function;

    public isInitialized = new Promise(resolve => {
        this.resolveInitialized = resolve;
    });

    protected async doInit(updateValueFromAttribute:boolean = false) {
        if (updateValueFromAttribute) {
            this.updateValue();
        } else {
            this.value = this._valueCandidate;
        }
        this.initialValue = this.getValueIsDefined(this.value) ? this.value : undefined;
        this.initialDisabled = this.disabled;
    }
    
    protected afterInit() {
        this.initializing = false;
        this.initialized = true;
        if (this.resolveInitialized) {
            this.resolveInitialized();
        }
        this.fireRegisterEvent();
    }


    // --- LOGIC -----------------
    public reset() {
        this.value = this.initialValue;
        this.disabled = this.initialDisabled;
        this.requestUpdate();
        this.onChange();
    }

    public clear() {
        this.value = this.resetValue;
        this.requestUpdate();
        this.onChange();
    }

    public updateValue() {
        this.value = <string>this.getAttribute('value');
    }

    public async respawn() {
        await this.init(true);
        this.onChange();
    }
    
    public resetState() {
        this.touched = false;
        this.error = '';
    }


    // --- VALIDATE -----------------
    public get valid():boolean {
        if (!this.canValidate) {
            return true;
        }

        return this.validate();
    }

    /**
     * @return Boolean
     */
    protected validate() {
        this.error = '';
        if (this.required && !this.disabled && !this.hidden && !this.hasValue) {
            this.error = 'Dieses Feld wird benötigt';
            return false;
        }

        return true;
    }



    // --- ACTIONS -----------------
    public focus() {
        this.inputFocusEventBlocked = true;
        this.focusInput();
        this.inputFocusEventBlocked = false;
    }

    public blur() {

    }

    protected focusInput() {
        this.input?.focus();
    }

    protected blurInput() {
        this.input?.blur();
    }

    protected sanitizeInputFocused() {
        this.inputFocused = this.shadowRoot?.activeElement === this.input;
    }



    // --- EVENTS -----------------
    // protected hasFocus() {
    //     return this.field?.classList.contains("kode4-focus")
    // }

    protected fireRegisterEvent() {
        let event = new CustomEvent("kode4-field-created", {
            bubbles: true,
            composed: true,
            detail: {
                field: this,
            }
        });

        this.dispatchEvent(event);
    }

    protected fireUnregisterEvent() {
        let event = new CustomEvent("kode4-field-destroyed", {
            bubbles: true,
            composed: true,
            detail: {
                fieldname: this.name,
                fieldid: this.id,
            }
        });

        this.dispatchEvent(event);
    }

    protected onFocusInput() {
        this.inputFocused = !this.forceBlur;
    }

    protected onBlurInput() {
        if (this.preventBlur) {
            this.sanitizeInputFocused();
            this.preventBlur = false;
            this.focusInput();

        } else {
            this.inputFocused = false;
        }
    }

    protected onChange(originalEvent?:Event) {
        this.touched = true;

        let event = new CustomEvent("change", {
            bubbles: true,
            composed: true,
            detail: {
                disabled: this.disabled,
                readonly: this.readonly,
                manageValue: this.manageValue,
                value: this.value,
                name: this.name,
                originalEvent: originalEvent,
                dirty: this.dirty,
            }
        });

        if (originalEvent) {
            originalEvent.preventDefault();
        }

        this.dispatchEvent(event);
    }

    protected onSubmit(originalEvent?:Event, submitValue:boolean = true) {
        const detail:any = {
            originalEvent: originalEvent
        };
        if (submitValue) {
            detail.value = this.value,
            detail.name = this.name
        };
        let event = new CustomEvent("submitForm", {
            bubbles: true,
            composed: true,
            detail
        });

        if (originalEvent) {
            originalEvent.preventDefault();
        }

        this.dispatchEvent(event);
    }



    protected onMousedownPreventBlur() {
        if (this.isFocused) {
            this.preventBlur = true;
        }
    }

    protected onMousedownPreventBlurOrForceBlur() {
        if (this.isFocused) {
            this.preventBlur = true;
        } else {            
            this.forceBlur = true;
        }
    }

    protected onClear() {
        this.clear();
    }

    protected onReset() {
        this.reset();
    }



    // --- RENDER -----------------
    @property()
    protected get cssClasses():ClassInfo {
        const cssClasses = {
            'kode4-focus': this.isFocused,
            'busy': this.busy,
            'hidden': this.hidden,
            'disabled': this.disabled,
            'readonly': this.readonly,
            'kode4-field-hasValue': this.hasValue,
            'kode4-field-empty': !this.hasValue,
        };

        return cssClasses;
    }

    protected render():TemplateResult {
        return html`
            <div class="component ${classMap(this.cssClasses)}">
                ${this.renderInput()}
            </div>
        `;
    }

    protected renderInput() {
        return html`
            <input
                id="field"
                type="hidden"
                class="field-control-input"
                ?name="${this.name}"
                .value="${this.displayValue}"
                />
        `;
    }


    @property({ type: Number })
    protected labelWidth?:number;

    @property({ type: Boolean })
    protected get isLabelFloating():boolean {
        return this.floatingLabel && this.label && !this.hasValue && !this.isFocused;
    }

    @property({ type: String })
    protected get legendWidth():string {
        if (this.floatingLabel && this.isLabelFloating) {
            return '0px';
        }
        
        if (!this.labelWidth) {
            const el:HTMLLabelElement|null = this.shadowRoot?.querySelector('.field-label') || null;
            if (!el) {
                return 'auto';
            }
            this.labelWidth = el.clientWidth;
        }
        return this.labelWidth ? `${this.labelWidth}px`: 'auto';
    }

    protected renderLabel(text:String) {
        return html`
            <legend class="field-legend" style="${styleMap(<StyleInfo>{'width': this.legendWidth})}">${text}</legend>
            <label class="field-label" for="field">${text}${this.required && this.requiredSuffix ? html` ${this.requiredSuffix}`: ''}</label>
        `;
    }

    protected renderMessages() {
        return html`
            <div class="kode4-field-messages">
                <div class="kode4-field-messages-container">
                    ${this.renderHint()}
                    ${this.renderError()}
                </div>

                ${this.renderMessagesAddon()}
            </div>
        `;
    }

    protected renderHint() {
        if (!this.hint) {
            return '';
        }
        return html`
            <div class="kode4-field-hint">${this.hint}</div>
        `;
    }

    protected renderError() {
        if (!this.error) {
            return '';
        }
        return html`
            <div class="kode4-field-error">${this.error}</div>
        `;
    }

    protected renderMessagesAddon() {
        return html``;
    }

    // --- STYLES -----------------
    static get styles() {
        return css`
        
            * {
                box-sizing: border-box;
                text-overflow: ellipsis;
            }

            :host {
                display: inline-block;
                font-size: 1em;
                line-height: 1.0em;
                width: auto;
                max-width: 100%;
                box-sizing: border-box;

                margin-top: var(--kode4-field-padding-top, 0px);
                margin-right: var(--kode4-field-margin-right, 0px);
                margin-bottom: var(--kode4-field-padding-bottom, 0px);
                margin-left: var(--kode4-field-margin-left, 0px);
            }

            :host(.kode4-block) {
                display: block;
                width: 100%;
            }

            .component {
                transition: opacity 0.4s ease-in-out;
                display: flex;
                flex-direction: row;
            }

            .component.hidden {
                position: absolute;
                width: 0;
                overflow: hidden;
                visibility: hidden;
                opacity: 0;
            }

            .component.disabled,
            .component.disabled > *,
            .component.disabled .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .component.readonly,
            .component.readonly > *,
            .component.readonly .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .field-container {
                flex: 1 0 1px;
                /* flex: 0 1 100%; */
                position: relative;
            }

            .field-input-container-layout {
                display: flex;
                flex-direction: row;
                /* max-width: 100%; */
                overflow: hidden;
            }

            .field-input {
                /* display: block; */
                flex: 1 0 1px;
                width: 100%;
                min-width: 10px;
                /* flex: 0 1 100%; */
                background: transparent;
                border: none;
                outline: none;
                box-shadow: none; /* Firefox adds a red shadow to required fields */
            }

            ${this.getLabelStyles()}
            ${this.getOutlineStyles()}
            ${this.getFilledStyles()}


            .kode4-field-messages {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;

                color: var(--kode4-field-messages-color);
                font-family: var(--kode4-field-messages-font-family);
                font-size: var(--kode4-field-messages-font-size);
                line-height: var(--kode4-field-messages-line-height);
                font-weight: var(--kode4-field-messages-font-weight);

                margin-right: var(--kode4-field-messages-margin-right);
                margin-left: var(--kode4-field-messages-margin-left);
                padding-right: var(--kode4-field-messages-padding-right);
                padding-left: var(--kode4-field-messages-padding-left);
            }
            
            .kode4-field-messages-container {
                flex: 1 0 1px;
            }

            .kode4-field-messages-container > *:first-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-top: var(--kode4-field-messages-padding-top);
            }
            
            .kode4-field-messages-container > *:last-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-bottom: var(--kode4-field-messages-padding-bottom);
            }
                
            .kode4-field-messages-container > *:not(:first-child) {
                margin-top: var(--kode4-field-messages-spacing);
            }
                
            .outline .kode4-field-messages {
                padding-top: var(--kode4-field-outline-messages-padding-top);
                padding-right: var(--kode4-field-outline-messages-padding-right);
                padding-bottom: var(--kode4-field-outline-messages-padding-bottom);
                padding-left: var(--kode4-field-outline-messages-padding-left);
            }

            .kode4-field-hint {
                font-family: var(--kode4-field-hint-font-family, inherit);
                font-size: var(--kode4-field-hint-font-size, inherit);
                line-height: var(--kode4-field-hint-line-height, inherit);
                font-weight: var(--kode4-field-hint-font-weight, inherit);
                color: var(--kode4-field-hint-color, inherit);
            }

            .kode4-field-error {
                font-family: var(--kode4-field-error-font-family, inherit);
                font-size: var(--kode4-field-error-font-size, inherit);
                line-height: var(--kode4-field-error-line-height, inherit);
                font-weight: var(--kode4-field-error-font-weight, inherit);
                color: var(--kode4-field-error-color, inherit);
            }


            /*
            .field-hint-content {
                display: inline-block;
                font-family: var(--kode4-field-hint-font-family);
                font-size: var(--kode4-field-hint-font-size);
                line-height: var(--kode4-field-hint-line-height);
                font-weight: var(--kode4-field-hint-font-weight);
                color: var(--kode4-field-hint-color);
                padding-top: var(--kode4-field-hint-padding-top);
                padding-right: var(--kode4-field-hint-padding-right);
                padding-bottom: var(--kode4-field-hint-padding-bottom);
                padding-left: var(--kode4-field-hint-padding-left);
            }

            .field-error-content {
                display: inline-block;
                font-family: var(--kode4-field-error-font-family);
                font-size: var(--kode4-field-error-font-size);
                line-height: var(--kode4-field-error-line-height);
                font-weight: var(--kode4-field-error-font-weight);
                color: var(--kode4-field-error-color);
                padding-top: var(--kode4-field-error-padding-top);
                padding-right: var(--kode4-field-error-padding-right);
                padding-bottom: var(--kode4-field-error-padding-bottom);
                padding-left: var(--kode4-field-error-padding-left);
            }

            .outline .field-hint-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-hint-padding-top);
                padding-right: var(--kode4-field-outline-hint-padding-right);
                padding-bottom: var(--kode4-field-outline-hint-padding-bottom);
                padding-left: var(--kode4-field-outline-hint-padding-left);
            }

            .outline .field-error-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-error-padding-top);
                padding-right: var(--kode4-field-outline-error-padding-right);
                padding-bottom: var(--kode4-field-outline-error-padding-bottom);
                padding-left: var(--kode4-field-outline-error-padding-left);
            }
            */
        `;
    }

    static getLabelStyles() {
        return css`
            .field-label {
                display: block;
                text-overflow: ellipsis;
                max-width: 75%;
                overflow: hidden;
                white-space: nowrap;
            }

            .component {
            }

            .component .field-label {
                transition: all 0.3s ease-out;
                position: absolute;
                z-index: 10;
            }

            .field-input-container {
                margin-top: var(--kode4-field-input-container-margin-top);
                margin-right: var(--kode4-field-input-container-margin-right);
                margin-bottom: var(--kode4-field-input-container-margin-bottom);
                margin-left: var(--kode4-field-input-container-margin-left);
                padding-top: var(--kode4-field-input-container-padding-top);
                padding-right: var(--kode4-field-input-container-padding-right);
                padding-bottom: var(--kode4-field-input-container-padding-bottom);
                padding-left: var(--kode4-field-input-container-padding-left);
            }

            .field-label {
                transform: translate(var(--kode4-field-label-translate-x), var(--kode4-field-label-translate-y)) scale(var(--kode4-field-label-scale));
            }

            legend.field-legend {
                width: 0;
                opacity: 0;
                height: 0;
                margin: 0;
                padding: 0;
                visibility: hidden;
                transition: width 0.05s ease-out 0.15s;
            }

            .floatinglabel .field-label {
                transform: translate(var(--kode4-field-floatinglabel-label-translate-x), var(--kode4-field-floatinglabel-label-translate-y)) ;
            }
        `;
    }

    static getOutlineStyles() {
        return css`
            .component:not(.outline) .field-input-container {
                border: none;
            }

            .outline .field-input-container {
                border: var(--kode4-field-outline-border-style) var(--kode4-field-outline-border-width) var(--kode4-field-outline-border-color);
                margin-top: var(--kode4-field-outline-input-container-margin-top);
                margin-right: var(--kode4-field-outline-input-container-margin-right);
                margin-bottom: var(--kode4-field-outline-input-container-margin-bottom);
                margin-left: var(--kode4-field-outline-input-container-margin-left);
                padding-top: var(--kode4-field-outline-input-container-padding-top);
                padding-right: var(--kode4-field-outline-input-container-padding-right);
                padding-bottom: var(--kode4-field-outline-input-container-padding-bottom);
                padding-left: var(--kode4-field-outline-input-container-padding-left);
                border-radius: var(--kode4-field-outline-border-radius);
            }
            
            .outline .field-label {
                transform: translate(var(--kode4-field-outline-label-translate-x), var(--kode4-field-outline-label-translate-y)) scale(var(--kode4-field-outline-label-scale));
            }

            @-moz-document url-prefix() {
                .outline.floatinglabel .field-legend {
                    /* Fix. Firefox creates an overflow without this */
                    display: none;
                }
            }

            .outline.floatinglabel .field-label {
                transform: translate(var(--kode4-field-outline-floatinglabel-label-translate-x), var(--kode4-field-outline-floatinglabel-label-translate-y));
            }
        `;
    }

    static getFilledStyles() {
        return css`
            .component:not(.filled) .field-input-container {
                background-color: transparent;
            }

            .filled .field-input-container {
                background-color: var(--kode4-field-filled-background);
            }
        `;
    }

}
