import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4Field from "./kode4-field";
import debounce from "lodash-es/debounce";

// const kode4FieldTypes = [
//     'kode4-field',
//     'kode4-textfield',
//     'kode4-textarea',
//     'kode4-check',
//     'kode4-select',
//     'kode4-datetime-picker',
//     'kode4-custom-select',
// ];

@customElement('kode4-form')
export default class Kode4Form extends LitElement {

    @property()
    action:String = '';

    @property()
    method:String = 'get';

    isValid:Boolean = false;

    @property({ type: Boolean })
    private formElementsInitialized:boolean = false;

    private formElements:Array<Kode4Field> = [];
    
    private fieldsDirty:FormData = new FormData();

    @property()
    public dirty:boolean = false;

    @property({ type: Number })
    public debounce:number = 150;

    // protected async updated(changedProperties:any) {
    //     super.updated(changedProperties);

    //     await this.initFormElements();
    // }

    // protected async initFormElements() {
    //     const slt = <HTMLSlotElement>this.shadowRoot?.querySelector('slot');

    //     this.formElements = [];

    //     if (slt?.assignedNodes()) {
    //         Object.values(slt?.assignedNodes()).forEach(el => {
    //             if (el instanceof Kode4Field && !(el instanceof Kode4Button)) {
    //                 this.formElements.push(el);
                    
    //             } else if (el instanceof HTMLElement) {
    //                 this.formElements = [
    //                     ...this.formElements,
    //                     ...<NodeListOf<Kode4Field>>el.querySelectorAll(kode4FieldTypes.join(','))
    //                 ];
    //             }
    //         });
    //     }

    //     for (let i=0; i<this.formElements.length; i+=1) {
    //         const el = this.formElements[i]
    //         await el.isInitialized;
    //         if (!el.classList.contains('kode4-form-initialized')) {
    //             el.classList.add('kode4-form-initialized');
    //         }
    //     }

    //     this.formElementsInitialized = true;

    //     await this.applyConditions();

    //     return true;
    // }

    connectedCallback() {
        this.addEventListener('kode4-field-destroyed', (e:Event) => {
            this.fieldDestroyed(<CustomEvent>e);
        });
        super.connectedCallback();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    protected checkDirty(e?:Event) {

        // filter all fields that are not in the list

        const dirtyBefore = this.dirty;

        for(let key of [...this.fieldsDirty.keys()]) {
            if (!this.formElements.find((el:Kode4Field) => el.name === key)) {
                this.fieldsDirty.delete(key);
            }
        }

        this.dirty = [...this.fieldsDirty.keys()].length > 0;
        this.dirty ? this.classList.add('kode4-dirty') : this.classList.remove('kode4-dirty');

        if (dirtyBefore !== this.dirty) {
            const detail:any = {
                dirty: this.dirty,
            }

            if (e) {
                detail.originalEvent = e;
            }

            let event = new CustomEvent("stateChanged", {
                detail: detail
            });
            
            this.dispatchEvent(event);
        }
    }

    protected async onChange(e:Event) {
        const { name, dirty } = (<CustomEvent>e).detail;

        if (dirty && !this.fieldsDirty.has(name)) {
            this.fieldsDirty.set(name, dirty);
        } else if (!dirty && this.fieldsDirty.has(name)) {
            this.fieldsDirty.delete(name);
        }

        this.checkDirty();

        await this.applyConditions();

        return;
    }

    protected async applyConditions() {
        for (let i=0; i<this.formElements.length; i+=1) {
            await this.formElements[i].applyConditions(this);
        }
        return;
    }

    public getField(name:string):Kode4Field|null {
        return this.formElements.reduce((result:Kode4Field|null, el:Kode4Field) => {
            if (el.name === name) {
                result = el;
            }
            return result;
        }, null);
    }

    // @property({ type: Boolean })
    // public dirty:boolean = false;

    // public get onDirtyChange():boolean {
    //     let dirty = false;
    //     this.formElements.forEach((el:Kode4Field) => {
    //         dirty = dirty || el.dirty;
    //     });

    //     return dirty;
    // }

    public validate() {
        let valid = true;

        this.formElements.forEach(el => {
            try {
                if (el.canValidate && !el.valid) {
                    valid = false;
                }
            } catch (e) {
                console.warn('unexpected error while validating', e);
            }
        });

        return valid;
    }

    get formData():FormData {
        const formData = new FormData();

        this.formElements.map(({name, stringValue, hidden, disabled, hasValue, manageValue}) => {
            if (manageValue && hasValue && name && !hidden && !disabled) {
                formData.append(<string>name, <string>stringValue);
            }
        });
        // this.formElements.map(({name, value, stringValue, hidden, disabled, hasValue, manageValue}) => {
        //     if (manageValue && hasValue && name && !hidden && !disabled) {
        //         formData.append(<string>name, <string>stringValue);
        //     }
        // });

        return formData;
    }

    protected onResetForm(e:Event) {
        e.preventDefault();
        this.reset();
    }

    protected onClearForm(e:Event) {
        e.preventDefault();
        this.clear();
    }

    // protected onUpdateFields(e:Event) {
    //     e.preventDefault();
    //     this.updateFields();
    // }

    protected onUpdateValues(e:Event) {
        e.preventDefault();
        this.updateValues();
    }

    protected onRespawnForm(e:Event) {
        e.preventDefault();
        this.respawn();
    }

    protected onSubmit(e:CustomEvent) {
        console.log('GO SUBMIT!');
        e.preventDefault();

        let event;

        const formData = this.formData;
        if (e.detail.name && e.detail.value) {
            formData.append(e.detail.name, e.detail.value);
        }

        if (this.validate()) {
            event = new CustomEvent("submit", {
                bubbles: true,
                detail: {
                    formData,
                    form: this,
                }
            });
            
            console.log('SUBMIT:');
            console.log([...event.detail.formData?.entries()]);

        } else {
            event = new CustomEvent("submit-error", {
                bubbles: true,
                detail: {
                    formData,
                    form: this,
                }
            });

            console.log('SUBMIT-ERROR:');
            console.log([...event.detail.formData?.entries()]);
        }

        this.dispatchEvent(event);
    }

    public reset() {
        this.formElements.forEach(el => {
            el.reset();
        });
    }

    public clear() {
        this.formElements.forEach(el => {
            el.clear();
        });
    }

    // public async updateFields() {
    //     await this.initFormElements();
    //     this.updateValues();
    // }

    public updateValues() {
        this.formElements.forEach(el => {
            el.updateValue();
        });
    }

    public respawn() {
        this.formElements.forEach(el => {
            el.respawn();
        });
    }

    private fieldsToAdd:Array<Kode4Field> = [];
    
    private fieldsToRemove:Array<string> = [];

    protected fieldCreated(e:CustomEvent) {
        if (!e.detail?.field?.name) {
            return;
        }

        this.fieldsToAdd.push(e.detail?.field);
        this.fieldsToRemove.filter((fieldid:string) => fieldid !== e.detail?.field?.id);

        this.onFieldsUpdatedDebounced();
    }

    protected fieldDestroyed(e:CustomEvent) {
        if (!e.detail?.fieldid) {
            return;
        }

        this.fieldsToRemove.push(e.detail?.fieldid);
        this.fieldsToAdd.filter((el:Kode4Field) => el.id !== e.detail?.fieldid);

        this.onFieldsUpdatedDebounced();
    }

    protected onFieldsUpdatedDebounced = debounce(this.onFieldsUpdated, this.debounce);

    protected onFieldsUpdated() {
        const formElements:Array<Kode4Field> = [
            ...this.formElements.filter((fe:Kode4Field) => !this.fieldsToRemove.find((fieldid:string) => fieldid === fe.id))        
        ];

        this.fieldsToAdd.forEach((el:Kode4Field) => {
            if (!formElements.find((fe:Kode4Field) => fe.id === el.id)) {
                formElements.push(el);
            }
        });
        
        this.fieldsToAdd = [];
        this.fieldsToRemove = [];

        this.formElements = [
            ...formElements,
        ]

        this.applyConditions();
        this.checkDirty();
        this.formElementsInitialized = true;
    }

    public render() {
        // @updateFields="${this.onUpdateFields}"
        return html`
        <form
            class="kode4-form ${this.formElementsInitialized ? 'initialized' : ''}"
            action="${this.action}"
            method="${this.method}"
            @submitForm="${this.onSubmit}"
            @change="${this.onChange}"
            @resetForm="${this.onResetForm}"
            @clearForm="${this.onClearForm}"
            @updateValues="${this.onUpdateValues}"
            @respawnForm="${this.onRespawnForm}"
            @kode4-field-created="${this.fieldCreated}"
            >
            <slot></slot>
        </form>
        `;

        // @managedValueChanged="${this.onManagedValueChanged}"
    }

    static get styles() {
        return css`
            form:not(.initialized) {
                visibility: hidden;
                opacity: 0;
            }

            form {
                transition: opacity 0.4s ease-in-out;
            }
        `;
    }

}
