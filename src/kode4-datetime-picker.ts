import { html, css, TemplateResult } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import Kode4Textfield from "./kode4-textfield";
import { faChevronRight} from '@fortawesome/free-solid-svg-icons/faChevronRight';
import { faChevronLeft} from '@fortawesome/free-solid-svg-icons/faChevronLeft';
import '@kode4/ui/kode4-fa-icon';

@customElement("kode4-datetime-picker")
export default class Kode4DatetimePicker extends Kode4Textfield {

    protected get typeCssClass():string {
        return `kode4-${this.type}`;
    }

    // all dates are handles as UTC timezone
    @property()
    protected _timezoneOffset:number = 0;
    // protected _timezoneOffset:number = Math.round(new Date().getTimezoneOffset() / 60);
    // protected timezoneOffset:any = Intl.DateTimeFormat().resolvedOptions().timeZone;
    
    @property({ type: String })
    protected get timezoneOffset():string {
        return `${this._timezoneOffset <= 0 ? '+' : '-'}${this.padZeros(Math.abs(this._timezoneOffset))}:00`;
    }

    @query('#calendarYear')
    protected yearSelector?:HTMLInputElement;

     @property({ type: String })
    protected format?:string;

    @property({ type: Boolean })
    protected nodialog:Boolean = false;

    @property({ type: Boolean })
    protected dialogonly:Boolean = false;

    @property({ type: Boolean })
    protected noyear:Boolean = false;

    @property({ type: Boolean })
    protected nomonth:Boolean = false;

    @property({ type: Boolean })
    protected noday:Boolean = false;

    @property({ type: Boolean })
    protected nohours:Boolean = false;

    @property({ type: Boolean })
    protected nominutes:Boolean = false;

    @property({ type: Boolean })
    protected noseconds:Boolean = false;

    @property({ type: Boolean })
    protected noweek:Boolean = false;

    @property({ type: Boolean })
    protected nozeropadding:Boolean = false;

    @property({ type: Boolean })
    protected timestamp:boolean = false;

    @property({ type: String })
    protected output?:'isodate'|'timestamp';

    @property()
    protected dateTimeValue:Date = new Date();

    @property()
    protected calendarDateTime = new Date();

    @property()
    public get value():String|Number|Boolean|undefined {
            // console.log('POSSIBLE DETECTED TIMEZONES: ', Math.round(new Date().getTimezoneOffset() / 60), Intl.DateTimeFormat().resolvedOptions().timeZone);
        // protected _timezoneOffset:number = Math.round(new Date().getTimezoneOffset() / 60);
        // protected timezoneOffset:any = Intl.DateTimeFormat().resolvedOptions().timeZone;


        const myTimezoneName = "Europe/Berlin";
    
        // Generating the formatted text
        let dateText = Intl.DateTimeFormat([], {timeZone: myTimezoneName, timeZoneName: "short"}).format(new Date());
        
        // Scraping the numbers we want from the text
        let timezoneString = dateText.split(" ")[1].slice(3);
        
        // Getting the offset
        var timezoneOffset = parseInt(timezoneString.split(':')[0])*60;
        
        // Checking for a minutes offset and adding if appropriate
        if (timezoneString.includes(":")) {
            var timezoneOffset = timezoneOffset + parseInt(timezoneString.split(':')[1]);
        }



        if (!this.getValueIsDefined(this._value) || !this.getValueIsDefined(this.dateTimeValue)) {
            return undefined;
        }
    
        if (this.output === 'isodate') {
            if (this.type === 'date') {
                // this.dateTimeValue.setHours(0);
                // this.dateTimeValue.setMinutes(0);
                // this.dateTimeValue.setSeconds(0);
                // console.log('ISO TIME: ', this.dateTimeValue.toISOString(), this.timezoneOffset);
                // return this.dateTimeValue.toISOString();
                return `${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth()+1)}-${this.padZeros(this.dateTimeValue.getDate())}T00:00:00${this.timezoneOffset}`;
            } else {
                // console.log('ISO TIME: ', this.dateTimeValue.toISOString(), this.timezoneOffset);
                // return this.dateTimeValue.toISOString();
                return `${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth()+1)}-${this.padZeros(this.dateTimeValue.getDate())}T${this.padZeros(this.dateTimeValue.getHours())}:${this.padZeros(this.dateTimeValue.getMinutes())}:${this.padZeros(this.dateTimeValue.getSeconds())}${this.timezoneOffset}`;
            }

        } else if (this.output === 'timestamp') {
            return String(Math.round(this.dateTimeValue.getTime() / 1000));
        }

        throw new Error('Date is neither isodate nor timestamp');
    }

    public set value(v:String|Number|Boolean|undefined) {
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }
        
        try {
            this.dateTimeValue = this.convertParameterToValue(String(v), this.timestamp);
            this._value = this.dateTimeValue.getTime();
            
        } catch(e) {
            this._value = undefined;
        }

        // try {
        //     switch (this.type) {
        //         case 'date':
        //             if (this.timestamp) {
        //                 const dt = new Date(Number(v) * 1000);
        //                 this.dateTimeValue = new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T00:00:00${this.timezoneOffset}`);

        //             } else {
        //                 const date:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}/);
        //                 if (!date) {
        //                     throw new Error('Type mismatch for date');
        //                 }
    
        //                 this.dateTimeValue = new Date(`${date[0]}T00:00:00${this.timezoneOffset}`);
        //             }
        //             this._value = this.dateTimeValue.getTime();
        //             break;
                    
        //         case 'datetime':
        //             if (this.timestamp) {
        //                 const dt = new Date(Number(v) * 1000);
        //                 this.dateTimeValue = new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T${this.padZeros(dt.getHours())}:${this.padZeros(dt.getMinutes())}:00${this.timezoneOffset}`);
        //             } else {
        //                 const datetime:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);
        //                 if (!datetime) {
        //                     throw new Error('Type mismatch for date');
        //                 }
                       
        //                 this.dateTimeValue = new Date(`${datetime[0]}:00${this.timezoneOffset}`);
        //             }
        //             this._value = this.dateTimeValue.getTime();
        //             break;

        //         default :
        //             throw new Error('Undefined type');
        //     }
            
        // } catch(e) {
        //     this._value = undefined;
        // }

        if (this.initialized) {
            this.dirty = this.getDirty();
        }
    }

    @property()
    private _min?:Date;

    @property()
    public get mind():Date|string|undefined {
        return this._min;
    }

    public set mind(v:Date|string|undefined) {
        if (v instanceof Date) {
            this._min = v;

        } else if (typeof v === 'string') {
            try {
                this._min = this.convertParameterToValue(v, Boolean(v.match(/^\d+$/)));

            } catch (e) {
                this._min = undefined;
            }

        } else {
            this._min = undefined;
        }

    }   

    private convertParameterToDateValue(v:string, timestamp:boolean = false):Date {
        if (timestamp) {
            const dt = new Date(Number(v) * 1000);
            return new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T00:00:00${this.timezoneOffset}`);

        } else {
            const date:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}/);
            if (!date) {
                throw new Error('Type mismatch for date');
            }

            return new Date(`${date[0]}T00:00:00${this.timezoneOffset}`);
        }
}

    private convertParameterToDateTimeValue(v:string, timestamp:boolean = false):Date {
        if (timestamp) {
            const dt = new Date(Number(v) * 1000);
            return new Date(`${dt.getFullYear()}-${this.padZeros(dt.getMonth()+1)}-${this.padZeros(dt.getDate())}T${this.padZeros(dt.getHours())}:${this.padZeros(dt.getMinutes())}:${this.padZeros(dt.getSeconds())}${this.timezoneOffset}`);
        } else {
            const datetime:RegExpMatchArray|null = v.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);
            if (!datetime) {
                throw new Error('Type mismatch for date');
            }
            
            return new Date(`${datetime[0]}:00${this.timezoneOffset}`);
        }
    }

    private convertParameterToValue(v:string, timestamp:boolean = false):Date {
        switch (this.type) {
            case 'date':
                return this.convertParameterToDateValue(v, timestamp);
                break;
                
            case 'datetime':
                return this.convertParameterToDateTimeValue(v, timestamp);
                break;

            default :
                throw new Error('Undefined type');
        }
            
    }

    /**
     * @return Boolean
     */
    protected validate() {
        if (!super.validate()) {
            return false;
        }

        this.error = '';

        if (this.mind instanceof Date && this.dateTimeValue instanceof Date && this.mind.getTime() > this.dateTimeValue.getTime()) {
            this.error = 'Bitte beachten Sie Mindestdatum';
            return false;
        }
        return true;
    }

    @property()
    protected get displayValue():TemplateResult|String|Number|Boolean|undefined {
        if (!this.getValueIsDefined(this._value) || !this.getValueIsDefined(this.dateTimeValue)) {
            return '';
        }

        let output = String(this.format);

        output = output.replace(/D/, this.day);
        output = output.replace(/M/, this.month);
        output = output.replace(/Y/, this.year);
        output = output.replace(/h/, this.hours);
        output = output.replace(/m/, this.minutes);
        output = output.replace(/s/, this.seconds);

        return output;
    }

    @property()
    public searchTerm:String|undefined = undefined;

    @property()
    public dialogMessagePrepend?:String

    @property()
    public dialogMessageAppend?:String

    @property()
    public dialogErrorMessage?:String




    // --- INSTANTIATION -----------------
    constructor() {
        super();
        this.type = 'date';
        this.dateTimeValue = new Date(); // which is now
        // this.dateTimeValue = new Date(`${dateNow.getFullYear()}-${this.padZeros(dateNow.getMonth()+1)}-${this.padZeros(dateNow.getDate())}T${this.padZeros(dateNow.getHours())}:${this.padZeros(dateNow.getMinutes())}:${this.padZeros(dateNow.getSeconds())}${this.timezoneOffset}`);
        
        this.action = true;
        this.hasDialog = true;
        this.openDialogOnFocus = true;

        this.changeOnType = true;
        document.addEventListener("click", this.onOutsideClick.bind(this));
    }

    protected async firstUpdated(changedProperties: Map<PropertyKey, unknown>) {
        super.firstUpdated(changedProperties);

        if (this.nodialog && this.dialogonly) {
            this.nodialog = false;
        }

        // if (this.openDialogOnFocus === undefined) {
        //     this.openDialogOnFocus = true;
        // }
    }

    // protected async updated(changedProperties: Map<PropertyKey, unknown>) {
    //     super.updated(changedProperties);

    //     if (changedProperties.has('_options') && this.type !== 'autocomplete' && !this.optionsProvider) {
    //         this.value = this.value;
    //     }
    // }

    protected async doInit(/*updateValueFromAttribute:boolean = false*/) {
        // if (!this.optionsProvider) {
        //     const op = new Kode4StaticOptionsProvider();
        //     op.list = this._optionsCandidate || [];
        //     this.optionsProvider = op;
        // }

        if (!this.output) {
            this.output = this.timestamp ? 'timestamp' : 'isodate';
        }

        if (!this.format) {
            switch (this.type) {
                case 'date' :
                    this.format = 'D.M.Y';
                    break;

                case 'datetime' :
                    this.format = 'D.M.Y h:m:s';
                    break;
            }
        }

        this.value = this._valueCandidate;
        if (this.getValueIsDefined(this._valueCandidate)) {
            this.initialValue = this._value;
            this.calendarDateTime = new Date(this.dateTimeValue.getTime());
            this.calendarDateTime.setDate(1);
            
        } else {
            this.initialValue = undefined;
        }

        // this.updateOptionsFromOptionsProvider();
        // if (updateValueFromAttribute) {
        //     this.updateValue();
        // } else {
        //     this.value = this._valueCandidate;
        // }
    }


    // --- LOGIC -----------------
    public reset() {
        if (this.getValueIsDefined(this.initialValue)) {
            this.dateTimeValue = new Date(Number(this.initialValue));
            this._value = this.dateTimeValue.getTime();
        } else {
            this.dateTimeValue = new Date()
            this._value = undefined;
        }
        this.dirty = this.getDirty();

        this.requestUpdate();
        this.onChange();
    }



    // --- UI MANIPULATON -----------------
    protected addFocusClass() {

    }

    protected removeFocusClass() {

    }


    // --- EVENTS -----------------
    protected onKeydown(e:KeyboardEvent) {
        switch (e.key) {
            case 'Tab' :
                if (this.dialogOpen) {
                    // this.selectHighlightedOption();
                    this.closeDialog();
                }
                return;

            default :
                if (this.type !== 'autocomplete') {
                    e.preventDefault();
                }
            }
    }

    protected onKeyup(e:KeyboardEvent) {
        switch (e.key) {
            case 'Escape' :
                if (this.dialogOpen) {
                    this.closeDialog();
                } else {
                    this.handleEsc();
                }
                return;

            case 'ArrowDown' :
                if (!this.dialogOpen) {
                    this.openDialog();
                } else {
                    // this.highlightNextOption();
                }
                return;

            // case 'ArrowUp' :
            //     if (!this.dialogOpen) {
            //         this.openDialog();
            //     } else {
            //         this.highlightPrevOption();
            //     }
            //     return;

            case 'ArrowRight' :
                if (this.dialogOpen) {
                    // this can be nice if typing is not allowed.
                    // but I think moving the cursor left an right
                    // while typing is more convenient than
                    // making a selection with ArrowRight.

                    // this.selectHighlightedOption();
                } else {
                    this.openDialog();
                }
                return;

            case 'ArrowLeft' :
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                return;

            case 'Enter' :
                if (this.dialogOpen) {
                    // this.selectHighlightedOption();
                    this.closeDialog();
                } else {
                    this.onSubmit(e);
                }
                return;

            case 'Tab' :
                // action handled in onKeydown
                return;

            default :
                if (this.type === 'autocomplete') {
                    if (!this.dialogOpen) {
                        this.openDialog();
                    }
                    // this.onSearchTermChangeDebounced(<String>this.input?.value);
                }
                e.preventDefault();
        }
    }

    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    protected onInputChange() {
        // do nothing
    }

    // protected onSearchTermChangeDebounced = debounce(this.onSearchTermChange, this.debounce);

    // protected onSearchTermChange(searchTerm:String) {
    //     if (this.searchTerm !== searchTerm) {
    //         this.searchTerm = searchTerm;

    //         // if (this.optionsProvider) {
    //         //     this.filters.set('searchterm', <string>searchTerm);
    //         //     this.updateOptionsFromOptionsProvider();
    //         // }
    //     }
    // }

    protected onAction() {
        if (this.readonly || this.disabled) {
            return;
        }
        if (this.dialogActionBlocked !== undefined) {
            this.dialogOpen = this.dialogActionBlocked;
        }
    }

    protected onOptionClick(e: Event) {
        e.preventDefault();
    //     const selectedOption = (<Element>e.target)?.closest(`[data-value]`);
    //     if (selectedOption === undefined) {
    //         return;
    }   

    //     if (!this.shadowRoot?.contains(selectedOption) && !this.contains(selectedOption)) {
    //         return;
    //     }

    //     this.value = (<String|Number>(<HTMLElement>selectedOption)?.dataset.value) || undefined;
    //     this.closeDialog();
    // }

    protected onOutsideClick(e: Event) {
        if (!(<Element>e.target)?.closest(`#${this.id}`)) {
            this.closeDialog();
        }
    }

    public static labels = {
        months: [
            'Januar',
            'Februar',
            'März',
            'April',
            'Mai',
            'Juni',
            'Juli',
            'August',
            'September',
            'Oktober',
            'November',
            'Dezember',
        ],
        days: [
            'Sonntag',
            'Montag',
            'Dienstag',
            'Mittwoch',
            'Donnerstag',
            'Freitag',
            'Samstag',
        ],
        daysShort: [
            'So',
            'Mo',
            'Di',
            'Mi',
            'Do',
            'Fr',
            'Sa',
        ]
    }

    @property()
    protected get year():string {
        return String(this.dateTimeValue?.getFullYear());
    }

    protected padZeros(val:number):string {
        if (this.nozeropadding) {
            return String(val);

        } else if (val < 10) {
            return `0${val}`;
        }

        return String(val);
    }

    @property()
    protected get month():string {
        return this.padZeros(this.dateTimeValue?.getMonth() + 1);
    }

    @property()
    protected get monthLabel():string {
        return Kode4DatetimePicker.labels.months[this.dateTimeValue?.getMonth()];
    }

    @property()
    protected get day():string {
        return this.padZeros(this.dateTimeValue?.getDate());
    }

    @property()
    protected get weekDay():string {
        if (this.dateTimeValue?.getDay() === 0) {
            return '7';
        }
        return String(this.dateTimeValue?.getDay());
    }

    @property()
    protected get weekDayLabel():string {
        return Kode4DatetimePicker.labels.days[this.dateTimeValue?.getDay()];
    }

    @property()
    protected get hours():string {
        return this.padZeros(this.dateTimeValue?.getHours());
    }

    @property()
    protected get minutes():string {
        return this.padZeros(this.dateTimeValue?.getMinutes());
    }

    @property()
    protected get seconds():string {
        return this.padZeros(this.dateTimeValue?.getSeconds());
    }


    protected renderInput() {
        return html`
            <input
                class="field-input"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="text"
                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${(e:MouseEvent) => e.stopPropagation()}"
                />
            <label
                for="field"
                class="field-input field-input-display ${!this.displayValue ? 'field-input-display-placeholder' : ''}"
                >${this.displayValue || this.placeholder || html`&nbsp;`}</label>
        `;
    }

    protected renderDialog() {
        if (!this.initialized) {
            return html``;
        }
        return html`
            ${this.dialogErrorMessage ? html`
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            `: ''}
            ${this.dialogMessagePrepend ? html`
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            `: ''}
            <div class="calendar">
                ${this.renderMonthPagination()}
                ${this.renderCalendarMonthContainer()}
            </div>
            <div class="time">
                ${this.type==='datetime' ? this.renderTimeEditor() : ''}
            </div>
            ${this.dialogMessageAppend ? html`
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            `: ''}
        `;
    }

    protected renderCalendarYearContainer() {
        return html`
            <div class="calendar-year-container">
                <input
                    id="calendarYear"
                    type="number"
                    step="1"
                    value="${this.calendarDateTime.getFullYear()}"
                    class="calendar-year-input"
                    @input="${(e:KeyboardEvent) => this.selectYear(Number((<HTMLInputElement>e.target)?.value))}"
                    tabindex="-1" 
                    />
            </div>
        `;
    }

    protected renderCalendarMonthContainer() {
        return html`
            <div class="calendar-month-container">
                ${this.renderCalendarWeekContainer()}
            </div>
        `;
    }
    
    protected renderMonthPagination() {
        return html`
            <div class="calendar-month-pagination">
                ${this.renderCalendarMonthPaginationPrev()}
                <div class="spacer"></div>
                <div class="calendar-month-container-monthlabel">
                    ${Kode4DatetimePicker.labels.months[this.calendarDateTime.getMonth()]}
                </div>
                ${this.renderCalendarYearContainer()}
                <div class="spacer"></div>
                ${this.renderCalendarMonthPaginationNext()}
            </div>
        `;
    }
    
    protected renderCalendarMonthPaginationPrev() {
        return html`
            <div class="calendar-month-container-monthPaginationButton" @click="${() => this.selectMonth(this.calendarDateTime.getMonth() - 1)}">
                <kode4-fa-icon
                    .icon="${faChevronLeft}"
                ></kode4-fa-icon>
            </div>
        `;
    }
    
    protected renderCalendarMonthPaginationNext() {
        return html`
            <div class="calendar-month-container-monthPaginationButton" @click="${() => this.selectMonth(this.calendarDateTime.getMonth() + 1)}">
                <kode4-fa-icon
                    .icon="${faChevronRight}"
                ></kode4-fa-icon>
            </div>
        `;
    }
    
    protected renderCalendarWeekContainer() {
        return html`
            <div class="calendar-week-container">
                ${this.renderCalendarWeekContent()}
            </div>
        `;
    }
    
    protected renderCalendarWeekContent() {

        const weeks:Array<Array<number>> = [];
        let currentWeek:Array<number> = [];

        const monthDate = new Date(this.calendarDateTime.getTime());
        monthDate.setDate(1);

        const currentMonth = monthDate.getMonth();

        const blankDays = monthDate.getDay() === 0 ? 6 : monthDate.getDay()-1;
        for (let i=0; i<blankDays; i+=1) {
            currentWeek.push(0);
        }

        let countX = 0;

        do {   
            countX += 1;
            let count = 0;
            do {
                count += 1;
                currentWeek.push(monthDate.getDate());
                monthDate.setDate(monthDate.getDate() + 1);
            } while (count < 50 && monthDate.getDay() !== 1 && monthDate.getMonth() === currentMonth)

            for (let i=currentWeek.length; i<7; i+=1) {
                currentWeek.push(0);
            }
        
            weeks.push(currentWeek);
            currentWeek = [];

        } while (countX < 50 && monthDate.getMonth() === currentMonth);

        return html `
            <table class="calendarTable">
                <thead>
                    <tr>
                        <th>${Kode4DatetimePicker.labels.daysShort[1]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[2]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[3]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[4]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[5]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[6]}</th>
                        <th>${Kode4DatetimePicker.labels.daysShort[0]}</th>
                    </tr>
                </thead>
                <tbody>
                    ${weeks.map((week:Array<number>) => this.renderCalendarWeekRow(week))}
                </tbody>
            </table>
        `;
    }
    
    protected renderCalendarWeekRow(week:Array<number>) {
        return html`
            <tr>
                ${week.map((day:number) => this.renderCalendarDayCell(day))}
            </tr>
        `;
    }

    protected renderCalendarDayCell(day:number) {
        if (day === 0) {
            return html `
                <td>
                </td>
            `;
        }
        return html`
            <td @click="${() => this.selectDay(day)}">
                ${day}
            </td>
        `;
    }

    protected selectYear(year:number) {
        const newDate = new Date(this.calendarDateTime.getTime());
        this.calendarDateTime.setFullYear(year);
        newDate.setFullYear(year);
        if (this.yearSelector && this.yearSelector?.value !== String(this.calendarDateTime.getFullYear())) {
            this.yearSelector.value = String(this.calendarDateTime.getFullYear());
        }
        this.requestUpdate();
    }

    protected selectMonth(month:number) {
        const newDate = new Date(this.calendarDateTime.getTime());
        this.calendarDateTime.setMonth(month);
        newDate.setMonth(month);
        if (this.yearSelector && this.yearSelector?.value !== String(this.calendarDateTime.getFullYear())) {
            this.yearSelector.value = String(this.calendarDateTime.getFullYear());
        }
        this.requestUpdate();
        // this.dateTimeValue = newDate;
    }

    protected selectDay(day:number) {
        const newDate = new Date(this.calendarDateTime.getTime());
        newDate.setDate(day);
        // if (!this.getValueIsDefined(this.dateTimeValue)) {
        //     this.dateTimeValue = new Date(this.calendarDateTime.getTime());
        //     console.log('SELECT DAY - undefined case dateTimeValue', this.name, this.dateTimeValue);
        // }
        this.dateTimeValue = newDate;
        this._value = this.dateTimeValue.getTime();
        if (this.initialized) {
            this.dirty = this.getDirty();
        }
        this.onChange();
        this.closeDialog();
    }

    protected renderTimeEditor() {
        return html`
            <div class="time-editor">
                Uhrzeit:
                <div class="time-editor-body">
                    <input
                        id="timeHours"
                        type="number"
                        min="0"
                        max="24"
                        step="1"
                        value="${this.calendarDateTime.getHours()}"
                        class="time-hours-input"
                        @input="${(e:KeyboardEvent) => this.selectHours(Number((<HTMLInputElement>e.target)?.value))}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeMinutes"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getMinutes()}"
                        class="time-minutes-input"
                        @input="${(e:KeyboardEvent) => this.selectMinutes(Number((<HTMLInputElement>e.target)?.value))}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeSeconds"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getSeconds()}"
                        class="time-seconds-input"
                        @input="${(e:KeyboardEvent) => this.selectSeconds(Number((<HTMLInputElement>e.target)?.value))}"
                        tabindex="-1" 
                        />
                    </div>
            </div>
        `;
    }

    protected selectHours(hours:number) {
        this.calendarDateTime.setHours(hours);
        this.dateTimeValue.setHours(hours);
        this.requestUpdate();
        this.onChange();
    }
    
    protected selectMinutes(minutes:number) {
        this.calendarDateTime.setMinutes(minutes);
        this.dateTimeValue.setMinutes(minutes);
        this.requestUpdate();
        this.onChange();
    }

    protected selectSeconds(minutes:number) {
        this.calendarDateTime.setSeconds(minutes);
        this.dateTimeValue.setSeconds(minutes);
        this.requestUpdate();
        this.onChange();
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            :host(.kode4-date) #field.field-input,
            :host(.kode4-datetime) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            .calendar {
                padding-top: var(--kode4-datetime-dialog-calendar-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            .calendar-month-pagination {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: var(--kode4-datetime-dialog-calendar-month-pagination-margin-top);
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-margin-right);
                margin-bottom: var(--kode4-datetime-dialog-calendar-month-pagination-margin-bottom);
                margin-left: var(--kode4-datetime-dialog-calendar-month-pagination-margin-left);
            }

            .calendar-month-pagination .spacer {
                flex: 1 0 1px;
            }

            .calendar-month-container-monthPaginationButton {
                width: var(--kode4-datetime-dialog-calendar-month-pagination-button-width);
                text-align: center;
                cursor: pointer;
            }

            .calendar-month-container-monthlabel {
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-label-spacer);
                vertical-align: middle;
            }

            .calendarTable {
                width: 100%;
                table-layout: fixed;
                border: none;
                border-collapse: collapse;
                border-spacing: 0;
            }

            .calendarTable th,
            .calendarTable td {
                text-align: center;
                padding-top: var(--kode4-datetime-dialog-calendar-tablecell-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-tablecell-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-tablecell-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-tablecell-padding-left);
                cursor: pointer;
            }

            .calendarTable th {
                font-weight: bold;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .calendar-year-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-calendar-year-input-width);
                color: var(--kode4-datetime-dialog-calendar-year-input-color);
                font-family: var(--kode4-datetime-dialog-calendar-year-input-font-family);
                font-size: var(--kode4-datetime-dialog-calendar-year-input-font-size);
                line-height: var(--kode4-datetime-dialog-calendar-year-input-line-height);
                font-weight: var(--kode4-datetime-dialog-calendar-year-input-font-weight);
                -moz-appearance: textfield;
            }

            .calendar-year-input::-webkit-outer-spin-button,
            .calendar-year-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }


            .time-editor {
                padding-top: var(--kode4-datetime-dialog-time-padding-top);
                padding-right: var(--kode4-datetime-dialog-time-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-time-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-time-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            
            .time-hours-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-hours-input-width);
                color: var(--kode4-datetime-dialog-time-hours-input-color);
                font-family: var(--kode4-datetime-dialog-time-hours-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-hours-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-hours-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-hours-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-hours-input-text-align);
                -moz-appearance: textfield;
            }

            .time-hours-input::-webkit-outer-spin-button,
            .time-hours-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            .time-minutes-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-minutes-input::-webkit-outer-spin-button,
            .time-minutes-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .time-seconds-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-seconds-input::-webkit-outer-spin-button,
            .time-seconds-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

        `;
    }
}
