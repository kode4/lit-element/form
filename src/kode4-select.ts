import { html, css, TemplateResult } from "lit";
import { customElement, property } from "lit/decorators.js";
import { unsafeHTML } from "lit/directives/unsafe-html.js";
import Kode4Textfield from "./kode4-textfield";
import { Kode4OptionsProviderSubscriber, Kode4Option, Kode4ListResponse, Kode4OptionsProvider, Kode4StaticOptionsProvider } from "./Kode4OptionsProvider";
import debounce from "lodash-es/debounce";

@customElement("kode4-select")
export default class Kode4Select extends Kode4Textfield implements Kode4OptionsProviderSubscriber {

    protected get typeCssClass():string {
        return `kode4-${this.type}`;
    }

    // @property({ type: Boolean })
    // protected allowCustomOption:Boolean = false;

    // @property({ type: Boolean })
    // protected cacheSelectedValue:Boolean = false;

    @property()
    protected valueObject?:any;

    @property()
    public get value():String|Number|Boolean|undefined {
        return super.value;
    }

    public set value(v:String|Number|Boolean|undefined) {
        if (!this.initialized && !this.initializing) {
            this._valueCandidate = v;
            return;
        }

        super.value = v;


        if (v === undefined) {
            if (this.type === 'autocomplete') {
                this.searchTerm = undefined;
            }

            this._selectedOption = undefined;
            this.onChange();

        } else {
            if (this._selectedOption?.value != v) {
                (async () => {
                    try {
                        if (!v || v == 'undefined' || v == 'null') {
                            throw new Error('Skip getItem on optionsProvider, value is falsy');
                        }

                        if (this.type === 'autocomplete') {
                            this.searchTerm = undefined;
                        }

                        let selectedOption = await this.optionsProvider?.getItem(<String>v);
                        this._selectedOption = selectedOption;
                        this.onChange();

                    } catch (e) {
                        super.value = undefined;
                        this._selectedOption = undefined;
                    }
                })();
            }
        }
    }

    @property()
    protected get displayValue():TemplateResult|String|Number|Boolean|undefined {
        if (this.type === 'autocomplete' && this.searchTerm !== undefined && this.searchTerm !== <String>this.selectedOption?.value) {
            return this.searchTerm;
        }
        
        if (!this.selectedOption) {
            return '';
        }
        
        return this.selectedOption.unsafe ? html`${unsafeHTML(String(this.selectedOption.label))}` : <String>this.selectedOption.label;
    }

    @property({ type: Array })
    protected _options: Array<Kode4Option> = [];

    @property({ type: Array })
    protected _optionsCandidate?: Array<Kode4Option>;

    @property()
    public get options():Array<Kode4Option> {
        return this._options;
    }
    
    public set options(options:Array<Kode4Option>) {
        if (!this.initialized && !this.initializing) {
            this._optionsCandidate = options;
            return;
        }

        this._options = options.map(el => {
            if (typeof el === 'object') {
                if (this.valueField) {
                    el.value = (<any>el)[this.valueField];
                }
                if (this.labelField) {
                    el.label = (<any>el)[this.labelField];
                }
            }
            return typeof el === 'object' ? {
                ...el,
                value: (<Kode4Option>el).value || undefined,
                label: (<Kode4Option>el).label || '',
                unsafe: (<Kode4Option>el).unsafe || false
            } : {
                value: el,
                label: el,
                unsafe: false
            };
        });
    }

    @property({ type: String })
    public valueField?:string;
    
    @property({ type: String })
    public labelField?:string;
    
    
    @property()
    public _optionsProvider: Kode4OptionsProvider | undefined;

    @property()
    public get optionsProvider(): Kode4OptionsProvider | undefined {
        return this._optionsProvider;
    }

    public set optionsProvider(optionsProvider:Kode4OptionsProvider | undefined) {
        if (this._optionsProvider) {
            this._optionsProvider.unsubscribe(this);
        }
        this._optionsProvider = optionsProvider;
        this.initOptionsProvider();
    }

    @property()
    protected _selectedOption:Kode4Option | undefined;

    @property()
    protected get selectedOption():Kode4Option|undefined {
        if (this._selectedOption) {
            return this._selectedOption;
        }
        return undefined;
    }

    protected set selectedOption(selectedOption:Kode4Option|undefined) {
        this._selectedOption = selectedOption;
        this.value = selectedOption?.value || undefined;
    }

    protected getOptionByValue(v:String|Number|Boolean|undefined):Kode4Option|undefined {
        if (!v) {
            return undefined;
        }
        return this.options.reduce((result:Kode4Option|undefined, el:Kode4Option) => {
            return el?.value == v ? el : result;
        }, undefined);
    }

    @property()
    public optionHighlighted:number = -1;

    @property({ type: Boolean })
    public get busy():boolean {
        if (super.busy) {
            return true;
        }
        if (!this.optionsProvider) {
            return false;
        }
        return this.optionsProvider.busy;
    }

    @property()
    public searchTerm:String|undefined = undefined;

    @property()
    public filters:FormData = new FormData();

    @property()
    public dialogMessagePrepend?:String

    @property()
    public dialogMessageAppend?:String

    @property()
    public dialogErrorMessage?:String

    @property({ type: Boolean, attribute: 'force-selection' })
    public forceSelection:Boolean = false;




    // --- INSTANTIATION -----------------
    constructor() {
        super();
        this.type = 'select';
        this.action = true;
        this.hasDialog = true;
        this.openDialogOnFocus = this.type === 'select';
        this.changeOnType = true;
        document.addEventListener("click", this.onOutsideClick.bind(this));
    }

    protected async firstUpdated(changedProperties: Map<PropertyKey, unknown>) {
        super.firstUpdated(changedProperties);
        if (this.openDialogOnFocus === undefined) {
            this.openDialogOnFocus = true;
        }
    }

    // protected async updated(changedProperties: Map<PropertyKey, unknown>) {
    //     super.updated(changedProperties);

    //     if (changedProperties.has('_options') && this.type !== 'autocomplete' && !this.optionsProvider) {
    //         this.value = this.value;
    //     }
    // }

    protected async doInit(updateValueFromAttribute:boolean = false) {
        this.initOptionsProvider();
        // if (!this.optionsProvider) {
        //     const op = new Kode4StaticOptionsProvider();
        //     op.list = this._optionsCandidate || [];
        //     this.optionsProvider = op;            
        // }

        // this.optionsProvider.subscribe(this);

        // this.updateOptionsFromOptionsProvider();
        if (updateValueFromAttribute) {
            this.updateValue();
        } else {
            this.value = this._valueCandidate;
        }
        this.initialValue = this.value;
        if (this.outline) {
            this.busyStyle = 'icon';
        }
    }

    protected async initOptionsProvider() {
        if (!this.optionsProvider) {
            const op = new Kode4StaticOptionsProvider();
            op.list = this._optionsCandidate || [];
            this.optionsProvider = op;            
        }

        this.optionsProvider.subscribe(this);
        
        this.updateOptionsFromOptionsProvider();
    }


    public handleListUpdated() {
        this.updateOptionsFromOptionsProvider();
    }

    public async updateOptionsFromOptionsProvider() {
        try {
            this.handleOptionsProviderListResponse(await this.optionsProvider?.getList(this.filters));
        } catch (e) {
            this.handleOptionsProviderListResponseError(<Error>e);
        }
    }

    protected async handleOptionsProviderListResponse(lr:Kode4ListResponse) {
        try {
            if (lr.error !== 0) {
                throw new Error('Errorcode');
            }
            this.options = <Array<Kode4Option>>lr.list;
            this.dialogErrorMessage = undefined;

            if (!this.value) {
                this.updateValue();
            }
            const changedSelectedOption = await this.optionsProvider?.checkItemChangedAfterListUpdate(String(this.value), <Kode4Option>this._selectedOption);
            if (changedSelectedOption !== false) {
                this._selectedOption = <Kode4Option>changedSelectedOption;
            }

        } catch (e) {
            this.handleOptionsProviderListResponseError(<Error>e, lr);
        }
    }

    protected handleOptionsProviderListResponseError(e:Error, lr?:Kode4ListResponse) {
        this.options = [];
        this.dialogErrorMessage = lr?.errorMessage || 'Beim Laden ist ein Fehler aufgetreten';
        throw e;
    }




    // --- UI MANIPULATON -----------------
    protected addFocusClass() {

    }

    protected removeFocusClass() {

    }

    protected handleOnFocus() {
    }

    protected handleOnBlur() {
        this.optionHighlighted = -1;
        this.searchTerm = undefined;

        // console.log('SELECT BLUR', this.value, this.selectedOption?.value, this.searchTerm, this.input?.value, this.displayValue);
        // if (this.value !== this.selectedOption?.value) {
        //     console.log('Value not synced :(');
        //     this.searchTerm = undefined;
        //     // this.value = this.resetValue;
        // } else {
        //     console.log('Value synced :)');
        // }

        // this.requestUpdate();
    }




    protected highlightPrevOption() {
        if (!this.options) {
            this.optionHighlighted = -1;
            return;
        }
        if (this.optionHighlighted === -1) {
            this.optionHighlighted = this.options.length-1;
            return;
        }
        this.optionHighlighted -= 1;
        if (this.optionHighlighted < 0) {
            this.optionHighlighted = 0;
        }
    }

    protected highlightNextOption() {
        if (!this.options) {
            this.optionHighlighted = -1;
            return;
        }
        if (this.optionHighlighted === -1) {
            this.optionHighlighted = 0;
            return;
        }
        this.optionHighlighted += 1;
        if (this.optionHighlighted > this.options.length-1) {
            this.optionHighlighted = this.options.length-1;
        }
    }

    protected selectHighlightedOption() {
        if (this.optionHighlighted !== -1) {
            this.value = this.options[this.optionHighlighted]?.value;
            this.searchTerm = undefined;

        } else if (this.searchTerm?.length && this.options.length) {
            this.value = this.options[0]?.value;
            this.searchTerm = undefined;

        } else if (this.forceSelection && this.options.length && !this.value) {
            this.value = this.options[0]?.value;
            this.searchTerm = undefined;

        } else if (!this.options.length) {
            this.value = undefined;
            this.searchTerm = undefined;
        }

        // this.selectOption(this.options[this.optionHighlighted]);
        // this.selectedOption = this.options[this.optionHighlighted];
    }



    // --- EVENTS -----------------
    protected onChange(originalEvent?:Event) {
        this.touched = true;

        let event = new CustomEvent("change", {
            bubbles: true,
            composed: true,
            detail: {
                disabled: this.disabled,
                manageValue: this.manageValue,
                value: this.value,
                valueObject: this._selectedOption,
                name: this.name,
                originalEvent: originalEvent,
                dirty: this.dirty,
            }
        });

        if (originalEvent) {
            originalEvent.preventDefault();
        }

        this.dispatchEvent(event);
    }

    protected onKeydown(e:KeyboardEvent) {
        switch (e.key) {
            case 'Tab' :
                if (this.dialogOpen) {
                    this.selectHighlightedOption();
                    this.closeDialog();
                }
                return;

            default :
                if (this.type !== 'autocomplete') {
                    e.preventDefault();
                }
            }
    }

    protected onKeyup(e:KeyboardEvent) {
        switch (e.key) {
            case 'Escape' :
                if (this.dialogOpen) {
                    this.closeDialog();
                } else {
                    this.handleEsc();
                }
                return;

            case 'ArrowDown' :
                if (!this.dialogOpen) {
                    this.openDialog();
                } else {
                    this.highlightNextOption();
                }
                return;

            case 'ArrowUp' :
                if (!this.dialogOpen) {
                    this.openDialog();
                } else {
                    this.highlightPrevOption();
                }
                return;

            case 'ArrowRight' :
                if (this.dialogOpen) {
                    // this can be nice if typing is not allowed.
                    // but I think moving the cursor left an right
                    // while typing is more convenient than
                    // making a selection with ArrowRight.

                    // this.selectHighlightedOption();
                } else {
                    this.openDialog();
                }
                return;

            case 'ArrowLeft' :
                if (this.dialogOpen) {
                    this.closeDialog();
                }
                return;

            case 'Enter' :
                if (this.dialogOpen) {
                    this.selectHighlightedOption();
                    this.closeDialog();
                } else {
                    this.onSubmit(e);
                }
                return;

            case 'Tab' :
                // action handled in onKeydown
                return;

            default :
                if (this.type === 'autocomplete') {
                    if (!this.dialogOpen) {
                        this.openDialog();
                    }
                    this.onSearchTermChangeDebounced(<String>this.input?.value);
                }
                e.preventDefault();
        }
    }

    /**
     * handle onInputChange
     *
     * Do nothing here. Keybard events are handled in the
     * onKeyup event handler.
     */
    protected onInputChange() {
        // do nothing
    }

    protected onSearchTermChangeDebounced = debounce(this.onSearchTermChange, this.debounce);

    protected onSearchTermChange(searchTerm:String) {
        if (!this.isFocused) {
            this.searchTerm = undefined;
        
        } else if (this.searchTerm !== searchTerm) {
            this.searchTerm = searchTerm;

            if (this.optionsProvider) {
                this.filters.set('searchterm', <string>searchTerm);
                this.updateOptionsFromOptionsProvider();
            }
        }
    }

    protected onAction() {
        if (this.dialogActionBlocked !== undefined) {
            this.dialogOpen = this.dialogActionBlocked;
        }
    }

    protected onOptionClick(e: Event) {
        e.preventDefault();
        const selectedOption = (<Element>e.target)?.closest(`[data-value]`);
        if (selectedOption === undefined) {
            return;
        }

        if (!this.shadowRoot?.contains(selectedOption) && !this.contains(selectedOption)) {
            return;
        }

        this.value = (<String|Number>(<HTMLElement>selectedOption)?.dataset.value) || undefined;
        this.searchTerm = undefined;
        this.closeDialog();
    }

    protected onOutsideClick(e: Event) {
        if (!(<Element>e.target)?.closest(`#${this.id}`)) {
            this.closeDialog();
        }
    }

    protected renderInput() {
        return html`
            ${super.renderInput()}
            <label
                for="field"
                class="field-input field-input-display ${!this.displayValue ? 'field-input-display-placeholder' : ''}"
                >${this.displayValue || this.placeholder || html`&nbsp;`}</label>
        `;
    }

    protected renderDialog() {
        return html`
            ${this.dialogErrorMessage ? html`
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            `: ''}
            ${this.dialogMessagePrepend ? html`
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            `: ''}
            <div class="list" @click="${this.onOptionClick}">
                ${this.options.map(opt => this.renderOption(opt))}
            </div>
            ${this.dialogMessageAppend ? html`
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            `: ''}
        `;
    }

    protected renderOption(opt: Kode4Option) {
        return html`
            <div class="kode4-select-option ${this.optionHighlighted !== -1 && opt === this.options[this.optionHighlighted] ? 'kode4-select-option-highlight' : ''}" data-value="${opt.value}">
                ${opt.unsafe ? html`${unsafeHTML(String(opt.label))}` : opt.label}
            </div>
        `;
    }

    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            :host(.kode4-select) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            :host(.kode4-autocomplete) .field-input.field-input-display {
                display: none !important;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .kode4-select-option {
                display: block;
                padding-top: var(--kode4-select-option-padding-top);
                padding-right: var(--kode4-select-option-padding-right);
                padding-bottom: var(--kode4-select-option-padding-bottom);
                padding-left: var(--kode4-select-option-padding-left);
                margin-top: var(--kode4-select-option-margin-top);
                margin-right: var(--kode4-select-option-margin-right);
                margin-bottom: var(--kode4-select-option-margin-bottom);
                margin-left: var(--kode4-select-option-margin-left);
                cursor: pointer;

                color: var(--kode4-select-option-color);
                font-family: var(--kode4-select-option-font-family);
                font-size: var(--kode4-select-option-font-size);
                line-height: var(--kode4-select-option-line-height);
                font-weight: var(--kode4-select-option-font-weight);
                opacity: var(--kode4-select-option-opacity);
                background-color: var(--kode4-select-option-background-color);
            }

            .kode4-select-option:first-child {
                border-top-left-radius: var(--kode4-field-dialog-border-radius);
                border-top-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option:last-child {
                border-bottom-left-radius: var(--kode4-field-dialog-border-radius);
                border-bottom-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option.kode4-select-option-highlight,
            .kode4-select-option:hover {
                color: var(--kode4-select-option-selected-color);
                font-weight: var(--kode4-select-option-selected-font-weight);
                opacity: var(--kode4-select-option-selected-opacity);
                background-color: var(--kode4-select-option-selected-background-color);
            }

            .kode4-field-dialog {
                max-height: var(--kode4-select-dialog-max-height);
                overflow: auto;
            }
        `;
    }
}
