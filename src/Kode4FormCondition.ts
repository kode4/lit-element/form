import Kode4Field from "./kode4-field";
import Kode4Form from "./kode4-form";

export interface Kode4FormConditionExpression {
    conditions: Kode4FormCustomCondition|Array<Kode4FormCustomCondition>;
    handleThen?: Kode4FormCustomConditionThenHandler;
    handleElse?: Kode4FormCustomConditionElseHandler;
}

// das müsste eigentlch ein Promise sein. Eigentlich ein Array<Promise>.
export type Kode4FormCustomCondition = (form:Kode4Form, field:Kode4Field) => boolean;

export type Kode4FormCustomConditionThenHandler = (form:Kode4Form, field:Kode4Field) => void;

export type Kode4FormCustomConditionElseHandler = (form:Kode4Form, field:Kode4Field) => void;

export const kode4CustomConditionFieldEquals = (fieldname:string, value:any) => (form:Kode4Form/*, field:Kode4Field*/) => {
    const fld:Kode4Field|null = form.getField(fieldname);
    return Boolean(fld?.value == value);
}

export const kode4CustomConditionFieldEqualsNot = (fieldname:string, value:any) => (form:Kode4Form/*, field:Kode4Field*/) => {
    const fld:Kode4Field|null = form.getField(fieldname);
    return Boolean(fld?.value != value);
}

/* _form is prefixed with an underscore for typescript */
export const kode4FormCustomConditionHandlerActivate = (options:Array<string> = ['hidden', 'disabled']) => (_form:Kode4Form, field:Kode4Field) => {
    if (options.includes('hidden')) {
        field.hidden = false;
    }
    if (options.includes('disabled')) {
        field.disabled = false;
    }
}

/* _form is prefixed with an underscore for typescript */
export const kode4FormCustomConditionHandlerDeactivate = (options:Array<string> = ['hidden', 'disabled']) => (_form:Kode4Form, field:Kode4Field) => {
    if (options.includes('hidden')) {
        field.hidden = true;
    }
    if (options.includes('disabled')) {
        field.disabled = true;
    }
}

export const showIfValue = (fieldname:string, value:any):Kode4FormConditionExpression => {
    return {
        conditions: kode4CustomConditionFieldEquals(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerActivate(),
        handleElse: kode4FormCustomConditionHandlerDeactivate(),
    }
}

export const hideIfValue = (fieldname:string, value:any):Kode4FormConditionExpression => {
    return {
        conditions: kode4CustomConditionFieldEquals(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerDeactivate(),
        handleElse: kode4FormCustomConditionHandlerActivate(),
    }
}

export const showIfNotValue = (fieldname:string, value:any):Kode4FormConditionExpression => {
    return {
        conditions: kode4CustomConditionFieldEqualsNot(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerActivate(),
        handleElse: kode4FormCustomConditionHandlerDeactivate(),
    }
}

export const hideIfNotValue = (fieldname:string, value:any):Kode4FormConditionExpression => {
    return {
        conditions: kode4CustomConditionFieldEqualsNot(fieldname, value),
        handleThen: kode4FormCustomConditionHandlerDeactivate(),
        handleElse: kode4FormCustomConditionHandlerActivate(),
    }
}