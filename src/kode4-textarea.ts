import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4Textfield from "./kode4-textfield";

@customElement('kode4-textarea')
export default class Kode4Textarea extends Kode4Textfield {

    protected get typeCssClass():string {
        return 'kode4-textarea';
    }

    protected get inputCssSelector():string {
        return 'textarea';
    };

    @property()
    public resize:String = 'vertical';

    constructor() {
        super();

        this.confirmOnEnter = false;
    }



    // --- RENDER -----------------
    protected renderInput() {
        return html`
            <textarea
                class="field-input ${this.resize}"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${(e:MouseEvent) => e.stopPropagation()}"
                >${this.displayValue}</textarea>
        `;
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            textarea.field-input {
                min-height: 3em;
                height: 3em;
            }

            textarea.field-input.both {
                resize: both;
            }

            textarea.field-input.vertical {
                resize: vertical;
            }

            textarea.field-input.horizontal {
                resize: horizontal;
            }

            textarea.field-input.none {
                resize: none;
            }

        `;
    }

}
