import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import { Kode4RestOptionsProvider } from '../../../src';
import Kode4Form from "../../kode4-form";
import Kode4Field from "../../kode4-field";
import { showIfValue, showIfNotValue, Kode4FormConditionExpression, Kode4FormCustomCondition } from '../../Kode4FormCondition';

const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-dynamic-forms')
export default class Kode4FormDemoDynamicForms extends LitElement {

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: Boolean })
    private showFields:boolean = true;

    protected condition123:Kode4FormCustomCondition = (form:Kode4Form, _field:Kode4Field) => {
        const fld:Kode4Field|null = form.getField('text1');
        console.log('== 123?', fld?.value);
        return Boolean(fld?.value == '123');
    }

    protected conditionNot123:Kode4FormCustomCondition = (form:Kode4Form, _field:Kode4Field) => {
        const fld:Kode4Field|null = form.getField('text1');
        console.log('!= 123?', fld?.value);
        return Boolean(fld?.value != '123');
    }

    protected showIf123:Kode4FormConditionExpression = {
        conditions: this.condition123,
        handleThen(_form:Kode4Form, field:Kode4Field) {
            console.log('showIf123 then')
            field.hidden = false;
            field.disabled = false;
        },
        handleElse(_form:Kode4Form, field:Kode4Field) {
            console.log('showIf123 else')
            field.hidden = true;
            field.disabled = true;
        }
    };
    
    protected showIfNot123:Kode4FormConditionExpression = {
        conditions: this.conditionNot123,
        handleThen(_form:Kode4Form, field:Kode4Field) {
            console.log('showIfNot123 then')
            field.hidden = false;
            field.disabled = false;
        },
        handleElse(_form:Kode4Form, field:Kode4Field) {
            console.log('showIfNot123 else')
            field.hidden = true;
            field.disabled = true;
        }
    };

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }
    render() {
        return html`
            <kode4-form action="/" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>

                        ${this.showFields ? html`
                            <div>
                            <div>
                            <kode4-textfield
                                name="text0"
                                label="Text 0"
                                hint="This field is included in submit even if it is empty"

                                submitEmpty

                                floatingLabel
                                outline
                                block
                                changeOnType

                                ></kode4-textfield>

                            <kode4-textfield
                                name="text1"
                                label="Text 1"
                                hint="try '123'"

                                floatingLabel
                                outline
                                block
                                changeOnType

                                ></kode4-textfield>

                            <kode4-formblock
                                block
                                .conditions="${showIfValue('text1', '123')}"
                                >
                                <div style="width: 100%; border: dotted 2px #f00; padding: 10px; margin: 10px;">
                                    Text 1 is now 123 !!!!
                                </div>
                            </kode4-formblock>
                                
                            <kode4-textfield
                                name="text2"
                                label="Text 2"
                                hint="Visible if Text 1 is 123"
                                .conditions="${showIfValue('text1', '123')}"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>
                                
                            <kode4-textfield
                                name="text3"
                                label="Text 3"
                                hint="Visible if Text 1 is not 123"
                                .conditions="${showIfNotValue('text1', '123')}"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>
                                
                            <kode4-textfield
                                name="text4"
                                label="Text 4"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <hr>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 1"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 2"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 3"
                                hint="This Field is required"

                                floatingLabel
                                outline
                                block

                                .conditions="${showIfValue('text1', '123')}"
                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 4"

                                floatingLabel
                                outline
                                block

                                .conditions="${showIfValue('text1', '123')}"

                                ></kode4-textfield>
                            </div>
                        </div>

                        `: ''}
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>

                        ${this.showFields ? html`
                            <kode4-button
                                filled 
                                danger 
                                flat
                                block 
                                @click="${() => this.showFields = false}">
                            
                                 Hide all fields
                            </kode4-button>
                        `: ''}
                        ${!this.showFields ? html`
                            <kode4-button
                            filled 
                            success 
                            block 
                            flat
                            @click="${() => this.showFields = true}">Show all fields</kode4-button>
                        `: ''}
                        
                        <br><br>
                        <div class="kode4-button-group">
                            <kode4-button
                                filled 
                                secondary
                                manageValue
                                name="visibleSelected"
                                value="visible"
                                ?disabled="${!this.showFields}"
                                @click="${(e:Event) => { console.log('You should see this only if the button is not disabled', e); }}">
                                Fields are VISIBLE ${!this.showFields ? '(DISABLED)' : ''}
                            </kode4-button>
                            <kode4-button
                                filled 
                                secondary 
                                manageValue
                                name="invisibleSelected"
                                value="invisible"
                                ?disabled="${this.showFields}"
                                @click="${(e:Event) => { console.log('You should see this only if the button is not disabled', e); }}">
                                Fields are INVISIBLE ${this.showFields ? '(DISABLED)' : ''}
                            </kode4-button>
                        </div>

                    </div>
                </div>

                ${false ? html`
                    <kode4-button type="submit" filled primary>Submit</kode4-button>
                    <kode4-button type="submit" name="submitButton" value="submitButton-value" filled primary>Submit (width value)</kode4-button>
                    <kode4-button type="reset" filled warning>Reset</kode4-button>
                    <kode4-button type="clear" filled danger>Clear</kode4-button>
                    <kode4-button type="updateFields" filled secondary>Update Fields</kode4-button>
                    <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                    <kode4-button type="respawn" filled success>Respawn</kode4-button>
                `: ''}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
