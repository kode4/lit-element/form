import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faCalendar } from '@fortawesome/free-solid-svg-icons/faCalendar';
import { Kode4RestOptionsProvider } from '../../../src';
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-datetimepickers')
export default class Kode4FormDemoDateTimePickers extends LitElement {

    // @query('kode4-form')
    // private k4form:Kode4Form|null = null;

    @property({ type: String })
    private format1:string = 'dd.mm.yyyy';

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: String })
    private value1:string = '';

    @property({ type: String })
    private value2?:string;

    @property({ type: String })
    private geburtsdatum:string = '1980-03-27';

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Date-Time-Pickers</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <!-- <kode4-datetime-picker
                            name="date1"

                            label="Date"
                            hint="Format is: ${this.format1}"

                            value="2018-07-02T19:29:00+0200"
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime1"

                            label="Date and Time"
                            hint="Format is: ${this.format1}"

                            value="2018-09-02T19:29:00+0000"
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            name="date2"

                            label="Date from Timestamp"

                            value="1535909340"
                            timestamp
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime2"

                            label="Date and Time from Timestamp"

                            value="1590135676813"
                            timestamp
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime3"

                            label="Date and Time from Datestring"

                            value="2018-09-02T19:29:00+0000"
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-datetime-picker
                            name="dateMin1"

                            label="Date"
                            hint="Format is: ${this.format1}"

                            value="2018-07-02T19:29:00+0200"
                            mind="${Math.round((new Date()).getTime() / 1000)+(60 * 60 * 24 * 14)}"
                            minX="2020-07-02T19:29:00+0200"
                            
                            .iconPrependOutside="${faCalendar}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker> -->


                            <br>
                            Effects to: ${this.value1}
                            <br>
                            <kode4-datetime-picker
                                type="datetime"
                                name="datetime1"

                                label="Date and Time from Datestring"

                                value="2018-09-02T19:29:00+0200"
                                @change="${(e:CustomEvent) => this.value1 = e.detail.value}"
                                
                                .iconPrependOutside="${faCalendar}"

                                filled
                                clearable

                                required
                                ></kode4-datetime-picker>

                            <kode4-datetime-picker
                                type="datetime"
                                name="datetime2"

                                label="Date and Time from Datestring"

                                value="${this.value2}"
                                @change="${(e:CustomEvent) => this.value2 = e.detail.value}"
                                
                                .iconPrependOutside="${faCalendar}"

                                filled
                                clearable

                                required
                                ></kode4-datetime-picker>
                        
                            <div>

                            </div>
                                <kode4-datetime-picker
                                        type="date"
                                        name="geburtsdatum"
                                        label="Geburtsdatum"
                                        type="date"
                                        value="${this.geburtsdatum}"
                                        block
                                        outline
                                        floatingLabel
                                        ></kode4-datetime-picker>
                                
                                <kode4-datetime-picker
                                        type="date"
                                        name="geburtsdatum2"
                                        label="Geburtsdatum"
                                        hint="${this.geburtsdatum ? 'Falls Sie Ihr Geburtsdatum falsch eingegeben haben, kontaktieren Sie uns bitte um es zu ändern.' : ''}"
                                        type="date"
                                        ?readonly="${this.geburtsdatum}"
                                        value="${this.geburtsdatum}"
                                        block
                                        outline
                                        floatingLabel
                                        ></kode4-datetime-picker>
                                

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>

                </div>

                <div class="kode4demo-spacer-v"></div>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="kode4demo-spacer-v"></div>

                <div class="form-dirty">Dirty</div>
                <div class="form-clean">Clean</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty ? html`<div class="form-dirty">Dirty</div>` : html`<div class="form-clean">Clean</div>`}
        `;
    }

}
