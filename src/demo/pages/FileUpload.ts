import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { ifDefined } from 'lit/directives/if-defined.js';
import { faLock } from '@fortawesome/free-solid-svg-icons/faLock';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';
import { faFileUpload } from '@fortawesome/free-solid-svg-icons/faFileUpload';

import { Kode4RestOptionsProvider } from '../../../src';

import { Kode4FileUploaderSimulator } from "../../Kode4FileUploader";

const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

const fileUploaderSimulator = new Kode4FileUploaderSimulator();
const fileUploaderSimulator1 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator2 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator3 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator4 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator5 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator6 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator7 = new Kode4FileUploaderSimulator();
const fileUploaderSimulator8 = new Kode4FileUploaderSimulator();

const demoImage1 = '/demo/assets/fat_cat.jpg';

@customElement('kode4form-demo-fileupload')
export default class Kode4FormDemoFileUpload extends LitElement {

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: String })
    private aspectRatio:string = '16:9';

    @property({ type: String })
    private uploadLabel?:string = 'Upload an Image';

    @property({ type: String })
    private uploadPlaceholder?:string = 'Drag and drop an image here or click to select';

    @property({ type: Boolean })
    private showFilename:boolean = true;

    @property({ type: String })
    private upload1?:string = demoImage1;

    @property({ type: String })
    private upload2?:string;

    @property({ type: String })
    private upload3?:string;

    @property({ type: String })
    private upload4?:string;

    @property({ type: String })
    private upload5?:string;

    @property({ type: String })
    private upload6?:string;

    @property({ type: String })
    private upload7?:string;

    @property({ type: String })
    private upload8?:string;

    @property({ type: String })
    private strg1?:string = 'test';

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }
    render() {
        return html`
            <kode4-form action="/" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>File-Upload</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>

                        <kode4-filefield
                            name="upload"
                            label="${ifDefined(this.uploadLabel)}"
                            placeholder="${ifDefined(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            .fileUploader="${fileUploaderSimulator}"

                            @change="${(e:Event) => {
                                console.warn('...CHANGE', e);
                            }}"
                            @changed="${(e:Event) => {
                                console.warn('...CHANGED', e);
                            }}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${faUser}"
                            .iconPrepend="${faKey}"
                            .iconAppend="${faSearch}"
                            .iconAppendOutside="${faLock}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${(e:CustomEvent) => { (<Event>e.detail.originalEvent).preventDefault(); console.log('click-icon-prepend-outside')}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-filefield>

                        <kode4-filefield
                            name="upload"
                            label="${ifDefined(this.uploadLabel)}"
                            placeholder="${ifDefined(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            value="${this.upload1}"
                            .fileUploader="${fileUploaderSimulator}"

                            @change="${(e:Event) => {
                                console.warn('...CHANGE', e);
                            }}"
                            @changed="${(e:Event) => {
                                console.warn('...CHANGED', e);
                            }}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${faUser}"
                            .iconPrepend="${faKey}"
                            .iconAppend="${faSearch}"
                            .iconAppendOutside="${faLock}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${(e:CustomEvent) => { (<Event>e.detail.originalEvent).preventDefault(); console.log('click-icon-prepend-outside')}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-filefield>

                        <kode4-imagefield
                            name="upload"
                            label="${ifDefined(this.uploadLabel)}"
                            placeholder="${ifDefined(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            value="${this.upload1}"
                            .fileUploader="${fileUploaderSimulator}"

                            @change="${(e:Event) => {
                                console.warn('...CHANGE', e);
                            }}"
                            @changed="${(e:Event) => {
                                console.warn('...CHANGED', e);
                            }}"
                            @aspect-ratio-changed="${(e:CustomEvent) => {
                                console.warn('ASPECT RATIO CHANGED', e.detail.element!.getAttribute('aspect-ratio'), e.detail.aspectRatio);
                                this.aspectRatio = e.detail.element!.getAttribute('aspect-ratio');
                                console.log(this.aspectRatio);
                                this.requestUpdate();
                            }}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${faUser}"
                            .iconPrepend="${faKey}"
                            .iconAppend="${faSearch}"
                            .iconAppendOutside="${faLock}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${(e:CustomEvent) => { (<Event>e.detail.originalEvent).preventDefault(); console.log('click-icon-prepend-outside')}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload1"
                                label="Upload 1"
                                ?showFilename="${this.showFilename}"
                                value="${this.upload1}"
                                .fileUploader="${fileUploaderSimulator1}"
                                
                                aspect-ratio="${this.aspectRatio}"
                                preview-image-mode="cover"
                                placeholder="Upload image"
                                .placeholderIcon="${faFileUpload}"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload2"
                                label="Upload 2"
                                value="${this.upload2}"
                                .fileUploader="${fileUploaderSimulator2}"
                                busyStyle="icon"
                                preview-thumb="true"
                                placeholder="You can upload an image here"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload3"
                                label="Upload 3"
                                value="${this.upload3}"
                                .fileUploader="${fileUploaderSimulator3}"
                                placeholder-icon

                                floatingLabel
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="string4"
                                label="String 4"
                                value="${this.upload4}"
                                .fileUploader="${fileUploaderSimulator4}"

                                floatingLabel
                                filled
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload5"
                                label="Upload 5"
                                value="${this.upload5}"
                                .fileUploader="${fileUploaderSimulator5}"

                                filled
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload6"
                                label="Upload 6"
                                value="${this.upload6}"
                                .fileUploader="${fileUploaderSimulator6}"

                                filled
                                floatingLabel
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload7"
                                label="Upload 7"
                                value="${this.upload7}"
                                .fileUploader="${fileUploaderSimulator7}"

                                filled
                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload8"
                                label="Upload 8"
                                value="${this.upload8}"
                                .fileUploader="${fileUploaderSimulator8}"

                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <div style="border-top: solid 4px #f00; margin-top: 35px;"></div>

                            <kode4-textfield
                                name="string2"
                                label="String 2"
                                value="${this.strg1}"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-textfield>

                        <br><br>
                        <h1>Fields with submitEmpty</h1>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                        <br>
                        <br>
                        <kode4-button
                            type="button" filled secondary block
                            @click="${() => {
                                this.aspectRatio = this.aspectRatio === '16:9' ? '4:3' : '16:9';
                            }}"
                            >Toggle Aspect Ratio</kode4-button>
                    </div>
                </div>

                ${false ? html`
                    <kode4-button type="submit" filled primary>Submit</kode4-button>
                    <kode4-button type="submit" name="submitButton" value="submitButton-value" filled primary>Submit (width value)</kode4-button>
                    <kode4-button type="reset" filled warning>Reset</kode4-button>
                    <kode4-button type="clear" filled danger>Clear</kode4-button>
                    <kode4-button type="updateFields" filled secondary>Update Fields</kode4-button>
                    <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                    <kode4-button type="respawn" filled success>Respawn</kode4-button>
                `: ''}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
