import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faLock } from "@fortawesome/free-solid-svg-icons/faLock";
import { faUser } from "@fortawesome/free-solid-svg-icons/faUser";

import {
  Kode4RestOptionsProvider,
  Kode4StaticOptionsProvider,
} from "../../../src";
import Kode4Form from "../../kode4-form";

@customElement("kode4form-demo-select")
export default class Kode4FormDemoSelect extends LitElement {
  @property()
  private k4form: Kode4Form | null = null;

  @property({ type: Boolean })
  private formDirty: boolean = false;

  // @property()
  // private staticOpField: Kode4Select | null = null;

  @property()
  private staticOp: Kode4StaticOptionsProvider = new Kode4StaticOptionsProvider();

  @property()
  private staticOpValue?: number;

  @property()
  private userOpValue?: number;

  @property()
  private userOpList?: Array<any>;

  @property()
  private formenList: Array<any> = [];

  @property()
  private jediOptionsProvider?:Kode4RestOptionsProvider;

  constructor() {
    super();
  }

  protected firstUpdated(changedProperties: any) {
    super.firstUpdated(changedProperties);

    this.jediOptionsProvider = new Kode4RestOptionsProvider();
    this.jediOptionsProvider.baseUrl = process.env.KODE4_API + "/jedi";
    this.jediOptionsProvider.valueField = 'value';
    this.jediOptionsProvider.labelField = 'label';
    // optionsProvider.noSelection = "Bitte wählen...";
    

    this.k4form = this.querySelector("kode4-form");
    // this.staticOpField = this.querySelector("#staticOp");
    // this.staticOp.valueField = "id";
    // this.staticOp.labelField = "bezeichnung";
    this.staticOp.setList([
      { value: 1, label: 'ARR' },
      { value: 2, label: 'ARR ARR' },
      { value: 3, label: 'ARR ARR ARR' },
    ]);
    this.staticOpValue = 2;

    setTimeout(() => {
        this.formenList = [
            {
            id: 1,
            label: "Stab",
            },
            {
            id: 2,
            label: "Vierkantstab",
            },
            {
            id: 3,
            label: "Vierkantrohr",
            },
            {
            id: 4,
            label: "Rohr",
            },
            {
            id: 5,
            label: "Träger",
            },
        ];
    }, 5000);
  }

  protected useStaticOpUserValue(e: MouseEvent) {
    e.preventDefault();
    this.staticOpValue = this.userOpValue;
    console.log("Now using value", this.staticOpValue);
  }

  protected useStaticOpUserValues(e: MouseEvent) {
    e.preventDefault();
    this.staticOp.setList(<any>this.userOpList);
    this.formenList = <any>this.userOpList;
    console.log("Now using list", this.staticOp.list);
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <kode4-form
        action="/"
        @stateChanged="${() => /*(this.formDirty = e.detail.dirty)*/ console.log('FORM STATE CHANGD')}"
      >
        <h1>Form-Style</h1>

        <div style="display: flex; flex-direction: row; width: 100%;;">
          <div
            style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;"
          >
            <kode4-select
              id="staticOp"
              name="leistungId"
              label="Static Options Provider"
              hint="Value = ${this.staticOpValue}"
              .optionsProvider="${this.staticOp}"
              value="${this.staticOpValue}"
              @change="${(e: CustomEvent) => {
                this.staticOpValue = e.detail.value;
              }}"
              block
              outline
              floatingLabel
            ></kode4-select>

            <kode4-select
              name="select-jedi"
              type="autocomplete"
              label="Choose Jedi"
              value="${this.staticOpValue}"
              .optionsProvider="${this.jediOptionsProvider}"
              .iconAppend="${faLock}"
              .iconPrependOutside="${faUser}"
              block
              floatingLabel
              outline
              force-selection
            ></kode4-select>


            <kode4-custom-select
                name="customSelect1"
                label="Please click an image"

                label="Image Type"
                hint="Click an Image to select"

                value="${this.staticOpValue}"
                @change="${(e: CustomEvent) => {
                    this.staticOpValue = e.detail.value;
                }}"

                filled
                clearable

                required
                >

                ${this.formenList.map(frm => {
                    return html`
                        <kode4-custom-select-option
                            value="${frm.id}"
                            class="kode4-custom-option-opacity kode4-custom-option-grayscale">
                            <div>${frm.id}: ${frm.label}</div>
                        </kode4-custom-select-option>
                    `;
                })}

            </kode4-custom-select>

          </div>

          <div
            style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;"
          >
            <input
              style="display: block; width: 100%;"
              type="number"
              @change="${(e: Event) => {
                e.stopPropagation();
                e.preventDefault();
                this.userOpValue = Number((<HTMLInputElement>e.target).value);
              }}"
            />

            <textarea
              style="display: block; width: 100%; height: 300px; margin-top: 10px;"
              @change="${(e: Event) => {
                e.stopPropagation();
                e.preventDefault();
                this.userOpList = JSON.parse(
                  (<HTMLTextAreaElement>e.target).value.trim()
                );
              }}"
            >
[
    {
        "id": 1,
        "bezeichnung": "Hohe Standzeit"
    },
    {
        "id": 2,
        "bezeichnung": "Normal"
    },
    {
        "id": 3,
        "bezeichnung": "Hohe Leistung"
    }
]
                            </textarea
            >

            <div style="margin-top: 10px;">
              <kode4-button
                type="button"
                filled
                primary
                @click="${this.useStaticOpUserValues}"
                >Update List</kode4-button
              >
              <kode4-button
                type="button"
                filled
                primary
                @click="${this.useStaticOpUserValue}"
                >Update Value</kode4-button
              >
            </div>
          </div>
        </div>

        <kode4-button type="submit" filled primary>Submit</kode4-button>
        <kode4-button
          type="submit"
          name="submitButton"
          value="submitButton-value"
          filled
          primary
          >Submit (with value)</kode4-button
        >
        <kode4-button
          type="button"
          filled
          primary
          @click="${() => {
            console.log('UPDATE OPTIONS PROVIDER')

            this.jediOptionsProvider = new Kode4RestOptionsProvider();
            this.jediOptionsProvider.baseUrl = process.env.KODE4_API + "/jedi";
            this.jediOptionsProvider.valueField = 'value';
            this.jediOptionsProvider.labelField = 'label';

            console.log('> UPDATE OPTIONS PROVIDER DONE')
          }}"
          >Update Options Provider</kode4-button
        >
        <kode4-button type="reset" filled warning>Reset</kode4-button>
        <kode4-button type="clear" filled danger>Clear</kode4-button>
        <kode4-button type="updateFields" filled secondary
          >Update Fields</kode4-button
        >
        <kode4-button type="updateValues" filled secondary
          >Update Values</kode4-button
        >
        <kode4-button type="respawn" filled success>Respawn</kode4-button>
        <hr />
        <div class="form-dirty">DIRTY!!!</div>
        <div class="form-clean">CLEAN!!!</div>
        <hr>
        ${this.k4form?.dirty ? 'DIRRY' : 'KLEEN'}
        <hr />
        Dirty? ${this.formDirty}
      </kode4-form>
    `;
  }
}
