import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { Kode4RestOptionsProvider } from '../../../src';
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-valuetests')
export default class ValueTests extends LitElement {

    // @query('kode4-form')
    // private k4form:Kode4Form|null = null;

    @property({ type: Boolean })
    private formDirty:boolean = false;

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Value-Tests</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            block
                            counter
                            ></kode4-textfield>

                        <kode4-check
                            name="cb6"
                            label="Dies ist eine Checkbox"
                            block
                            filled
                            ></kode4-check>
                        
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>
                </div>

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>

                <kode4-button filled primary type="submit">Submit</kode4-button>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
