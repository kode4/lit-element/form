import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { Kode4RestOptionsProvider } from '../../../src';
import Kode4Form from "../../kode4-form";
import Kode4Field from "../../kode4-field";
import { Kode4FormConditionExpression } from "../../Kode4FormCondition";
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

const condHideLogin:Kode4FormConditionExpression = {
    conditions: [
        (form:Kode4Form, _field:Kode4Field) => {
            const checkField = form.getField('showFields');
            return Boolean(checkField?.value);        
        }
    ],
    handleThen(_form: Kode4Form, field:Kode4Field) {
        field.hidden = false;
    },
    handleElse(_form: Kode4Form, field:Kode4Field) {
        field.hidden = true;
    }
};

const condSetValue:Kode4FormConditionExpression = {
    conditions: [
        (form:Kode4Form, _field:Kode4Field) => {
            const checkField = form.getField('setValue');
            return Boolean(checkField?.value);        
        }
    ],
    handleThen(_form: Kode4Form, field:Kode4Field) {
        field.value = 'option3';
    }
};

@customElement('kode4form-demo-cutomselects')
export default class Kode4FormDemoCustomSelects extends LitElement {

    // @property()
    // private k4form:Kode4Form|null = null;

    // @property()
    // private k4form:Kode4Form|null = null;

    // @property({ type: String })
    // private format1:string = 'dd.mm.yyyy';

    @property({ type: Boolean })
    private formDirty:boolean = false;

    constructor() {
        super();
    }

    // protected firstUpdated(changedProperties:any) {
    //     super.firstUpdated(changedProperties);
    //     this.k4form = this.querySelector('kode4-form');
    // }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Custom Selects</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-check
                            name="showFields"
                            label="Show Fields"
                            block
                            value="true"
                            >
                            </kode4-check>

                        <kode4-check
                            name="setValue"
                            label="Set Value"
                            block
                            >
                            </kode4-check>

                        <kode4-textfield
                            name="dummy1"
                            label="Dummy"
                            outline
                            floatingLabel
                            ></kode4-textfield>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-custom-select
                            name="customSelect1"
                            label="Please click an image"

                            label="Image Type"
                            hint="Click an Image to select"

                            
                            filled
                            clearable

                            required
                            .conditions="${[condHideLogin, condSetValue]}">
                                
                            <kode4-custom-select-option 
                                value="option1" 
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 1</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option 
                                value="option2"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 2</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option
                                value="option3"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 3</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option
                                value="option4"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 4</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option 
                                value="option5" 
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 5</div>
                            </kode4-custom-select-option>
                        
                            
                        </kode4-custom-select>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-textfield
                            name="dummy2"
                            label="Dummy"
                            outline
                            floatingLabel
                            ></kode4-textfield>
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>

                </div>

                <div class="kode4demo-spacer-v"></div>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="kode4demo-spacer-v"></div>

                <div class="form-dirty">Dirty</div>
                <div class="form-clean">Clean</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty ? html`<div class="form-dirty">Dirty</div>` : html`<div class="form-clean">Clean</div>`}
        `;
    }

}
