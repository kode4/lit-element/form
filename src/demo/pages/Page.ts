import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement('kode4form-demo-page')
export default class Kode4FormDemopage extends LitElement {

    @property({ type: String })
    public mode:string = window.localStorage.getItem('KODE4_FORMS_DEMO_MODE') || 'buttons';

    constructor() {
        super();
    }

    setMode(mode:string) {
        window.localStorage.setItem('KODE4_FORMS_DEMO_MODE', mode);
        this.mode = mode;
    }

    createRenderRoot() {
        return this;
    }

    protected render() {
        return html`
            <div class="kode4demo-section">
                <h2>
                    KODE4 Form Demo
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    <small>${this.mode}</small>
                </h2>
                ${['valuetests', 'fileupload', 'required', 'dynamicForms', 'conditions', 'customselects', 'datetimepickers', 'buttons', 'formlogic', 'textfields', 'dirtychecking', 'check', 'select', 'textarea', 'slider'].map((el:any) => html `<span @click="${() => { this.setMode(el); }}" class="kode4demo-menuitem">${el}</span>`)}
            </div>

            ${this.mode === 'formlogic' ? this.renderFormlogic() : ''}
            ${this.mode === 'textfields' ? this.renderTextfields() : ''}
            ${this.mode === 'dirtychecking' ? this.renderDirtychecking() : ''}
            ${this.mode === 'buttons' ? this.renderButtons() : ''}
            ${this.mode === 'datetimepickers' ? this.renderDatetimepickers() : ''}
            ${this.mode === 'customselects' ? this.renderCustomselects() : ''}
            ${this.mode === 'conditions' ? this.renderConditions() : ''}
            ${this.mode === 'dynamicForms' ? this.renderDynamicForms() : ''}
            ${this.mode === 'select' ? this.renderSelect() : ''}
            ${this.mode === 'check' ? this.renderCheck() : ''}
            ${this.mode === 'required' ? this.renderRequired() : ''}
            ${this.mode === 'fileupload' ? this.renderFileUpload() : ''}
            ${this.mode === 'valuetests' ? this.renderValueTests() : ''}
        `;
    }

    protected renderFormlogic() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-formlogic></kode4form-demo-formlogic>
            </div>
        `;
    }

    protected renderTextfields() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-textfields></kode4form-demo-textfields>
            </div>
        `;
    }

    protected renderDirtychecking() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-dirtychecking></kode4form-demo-dirtychecking>
            </div>
        `;
    }

    protected renderButtons() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-buttons></kode4form-demo-buttons>
            </div>
        `;
    }

    protected renderDatetimepickers() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-datetimepickers></kode4form-demo-datetimepickers>
            </div>
        `;
    }

    protected renderCustomselects() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-cutomselects></kode4form-demo-cutomselects>
            </div>
        `;
    }

    protected renderConditions() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-conditions></kode4form-demo-conditions>
            </div>
        `;
    }

    protected renderDynamicForms() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-dynamic-forms></kode4form-demo-dynamic-forms>
            </div>
        `;
    }

    protected renderSelect() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-select></kode4form-demo-select>
            </div>
        `;
    }

    protected renderCheck() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-check></kode4form-demo-check>
            </div>
        `;
    }

    protected renderRequired() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-required></kode4form-demo-required>
            </div>
        `;
    }

    protected renderFileUpload() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-fileupload></kode4form-demo-fileupload>
            </div>
        `;
    }

    protected renderValueTests() {
        return html`
            <div class="kode4demo-section">
                <kode4form-demo-valuetests></kode4form-demo-valuetests>
            </div>
        `;
    }

}
