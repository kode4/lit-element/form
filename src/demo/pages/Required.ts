import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import { Kode4RestOptionsProvider } from '../../../src';

const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

interface DemoValueObject {
    strg1:string|null;
    strg2?:string|null;
    strg3?:string;
    strg4?:string;
    strg5?:string;
    strg6:string|null;
    strg7:string|null;
    int1?:number;
}

@customElement('kode4form-demo-required')
export default class Kode4FormDemoRequired extends LitElement {

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: Boolean })
    private valueObject:DemoValueObject = {
        strg1: null,
        strg3: '',
        strg5: undefined,
        strg6: '',
        strg7: null,
    };

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }
    render() {
        return html`
            <kode4-form action="/" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Required-Check</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>


                            <kode4-select
                                name="int1"
                                label="Int 1"
                                value="${this.valueObject.int1}"
                                .options="${[{ label: 'Choose...' }, { label: 'Value 0', value: '0' }, { label: 'Value 1', value: '1' }, { label: 'Value 2', value: '2' }]}"
                                openDialogOnFocus
                                block
                                required
                                outline
                                floatingLabel
                                ></kode4-select>

                            <kode4-textfield
                                name="string1"
                                label="String 1"
                                value="${this.valueObject.strg1}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string2"
                                label="String 2"
                                value="${this.valueObject.strg2}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string3"
                                label="String 3"
                                value="${this.valueObject.strg3}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string4"
                                label="String 4"
                                value="${this.valueObject.strg4}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string5"
                                label="String 5"
                                value="${this.valueObject.strg5}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string6"
                                label="String 6"
                                value="${this.valueObject.strg6}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string7"
                                label="String 7"
                                value="${this.valueObject.strg7}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                        <br><br>
                        <h1>Fields with submitEmpty</h1>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                    </div>
                </div>

                ${false ? html`
                    <kode4-button type="submit" filled primary>Submit</kode4-button>
                    <kode4-button type="submit" name="submitButton" value="submitButton-value" filled primary>Submit (width value)</kode4-button>
                    <kode4-button type="reset" filled warning>Reset</kode4-button>
                    <kode4-button type="clear" filled danger>Clear</kode4-button>
                    <kode4-button type="updateFields" filled secondary>Update Fields</kode4-button>
                    <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                    <kode4-button type="respawn" filled success>Respawn</kode4-button>
                `: ''}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
