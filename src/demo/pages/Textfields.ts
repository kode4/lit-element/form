import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faLock } from '@fortawesome/free-solid-svg-icons/faLock';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';

import { Kode4RestOptionsProvider } from '../../../src';
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-textfields')
export default class Kode4FormDemoTextfields extends LitElement {

    // @query('kode4-form')
    // private k4form:Kode4Form|null = null;

    @property({ type: Boolean })
    private formDirty:boolean = false;

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            
                            block

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            name="floatinglabel"
                            label="Floating Label (Number)"
                            type="number"

                            min="5"
                            max="15"
                            step="1"
                            value="10"
                            
                            block
                            floatingLabel

                            counter

                            ></kode4-textfield>

                        <kode4-textarea
                            name="floatinglabel2"
                            label="Floating Label (Number)"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block
                            floatingLabel

                            counter
                            required

                            ></kode4-textarea>

                        <kode4-textfield
                            name="outline"
                            label="Outline"
                            value="312312321313123123123"

                            block
                            outline

                            counter
                            counter-max="10"
                            counter-label="Zeichen"

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outlinefloatinglabel"
                            label="Outline"
                            suffix="mm"
                            hint="Das hier ist einfach nur ein hint."

                            block
                            floatingLabel
                            outline

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            name="filled"
                            label="Filled"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"
                            value="312312321313123123123"

                            block
                            filled

                            counter
                            counter-max="10"
                            counter-label="Chars"
                            counter-divider="of"

                            ></kode4-textfield>

                        <kode4-check
                            name="cb1"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb2"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb3"
                            label="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb4"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="searchsimple"
                            placeholder="Search"
                            .iconPrepend="${faSearch}"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block

                            counter
                            counter-max="10"
                            counter-label="Chars"
                            counter-divider="of"

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchfilled"
                            placeholder="Search"
                            .iconAppend="${faSearch}"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchoutline"
                            label="Search"
                            .iconAppendOutside="${faSearch}"

                            block
                            outline
                            floatingLabel

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="username"
                            .iconPrependOutside="${faUser}"
                            prefix="Username:"

                            block

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="password"
                            type="password"
                            .iconPrependOutside="${faKey}"
                            prefix="Password:"

                            block

                            ></kode4-textfield>

                        <kode4-check
                            name="cb5"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb6"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb7"
                            label="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb8"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>


                        </div>
                </div>

                <kode4-textfield
                        name="allin"
                        label="All in"

                        value="abc"
                        placeholder="Type something..."
                        hint="All features. Default value is 'abc'"

                        .iconPrependOutside="${faUser}"
                        .iconPrepend="${faKey}"
                        .iconAppend="${faSearch}"
                        .iconAppendOutside="${faLock}"
                        prefixOutside="Outside"
                        prefix="Inside"
                        suffix="Inside"
                        suffixOutside="Outside"

                        block
                        filled
                        outline
                        foatingLabel

                        clearable
                        resettable
                        changeOnType

                        required

                        ></kode4-textfield>


                <!--
                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>
                -->

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
