import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faLock, faKey, faUser, faSearch } from '@fortawesome/free-solid-svg-icons';

import { Kode4RestOptionsProvider } from '../../../src';
import Kode4Form from "../../kode4-form";
import Kode4Field from "../../kode4-field";
import { Kode4FormConditionExpression } from "../../Kode4FormCondition";
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

const condHideLogin:Kode4FormConditionExpression = {
    conditions: [
        (form:Kode4Form, _field:Kode4Field) => {
            const checkField = form.getField('showLogin');
            return Boolean(checkField?.value);        
        }
    ],
    handleThen(_form:Kode4Form, field:Kode4Field) {
        field.hidden = false;
    },
    handleElse(_form:Kode4Form, field:Kode4Field) {
        field.hidden = true;
    }
};

const condDisableLogin:Kode4FormConditionExpression = {
    conditions: [
        (form:Kode4Form, _field:Kode4Field) => {
            const checkField = form.getField('enableLogin');
            return Boolean(checkField?.value);        
        }
    ],
    handleThen(_form:Kode4Form, field:Kode4Field) {
        field.disabled = false;
    },
    handleElse(_form:Kode4Form, field:Kode4Field) {
        field.disabled = true;
    }
};

@customElement('kode4form-demo-conditions')
export default class Kode4FormDemoConditions extends LitElement {

    // @query('kode4-form')
    // private k4form:Kode4Form|null = null;

    @property({ type: Boolean })
    private formDirty:boolean = false;

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Conditions</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            
                            block

                            ></kode4-textfield>

                        <kode4-textfield
                            name="floatinglabel"
                            label="Floating Label (Number)"
                            type="number"
                            
                            block
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outline"
                            label="Outline"

                            block
                            outline

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outlinefloatinglabel"
                            label="Outline"
                            suffix="mm"

                            block
                            floatingLabel
                            outline

                            ></kode4-textfield>

                        <kode4-textfield
                            name="filled"
                            label="Filled"
                            suffix="mm"

                            block
                            filled

                            ></kode4-textfield>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="searchsimple"
                            placeholder="Search"
                            .iconPrepend="${faSearch}"

                            block

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchfilled"
                            placeholder="Search"
                            .iconAppend="${faSearch}"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchoutline"
                            label="Search"
                            .iconAppendOutside="${faSearch}"

                            block
                            outline
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-check
                            name="showLogin"
                            label="Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login "
                            block
                            >
                        </kode4-check>

                        <kode4-check
                            name="enableLogin"
                            label="Enable Login"
                            block
                            value="true"
                            >
                        </kode4-check>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="username"
                            .iconPrependOutside="${faUser}"
                            prefix="Username:"

                            block

                            .conditions="${[condHideLogin, condDisableLogin]}"

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="password"
                            type="password"
                            .iconPrependOutside="${faKey}"
                            prefix="Password:"

                            block

                            .conditions="${[condHideLogin, condDisableLogin]}"
                            ></kode4-textfield>

                        </div>
                </div>

                <kode4-textfield
                        name="allin"
                        label="All in"

                        value="abc"
                        placeholder="Type something..."
                        hint="All features. Default value is 'abc'"

                        .iconPrependOutside="${faUser}"
                        .iconPrepend="${faKey}"
                        .iconAppend="${faSearch}"
                        .iconAppendOutside="${faLock}"
                        prefixOutside="Outside"
                        prefix="Inside"
                        suffix="Inside"
                        suffixOutside="Outside"

                        block
                        filled
                        outline
                        foatingLabel

                        clearable
                        resettable
                        changeOnType

                        required

                        ></kode4-textfield>


                <!--
                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>
                -->

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
