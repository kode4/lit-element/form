import {LitElement, html} from "lit";
import {customElement, property} from "lit/decorators.js";
import {faChevronLeft, faChevronRight, faSearch} from '@fortawesome/free-solid-svg-icons'
import "@kode4/ui/kode4-fa-icon";
import {faEllipsisV} from "@fortawesome/free-solid-svg-icons/faEllipsisV";

@customElement("kode4form-demo-buttons")
export default class Kode4FormDemoButtons extends LitElement {

    @property({type: String})
    private bgColor: string = '#ccc';

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties: any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private cycleBgColor() {
        const colors = ['transparent', '#ccc', '#f00', '#0f0', '#00f', '#ff0', '#f0f', '#0ff'];
        const pos = colors.findIndex((el: string) => el === this.bgColor) + 1;
        this.bgColor = pos > colors.length - 1 ? colors[0] : colors[pos];
    }

    render() {
        return html`
            <div>
                <kode4-button flat .icon>
                    Button!
                    <br>
                    with Menu
                    <kode4-menu slot="menu" parent>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
                <kode4-button flat no-menu-icon secondary>
                    <kode4-fa-icon .icon="${faEllipsisV}"></kode4-fa-icon>
                    <kode4-menu slot="menu" parent right cover>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
                <kode4-button outline filled .icon>
                    Button!
                    <br>
                    without  Menu
                </kode4-button>
                <kode4-button outline filled xs no-menu-icon>
                    Small Button with Menu
                    <br>
                    but no Icon
                    <kode4-menu slot="menu" parent right>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
            </div>
            <div style="background-color: ${this.bgColor};" @click="${this.cycleBgColor}">
                <div>
                    <kode4-button>
                        Submit
                    </kode4-button>
                    <kode4-button filled>
                        Submit
                    </kode4-button>
                    <kode4-button outline>
                        Submit
                    </kode4-button>
                    <kode4-button flat>
                        Submit
                    </kode4-button>
                    <kode4-button filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary flat filled xxs>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xs>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled s>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled l>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xl>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xxl>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary>
                        Submit
                    </kode4-button>
                    <kode4-button primary filled>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat>
                        Submit
                    </kode4-button>
                    <kode4-button primary filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button secondary>
                        Submit
                    </kode4-button>
                    <kode4-button secondary filled>
                        Submit
                    </kode4-button>
                    <kode4-button secondary outline>
                        Submit
                    </kode4-button>
                    <kode4-button secondary flat>
                        Submit
                    </kode4-button>
                    <kode4-button secondary filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button secondary outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button success>
                        Submit
                    </kode4-button>
                    <kode4-button success filled>
                        Submit
                    </kode4-button>
                    <kode4-button success outline>
                        Submit
                    </kode4-button>
                    <kode4-button success flat>
                        Submit
                    </kode4-button>
                    <kode4-button success filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button success outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button danger>
                        Submit
                    </kode4-button>
                    <kode4-button danger filled>
                        Submit
                    </kode4-button>
                    <kode4-button danger outline>
                        Submit
                    </kode4-button>
                    <kode4-button danger flat>
                        Submit
                    </kode4-button>
                    <kode4-button danger filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button danger outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button warning>
                        Submit
                    </kode4-button>
                    <kode4-button warning filled>
                        Submit
                    </kode4-button>
                    <kode4-button warning outline>
                        Submit
                    </kode4-button>
                    <kode4-button warning flat>
                        Submit
                    </kode4-button>
                    <kode4-button warning filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button warning outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button info>
                        Submit
                    </kode4-button>
                    <kode4-button info filled>
                        Submit
                    </kode4-button>
                    <kode4-button info outline>
                        Submit
                    </kode4-button>
                    <kode4-button info flat>
                        Submit
                    </kode4-button>
                    <kode4-button info filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button info outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button icon info outline>
                        <kode4-fa-icon .icon="${faChevronLeft}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon info outline>
                        <kode4-fa-icon .icon="${faChevronRight}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon danger filled>
                        <kode4-fa-icon .icon="${faChevronLeft}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon success filled>
                        <kode4-fa-icon .icon="${faChevronRight}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div style="font-size: 25px;">
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div style="font-size: 25px;">
                    <kode4-button icon primary filled xxs>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xs>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled s>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled m>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled l>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xl>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xxl>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button iconSquare primary>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary filled>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary outline>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary filled flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary outline flat>
                        <kode4-fa-icon .icon="${faSearch}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary filled block>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline block>
                        Submit
                    </kode4-button>
                </div>
            </div>
        `;
    }
}
