import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faLock } from '@fortawesome/free-solid-svg-icons/faLock';
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';

import { Kode4RestOptionsProvider } from '../../../src';
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-formlogic')
export default class Kode4FormDemoFormlogic extends LitElement {

    // @query('kode4-form')
    // private k4form:Kode4Form|null = null;

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ reflect: true })
    private demoOptions = [
        {
            label: 'Bitte wählen',
        },
        {
            value: '1111',
            label: 'a one',
        },
        {
            value: '2222',
            label: 'and a two',
        },
        {
            value: '3333',
            labelX: html`Ich bin ein mächtiges<br>mehrzeiliges Monstelement!<br><span style="color: #c3c; font-weight: bold;">KLICK MICH!!!!</span>`,
            label: 'Ich bin ein mächtiges<br>mehrzeiliges Monstelement!<br><span style="color: #c3c; font-weight: bold;">KLICK MICH!!!!</span>',
            unsafe: true,
        },
        {
            value: '4444',
            label: 'and a four',
        },
        {
            value: '5555',
            label: 'and a five',
        },
    ];

    @property()
    private fields:Array<string> = [];

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    private onChangeFields() {
        this.fields = [
            ...this.fields,
            'extra-field-3',
            'extra-field-4',
            'extra-field-5',
        ];
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-select
                            name="select1"
                            label="Floating North"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            hint="Type something"
                            outline

                            .options="${this.demoOptions}"
                            value="2222"

                            .iconAppend="${faLock}"
                            .iconPrependOutside="${faUser}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            filled
                            floatingLabel
                            clearable

                            placeholder="Wähle weise"

                            required
                            ></kode4-select>

                        <kode4-select
                            name="select2"
                            type="autocomplete"
                            label="Autocomplete"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            .options="${this.demoOptions}"

                            .iconAppend="${faLock}"
                            .iconPrependOutside="${faUser}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            required

                            ></kode4-select>

                        <kode4-select
                            name="select-jedi"
                            type="autocomplete"
                            label="Choose Jedi"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            value="5"
                            .optionsProvider="${optionsProvider}"

                            .iconAppend="${faLock}"
                            .iconPrependOutside="${faUser}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel
                            outline

                            ></kode4-select>

                        <kode4-textfield
                            name="text1"
                            label="Floating North"
                            .iconAppend="${faLock}"
                            .iconPrependOutside="${faUser}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            floatingLabel
                            outline

                            ></kode4-textfield>
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text2"
                            label="Floating North"
                            .iconAppend="${faLock}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel
                            filled

                            clearable

                            ></kode4-textfield>

                        <kode4-textfield
                            name="text3"
                            label="Floating North"
                            .iconAppend="${faLock}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="text4"
                            label="Floating North"
                            .iconAppend="${faLock}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-textarea
                            name="text5"
                            label="Floating North"
                            .iconAppend="${faLock}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-textarea>

                            
                        <kode4-check
                            name="check1"
                            label="Checkbox"
                            .iconAppend="${faLock}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-check>
                        </div>
                </div>

                <hr>
                ${this.fields.map((el:string) => html`
                    <kode4-textfield
                        name="${el}"
                        label="${el}"
                        value="${el}"

                        block
                        floatingLabel

                        ></kode4-textfield>

                `)}
                <hr>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="submit" name="submitButton" value="submitButton-value" filled primary>Submit (width value)</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button @click="${this.onChangeFields}" filled secondary>Change Fields</kode4-button>
                <kode4-button type="updateFields" filled secondary>Update Fields</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;
    }

}
