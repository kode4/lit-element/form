import { LitElement, html } from "lit";
import { customElement } from "lit/decorators.js";
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';

@customElement('kode4form-demo-textfield')
export default class Kode4FormDemoTextfield extends LitElement {

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" data-value="theform">
                <h1>Anmeldung</h1>
                <div style="width: 500px; max-width: 95vw;">

                    <kode4-textfield
                        name="username"
                        labelPrefix="Benutzername:"
                        .iconPrepend="${faUser}"
                        required
                        ></kode4-textfield>

                    <kode4-textfield
                        type="password"
                        name="password"
                        labelPrefix="Passwort"
                        .iconPrepend="${faKey}"
                        required
                        ></kode4-textfield>

                </div>

                <kode4-button type="submit">Anmelden</kode4-button>
            </kode4-form>
        `;
    }

}
