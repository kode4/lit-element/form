import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { Kode4RestOptionsProvider } from '../../../src';

const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-check')
export default class Kode4FormDemoCheck extends LitElement {

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: Boolean })
    private field1:boolean = false;

    @property({ type: Boolean })
    private field2:boolean = true;

    @property({ type: Boolean })
    private field3:boolean = false;

    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    protected submitForm(e:CustomEvent) {
        if (!this.formDirty) {
            console.log('FORM IS NOT DIRTY');
        }
        console.log('SUBMIT FORM');
        console.log([...e.detail.formData.entries()]);
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <kode4-form action="/" @stateChanged="${(e:CustomEvent) => this.formDirty = e.detail.dirty}" @submit="${this.submitForm}">
                <h1>Checkboxes</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h2>Default Fields</h2>
                        <em>These fields will only be submitted if checked</em>

                        <kode4-check
                            name="accountConfirmed"
                            label="Registrierung bestätigt"

                            value="${this.field1}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountRejected"
                            label="Registrierung abgelehnt"

                            value="${this.field2}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountEnabled"
                            label="Benutzerkonto aktiviert"

                            value="${this.field3}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 2</h2>
                        <em>These fields will always be submitted</em>

                        <kode4-check
                            name="accountConfirmedAlways"
                            label="Registrierung bestätigt"

                            value="${this.field1}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountRejectedAlways"
                            label="Registrierung abgelehnt"

                            value="${this.field2}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountEnabledAlways"
                            label="Benutzerkonto aktiviert"

                            value="${this.field3}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 3</h2>
                        <em>These fields will only be submitted if checked and with a custom value</em>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 1"

                            value="${this.field1}"
                            trueValue="1"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 2"

                            value="${this.field2}"
                            trueValue="2"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 3"

                            value="${this.field3}"
                            trueValue="3"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 4</h2>
                        <em>These fields will always be submitted and with a custom value</em>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 1"

                            value="${this.field1}"
                            trueValue="1"
                            falseValue="-1"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 2"

                            value="${this.field2}"
                            trueValue="2"
                            falseValue="-2"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 3"

                            value="${this.field3}"
                            trueValue="3"
                            falseValue="-3"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                    </div>
                </div>

            </kode4-form>
        `;
    }

}
