import { LitElement, html } from "lit";
import { customElement, property, query } from "lit/decorators.js";

import { Kode4RestOptionsProvider, Kode4Option } from '../../../src';
import Kode4Form from "../../kode4-form";
const optionsProvider = new Kode4RestOptionsProvider();
optionsProvider.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider.noSelection = 'Bitte wählen...';

const optionsProvider2 = new Kode4RestOptionsProvider();
optionsProvider2.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider2.noSelection = 'Bitte wählen...';

const optionsProvider3 = new Kode4RestOptionsProvider();
optionsProvider3.baseUrl = process.env.KODE4_API+'/jedi';
optionsProvider3.noSelection = 'Bitte wählen...';

@customElement('kode4form-demo-dirtychecking')
export default class Kode4FormDemoDirtyChecking extends LitElement {

    @query('kode4-form')
    private k4form?:Kode4Form;

    private staticOptions:Array<Kode4Option> = [
        {
            value: 1,
            label: 'Option 1'
        },
        {
            value: 2,
            label: 'Option 2'
        },
        {
            value: 3,
            label: 'Option 3'
        },
        {
            value: 4,
            label: 'Option 4'
        },
        {
            value: 5,
            label: 'Option 5'
        },
    ];

    @property(({ type: String }))
    private text1?:string;

    @property(({ type: String }))
    private text2?:string = '123';

    @property(({ type: String }))
    private check1?:string = 'on';

    @property(({ type: Boolean }))
    private check2?:boolean = true;

    @property({ type: Boolean })
    private formDirty:boolean = false;

    @property({ type: Number })
    private select1?:number;

    @property({ type: Number })
    private select2?:number = 2;

    @property({ type: Number })
    private selectrest1?:number;

    @property({ type: Number })
    private selectrest2?:number = 5;

    @property({ type: Number })
    private autocompleterest1?:number = 5;


    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
        // this.k4form = this.querySelector('kode4-form');
    }

    createRenderRoot() {
        return this;
    }

    private onSubmit() {
    }

    private changeValues() {
        this.text2 = '456';
        this.text2 = '456';

        this.check1 = 'on';
        this.check2 = false;
        
        this.select1 = 4;
        this.select2 = undefined;
    
        this.selectrest1 = 5;
        this.selectrest2 = 2;
    
        this.autocompleterest1 = 7;
    }

    render() {
        return html`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${() => this.requestUpdate()}">
                <h1>Form Dirty-checking</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Text 1"
                            value="${this.text1}"
                            
                            floatingLabel
                            
                            clearable
                            resettable
                            
                            ></kode4-textfield>
                            
                            <kode4-textfield
                            name="text2"
                            label="Text 2"
                            
                            value="${this.text2}"
                            hint="Default value is '${this.text2}'"

                            floatingLabel

                            clearable
                            resettable

                            ></kode4-textfield>

                        <kode4-check
                            name="check1"
                            label="Check 1"
                            value="${this.check1}"
                            required
                            ></kode4-check>

                        <kode4-check
                            name="check2"
                            label="Check 2"
                            value="${this.check2}"
                            ></kode4-check>

                        <kode4-select
                            name="select1"
                            label="Select 1"
                            value="${this.select1}"
                            .options="${this.staticOptions}"
                            
                            clearable
                            resettable
                            
                            ></kode4-select>
                            
                        <kode4-select
                            name="select2"
                            label="Select 2"
                            value="${this.select2}"
                            .options="${this.staticOptions}"
                            hint="Default value is '2' ('Option 2')"
                            
                            clearable
                            resettable

                            ></kode4-select>
                            
                        <kode4-select
                            name="selectrest1"
                            label="Jedi Select"
                            value="${this.selectrest1}"
                            .optionsProvider="${optionsProvider}"

                            clearable
                            resettable

                            ></kode4-select>

                        <kode4-select
                            name="selectrest2"
                            label="Jedi Select 2"
                            value="${this.selectrest2}"
                            value="5"
                            .optionsProvider="${optionsProvider2}"
                            hint="Default value is '2' ('Option 2')"

                            clearable
                            resettable

                            ></kode4-select>

                            <kode4-select
                            label="Jedi Autocomplete"
                            name="autocompleterest1"
                            type="autocomplete"
                            value="${this.autocompleterest1}"
                            .optionsProvider="${optionsProvider3}"
                            hint="Default value is '2' ('Option 2')"

                            clearable
                            resettable

                            required

                            ></kode4-select>
                    </div>
                </div>

                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="button" @click="${this.changeValues}">Change Values</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>

                <br><br>
                <div class="form-dirty">Form is dirty</div>
                <div class="form-clean">Form is not dirty</div>
                <hr>
                ${this.k4form?.dirty ? 'DIRRY' : 'KLEEN'}
                <hr>
            </kode4-form>

            ${this.formDirty ? 'Form is dirty' : 'Form is not dirty'}
        `;
    }

}
