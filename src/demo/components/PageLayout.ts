import { LitElement, html, css } from "lit";
import { customElement } from "lit/decorators.js";

@customElement('kode4demo-pagelayout')
export default class Kode4PageLayout extends LitElement {

    constructor() {
        super();
    }

    public render() {
        return html`
            <header>
                <a href="/" class="header-item-1">
                    <img src="assets/hwt_logo_farbe.jpg"/>
                </a>
                <div class="header-item-2"></div>
                <div class="header-item-3">
                    [ICON]
                </div>
            </header>

            <slot>
                ... HIER KOMMT DER SLOT ...
            </slot>

            <footer>
                <div class="container">
                    <div class="footer-item-1">
                        Hansen Werbetechnik GmbH
                        <br/><br/>
                        Philipp-Reis-Straße 9-11
                        <br/>
                        63303 Dreieich-Sprendlingen
                    </div>
                    <div class="footer-item-2">
                        <img src="assets/hwt_logo_grau.jpg"/>
                    </div>
                    <div class="footer-item-3"></div>
                    <div class="footer-item-4">
                        <strong>Einstellungen</strong>
                        <br><br>
                        <a href="/assets">Motive</a>
                        <br>
                        <a href="/maerkte">Märkte</a>
                        <br>
                        <a href="/halterungen">Halterungen</a>
                        <br>
                        <a href="/seitenabschlusstafeltypen">Seitenabschlusstafel-Typen</a>
                        <br>
                        <a href="/wandnavigationtypen">Wandnavigation-Typen</a>
                        <br/><br/>
                        Impressum
                        <br/>
                        Datenschutz
                        <br/><br/>
                        Hansen Werbetechnik GmbH &copy; ${(new Date()).getFullYear()} Alle Rechte vorbehalten
                    </div>
                </div>
            </footer>
        `;
    }

    public static get styles() {
        return css`
            :host {
                display: block;
            }
        `;
    }
}
