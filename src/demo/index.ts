import {HttpRequestConfig, setConfig, setHttpServiceFactory} from "@kode4/core/http/HttpService";
import {FetchHttpService} from "@kode4/core/http/FetchHttpService";

require('@kode4/ui/css/theme.css');
require('./scss/style.scss');
require('./scss/fields-theme.scss');

require('../css/theme.css');

import '@kode4/ui';
import '../';


setHttpServiceFactory(() => new FetchHttpService());

setConfig('default', new HttpRequestConfig()
    .baseUrl('')
);

import './pages/Page';
import './pages/Textfields';
import './pages/Formlogic';
import './pages/DirtyChecking';
import './pages/Buttons';
import './pages/DateTimePickers';
import './pages/CustomSelects';
import './pages/Conditions';
import './pages/DynamicForms';
import './pages/Select';
import './pages/Check';
import './pages/Required';
import './pages/FileUpload';
import './pages/ValueTests';



// import { Kode4DatetimePicker } from '../../src';

// Kode4DatetimePicker.labels = {
//     months: [
//         'January',
//         'February',
//         'March',
//         'April',
//         'May',
//         'June',
//         'July',
//         'August',
//         'September',
//         'October',
//         'November',
//         'Dezember',
//     ],
//     days: [
//         'Sunday',
//         'Monday',
//         'Tuesay',
//         'Wednesday',
//         'Thursday',
//         'Fryday',
//         'Saturday',
//     ]
// }
