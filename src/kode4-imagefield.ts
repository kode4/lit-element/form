import { html, css} from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap, ClassInfo } from "lit/directives/class-map.js";
import { styleMap } from "lit/directives/style-map.js";
import Kode4Field from "./kode4-field";
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faUpload } from '@fortawesome/free-solid-svg-icons/faUpload';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons/faCaretDown';
import { faUndoAlt } from '@fortawesome/free-solid-svg-icons/faUndoAlt';
import { Kode4DropzoneOptions, dragover, dragleave, drop} from "@kode4/ui/kode4-draggable";
import { Kode4FileUploader, Kode4FileUploaderProgressSubscriber, Kode4FileUploaderResultItem } from "./Kode4FileUploader";
import '@kode4/ui/kode4-fa-icon';

interface AspectRatio {
    width:number;
    height:number;
}

// const stringToBoolean = (str:string, force:boolean = true):boolean|string => {
//     switch(str.toLowerCase().trim()){
//         case 'true':
//         case 'yes':
//         case 'on':
//         case '1':
//             return true;
//
//         case 'false':
//         case 'no':
//         case 'off':
//         case '0':
//         case 'null':
//              return false;
//
//         default:
//             return force ? Boolean(str) : str;
//     }
// }

@customElement('kode4-imagefield')
export default class Kode4Imagefield extends Kode4Field implements Kode4FileUploaderProgressSubscriber {

    protected get typeCssClass():string {
        return 'kode4-imagefield';
    }

    @property({ type: String, attribute: 'preview-image-mode', reflect: true, converter: { 
        fromAttribute: (value:string|null) => {
            if (value?.toLowerCase() !== 'cover') {
                return 'contain';
            }
            return 'cover';
        }
    }})
    public previewImageMode:string = 'contain';

    @property({ type: String, attribute: 'mime-types', reflect: true, converter: { 
        fromAttribute: (value:string|null) => {
            if (!value || !(<string>value).length) {
                return undefined;
            }

            return value.split(',');
        },
        toAttribute: (value:Array<string>|undefined) => {
            if (!value) {
                return null;
            }
            return value.join(',');
        }
    }})
    public mimeTypes:Array<string> = ['image/*'];

    @property({ type: String, attribute: 'aspect-ratio', reflect: true, converter: { 
        fromAttribute: (value:string|null) => {
            if (!value) {
                return undefined;
            }
            if (!(<string>value).length) {
                return undefined;
            }
            const parts = value.split(':');

            if (parts.length !== 2) {
                return undefined;
            }
            return {
                width: Number(parts[0]),
                height: Number(parts[1]),
            };
        },
        toAttribute: (value:AspectRatio|undefined) => {
            if (!value) {
                return null;
            }
            return String((<AspectRatio>value).width + ':' + (<AspectRatio>value).height);
        }
    }})
    public aspectRatio?:AspectRatio;

    @property({ type: Boolean, attribute: 'placeholder-aspect-ratio' })
    public placeholderAspectRatio:boolean = false;

    @property()
    public uploading:boolean = false;

    @property()
    public progress:number = 0;

    @property({ type: String })
    protected previewThumb?:boolean|string;

    @property()
    public get value():String|Number|Boolean|undefined {
        return this._value;
    }
    
    public set value(v:String|Number|Boolean|undefined) {
        super.value = v;
        
        if (this.hasValue && typeof this.value === 'string' && this.value.length) {
            try {
                JSON.parse(String(this.value));
                this.previewThumb = undefined;
                
            } catch (e) {
                if (this.previewThumb !== String(this.value)) {
                    this.previewThumb = String(this.value);
                }
            }
        }

        // if (!this.initialized && !this.initializing) {
        //     this._valueCandidate = v;
        //     return;
        // }
        // this._value = this.sanititzeValue(v);

        // if (this.initialized) {
        //     this.dirty = this.getDirty();
        // }
    }



    @property()
    protected _fileUploader?:Kode4FileUploader;

    @property()
    public get fileUploader():Kode4FileUploader|undefined {
        return this._fileUploader;
    };

    public set fileUploader(fileUploader:Kode4FileUploader|undefined) {
        if (this._fileUploader) {
            this._fileUploader.unsubscribe(this);
        }
        
        this._fileUploader = fileUploader;
        if (this._fileUploader) {
            this._fileUploader.subscribe(this);
        }
    };

    // we don't need a property here
    private imageDragoverOptions:Kode4DropzoneOptions = {
        acceptsFiles: true
    }





    @property({ type: Boolean })
    public showFilename:boolean = false;

    @property({ type: Number })
    public size?:number;



    @property({ type: Boolean })
    public outline:boolean = false;

    @property({ type: Boolean })
    public filled:boolean = false;

    @property({ type: String })
    public placeholder:String = '';

    @property({ type: String, attribute: 'placeholder-icon', reflect: true, converter: { 
        fromAttribute: () => {
            return faUpload;
        }
    }})
    public placeholderIcon?:IconDefinition;

    @property({ type: Boolean, attribute: 'placeholder-image', reflect: true, converter: { 
        fromAttribute: (value:string|null) => {
            if (!value) {
                return false;
            }

            
            switch(value.toLowerCase().trim()){
                case 'true':
                case 'yes':
                case 'on':
                case '1':
                    return true;
                
                case 'false':
                case 'no':
                case 'off':
                case '0':
                case 'null':
                     return false;
                
                default:
                    return Boolean(value);
            }
        },
        toAttribute: (value:boolean) => {
            return String(value);
        }
    }})
    public placeholderImage:string = 'dummy';

    @property({ type: String })
    public prefixOutside:string = '';

    @property({ type: String })
    public prefix:string = '';

    @property({ type: String })
    public suffix:string = '';

    @property({ type: String })
    public suffixOutside:string = '';

    @property()
    public iconPrependOutside:IconDefinition|undefined;

    @property()
    public iconPrepend:IconDefinition|undefined;

    @property()
    public iconAppend:IconDefinition|undefined;

    @property()
    public iconAppendOutside:IconDefinition|undefined;

    @property()
    public iconReset:IconDefinition = faUndoAlt;

    @property()
    public iconClear:IconDefinition = faTimes;

    @property()
    public iconAction:IconDefinition = faCaretDown;

    @property({ type: Boolean })
    public resettable:boolean = false;

    @property({ type: Boolean })
    public action:boolean = false;

    @property({ type: Boolean })
    public clearable:boolean = false;


    @property({ type: Boolean })
    public hasDialog:boolean = false;

    @property({ type: Boolean })
    public dialogOpen:boolean = false;

    @property({ type: Boolean })
    public get isDialogOpen():boolean {
        // if (this.openDialogOnFocus && this.isFocused) {
        //     return true;
        // }
        return this.dialogOpen;
    }

    @property({ type: Boolean })
    public openDialogOnFocus:boolean = false;

    @property({ type: Boolean })
    protected get isLabelFloating():boolean {
        return this.floatingLabel && this.label && !this.hasValue && !this.showPlaceholder && !this.prefix && !this.iconPrepend;
    }

    // protected set isLabelFloating(floating:boolean) {

    // }

    @property({ type: Boolean })
    protected get showPlaceholder():boolean {
        return !this.hasValue && (Boolean(this.placeholder) || Boolean(this.placeholderIcon));
    }


    // --- INIT -----------------
    constructor() {
        super();
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
    }

    protected async doInit(updateValueFromAttribute:boolean = false) {
        super.doInit(updateValueFromAttribute);
        // if (this.outline) {
        //     this.busyStyle = 'icon';
        // }
    }


    // --- LOGIC -----------------
    public reset() {
        super.reset();
        // if (!this.changeOnType && this.input) {
        //     this.input.value = <string>this.displayValue;
        // }
    }

    public clear() {
        super.clear();
        // if (!this.changeOnType && this.input) {
        //     this.input.value = <string>this.displayValue;
        // }
    }

    protected handleEsc() {
        this.onClear();
    }



    // --- UI LOGIC -----------------
    public openDialog() {
        this.dialogOpen = true;
    }

    public closeDialog() {
        this.dialogOpen = false;
    }

    public toggleDialog() {
        this.dialogOpen ? this.closeDialog() : this.openDialog();
    }



    // --- EVENTS -----------------
    protected onFocusInput() {
        super.onFocusInput();
        
        if (this.openDialogOnFocus && !this.forceBlur) {
            this.openDialog();
        }
        
        this.inputBlurEventBlocked = true;
        this.forceBlur = false;
    }

    // protected onInputChangeDebounced = debounce(this.onInputChange, this.debounce);

    private async uploadFiles(files?:FileList) {
        try {
            if (!this.fileUploader) {
                throw new Error('FileUploader is not defined.');
            }
            
            this.updateValueAfterUpload(await this.fileUploader.upload(files));

        } catch (e) {
            this.handleOnUploadError(<Error>e);

            // fire upload error event with error:Error
        }
    }

    protected updateValueAfterUpload(result:Array<Kode4FileUploaderResultItem>) {
        const value:Array<unknown> = [];
        result.forEach((file:Kode4FileUploaderResultItem) => {
            value.push({
                uploadedFilename: file.filename,
                originalFilename: (<File>file.file).name,
                originalFilesize: (<File>file.file).size,
            });
        })
        this.value = JSON.stringify(value);
        this.onChange();

        const thumb:Kode4FileUploaderResultItem = result[0];

        var reader = new FileReader();

        reader.onload = (e:ProgressEvent) => {
            this.previewThumb = String((<FileReader>e.target!).result);
        }

        reader.readAsDataURL(thumb.file);
    }

    public handleOnBeginUpload():void {
        this.error = '';
        this.uploading = true;
        this.progress = 0;
    }

    public handleOnUploadProgress(progress:number):void {
        this.progress = progress;
    }

    public handleOnUploadComplete():void {
        this.uploading = false;
        this.progress = 0;
    }

    public handleOnUploadError(e:Error):void {
        this.error = e.message;
        this.uploading = false;
        this.progress = 0;
    }


    protected onInputChange(e:Event) {
        this.uploadFiles((<HTMLInputElement>e.target).files || undefined);
        // this.value = <String>this.input?.value;
        // this.onChange(e);
    }


    protected dialogActionBlocked:boolean|undefined = undefined;

    protected onActionMousedown(e:Event) {
        e.preventDefault();
        this.onMousedownPreventBlur();
        this.dialogActionBlocked = !this.dialogOpen;
    }

    protected onAction() {
        this.dialogActionBlocked = undefined;
    }


    // --- RENDER -----------------
    @property()
    protected get cssClasses():ClassInfo {
        const cssClasses = {
            ...super.cssClasses,
            'kode4-dialog-open': this.isDialogOpen,
            // 'kode4-focus': this.isFocused,
            // 'busy': this.busy,
            // 'kode4-field-hasValue': this.valueIsDefined,
            // 'kode4-field-empty': !this.valueIsDefined,
            'outline': this.outline,
            'filled': this.filled,
            'floatinglabel': this.isLabelFloating,
        };

        return cssClasses;
    }


    private onDropOnAssets(e:DragEvent) {
        this.uploadFiles(e.dataTransfer?.files);
    }

    protected render() {
        return html`
            <div class="component ${classMap(this.cssClasses)}">

                ${this.iconPrependOutside ? this.renderIcon(this.iconPrependOutside, "icon-prepend-outside", 'click-icon-prepend-outside') : ""}

                ${this.prefixOutside ? this.renderPrefixOutside(this.prefixOutside) : ""}

                <div class="field-container"
                
                    @dragover="${(e:DragEvent) => dragover(e, this.imageDragoverOptions)}"
                    @dragleave="${dragleave}"
                    @drop="${(e:DragEvent) => drop(e, this.onDropOnAssets.bind(this))}"
                    >
                
                    <fieldset class="field-input-container">
                        ${this.label ? this.renderLabel(this.label) : ""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend ? this.renderIcon(this.iconPrepend, "icon-prepend", 'click-icon-prepend') : ""}
                            ${this.prefix ? this.renderInlinePrefix(this.prefix) : ""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix ? this.renderInlineSuffix(this.suffix) : ""}
                            ${this.action ? this.renderActionButton() : ""}
                            ${this.resettable && this.initialValue && this.initialValue !== this.value ? this.renderResetButton() : ""}
                            ${this.clearable && this.displayValue !== '' ? this.renderClearButton() : ""}
                            ${this.iconAppend ? this.renderIcon(this.iconAppend, "icon-append", 'click-icon-append') : ""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}

                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside ? this.renderSuffixOutside(this.suffixOutside) : ""}

                ${this.iconAppendOutside ? this.renderIcon(this.iconAppendOutside, "icon-append-outside", 'click-icon-append-outside') : ""}

            </div>
        `;
    }

    protected renderIcon(icon:IconDefinition, classes?:String, eventName?:string) {
        return html`
            <label for="field" class="${classes}" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, eventName || 'click-icon')}"><kode4-fa-icon .icon="${icon}"></kode4-fa-icon></label>
        `;
    }

    protected renderInlinePrefix(text:String) {
        return html`
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-prefix')}">${text}</label>
        `;
    }

    protected renderPrefixOutside(text:String) {
        return html`
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-prefix-outside')}">${text}</label>
        `;
    }

    protected renderSuffixOutside(text:String) {
        return html`
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-suffix-outside')}">${text}</label>
        `;
    }

    protected fireIconEvent(e:Event, eventName:string) {
        let event = new CustomEvent(eventName, {
            bubbles: true,
            composed: true,
            detail: {
                aspectRatio: this.aspectRatio,
                element: this,
                originalEvent: e,
            },
        });
        this.dispatchEvent(event);
    }

    protected renderPlaceholder() {
        if (!this.showPlaceholder && this.hasValue) {
            return '';
        }
        
        const stylesContainer = {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            alignItems: 'center',
            margin: '0',
            padding: '0',
        };

        const stylesImage = {
            flex: '1 0 1px',
            textAlign: 'center',
        };

        return html `
            <div style="${styleMap(stylesContainer)}">
                ${this.placeholderAspectRatio && this.aspectRatio ? html`
                    <div style="${styleMap({ paddingTop: `calc(${this.aspectRatio!.height} / ${this.aspectRatio!.width} * 100%)`, width: '0' })}"></div>
                ` : ''}
                <div style="${styleMap(stylesImage)}" class="imagePreviewAspectRatio">
                    <div class="placeholder-container">
                        ${this.placeholderIcon ? html`<div class="placeholder-icon"><kode4-fa-icon .icon="${this.placeholderIcon}"></kode4-fa-icon></div>` : ''}
                        ${this.placeholder ? html`<div class="placeholder">${this.placeholder}</div>` : html`<div class="placeholder">&nbsp;</div>`}
                    </div>
                </div>
            </div>
        `;
    }


    protected renderPreviewThumb() {
        if (!this.previewThumb) {
            return '';
        }

        const stylesContainer = {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            alignItems: 'stretch',
        };

        const stylesImage = {
            flex: '1 0 1px',
            textAlign: 'center',
            background: 'center center no-repeat transparent',
            backgroundSize: this.previewImageMode === 'cover' ? 'cover' : 'contain',            
            backgroundImage: this.previewThumb ? `url("${(<String>this.previewThumb!)}")` : ''
        };

        return html `
            <div style="${styleMap(stylesContainer)}">
                ${this.aspectRatio ? html`
                    <div style="${styleMap({ paddingTop: `calc(${this.aspectRatio!.height} / ${this.aspectRatio!.width} * 100%)`, width: '0' })}"></div>
                    <div style="${styleMap(stylesImage)}" class="imagePreviewAspectRatio"></div>
                ` : html`
                    <img src="${this.previewThumb}" style="max-width: 100%; display: block;">
                `}
            </div>
        `;
    }

    protected renderInput() {
        return html`
            <div
                class="field-input">
                ${this.renderPlaceholder()}
                ${this.renderPreviewThumb()}
                <div
                    style="overflow: hidden !important; width: 0 !important; height: 0 !important; padding: 0 !important; margin: 0 !important; border: none !important; display: block !important;">
                    <input 
                        id="field"
                        type="file" 
                        accept="${this.mimeTypes.join(',')}"
                        @change="${this.onInputChange}"
                        @focus="${this.onFocusInput}"
                        @blur="${this.onBlurInput}"
                        >
                </div>
            </div>
            <input
                type="hidden"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                type="text"

                .value="${this.displayValue}"
                autocomplete="off"
                />
        `;
    }

    protected renderDecoratorBar() {
        if (!this.uploading || this.busyStyle !== 'linear') {
            return '';
        }
        return html`
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `;
    }

    protected renderProgressBar() {
        return this.uploading && this.busyStyle === 'linear' ? html`
            <kode4-progress progress="${this.progress}"></kode4-progress>
        ` : '';
    }

    protected renderProgressIcon() {
        return this.uploading && this.busyStyle === 'icon' ? html`
            <kode4-progress
                type="icon"
                progress="${this.progress}"
                class="icon-busy"
                @mousedown="${this.onActionMousedown}"
                ></kode4-progress>
        ` : '';
    }

    protected renderInlineSuffix(text:String) {
        return html`
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${(e:MouseEvent) => this.fireIconEvent(e, 'click-suffix')}">${text}</label>
        `;
    }

    protected renderResetButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `;
    }

    protected renderActionButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `;
    }

    protected renderClearButton() {
        return html`
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `;
    }

    protected renderDialogContainer() {
        if (!this.hasDialog) {
            return '';
        }
        return html`
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `;
    }
    protected renderDialog() {
        return html`
        `;
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            .field-input-container {
                overflow: hidden;
            }





            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                margin-left: -9px;
                margin-right: -9px;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }




            .placeholder-container {
                dispplay: block;
            }

            .placeholder-icon {
                dispplay: block;
                font-size: 2.0em;
            }
            
            .placeholder {
                dispplay: block;
            }

            .placeholder-icon + .placeholder {
                margin-top: 0.5em;
            }

        `;
    }
    
}
