import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4Field from "./kode4-field";
import debounce from "lodash-es/debounce";
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons/faCaretDown';
import '@kode4/ui/kode4-fa-icon';

@customElement('kode4-slider')
export default class Kode4Slider extends Kode4Field {

    protected get typeCssClass():string {
        return 'kode4-slider';
    }

    @property()
    public placeholder:String = '';

    @property()
    public labelPrefix:String = '';

    @property()
    public labelSuffix:String = '';

    @property()
    public iconPrependOutside:IconDefinition|undefined;

    @property()
    public iconPrepend:IconDefinition|undefined;

    @property()
    public iconAppend:IconDefinition|undefined;

    @property()
    public iconAppendOutside:IconDefinition|undefined;

    @property()
    public iconClear:IconDefinition = faTimes;

    @property()
    public iconAction:IconDefinition = faCaretDown;

    @property({ type: Boolean })
    public action:Boolean = false;

    @property({ type: Boolean })
    public clearable:Boolean = false;

    @property({ type: Boolean })
    public changeOnType:Boolean = false;

    @property({ type: Boolean })
    public counter:Boolean = false;

    @property()
    public get charCount():Number {
        return (<String>this.value).length;
    }

    @property({ type: Boolean })
    public hasDialog:Boolean = false;

    @property({ type: Boolean })
    public dialogOpen:Boolean = false;

    @property({ type: Boolean })
    public confirmOnEnter:Boolean = true;



    // --- LOGIC -----------------
    handleEsc() {
        this.onClear();
    }



    // --- EVENTS -----------------
    protected onInputChange(e:Event) {
        this.value = <String>this.input?.value;
        this.onChange(e);
    }

    onKeyup(e:KeyboardEvent) {
        if (e.key === "Escape") {
            this.handleEsc();

        } else if (e.key === "Enter" && this.confirmOnEnter) {
            this.onSubmit(e);

        } else if (this.changeOnType) {
            this.onInputChange(e);
        }
    }

    onAction() {
    }

    // --- RENDER -----------------
    protected render() {
        return html`
            <div class="kode4-field ${this.dialogOpen ? 'kode4-dialog-open' : ''} ${this.isFocused ? 'kode4-focus' : ''}">
                ${this.label ? this.renderLabel(this.label) : ""}
                <div class="kode4-field-control">
                    ${this.iconPrependOutside ? this.renderIcon(this.iconPrependOutside, "icon-prepend-outside") : ""}
                    <div class="kode4-field-control-box">
                        <label for="field" class="kode4-field-control-inside">
                            ${this.iconPrepend ? this.renderIcon(this.iconPrepend, "icon-prepend") : ""}
                            ${this.labelPrefix ? this.renderInlinePrefix(this.labelPrefix) : ""}
                            ${this.renderInput()}
                            ${this.labelSuffix ? this.renderInlineSuffix(this.labelSuffix) : ""}
                            ${this.action ? this.renderActionButton() : ""}
                            ${this.clearable ? this.renderClearButton() : ""}
                            ${this.iconAppend ? this.renderIcon(this.iconAppend, "icon-append") : ""}
                        </label>
                        <div class="kode4-field-border"></div>
                        <div class="kode4-field-progress"></div>
                        <div class="kode4-field-hint">
                            <div class="kode4-field-hint-left">
                                ${this.hint ? this.renderHint() : ""}
                            </div>
                            <div class="kode4-field-hint-right">
                                ${this.counter ? this.renderCounter() : ""}
                            </div>
                        </div>
                        <div class="kode4-field-error">
                            ${this.error ? this.renderError() : ""}
                        </div>
                        <div class="kode4-field-dialog">
                            ${this.hasDialog ? this.renderDialog() : ""}
                        </div>
                    </div>
                    ${this.iconAppendOutside ? this.renderIcon(this.iconAppendOutside, "icon-append-outside") : ""}
                </div>
            </div>
        `;
    }


    renderIcon(icon:IconDefinition, classes?:String) {
        return html`
            <label for="field" class="${classes}"><kode4-fa-icon .icon="${icon}"></kode4-fa-icon></label>
        `;
    }

    renderInlinePrefix(text:String) {
        return html`
            <label class="field-prefix" for="field">${text}</label>
        `;
    }

    protected renderInput() {
        return html`
            <input
                class="field-control-input"
                ?name="${this.name}"
                ?readonly="${this.readonly || this.controlReadonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"
                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${debounce(this.onKeyup, <number>this.debounce)}"
                />
        `;
    }

    renderInlineSuffix(text:String) {
        return html`
            <label class="field-suffix" for="field">${text}</label>
        `;
    }

    renderActionButton() {
        return html`
            <kode4-fa-icon .icon="${this.iconAction}" class="icon-action" @click="${this.onAction}"></kode4-fa-icon>
        `;
    }

    renderClearButton() {
        return html`
            <kode4-fa-icon .icon="${this.iconClear}" class="icon-clear" @click="${this.onClear}"></kode4-fa-icon>
        `;
    }

    renderCounter() {
        return html`
            <span class="field-hint-counter">${this.charCount}</span>
        `;
    }

    renderDialog() {
        return html`
        `;
    }



    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            .kode4-field-control {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            .kode4-field-control-box {
                flex: 1 0 1px;
                position: relative;
            }

            .kode4-field-control-inside {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
                border-top: solid 1px var(--kode4-field-border-color);
                border-left: solid 1px var(--kode4-field-border-color);
                border-right: solid 1px var(--kode4-field-border-color);
                background-color: var(--kode4-field-background-color);
                color: var(--kode4-field-color);
                transition: all 0.15s linear;
                padding-left: var(--kode4-field-padding-left);
                padding-right: var(--kode4-field-padding-right);
                padding-left: var(--kode4-field-padding-left);
                padding-right: var(--kode4-field-padding-right);
            }

            .field-control-input {
                flex: 1 0 1px;
                outline: none;
                border: none;
                background: transparent;
                color: inherit;
                font: inherit;
                margin: 0;
                padding-top: var(--kode4-field-padding-top);
                padding-bottom: var(--kode4-field-padding-bottom);
            }

            .field-control-input::placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .kode4-field-border {
                background-color: var(--kode4-field-focus-bar-color);
                /* height: 0; */
                height: 2px;
                /* margin-bottom: 2px; */
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
            }

            .kode4-focus .kode4-field-control-inside {
                border-top-color: var(--kode4-field-focus-border-color);
                border-left-color: var(--kode4-field-focus-border-color);
                border-right-color: var(--kode4-field-focus-border-color);
                background-color: var(--kode4-field-focus-background-color);
            }

            .kode4-focus .kode4-field-border {
                width: 100%;
                /* height: 2px; */
                /* margin-bottom: 0; */
            }

            .icon-action {
                cursor: pointer;
                margin-left: 10px;
            }

            .icon-clear {
                cursor: pointer;
                margin-left: 10px;
            }

            .icon-prepend-outside {
                align-self: flex-start;
                margin-right: 10px;
                padding-top: 12px;
            }

            .icon-prepend {
                margin-right: 10px;
            }

            .icon-append {
                margin-left: 10px;
            }

            .icon-append-outside {
                align-self: flex-start;
                margin-left: 10px;
                padding-top: 12px;
            }

            .field-prefix {
                display: inline-block;
                margin-right: 10px;
            }

            .field-suffix {
                display: inline-block;
                margin-left: 10px;
            }

            .field-label {
                color: var(--kode4-field-label-color);
            }

            .kode4-field-dialog {
                opacity: 0;
                transition: all 0.1s linear;
                z-index: -100;
                position: absolute;
                top: 100%;
                left: 0;
                right: 0;

                background-color: var(--kode4-field-focus-background-color);
                display: none;
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: 1;
                z-index: 100;
                display: block;
            }
        `;
    }

}
