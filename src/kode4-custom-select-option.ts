import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4CustomSelect from "./kode4-custom-select";
import { v4 as uuidv4 } from "uuid";

@customElement('kode4-custom-select-option')
export default class Kode4CustomSelectOption extends LitElement {

    /**
     * This field is required because apparently we cannot
     * dispatch event on "this" in disconnectedCallback.
     * But we can on an element outside "this", so we save
     * a reference to the parent element.
     */
    private kode4CustomSelect?:Kode4CustomSelect|HTMLElement;

    @property({ type: String })
    protected value?:string;

    @property({ type: Boolean})
    protected selected:boolean = false;

    constructor() {
        super();
        this.setAttribute('id', this.id);
    }

    public connectedCallback() {
        if (!this.id) {
            this.id = `kode4field_${uuidv4()}`;
        }

        super.connectedCallback()
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);

        this.kode4CustomSelect = <Kode4CustomSelect>this.closest('kode4-custom-select');

        this.addEventListener('click', this.onSelect.bind(this));
        this.classList.remove('kode4-custom-option-selected')
        this.fireRegisterEvent();
    }

    protected fireRegisterEvent() {
        let event = new CustomEvent("kode4-custom-select-option-created", {
            bubbles: true,
            composed: true,
            detail: {
                field: this,
            }
        });

        this.dispatchEvent(event);
    }


    public disconnectedCallback() {
        let event = new CustomEvent("kode4-custom-select-option-destroyed", {
            bubbles: false,
            composed: true,
            detail: {
                fieldid: this.id,
            }
        });
        this.kode4CustomSelect?.dispatchEvent(event);

        super.disconnectedCallback();
    }

    protected onSelect() {
        this.selected = true;
        this.classList.add('kode4-custom-option-selected')
        this.dispatchEvent(new CustomEvent("customOptionSelected", {
            bubbles: true,
            composed: true,
                detail: {
                value: this.value,
            }
        }));
    }

    public onOptionSelected(value:any) {
        const wasSelected = this.selected;
        this.selected = this.value == value;
        if (wasSelected !== this.selected) {
            this.selected ? this.classList.add('kode4-custom-option-selected') : this.classList.remove('kode4-custom-option-selected')
        }
    }

    public render() {
        return html`
            <slot></slot>
        `;
    }

    // --- STYLES -----------------
    static get styles() {
        return css`
            :host {
                flex: 1 0 1px;
                overflow: hidden;
                cursor: pointer;
                transition: all 0.3s ease-in-out;
                box-sizing: border-box;
            }
            
            :host(.kode4-custom-option-grayscale:not(.kode4-custom-option-selected)) {
                filter: grayscale(100%);
            }

            :host(.kode4-custom-option-grayscale:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-grayscale.kode4-custom-option-selected) {
                filter: grayscale(0);
            }

            :host(.kode4-custom-option-opacity:not(.kode4-custom-option-selected)) {                
                opacity: var(--kode4-customselect-option-deselected-opacity);
            }
            
            :host(.kode4-custom-option-opacity:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-opacity.kode4-custom-option-selected) {
                opacity: var(--kode4-customselect-option-selected-opacity);
            }
        `;
    }

}
