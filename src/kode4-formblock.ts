import { html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";

@customElement('kode4-formblock')
export default class Kode4Formblock extends Kode4Field {

    protected get typeCssClass():string {
        return 'kode4-formblock';
    }

    constructor() {
        super();
        this.canValidate = false;
    }

    @property()
    public get value():String|Number|Boolean|undefined {
        return undefined;
    }

    // @ts-ignore
    public set value(v:String|Number|Boolean|undefined) {}

    // --- RENDER -----------------
    protected render() {
        return html`
            <div class="component ${classMap(this.cssClasses)}">
                <slot style="border: solid 2px #ccc;"></slot>
            </div>
        `;
    }

}
