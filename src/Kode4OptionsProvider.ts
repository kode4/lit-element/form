// import axios from "axios";

import {ConvertableDynamicCustomParams, httpServiceFactory} from "@kode4/core/http/HttpService";

export interface Kode4OptionsProviderSubscriber {
    handleListUpdated: Function;
}

export interface Kode4Option {
    value: String | Number;
    label: String | Element;
    unsafe?: Boolean;
}

export interface Kode4ListResponse {
    total?: Number;
    list?: Array<any>;
    infoMessage?: String;
    errorMessage?: String;
    error: Number;
}

export interface Kode4OptionsProvider {

    busy:boolean;

    noSelection?:String;
    
    filters?:FormData;

    subscribe(subscriber:Kode4OptionsProviderSubscriber):void;

    unsubscribe(subscriber:Kode4OptionsProviderSubscriber):void;

    getList(filters?:FormData):Promise<any>;

    checkItemChangedAfterListUpdate(primaryKey:String, item:Kode4Option):Promise<Kode4Option|boolean>;

    getItem(primaryKey:String):Promise<any>;
}

export class Kode4StaticOptionsProvider implements Kode4OptionsProvider {

    public busy = false;

    public noSelection?:String;

    public valueField?:string;

    public labelField?:string;

    public list:Array<Kode4Option> = [];

    private subscribers:Array<Kode4OptionsProviderSubscriber> = [];

    public constructor(list?:Array<Kode4Option>) {
        if (list) {
            this.setList(list);
        }
    }

    public subscribe(subscriber:Kode4OptionsProviderSubscriber) {
        if (!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }

    public unsubscribe(subscriber:Kode4OptionsProviderSubscriber) {
        if (this.subscribers.includes(subscriber)) {
            const i = this.subscribers.indexOf(subscriber);
            this.subscribers.splice(i, 1);
        }
    }

    public fireListChanged() {
        this.subscribers.map((el:Kode4OptionsProviderSubscriber) => {
            el.handleListUpdated();
        });
    }

    public clearList() {
        this.list = [];
        this.fireListChanged();
}

    public setList(list:Array<Kode4Option>) {
        if (list.length && <any>list !== <any>this.list) {
            this.list = list;
            this.fireListChanged();
        }
    }

    public applyListFilters(list:Array<Kode4Option>) {
        return list;
    }

    public async getList(filters?:FormData):Promise<any> {
        let list;
        if (this.valueField || this.labelField) {
            list = this.list.map((el:any) => {
                if (this.valueField) {
                    el['value'] = el[this.valueField];
                }
                if (this.labelField) {
                    el['label'] = el[this.labelField];
                }
                return el;
            });
        } else {
            list = this.list;
        }

        list = filters && filters.has('searchterm') ? list.filter((el:Kode4Option) => el.label === filters.get('searchterm')) : list;
        list = this.applyListFilters(list);

        if (this.noSelection) {
            (<Array<any>>list).unshift({
                label: this.noSelection,
                unsafe: false
            });
        }

        return {
            error: 0,
            list
        };
    }

    public async checkItemChangedAfterListUpdate(primaryKey:String, item?:Kode4Option) {
        try {
            const newItem = await this.getItem(primaryKey);
            if ((newItem && !item) || (newItem?.label != item?.label)) {
                return newItem;
            }
            return false;

        } catch (e) {
            // item is not in list. return false? or unset value?
            return false;
        }
    }

    public async getItem(primaryKey:String):Promise<any> {
        const item = this.list.reduce((result:Kode4Option|undefined, el:Kode4Option) => {
            if (el.value == primaryKey) {
                result = el;
            }
            return result;
        }, undefined);

        if (item === undefined) {
            throw new Error('Item not found for '+primaryKey);
        }

        if (this.valueField) {
            (<any>item)['value'] = (<any>item)[this.valueField];
        }
        if (this.labelField) {
            (<any>item)['label'] = (<any>item)[this.labelField];
        }

        return item;
    }

}

export class Kode4RestOptionsProvider implements Kode4OptionsProvider {

    public httpConfigBase:string = 'default';

    public baseUrl:String = '';

    public listCacheEnabled:boolean = true;
    
    public listCache:Array<Kode4Option> = [];

    public listUrl:String = '';

    public itemUrl:String = '';

    public valueField?:string;

    public labelField?:string;

    public noSelection?:String;

    public get busy():boolean {
        return this.busyList || this.busyItem;
    }

    protected busyList:boolean = false;

    protected busyItem:boolean = false;

    protected lastRequestHash?:string;

    private subscribers:Array<Kode4OptionsProviderSubscriber> = [];

    constructor(httpConfigBase: string = 'default') {
        this.httpConfigBase = httpConfigBase;
    }

    public subscribe(subscriber:Kode4OptionsProviderSubscriber) {
        if (!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }

    public unsubscribe(subscriber:Kode4OptionsProviderSubscriber) {
        if (this.subscribers.includes(subscriber)) {
            const i = this.subscribers.indexOf(subscriber);
            this.subscribers.splice(i, 1);
        }
    }

    public fireListChanged() {
        this.subscribers.map((el:Kode4OptionsProviderSubscriber) => {
            el.handleListUpdated();
        });
    }

    public applyRequestFilters(params:any) {
        return params;
    }

    public applyListFilters(list:Array<Kode4Option>) {
        return list;
    }

    public async getList(filters?:FormData):Promise<any> {
        try {
            // if (filters) {
            //     const queryString = (new URLSearchParams(filters) ).toString()
            // }

            const params:ConvertableDynamicCustomParams = {
                searchterm: String(filters?.get('searchterm'))
            }

            const newRequestHash:string = JSON.stringify(params);

            if (this.lastRequestHash === newRequestHash && this.listCache) {
                return {
                    total: this.listCache.length,
                    list: this.listCache,
                    error: 0,
                };
            }
            this.lastRequestHash = newRequestHash;

            this.busyList = true;

            const response:any = (await httpServiceFactory()
                .config(this.httpConfigBase)
                .url(`${this.baseUrl}${this.listUrl}`)
                .param(params)
                .get());

            console.log('[Kode4RestOptionsProvider] response', response);
            const lr = {
                list: response.data?.list || response.data,
                error: 0,
            }

            // // let data = await axios.get(`${this.baseUrl}${this.itemUrl}`, {
            // //     params: this.applyRequestFilters(params)
            // // });
            // let data = {
            //     data: {},
            // };
            // if (!data.data || typeof data.data !== 'object') {
            //     throw Error('Invalid result: '+typeof data);
            // }
            //
            // const lr:Kode4ListResponse = <Kode4ListResponse>data.data;

            // if (!lr.list) {
            //     lr.list = [];
            // }

            if (!Array.isArray(lr.list)) {
                throw Error('Invalid list result: '+typeof lr.list);
            }

            if (this.valueField || this.labelField) {
                lr.list = lr.list.map((el:any) => {
                    if (this.valueField) {
                        el['value'] = el[this.valueField];
                    }
                    if (this.labelField) {
                        el['label'] = el[this.labelField];
                    }
                    return el;
                });
            }

            lr.list = this.applyListFilters(lr.list);

            if (this.listCacheEnabled) {
                this.listCache = this.listCache.concat(lr.list);
            }

            if (this.noSelection) {
                (<Array<any>>lr.list).unshift({
                    label: this.noSelection,
                    unsafe: false
                });
            }
            this.busyList = false;
            return lr;

        } finally {
            this.busyList = false;
        }
    }

    public async checkItemChangedAfterListUpdate(/*primaryKey:String, item:Kode4Option*/) {
        // TODO:
        //      - check if item is in list
        //          - YES: return newItem != listItem
        //          - NO: return false
        return false;
    }

    protected getItemFromListCache(primaryKey:String) {
        return this.listCache.find((el:Kode4Option) => String(el.value) === primaryKey);
    }

    public clearListCache() {
        this.listCache = [];
    }

    public async getItem(primaryKey:String):Promise<any> {
        try {
            if (!primaryKey) {
                throw new Error('primary Key is required');
            }

            if (this.listCacheEnabled) {
                const cachedItem = this.getItemFromListCache(primaryKey);
                if (cachedItem !== undefined) {
                    return cachedItem;
                }
            }

            this.busyItem = true;

            const data:any = (await httpServiceFactory()
                .config(this.httpConfigBase)
                .url(`${this.baseUrl}${this.itemUrl}/${primaryKey}`)
                .get()).data;

            // let data = await axios.get(`${this.baseUrl}${this.itemUrl}/${primaryKey}`);
            // let data = {
            //     data: {
            //         value: '',
            //         label: '',
            //     },
            // };
            const item:Kode4Option = data.data;

            if (this.valueField) {
                (<any>item)['value'] = (<any>item)[this.valueField];
            }
            if (this.labelField) {
                (<any>item)['label'] = (<any>item)[this.labelField];
            }

            this.busyItem = false;
            this.listCache.push(item);
            return item;

        } finally {
            this.busyList = false;
        }

    }

}

// export class Kode4SelectOptionsProvider implements Kode4optionsProvider {

//     public baseUrl:String = '';

//     public listUrl:String = '';

//     public itemUrl:String = '';

//     public noSelection?:String;

//     public get busy():boolean {
//         return this.busyList || this.busyItem;
//     }

//     protected busyList:boolean = false;

//     protected busyItem:boolean = false;

//     public async getList(filters?:FormData):Promise<any> {
//         try {
//             this.busyList = true;
//             // if (filters) {
//             //     const queryString = (new URLSearchParams(filters) ).toString()
//             // }
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}`, {
//                 params: {
//                     searchterm: filters?.get('searchterm')
//                 }
//             });
//             if (!data.data || typeof data.data !== 'object') {
//                 throw Error('Invalid result: '+typeof data);
//             }

//             const lr:Kode4ListResponse = <Kode4ListResponse>data.data;

//             if (!lr.list) {
//                 lr.list = [];
//             }
//             if (!Array.isArray(lr.list)) {
//                 throw Error('Invalid list result: '+typeof lr.list);
//             }

//             if (this.noSelection) {
//                 (<Array<any>>lr.list).unshift({
//                     label: this.noSelection,
//                     unsafe: false
//                 });
//             }
//             this.busyList = false;
//             return lr;

//         } catch (e) {
//             this.busyList = false;
//             throw e;
//             return [];
//         }
//     }

//     public async getItem(primaryKey:String):Promise<any> {
//         try {
//             if (!primaryKey) {
//                 throw new Error('primary Key is required');
//             }
//             this.busyItem = true;
//             let data = await axios.get(`${this.baseUrl}${this.itemUrl}/${primaryKey}`);
//             data = data.data;
//             this.busyItem = false;
//             return data;

//         } catch (e) {
//             this.busyItem = false;
//             throw e;
//         }

//     }

// }
