import { html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import Kode4Field from "./kode4-field";
import { faSquare } from '@fortawesome/free-regular-svg-icons/faSquare';
import { faCheckSquare } from '@fortawesome/free-regular-svg-icons/faCheckSquare';
import '@kode4/ui/kode4-fa-icon';

@customElement('kode4-check')
export default class Kode4Check extends Kode4Field {

    @property()
    protected trueValue:String|Number|Boolean|undefined = true;

    @property()
    protected falseValue:String|Number|Boolean|undefined = false;

    protected get typeCssClass():string {
        return 'kode4-check';
    }

    protected firstUpdated(changedProperties:any) {
        super.firstUpdated(changedProperties);
        (<HTMLInputElement>this.input).autofocus = true;
    }

    @property()
    protected get displayValue():String|Number|Boolean|undefined {
        return <Boolean>this.value;
    }
    
    @property()
    public get stringValue() {  
        return String(this.value ? this.trueValue : this.falseValue);
    }
    
    protected sanititzeValue(v:any):String|Number|Boolean|undefined {
        const tp = (typeof v);
        if (tp === 'string') {
            const vlc = v.toLowerCase();
            const c = vlc !== 'false' && vlc !== 'off' && vlc !== 'null' && vlc !== 'undefined' && vlc !== '0';
            return c;
        
        } else if (tp === 'number') {
            return v > 0;
        }

        return <Boolean>v;
    }


    // --- INIT -----------------
    constructor() {
        super();
        this.type = 'checkbox';
        this.value = false;
        // this.submitEmpty = true;
    }

    // --- LOGIC -----------------
    handleEsc() {
        this.onClear();
    }



    // --- EVENTS -----------------
    protected onInputChange(e:Event) {
        this.value = <Boolean>this.input?.checked;
        this.onChange(e);
    }


    // --- RENDER -----------------
    protected render() {
        return html`
            <div class="component ${classMap(this.cssClasses)}">
                ${this.renderInput()}
                <label class="icon-unchecked" for="field">
                    <kode4-fa-icon .icon="${faSquare}"></kode4-fa-icon>
                </label>
                <label class="icon-checked" for="field">
                    <kode4-fa-icon .icon="${faCheckSquare}"></kode4-fa-icon>
                </label>
                <div class="kode4-field-box">
                    ${this.label ? this.renderLabel(this.label) : ""}



                    ${this.renderMessages()}

                </div>
            </div>
        `;
    }

    protected renderInput() {
        return html`
            <input
                class="field-input"
                value="1"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="checkbox"
                .checked="${this.displayValue}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                />
        `;
    }

    renderLabel(text:String) {
        return html`
            <label class="field-label-block" for="field">${text}</label>
        `;
    }


    // --- STYLES -----------------
    static get styles() {
        return css`
            ${super.styles}

            .field-input {
                display: inline-block;
                margin: 0;
                padding: 0;
                width: 0;
                min-width: 0;
                width: 0;
                min-height: 0;
                opacity: 0;
                flex: 0 1 0px;
            }

            .field-input:not(:checked) ~ .icon-checked {
                display: none;
            }

            .field-input:checked ~ .icon-unchecked {
                display: none;
            }

            .kode4-field {
                display: flex;
                flex-direction: row;
            }

            .kode4-field-box {
                flex: 1 0 1px;
                margin-left: 10px;
            }

            .kode4-focus .icon-checked,
            .kode4-focus .icon-unchecked,
            .kode4-focus .field-label {
                color: var(--kode4-field-decorator-color);
            }

            .icon-checked,
            .icon-unchecked {
                margin-top: var(--kode4-field-check-icon-spacing);
            }

            .field-label {
                transform: translate(0, 0) scale(1);
            }

            .field-label-block {
                display: block;
                line-height: var(--kode4-field-check-line-height, inherit);
            }
        `;
    }

}
