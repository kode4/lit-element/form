/*! For license information please see main.506656779c76db387164.bundle.js.LICENSE.txt */
"use strict";(self.webpackChunk_kode4_form=self.webpackChunk_kode4_form||[]).push([[179],{480:(e,t,i)=>{var o=i(712);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.a3=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},801:(e,t)=>{var i="square",o=[9723,9724,61590,9632],n="f0c8",r="M384 32C419.3 32 448 60.65 448 96V416C448 451.3 419.3 480 384 480H64C28.65 480 0 451.3 0 416V96C0 60.65 28.65 32 64 32H384zM384 80H64C55.16 80 48 87.16 48 96V416C48 424.8 55.16 432 64 432H384C392.8 432 400 424.8 400 416V96C400 87.16 392.8 80 384 80z";t.DF={prefix:"far",iconName:i,icon:[448,512,o,n,r]},t.pL=t.DF},712:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="square-check",o=[9989,61510,9745,"check-square"],n="f14a",r="M211.8 339.8C200.9 350.7 183.1 350.7 172.2 339.8L108.2 275.8C97.27 264.9 97.27 247.1 108.2 236.2C119.1 225.3 136.9 225.3 147.8 236.2L192 280.4L300.2 172.2C311.1 161.3 328.9 161.3 339.8 172.2C350.7 183.1 350.7 200.9 339.8 211.8L211.8 339.8zM0 96C0 60.65 28.65 32 64 32H384C419.3 32 448 60.65 448 96V416C448 451.3 419.3 480 384 480H64C28.65 480 0 451.3 0 416V96zM48 96V416C48 424.8 55.16 432 64 432H384C392.8 432 400 424.8 400 416V96C400 87.16 392.8 80 384 80H64C55.16 80 48 87.16 48 96z";t.definition={prefix:"far",iconName:i,icon:[448,512,o,n,r]},t.faSquareCheck=t.definition,t.prefix="far",t.iconName=i,t.width=448,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},322:(e,t)=>{var i="calendar",o=[128198,128197],n="f133",r="M96 32C96 14.33 110.3 0 128 0C145.7 0 160 14.33 160 32V64H288V32C288 14.33 302.3 0 320 0C337.7 0 352 14.33 352 32V64H400C426.5 64 448 85.49 448 112V160H0V112C0 85.49 21.49 64 48 64H96V32zM448 464C448 490.5 426.5 512 400 512H48C21.49 512 0 490.5 0 464V192H448V464z";t.DF={prefix:"fas",iconName:i,icon:[448,512,o,n,r]},t.fT=t.DF},421:(e,t)=>{var i="caret-down",o=[],n="f0d7",r="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z";t.DF={prefix:"fas",iconName:i,icon:[320,512,o,n,r]},t.eW=t.DF},606:(e,t)=>{var i="chevron-left",o=[9001],n="f053",r="M224 480c-8.188 0-16.38-3.125-22.62-9.375l-192-192c-12.5-12.5-12.5-32.75 0-45.25l192-192c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L77.25 256l169.4 169.4c12.5 12.5 12.5 32.75 0 45.25C240.4 476.9 232.2 480 224 480z";t.DF={prefix:"fas",iconName:i,icon:[320,512,o,n,r]},t.A3=t.DF},394:(e,t)=>{var i="chevron-right",o=[9002],n="f054",r="M96 480c-8.188 0-16.38-3.125-22.62-9.375c-12.5-12.5-12.5-32.75 0-45.25L242.8 256L73.38 86.63c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25l-192 192C112.4 476.9 104.2 480 96 480z";t.DF={prefix:"fas",iconName:i,icon:[320,512,o,n,r]},t._t=t.DF},955:(e,t)=>{var i="circle-notch",o=[],n="f1ce",r="M222.7 32.15C227.7 49.08 218.1 66.9 201.1 71.94C121.8 95.55 64 169.1 64 255.1C64 362 149.1 447.1 256 447.1C362 447.1 448 362 448 255.1C448 169.1 390.2 95.55 310.9 71.94C293.9 66.9 284.3 49.08 289.3 32.15C294.4 15.21 312.2 5.562 329.1 10.6C434.9 42.07 512 139.1 512 255.1C512 397.4 397.4 511.1 256 511.1C114.6 511.1 0 397.4 0 255.1C0 139.1 77.15 42.07 182.9 10.6C199.8 5.562 217.6 15.21 222.7 32.15V32.15z";t.DF={prefix:"fas",iconName:i,icon:[512,512,o,n,r]},t.IJ=t.DF},349:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="down-long",o=["long-arrow-alt-down"],n="f309",r="M281.6 392.3l-104 112.1c-9.498 10.24-25.69 10.24-35.19 0l-104-112.1c-6.484-6.992-8.219-17.18-4.404-25.94c3.811-8.758 12.45-14.42 21.1-14.42H128V32c0-17.69 14.33-32 32-32S192 14.31 192 32v319.9h72c9.547 0 18.19 5.66 22 14.42C289.8 375.1 288.1 385.3 281.6 392.3z";t.definition={prefix:"fas",iconName:i,icon:[320,512,o,n,r]},t.faDownLong=t.definition,t.prefix="fas",t.iconName=i,t.width=320,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},864:(e,t,i)=>{var o=i(798);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.iV=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},798:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="ellipsis-vertical",o=["ellipsis-v"],n="f142",r="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z";t.definition={prefix:"fas",iconName:i,icon:[128,512,o,n,r]},t.faEllipsisVertical=t.definition,t.prefix="fas",t.iconName=i,t.width=128,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},913:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="file-arrow-up",o=["file-upload"],n="f574",r="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM288.1 344.1C284.3 349.7 278.2 352 272 352s-12.28-2.344-16.97-7.031L216 305.9V408c0 13.25-10.75 24-24 24s-24-10.75-24-24V305.9l-39.03 39.03c-9.375 9.375-24.56 9.375-33.94 0s-9.375-24.56 0-33.94l80-80c9.375-9.375 24.56-9.375 33.94 0l80 80C298.3 320.4 298.3 335.6 288.1 344.1z";t.definition={prefix:"fas",iconName:i,icon:[384,512,o,n,r]},t.faFileArrowUp=t.definition,t.prefix="fas",t.iconName=i,t.width=384,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},124:(e,t,i)=>{var o=i(913);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.Y9=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},176:(e,t)=>{var i=[128273],o="f084",n="M282.3 343.7L248.1 376.1C244.5 381.5 238.4 384 232 384H192V424C192 437.3 181.3 448 168 448H128V488C128 501.3 117.3 512 104 512H24C10.75 512 0 501.3 0 488V408C0 401.6 2.529 395.5 7.029 391L168.3 229.7C162.9 212.8 160 194.7 160 176C160 78.8 238.8 0 336 0C433.2 0 512 78.8 512 176C512 273.2 433.2 352 336 352C317.3 352 299.2 349.1 282.3 343.7zM376 176C398.1 176 416 158.1 416 136C416 113.9 398.1 96 376 96C353.9 96 336 113.9 336 136C336 158.1 353.9 176 376 176z";t.DF={prefix:"fas",iconName:"key",icon:[512,512,i,o,n]},t.DD=t.DF},609:(e,t)=>{var i="lock",o=[128274],n="f023",r="M80 192V144C80 64.47 144.5 0 224 0C303.5 0 368 64.47 368 144V192H384C419.3 192 448 220.7 448 256V448C448 483.3 419.3 512 384 512H64C28.65 512 0 483.3 0 448V256C0 220.7 28.65 192 64 192H80zM144 192H304V144C304 99.82 268.2 64 224 64C179.8 64 144 99.82 144 144V192z";t.DF={prefix:"fas",iconName:i,icon:[448,512,o,n,r]},t.by=t.DF},566:(e,t,i)=>{var o=i(349);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.HH=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},982:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="magnifying-glass",o=[128269,"search"],n="f002",r="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z";t.definition={prefix:"fas",iconName:i,icon:[512,512,o,n,r]},t.faMagnifyingGlass=t.definition,t.prefix="fas",t.iconName=i,t.width=512,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},946:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="rotate-left",o=["rotate-back","rotate-backward","undo-alt"],n="f2ea",r="M480 256c0 123.4-100.5 223.9-223.9 223.9c-48.84 0-95.17-15.58-134.2-44.86c-14.12-10.59-16.97-30.66-6.375-44.81c10.59-14.12 30.62-16.94 44.81-6.375c27.84 20.91 61 31.94 95.88 31.94C344.3 415.8 416 344.1 416 256s-71.69-159.8-159.8-159.8c-37.46 0-73.09 13.49-101.3 36.64l45.12 45.14c17.01 17.02 4.955 46.1-19.1 46.1H35.17C24.58 224.1 16 215.5 16 204.9V59.04c0-24.04 29.07-36.08 46.07-19.07l47.6 47.63C149.9 52.71 201.5 32.11 256.1 32.11C379.5 32.11 480 132.6 480 256z";t.definition={prefix:"fas",iconName:i,icon:[512,512,o,n,r]},t.faRotateLeft=t.definition,t.prefix="fas",t.iconName=i,t.width=512,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},337:(e,t,i)=>{var o=i(982);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.wn=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},545:(e,t,i)=>{var o=i(709);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.NB=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},24:(e,t,i)=>{var o=i(946);t.DF={prefix:o.prefix,iconName:o.iconName,icon:[o.width,o.height,o.aliases,o.unicode,o.svgPathData]},t.R8=t.DF,o.prefix,o.iconName,o.width,o.height,o.aliases,o.unicode,o.svgPathData,o.aliases},723:(e,t)=>{var i="upload",o=[],n="f093",r="M105.4 182.6c12.5 12.49 32.76 12.5 45.25 .001L224 109.3V352c0 17.67 14.33 32 32 32c17.67 0 32-14.33 32-32V109.3l73.38 73.38c12.49 12.49 32.75 12.49 45.25-.001c12.49-12.49 12.49-32.75 0-45.25l-128-128C272.4 3.125 264.2 0 256 0S239.6 3.125 233.4 9.375L105.4 137.4C92.88 149.9 92.88 170.1 105.4 182.6zM480 352h-160c0 35.35-28.65 64-64 64s-64-28.65-64-64H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456z";t.DF={prefix:"fas",iconName:i,icon:[512,512,o,n,r]},t.cf=t.DF},879:(e,t)=>{var i="user",o=[62144,128100],n="f007",r="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z";t.DF={prefix:"fas",iconName:i,icon:[448,512,o,n,r]},t.IL=t.DF},709:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0});var i="xmark",o=[128473,10005,10006,10060,215,"close","multiply","remove","times"],n="f00d",r="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z";t.definition={prefix:"fas",iconName:i,icon:[320,512,o,n,r]},t.faXmark=t.definition,t.prefix="fas",t.iconName=i,t.width=320,t.height=512,t.ligatures=o,t.unicode=n,t.svgPathData=r,t.aliases=o},20:e=>{var t="%[a-f0-9]{2}",i=new RegExp(t,"gi"),o=new RegExp("("+t+")+","gi");function n(e,t){try{return decodeURIComponent(e.join(""))}catch(e){}if(1===e.length)return e;t=t||1;var i=e.slice(0,t),o=e.slice(t);return Array.prototype.concat.call([],n(i),n(o))}function r(e){try{return decodeURIComponent(e)}catch(r){for(var t=e.match(i),o=1;o<t.length;o++)t=(e=n(t,o).join("")).match(i);return e}}e.exports=function(e){if("string"!=typeof e)throw new TypeError("Expected `encodedURI` to be of type `string`, got `"+typeof e+"`");try{return e=e.replace(/\+/g," "),decodeURIComponent(e)}catch(t){return function(e){for(var t={"%FE%FF":"��","%FF%FE":"��"},i=o.exec(e);i;){try{t[i[0]]=decodeURIComponent(i[0])}catch(e){var n=r(i[0]);n!==i[0]&&(t[i[0]]=n)}i=o.exec(e)}t["%C2"]="�";for(var a=Object.keys(t),s=0;s<a.length;s++){var d=a[s];e=e.replace(new RegExp(d,"g"),t[d])}return e}(e)}}},806:e=>{e.exports=function(e,t){for(var i={},o=Object.keys(e),n=Array.isArray(t),r=0;r<o.length;r++){var a=o[r],s=e[a];(n?-1!==t.indexOf(a):t(a,s,e))&&(i[a]=s)}return i}},445:(e,t,i)=>{i.r(t)},375:(e,t,i)=>{i.r(t)},386:(e,t,i)=>{i.r(t)},631:(e,t,i)=>{i.r(t)},563:(e,t,i)=>{const o=i(610),n=i(20),r=i(500),a=i(806),s=Symbol("encodeFragmentIdentifier");function d(e){if("string"!=typeof e||1!==e.length)throw new TypeError("arrayFormatSeparator must be single character string")}function l(e,t){return t.encode?t.strict?o(e):encodeURIComponent(e):e}function c(e,t){return t.decode?n(e):e}function p(e){return Array.isArray(e)?e.sort():"object"==typeof e?p(Object.keys(e)).sort(((e,t)=>Number(e)-Number(t))).map((t=>e[t])):e}function u(e){const t=e.indexOf("#");return-1!==t&&(e=e.slice(0,t)),e}function h(e){const t=(e=u(e)).indexOf("?");return-1===t?"":e.slice(t+1)}function f(e,t){return t.parseNumbers&&!Number.isNaN(Number(e))&&"string"==typeof e&&""!==e.trim()?e=Number(e):!t.parseBooleans||null===e||"true"!==e.toLowerCase()&&"false"!==e.toLowerCase()||(e="true"===e.toLowerCase()),e}function g(e,t){d((t=Object.assign({decode:!0,sort:!0,arrayFormat:"none",arrayFormatSeparator:",",parseNumbers:!1,parseBooleans:!1},t)).arrayFormatSeparator);const i=function(e){let t;switch(e.arrayFormat){case"index":return(e,i,o)=>{t=/\[(\d*)\]$/.exec(e),e=e.replace(/\[\d*\]$/,""),t?(void 0===o[e]&&(o[e]={}),o[e][t[1]]=i):o[e]=i};case"bracket":return(e,i,o)=>{t=/(\[\])$/.exec(e),e=e.replace(/\[\]$/,""),t?void 0!==o[e]?o[e]=[].concat(o[e],i):o[e]=[i]:o[e]=i};case"colon-list-separator":return(e,i,o)=>{t=/(:list)$/.exec(e),e=e.replace(/:list$/,""),t?void 0!==o[e]?o[e]=[].concat(o[e],i):o[e]=[i]:o[e]=i};case"comma":case"separator":return(t,i,o)=>{const n="string"==typeof i&&i.includes(e.arrayFormatSeparator),r="string"==typeof i&&!n&&c(i,e).includes(e.arrayFormatSeparator);i=r?c(i,e):i;const a=n||r?i.split(e.arrayFormatSeparator).map((t=>c(t,e))):null===i?i:c(i,e);o[t]=a};case"bracket-separator":return(t,i,o)=>{const n=/(\[\])$/.test(t);if(t=t.replace(/\[\]$/,""),!n)return void(o[t]=i?c(i,e):i);const r=null===i?[]:i.split(e.arrayFormatSeparator).map((t=>c(t,e)));void 0!==o[t]?o[t]=[].concat(o[t],r):o[t]=r};default:return(e,t,i)=>{void 0!==i[e]?i[e]=[].concat(i[e],t):i[e]=t}}}(t),o=Object.create(null);if("string"!=typeof e)return o;if(!(e=e.trim().replace(/^[?#&]/,"")))return o;for(const n of e.split("&")){if(""===n)continue;let[e,a]=r(t.decode?n.replace(/\+/g," "):n,"=");a=void 0===a?null:["comma","separator","bracket-separator"].includes(t.arrayFormat)?a:c(a,t),i(c(e,t),a,o)}for(const e of Object.keys(o)){const i=o[e];if("object"==typeof i&&null!==i)for(const e of Object.keys(i))i[e]=f(i[e],t);else o[e]=f(i,t)}return!1===t.sort?o:(!0===t.sort?Object.keys(o).sort():Object.keys(o).sort(t.sort)).reduce(((e,t)=>{const i=o[t];return Boolean(i)&&"object"==typeof i&&!Array.isArray(i)?e[t]=p(i):e[t]=i,e}),Object.create(null))}t.extract=h,t.parse=g,t.stringify=(e,t)=>{if(!e)return"";d((t=Object.assign({encode:!0,strict:!0,arrayFormat:"none",arrayFormatSeparator:","},t)).arrayFormatSeparator);const i=i=>t.skipNull&&null==e[i]||t.skipEmptyString&&""===e[i],o=function(e){switch(e.arrayFormat){case"index":return t=>(i,o)=>{const n=i.length;return void 0===o||e.skipNull&&null===o||e.skipEmptyString&&""===o?i:null===o?[...i,[l(t,e),"[",n,"]"].join("")]:[...i,[l(t,e),"[",l(n,e),"]=",l(o,e)].join("")]};case"bracket":return t=>(i,o)=>void 0===o||e.skipNull&&null===o||e.skipEmptyString&&""===o?i:null===o?[...i,[l(t,e),"[]"].join("")]:[...i,[l(t,e),"[]=",l(o,e)].join("")];case"colon-list-separator":return t=>(i,o)=>void 0===o||e.skipNull&&null===o||e.skipEmptyString&&""===o?i:null===o?[...i,[l(t,e),":list="].join("")]:[...i,[l(t,e),":list=",l(o,e)].join("")];case"comma":case"separator":case"bracket-separator":{const t="bracket-separator"===e.arrayFormat?"[]=":"=";return i=>(o,n)=>void 0===n||e.skipNull&&null===n||e.skipEmptyString&&""===n?o:(n=null===n?"":n,0===o.length?[[l(i,e),t,l(n,e)].join("")]:[[o,l(n,e)].join(e.arrayFormatSeparator)])}default:return t=>(i,o)=>void 0===o||e.skipNull&&null===o||e.skipEmptyString&&""===o?i:null===o?[...i,l(t,e)]:[...i,[l(t,e),"=",l(o,e)].join("")]}}(t),n={};for(const t of Object.keys(e))i(t)||(n[t]=e[t]);const r=Object.keys(n);return!1!==t.sort&&r.sort(t.sort),r.map((i=>{const n=e[i];return void 0===n?"":null===n?l(i,t):Array.isArray(n)?0===n.length&&"bracket-separator"===t.arrayFormat?l(i,t)+"[]":n.reduce(o(i),[]).join("&"):l(i,t)+"="+l(n,t)})).filter((e=>e.length>0)).join("&")},t.parseUrl=(e,t)=>{t=Object.assign({decode:!0},t);const[i,o]=r(e,"#");return Object.assign({url:i.split("?")[0]||"",query:g(h(e),t)},t&&t.parseFragmentIdentifier&&o?{fragmentIdentifier:c(o,t)}:{})},t.stringifyUrl=(e,i)=>{i=Object.assign({encode:!0,strict:!0,[s]:!0},i);const o=u(e.url).split("?")[0]||"",n=t.extract(e.url),r=t.parse(n,{sort:!1}),a=Object.assign(r,e.query);let d=t.stringify(a,i);d&&(d=`?${d}`);let c=function(e){let t="";const i=e.indexOf("#");return-1!==i&&(t=e.slice(i)),t}(e.url);return e.fragmentIdentifier&&(c=`#${i[s]?l(e.fragmentIdentifier,i):e.fragmentIdentifier}`),`${o}${d}${c}`},t.pick=(e,i,o)=>{o=Object.assign({parseFragmentIdentifier:!0,[s]:!1},o);const{url:n,query:r,fragmentIdentifier:d}=t.parseUrl(e,o);return t.stringifyUrl({url:n,query:a(r,i),fragmentIdentifier:d},o)},t.exclude=(e,i,o)=>{const n=Array.isArray(i)?e=>!i.includes(e):(e,t)=>!i(e,t);return t.pick(e,n,o)}},500:e=>{e.exports=(e,t)=>{if("string"!=typeof e||"string"!=typeof t)throw new TypeError("Expected the arguments to be of type `string`");if(""===t)return[e];const i=e.indexOf(t);return-1===i?[e]:[e.slice(0,i),e.slice(i+t.length)]}},610:e=>{e.exports=e=>encodeURIComponent(e).replace(/[!'()*]/g,(e=>`%${e.charCodeAt(0).toString(16).toUpperCase()}`))},902:(e,t,i)=>{var o=i(563);const n=function(){this.__data__=[],this.size=0};const r=function(e,t){return e===t||e!=e&&t!=t};const a=function(e,t){for(var i=e.length;i--;)if(r(e[i][0],t))return i;return-1};var s=Array.prototype.splice;const d=function(e){var t=this.__data__,i=a(t,e);return!(i<0)&&(i==t.length-1?t.pop():s.call(t,i,1),--this.size,!0)};const l=function(e){var t=this.__data__,i=a(t,e);return i<0?void 0:t[i][1]};const c=function(e){return a(this.__data__,e)>-1};const p=function(e,t){var i=this.__data__,o=a(i,e);return o<0?(++this.size,i.push([e,t])):i[o][1]=t,this};function u(e){var t=-1,i=null==e?0:e.length;for(this.clear();++t<i;){var o=e[t];this.set(o[0],o[1])}}u.prototype.clear=n,u.prototype.delete=d,u.prototype.get=l,u.prototype.has=c,u.prototype.set=p;const h=u;const f=function(){this.__data__=new h,this.size=0};const g=function(e){var t=this.__data__,i=t.delete(e);return this.size=t.size,i};const m=function(e){return this.__data__.get(e)};const v=function(e){return this.__data__.has(e)};const b="object"==typeof global&&global&&global.Object===Object&&global;var k="object"==typeof self&&self&&self.Object===Object&&self;const y=b||k||Function("return this")();const x=y.Symbol;var w=Object.prototype,$=w.hasOwnProperty,C=w.toString,S=x?x.toStringTag:void 0;const O=function(e){var t=$.call(e,S),i=e[S];try{e[S]=void 0;var o=!0}catch(e){}var n=C.call(e);return o&&(t?e[S]=i:delete e[S]),n};var E=Object.prototype.toString;const D=function(e){return E.call(e)};var z=x?x.toStringTag:void 0;const A=function(e){return null==e?void 0===e?"[object Undefined]":"[object Null]":z&&z in Object(e)?O(e):D(e)};const L=function(e){var t=typeof e;return null!=e&&("object"==t||"function"==t)};const _=function(e){if(!L(e))return!1;var t=A(e);return"[object Function]"==t||"[object GeneratorFunction]"==t||"[object AsyncFunction]"==t||"[object Proxy]"==t};const P=y["__core-js_shared__"];var T,B=(T=/[^.]+$/.exec(P&&P.keys&&P.keys.IE_PROTO||""))?"Symbol(src)_1."+T:"";const I=function(e){return!!B&&B in e};var F=Function.prototype.toString;const M=function(e){if(null!=e){try{return F.call(e)}catch(e){}try{return e+""}catch(e){}}return""};var R=/^\[object .+?Constructor\]$/,N=Function.prototype,V=Object.prototype,U=N.toString,j=V.hasOwnProperty,q=RegExp("^"+U.call(j).replace(/[\\^$.*+?()[\]{}|]/g,"\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g,"$1.*?")+"$");const H=function(e){return!(!L(e)||I(e))&&(_(e)?q:R).test(M(e))};const W=function(e,t){return null==e?void 0:e[t]};const Y=function(e,t){var i=W(e,t);return H(i)?i:void 0};const K=Y(y,"Map");const X=Y(Object,"create");const Z=function(){this.__data__=X?X(null):{},this.size=0};const G=function(e){var t=this.has(e)&&delete this.__data__[e];return this.size-=t?1:0,t};var J=Object.prototype.hasOwnProperty;const Q=function(e){var t=this.__data__;if(X){var i=t[e];return"__lodash_hash_undefined__"===i?void 0:i}return J.call(t,e)?t[e]:void 0};var ee=Object.prototype.hasOwnProperty;const te=function(e){var t=this.__data__;return X?void 0!==t[e]:ee.call(t,e)};const ie=function(e,t){var i=this.__data__;return this.size+=this.has(e)?0:1,i[e]=X&&void 0===t?"__lodash_hash_undefined__":t,this};function oe(e){var t=-1,i=null==e?0:e.length;for(this.clear();++t<i;){var o=e[t];this.set(o[0],o[1])}}oe.prototype.clear=Z,oe.prototype.delete=G,oe.prototype.get=Q,oe.prototype.has=te,oe.prototype.set=ie;const ne=oe;const re=function(){this.size=0,this.__data__={hash:new ne,map:new(K||h),string:new ne}};const ae=function(e){var t=typeof e;return"string"==t||"number"==t||"symbol"==t||"boolean"==t?"__proto__"!==e:null===e};const se=function(e,t){var i=e.__data__;return ae(t)?i["string"==typeof t?"string":"hash"]:i.map};const de=function(e){var t=se(this,e).delete(e);return this.size-=t?1:0,t};const le=function(e){return se(this,e).get(e)};const ce=function(e){return se(this,e).has(e)};const pe=function(e,t){var i=se(this,e),o=i.size;return i.set(e,t),this.size+=i.size==o?0:1,this};function ue(e){var t=-1,i=null==e?0:e.length;for(this.clear();++t<i;){var o=e[t];this.set(o[0],o[1])}}ue.prototype.clear=re,ue.prototype.delete=de,ue.prototype.get=le,ue.prototype.has=ce,ue.prototype.set=pe;const he=ue;const fe=function(e,t){var i=this.__data__;if(i instanceof h){var o=i.__data__;if(!K||o.length<199)return o.push([e,t]),this.size=++i.size,this;i=this.__data__=new he(o)}return i.set(e,t),this.size=i.size,this};function ge(e){var t=this.__data__=new h(e);this.size=t.size}ge.prototype.clear=f,ge.prototype.delete=g,ge.prototype.get=m,ge.prototype.has=v,ge.prototype.set=fe;const me=ge;const ve=function(e){return this.__data__.set(e,"__lodash_hash_undefined__"),this};const be=function(e){return this.__data__.has(e)};function ke(e){var t=-1,i=null==e?0:e.length;for(this.__data__=new he;++t<i;)this.add(e[t])}ke.prototype.add=ke.prototype.push=ve,ke.prototype.has=be;const ye=ke;const xe=function(e,t){for(var i=-1,o=null==e?0:e.length;++i<o;)if(t(e[i],i,e))return!0;return!1};const we=function(e,t){return e.has(t)};const $e=function(e,t,i,o,n,r){var a=1&i,s=e.length,d=t.length;if(s!=d&&!(a&&d>s))return!1;var l=r.get(e),c=r.get(t);if(l&&c)return l==t&&c==e;var p=-1,u=!0,h=2&i?new ye:void 0;for(r.set(e,t),r.set(t,e);++p<s;){var f=e[p],g=t[p];if(o)var m=a?o(g,f,p,t,e,r):o(f,g,p,e,t,r);if(void 0!==m){if(m)continue;u=!1;break}if(h){if(!xe(t,(function(e,t){if(!we(h,t)&&(f===e||n(f,e,i,o,r)))return h.push(t)}))){u=!1;break}}else if(f!==g&&!n(f,g,i,o,r)){u=!1;break}}return r.delete(e),r.delete(t),u};const Ce=y.Uint8Array;const Se=function(e){var t=-1,i=Array(e.size);return e.forEach((function(e,o){i[++t]=[o,e]})),i};const Oe=function(e){var t=-1,i=Array(e.size);return e.forEach((function(e){i[++t]=e})),i};var Ee=x?x.prototype:void 0,De=Ee?Ee.valueOf:void 0;const ze=function(e,t,i,o,n,a,s){switch(i){case"[object DataView]":if(e.byteLength!=t.byteLength||e.byteOffset!=t.byteOffset)return!1;e=e.buffer,t=t.buffer;case"[object ArrayBuffer]":return!(e.byteLength!=t.byteLength||!a(new Ce(e),new Ce(t)));case"[object Boolean]":case"[object Date]":case"[object Number]":return r(+e,+t);case"[object Error]":return e.name==t.name&&e.message==t.message;case"[object RegExp]":case"[object String]":return e==t+"";case"[object Map]":var d=Se;case"[object Set]":var l=1&o;if(d||(d=Oe),e.size!=t.size&&!l)return!1;var c=s.get(e);if(c)return c==t;o|=2,s.set(e,t);var p=$e(d(e),d(t),o,n,a,s);return s.delete(e),p;case"[object Symbol]":if(De)return De.call(e)==De.call(t)}return!1};const Ae=function(e,t){for(var i=-1,o=t.length,n=e.length;++i<o;)e[n+i]=t[i];return e};const Le=Array.isArray;const _e=function(e,t,i){var o=t(e);return Le(e)?o:Ae(o,i(e))};const Pe=function(e,t){for(var i=-1,o=null==e?0:e.length,n=0,r=[];++i<o;){var a=e[i];t(a,i,e)&&(r[n++]=a)}return r};const Te=function(){return[]};var Be=Object.prototype.propertyIsEnumerable,Ie=Object.getOwnPropertySymbols;const Fe=Ie?function(e){return null==e?[]:(e=Object(e),Pe(Ie(e),(function(t){return Be.call(e,t)})))}:Te;const Me=function(e,t){for(var i=-1,o=Array(e);++i<e;)o[i]=t(i);return o};const Re=function(e){return null!=e&&"object"==typeof e};const Ne=function(e){return Re(e)&&"[object Arguments]"==A(e)};var Ve=Object.prototype,Ue=Ve.hasOwnProperty,je=Ve.propertyIsEnumerable;const qe=Ne(function(){return arguments}())?Ne:function(e){return Re(e)&&Ue.call(e,"callee")&&!je.call(e,"callee")};const He=function(){return!1};var We="object"==typeof exports&&exports&&!exports.nodeType&&exports,Ye=We&&"object"==typeof module&&module&&!module.nodeType&&module,Ke=Ye&&Ye.exports===We?y.Buffer:void 0;const Xe=(Ke?Ke.isBuffer:void 0)||He;var Ze=/^(?:0|[1-9]\d*)$/;const Ge=function(e,t){var i=typeof e;return!!(t=null==t?9007199254740991:t)&&("number"==i||"symbol"!=i&&Ze.test(e))&&e>-1&&e%1==0&&e<t};const Je=function(e){return"number"==typeof e&&e>-1&&e%1==0&&e<=9007199254740991};var Qe={};Qe["[object Float32Array]"]=Qe["[object Float64Array]"]=Qe["[object Int8Array]"]=Qe["[object Int16Array]"]=Qe["[object Int32Array]"]=Qe["[object Uint8Array]"]=Qe["[object Uint8ClampedArray]"]=Qe["[object Uint16Array]"]=Qe["[object Uint32Array]"]=!0,Qe["[object Arguments]"]=Qe["[object Array]"]=Qe["[object ArrayBuffer]"]=Qe["[object Boolean]"]=Qe["[object DataView]"]=Qe["[object Date]"]=Qe["[object Error]"]=Qe["[object Function]"]=Qe["[object Map]"]=Qe["[object Number]"]=Qe["[object Object]"]=Qe["[object RegExp]"]=Qe["[object Set]"]=Qe["[object String]"]=Qe["[object WeakMap]"]=!1;const et=function(e){return Re(e)&&Je(e.length)&&!!Qe[A(e)]};const tt=function(e){return function(t){return e(t)}};var it="object"==typeof exports&&exports&&!exports.nodeType&&exports,ot=it&&"object"==typeof module&&module&&!module.nodeType&&module,nt=ot&&ot.exports===it&&b.process,rt=function(){try{var e=ot&&ot.require&&ot.require("util").types;return e||nt&&nt.binding&&nt.binding("util")}catch(e){}}();var at=rt&&rt.isTypedArray;const st=at?tt(at):et;var dt=Object.prototype.hasOwnProperty;const lt=function(e,t){var i=Le(e),o=!i&&qe(e),n=!i&&!o&&Xe(e),r=!i&&!o&&!n&&st(e),a=i||o||n||r,s=a?Me(e.length,String):[],d=s.length;for(var l in e)!t&&!dt.call(e,l)||a&&("length"==l||n&&("offset"==l||"parent"==l)||r&&("buffer"==l||"byteLength"==l||"byteOffset"==l)||Ge(l,d))||s.push(l);return s};var ct=Object.prototype;const pt=function(e){var t=e&&e.constructor;return e===("function"==typeof t&&t.prototype||ct)};const ut=function(e,t){return function(i){return e(t(i))}}(Object.keys,Object);var ht=Object.prototype.hasOwnProperty;const ft=function(e){if(!pt(e))return ut(e);var t=[];for(var i in Object(e))ht.call(e,i)&&"constructor"!=i&&t.push(i);return t};const gt=function(e){return null!=e&&Je(e.length)&&!_(e)};const mt=function(e){return gt(e)?lt(e):ft(e)};const vt=function(e){return _e(e,mt,Fe)};var bt=Object.prototype.hasOwnProperty;const kt=function(e,t,i,o,n,r){var a=1&i,s=vt(e),d=s.length;if(d!=vt(t).length&&!a)return!1;for(var l=d;l--;){var c=s[l];if(!(a?c in t:bt.call(t,c)))return!1}var p=r.get(e),u=r.get(t);if(p&&u)return p==t&&u==e;var h=!0;r.set(e,t),r.set(t,e);for(var f=a;++l<d;){var g=e[c=s[l]],m=t[c];if(o)var v=a?o(m,g,c,t,e,r):o(g,m,c,e,t,r);if(!(void 0===v?g===m||n(g,m,i,o,r):v)){h=!1;break}f||(f="constructor"==c)}if(h&&!f){var b=e.constructor,k=t.constructor;b==k||!("constructor"in e)||!("constructor"in t)||"function"==typeof b&&b instanceof b&&"function"==typeof k&&k instanceof k||(h=!1)}return r.delete(e),r.delete(t),h};const yt=Y(y,"DataView");const xt=Y(y,"Promise");const wt=Y(y,"Set");const $t=Y(y,"WeakMap");var Ct="[object Map]",St="[object Promise]",Ot="[object Set]",Et="[object WeakMap]",Dt="[object DataView]",zt=M(yt),At=M(K),Lt=M(xt),_t=M(wt),Pt=M($t),Tt=A;(yt&&Tt(new yt(new ArrayBuffer(1)))!=Dt||K&&Tt(new K)!=Ct||xt&&Tt(xt.resolve())!=St||wt&&Tt(new wt)!=Ot||$t&&Tt(new $t)!=Et)&&(Tt=function(e){var t=A(e),i="[object Object]"==t?e.constructor:void 0,o=i?M(i):"";if(o)switch(o){case zt:return Dt;case At:return Ct;case Lt:return St;case _t:return Ot;case Pt:return Et}return t});const Bt=Tt;var It="[object Arguments]",Ft="[object Array]",Mt="[object Object]",Rt=Object.prototype.hasOwnProperty;const Nt=function(e,t,i,o,n,r){var a=Le(e),s=Le(t),d=a?Ft:Bt(e),l=s?Ft:Bt(t),c=(d=d==It?Mt:d)==Mt,p=(l=l==It?Mt:l)==Mt,u=d==l;if(u&&Xe(e)){if(!Xe(t))return!1;a=!0,c=!1}if(u&&!c)return r||(r=new me),a||st(e)?$e(e,t,i,o,n,r):ze(e,t,d,i,o,n,r);if(!(1&i)){var h=c&&Rt.call(e,"__wrapped__"),f=p&&Rt.call(t,"__wrapped__");if(h||f){var g=h?e.value():e,m=f?t.value():t;return r||(r=new me),n(g,m,i,o,r)}}return!!u&&(r||(r=new me),kt(e,t,i,o,n,r))};const Vt=function e(t,i,o,n,r){return t===i||(null==t||null==i||!Re(t)&&!Re(i)?t!=t&&i!=i:Nt(t,i,o,n,e,r))};const Ut=function(e,t){return Vt(e,t)};let jt=()=>{throw new Error("HttpServiceFactory no initialized")};const qt=new class{toObject(e){let t,i="undefined";try{e instanceof URLSearchParams&&(i="URLSearchParams")}catch(e){}if("undefined"===i)try{e instanceof FormData&&(e.entries?i="FormData":(e=e.toString(),i="string"))}catch(e){}if("undefined"===i)try{e instanceof Map&&(i="Map")}catch(e){}switch("undefined"===i&&(i=typeof e),i){case"URLSearchParams":t=this.convertUrlSearchParams(e);break;case"FormData":t=this.convertFormData(e);break;case"Map":t=this.convertMap(e);break;case"object":t={...e};break;case"string":t=this.convertString(e);break;default:return{}}return t}convertUrlSearchParams(e){throw console.log(e),new Error("method incomplete and untested: convertUrlSearchParams")}convertFormData(e){const t={};return[...e.entries()].map((async([e,i])=>{if("string"!=typeof i&&"number"!=typeof i&&"boolean"!=typeof i&&"function"!=typeof i)throw new Error("cannot convert "+typeof i);t[e]?(Array.isArray(t[e])||(t[e]=[t[e]]),t[e]=[...t[e],i]):t[e]=i})),t}convertMap(e){const t={};for(let[i,o]of e.entries())t[String(i)]=o;return t}convertString(e){try{return JSON.parse(e)}catch(e){}return o.parse(e,{arrayFormat:"bracket"})}},Ht={},Wt=e=>void 0!==Ht[e],Yt=(e,t)=>Ht[e]=t,Kt=new Map,Xt=e=>{if((e=>Kt.has(e))(e))return Kt.get(e)};class Zt{constructor(){this.requestStoreMode="auto",this.requestBaseUrl="",this.requestUrl="",this.requestMethod="get",this.requestHeaders={"content-type":"application/json"},this.requestParams={},this.requestBody={},this.requestErrorRecoverers={}}async getExecutableValues(e){const t={};if("string"==typeof e)t[e]="true";else{if(e instanceof URLSearchParams)throw new Error("Conversion of executables-values as URLSearchParams not implemented");if(e instanceof FormData)throw new Error("Conversion of executables-values as FormData not implemented");if(e instanceof Map)throw new Error("Conversion of executables-values as Map<string, DynamicCustomParamsValue> not implemented");if("object"!=typeof e)throw new Error("Invalid executables-values format: "+typeof e);Object.entries(e).map((async([e,i])=>{if("string"==typeof i||"number"==typeof i||"boolean"==typeof i||"function"==typeof i)t[e]=await this.getExecutableValue(i);else if("object"==typeof i){const o=[];for(let e of i)o.push(await this.getExecutableValue(e));t[e]=o}}))}return t}async getExecutableValue(e){let t="unknown";switch(t=Array.isArray(e)?"array":typeof e,t){case"number":return String(e);case"boolean":return String(e?1:0);case"string":return e;case"function":const t=e();return t instanceof Promise?await t:t;case"array":const i=[];return e.forEach((async e=>{i.push(String(await this.getExecutableValue(e)))})),i;default:throw new Error("Unsupported type: "+typeof e)}}getConfig(){return{url:`${this.requestBaseUrl}${this.requestUrl}`,method:this.requestMethod,headers:this.requestHeaders,params:this.requestParams,body:this.requestBody}}dumpConfig(){}storeName(e){return this.requestStoreName=e,this}storeMode(e){return this.requestStoreMode=e,this}baseUrl(e){return this.requestBaseUrl=e,this}method(e){return this.requestMethod=e,this}url(e){return this.requestUrl=e,this}config(e){var t;return"string"==typeof e?null===(t=Xt(e))||void 0===t||t.cloneConfig(this):e.cloneConfig(this),this}cloneConfig(e){return e.baseUrl(this.requestBaseUrl),e.url(this.requestUrl),e.method(this.requestMethod),e.header(this.requestHeaders),e.param(this.requestParams),e.body(this.requestBody),this.requestStoreName&&e.storeName(this.requestStoreName),e.storeMode(this.requestStoreMode),e.errorRecoverer(this.requestErrorRecoverers),e}applyConfig(){return this}header(e,t){return"string"==typeof e&&void 0!==t?this.requestHeaders[String(e)]=t:this.requestHeaders={...this.requestHeaders,...qt.toObject(e)},this}clearHeader(e){return void 0!==e?delete this.requestHeaders[e]:this.requestHeaders={},this}param(e,t){return"string"==typeof e&&void 0!==t?this.requestParams[String(e)]=t:this.requestParams={...this.requestParams,...qt.toObject(e)},this}clearParam(e){return void 0!==e?delete this.requestParams[e]:this.requestParams={},this}body(e,t){return"string"==typeof e&&void 0!==t?this.requestBody[String(e)]=t:this.requestBody={...this.requestBody,...qt.toObject(e)},this}clearBody(e){return void 0!==e?delete this.requestBody[e]:this.requestBody={},this}errorRecoverer(e,t){return"number"==typeof e&&void 0!==t?this.requestErrorRecoverers[e]=t:this.requestErrorRecoverers={...this.requestErrorRecoverers,...e},this}clearErrorRecoverer(e){return void 0!==e?delete this.requestErrorRecoverers[e]:this.requestErrorRecoverers={},this}getErrorRecoverer(e){if(this.requestErrorRecoverers[e])return this.requestErrorRecoverers[e]}}class Gt extends Zt{constructor(){super(...arguments),this.requestTerminated=!1,this.requestSubscribed=!1}terminate(){this.requestTerminated=!0}subscribe(){if(!this.runningPromise)throw new Error("Running promise not found!");return this.runningPromise}request(){return this.runningPromise=new Promise((async(e,t)=>{let i;try{if(this.requestStoreName&&Wt(this.requestStoreName)){const e=(e=>{if(Wt(e))return Ht[e]})(this.requestStoreName);let t=this.requestStoreMode;switch("auto"===this.requestStoreMode&&(t=Ut(e.getConfig(),this.getConfig())?"subscribe":"override"),t){case"override":e.terminate(),Yt(this.requestStoreName,this),i=await this.doExecRequest();break;case"subscribe":this.requestSubscribed=!0,i=await e.subscribe();break;default:throw new Error(`Error: Invalid requestStoreMode ${t}`)}}else this.requestStoreName&&Yt(this.requestStoreName,this),i=await this.doExecRequest();if(this.requestTerminated)throw new Error("Request has been overriden or terminated");if(200!==i.status){const e=this.getErrorRecoverer(i.status);if(!e)throw new Error(`Error: ${i.status}`);{const t=await e.recover(this,i);i=await t.request()}}e(i)}catch(e){t(e)}finally{!this.requestStoreName||this.requestSubscribed||this.requestTerminated||(o=this.requestStoreName,Wt(o)&&delete Ht[o]),this.requestSubscribed=!1,this.requestTerminated=!1}var o})),this.runningPromise}async doExecRequest(){const e={url:`${this.requestBaseUrl}${this.requestUrl}`,method:this.requestMethod,headers:await this.getExecutableValues(this.requestHeaders),params:await this.getExecutableValues(this.requestParams),body:await this.getExecutableValues(this.requestBody)},t=await this.buildRequest(e),i=await this.execRequest(t);return await this.buildResponse(i)}get(e){return e&&this.url(e),this.method("get"),this.request()}post(e){return e&&this.url(e),this.method("post"),this.request()}put(e){return e&&this.url(e),this.method("put"),this.request()}delete(e){return e&&this.url(e),this.method("delete"),this.request()}options(e){return e&&this.url(e),this.method("options"),this.request()}}class Jt extends Gt{async buildRequest(e){const t={...e};return t.url+=`?${o.stringify(t.params)}`,t.params={},t}async execRequest(e){const t={method:e.method,headers:e.headers};return e.body&&["post","put"].includes(e.method)&&(t.body=JSON.stringify(e.body)),console.log("======== PERFORM REQUEST ========"),console.log(e.url,t),await fetch(e.url,t)}async buildResponse(e){var t;let i={};const o=new Map(null===(t=e.headers)||void 0===t?void 0:t.entries());return"application/json"===o.get("content-type")&&e.json&&(i=await e.json()),{status:e.status,headers:o,data:i}}}function Qt(e,t,i,o){var n,r=arguments.length,a=r<3?t:null===o?o=Object.getOwnPropertyDescriptor(t,i):o;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)a=Reflect.decorate(e,t,i,o);else for(var s=e.length-1;s>=0;s--)(n=e[s])&&(a=(r<3?n(a):r>3?n(t,i,a):n(t,i))||a);return r>3&&a&&Object.defineProperty(t,i,a),a}Object.create;Object.create;const ei=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,ti=Symbol(),ii=new Map;class oi{constructor(e,t){if(this._$cssResult$=!0,t!==ti)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=e}get styleSheet(){let e=ii.get(this.cssText);return ei&&void 0===e&&(ii.set(this.cssText,e=new CSSStyleSheet),e.replaceSync(this.cssText)),e}toString(){return this.cssText}}const ni=(e,...t)=>{const i=1===e.length?e[0]:t.reduce(((t,i,o)=>t+(e=>{if(!0===e._$cssResult$)return e.cssText;if("number"==typeof e)return e;throw Error("Value passed to 'css' function must be a 'css' function result: "+e+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+e[o+1]),e[0]);return new oi(i,ti)},ri=ei?e=>e:e=>e instanceof CSSStyleSheet?(e=>{let t="";for(const i of e.cssRules)t+=i.cssText;return(e=>new oi("string"==typeof e?e:e+"",ti))(t)})(e):e;var ai;const si=window.trustedTypes,di=si?si.emptyScript:"",li=window.reactiveElementPolyfillSupport,ci={toAttribute(e,t){switch(t){case Boolean:e=e?di:null;break;case Object:case Array:e=null==e?e:JSON.stringify(e)}return e},fromAttribute(e,t){let i=e;switch(t){case Boolean:i=null!==e;break;case Number:i=null===e?null:Number(e);break;case Object:case Array:try{i=JSON.parse(e)}catch(e){i=null}}return i}},pi=(e,t)=>t!==e&&(t==t||e==e),ui={attribute:!0,type:String,converter:ci,reflect:!1,hasChanged:pi};class hi extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(e){var t;null!==(t=this.l)&&void 0!==t||(this.l=[]),this.l.push(e)}static get observedAttributes(){this.finalize();const e=[];return this.elementProperties.forEach(((t,i)=>{const o=this._$Eh(i,t);void 0!==o&&(this._$Eu.set(o,i),e.push(o))})),e}static createProperty(e,t=ui){if(t.state&&(t.attribute=!1),this.finalize(),this.elementProperties.set(e,t),!t.noAccessor&&!this.prototype.hasOwnProperty(e)){const i="symbol"==typeof e?Symbol():"__"+e,o=this.getPropertyDescriptor(e,i,t);void 0!==o&&Object.defineProperty(this.prototype,e,o)}}static getPropertyDescriptor(e,t,i){return{get(){return this[t]},set(o){const n=this[e];this[t]=o,this.requestUpdate(e,n,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(e){return this.elementProperties.get(e)||ui}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const e=Object.getPrototypeOf(this);if(e.finalize(),this.elementProperties=new Map(e.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const e=this.properties,t=[...Object.getOwnPropertyNames(e),...Object.getOwnPropertySymbols(e)];for(const i of t)this.createProperty(i,e[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(e){const t=[];if(Array.isArray(e)){const i=new Set(e.flat(1/0).reverse());for(const e of i)t.unshift(ri(e))}else void 0!==e&&t.push(ri(e));return t}static _$Eh(e,t){const i=t.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof e?e.toLowerCase():void 0}o(){var e;this._$Ep=new Promise((e=>this.enableUpdating=e)),this._$AL=new Map,this._$Em(),this.requestUpdate(),null===(e=this.constructor.l)||void 0===e||e.forEach((e=>e(this)))}addController(e){var t,i;(null!==(t=this._$Eg)&&void 0!==t?t:this._$Eg=[]).push(e),void 0!==this.renderRoot&&this.isConnected&&(null===(i=e.hostConnected)||void 0===i||i.call(e))}removeController(e){var t;null===(t=this._$Eg)||void 0===t||t.splice(this._$Eg.indexOf(e)>>>0,1)}_$Em(){this.constructor.elementProperties.forEach(((e,t)=>{this.hasOwnProperty(t)&&(this._$Et.set(t,this[t]),delete this[t])}))}createRenderRoot(){var e;const t=null!==(e=this.shadowRoot)&&void 0!==e?e:this.attachShadow(this.constructor.shadowRootOptions);return((e,t)=>{ei?e.adoptedStyleSheets=t.map((e=>e instanceof CSSStyleSheet?e:e.styleSheet)):t.forEach((t=>{const i=document.createElement("style"),o=window.litNonce;void 0!==o&&i.setAttribute("nonce",o),i.textContent=t.cssText,e.appendChild(i)}))})(t,this.constructor.elementStyles),t}connectedCallback(){var e;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(e=this._$Eg)||void 0===e||e.forEach((e=>{var t;return null===(t=e.hostConnected)||void 0===t?void 0:t.call(e)}))}enableUpdating(e){}disconnectedCallback(){var e;null===(e=this._$Eg)||void 0===e||e.forEach((e=>{var t;return null===(t=e.hostDisconnected)||void 0===t?void 0:t.call(e)}))}attributeChangedCallback(e,t,i){this._$AK(e,i)}_$ES(e,t,i=ui){var o,n;const r=this.constructor._$Eh(e,i);if(void 0!==r&&!0===i.reflect){const a=(null!==(n=null===(o=i.converter)||void 0===o?void 0:o.toAttribute)&&void 0!==n?n:ci.toAttribute)(t,i.type);this._$Ei=e,null==a?this.removeAttribute(r):this.setAttribute(r,a),this._$Ei=null}}_$AK(e,t){var i,o,n;const r=this.constructor,a=r._$Eu.get(e);if(void 0!==a&&this._$Ei!==a){const e=r.getPropertyOptions(a),s=e.converter,d=null!==(n=null!==(o=null===(i=s)||void 0===i?void 0:i.fromAttribute)&&void 0!==o?o:"function"==typeof s?s:null)&&void 0!==n?n:ci.fromAttribute;this._$Ei=a,this[a]=d(t,e.type),this._$Ei=null}}requestUpdate(e,t,i){let o=!0;void 0!==e&&(((i=i||this.constructor.getPropertyOptions(e)).hasChanged||pi)(this[e],t)?(this._$AL.has(e)||this._$AL.set(e,t),!0===i.reflect&&this._$Ei!==e&&(void 0===this._$E_&&(this._$E_=new Map),this._$E_.set(e,i))):o=!1),!this.isUpdatePending&&o&&(this._$Ep=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ep}catch(e){Promise.reject(e)}const e=this.scheduleUpdate();return null!=e&&await e,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var e;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((e,t)=>this[t]=e)),this._$Et=void 0);let t=!1;const i=this._$AL;try{t=this.shouldUpdate(i),t?(this.willUpdate(i),null===(e=this._$Eg)||void 0===e||e.forEach((e=>{var t;return null===(t=e.hostUpdate)||void 0===t?void 0:t.call(e)})),this.update(i)):this._$EU()}catch(e){throw t=!1,this._$EU(),e}t&&this._$AE(i)}willUpdate(e){}_$AE(e){var t;null===(t=this._$Eg)||void 0===t||t.forEach((e=>{var t;return null===(t=e.hostUpdated)||void 0===t?void 0:t.call(e)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(e)),this.updated(e)}_$EU(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ep}shouldUpdate(e){return!0}update(e){void 0!==this._$E_&&(this._$E_.forEach(((e,t)=>this._$ES(t,this[t],e))),this._$E_=void 0),this._$EU()}updated(e){}firstUpdated(e){}}var fi;hi.finalized=!0,hi.elementProperties=new Map,hi.elementStyles=[],hi.shadowRootOptions={mode:"open"},null==li||li({ReactiveElement:hi}),(null!==(ai=globalThis.reactiveElementVersions)&&void 0!==ai?ai:globalThis.reactiveElementVersions=[]).push("1.2.0");const gi=globalThis.trustedTypes,mi=gi?gi.createPolicy("lit-html",{createHTML:e=>e}):void 0,vi=`lit$${(Math.random()+"").slice(9)}$`,bi="?"+vi,ki=`<${bi}>`,yi=document,xi=(e="")=>yi.createComment(e),wi=e=>null===e||"object"!=typeof e&&"function"!=typeof e,$i=Array.isArray,Ci=e=>{var t;return $i(e)||"function"==typeof(null===(t=e)||void 0===t?void 0:t[Symbol.iterator])},Si=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,Oi=/-->/g,Ei=/>/g,Di=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,zi=/'/g,Ai=/"/g,Li=/^(?:script|style|textarea)$/i,_i=e=>(t,...i)=>({_$litType$:e,strings:t,values:i}),Pi=_i(1),Ti=(_i(2),Symbol.for("lit-noChange")),Bi=Symbol.for("lit-nothing"),Ii=new WeakMap,Fi=yi.createTreeWalker(yi,129,null,!1),Mi=(e,t)=>{const i=e.length-1,o=[];let n,r=2===t?"<svg>":"",a=Si;for(let t=0;t<i;t++){const i=e[t];let s,d,l=-1,c=0;for(;c<i.length&&(a.lastIndex=c,d=a.exec(i),null!==d);)c=a.lastIndex,a===Si?"!--"===d[1]?a=Oi:void 0!==d[1]?a=Ei:void 0!==d[2]?(Li.test(d[2])&&(n=RegExp("</"+d[2],"g")),a=Di):void 0!==d[3]&&(a=Di):a===Di?">"===d[0]?(a=null!=n?n:Si,l=-1):void 0===d[1]?l=-2:(l=a.lastIndex-d[2].length,s=d[1],a=void 0===d[3]?Di:'"'===d[3]?Ai:zi):a===Ai||a===zi?a=Di:a===Oi||a===Ei?a=Si:(a=Di,n=void 0);const p=a===Di&&e[t+1].startsWith("/>")?" ":"";r+=a===Si?i+ki:l>=0?(o.push(s),i.slice(0,l)+"$lit$"+i.slice(l)+vi+p):i+vi+(-2===l?(o.push(void 0),t):p)}const s=r+(e[i]||"<?>")+(2===t?"</svg>":"");if(!Array.isArray(e)||!e.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==mi?mi.createHTML(s):s,o]};class Ri{constructor({strings:e,_$litType$:t},i){let o;this.parts=[];let n=0,r=0;const a=e.length-1,s=this.parts,[d,l]=Mi(e,t);if(this.el=Ri.createElement(d,i),Fi.currentNode=this.el.content,2===t){const e=this.el.content,t=e.firstChild;t.remove(),e.append(...t.childNodes)}for(;null!==(o=Fi.nextNode())&&s.length<a;){if(1===o.nodeType){if(o.hasAttributes()){const e=[];for(const t of o.getAttributeNames())if(t.endsWith("$lit$")||t.startsWith(vi)){const i=l[r++];if(e.push(t),void 0!==i){const e=o.getAttribute(i.toLowerCase()+"$lit$").split(vi),t=/([.?@])?(.*)/.exec(i);s.push({type:1,index:n,name:t[2],strings:e,ctor:"."===t[1]?qi:"?"===t[1]?Wi:"@"===t[1]?Yi:ji})}else s.push({type:6,index:n})}for(const t of e)o.removeAttribute(t)}if(Li.test(o.tagName)){const e=o.textContent.split(vi),t=e.length-1;if(t>0){o.textContent=gi?gi.emptyScript:"";for(let i=0;i<t;i++)o.append(e[i],xi()),Fi.nextNode(),s.push({type:2,index:++n});o.append(e[t],xi())}}}else if(8===o.nodeType)if(o.data===bi)s.push({type:2,index:n});else{let e=-1;for(;-1!==(e=o.data.indexOf(vi,e+1));)s.push({type:7,index:n}),e+=vi.length-1}n++}}static createElement(e,t){const i=yi.createElement("template");return i.innerHTML=e,i}}function Ni(e,t,i=e,o){var n,r,a,s;if(t===Ti)return t;let d=void 0!==o?null===(n=i._$Cl)||void 0===n?void 0:n[o]:i._$Cu;const l=wi(t)?void 0:t._$litDirective$;return(null==d?void 0:d.constructor)!==l&&(null===(r=null==d?void 0:d._$AO)||void 0===r||r.call(d,!1),void 0===l?d=void 0:(d=new l(e),d._$AT(e,i,o)),void 0!==o?(null!==(a=(s=i)._$Cl)&&void 0!==a?a:s._$Cl=[])[o]=d:i._$Cu=d),void 0!==d&&(t=Ni(e,d._$AS(e,t.values),d,o)),t}class Vi{constructor(e,t){this.v=[],this._$AN=void 0,this._$AD=e,this._$AM=t}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(e){var t;const{el:{content:i},parts:o}=this._$AD,n=(null!==(t=null==e?void 0:e.creationScope)&&void 0!==t?t:yi).importNode(i,!0);Fi.currentNode=n;let r=Fi.nextNode(),a=0,s=0,d=o[0];for(;void 0!==d;){if(a===d.index){let t;2===d.type?t=new Ui(r,r.nextSibling,this,e):1===d.type?t=new d.ctor(r,d.name,d.strings,this,e):6===d.type&&(t=new Ki(r,this,e)),this.v.push(t),d=o[++s]}a!==(null==d?void 0:d.index)&&(r=Fi.nextNode(),a++)}return n}m(e){let t=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(e,i,t),t+=i.strings.length-2):i._$AI(e[t])),t++}}class Ui{constructor(e,t,i,o){var n;this.type=2,this._$AH=Bi,this._$AN=void 0,this._$AA=e,this._$AB=t,this._$AM=i,this.options=o,this._$Cg=null===(n=null==o?void 0:o.isConnected)||void 0===n||n}get _$AU(){var e,t;return null!==(t=null===(e=this._$AM)||void 0===e?void 0:e._$AU)&&void 0!==t?t:this._$Cg}get parentNode(){let e=this._$AA.parentNode;const t=this._$AM;return void 0!==t&&11===e.nodeType&&(e=t.parentNode),e}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(e,t=this){e=Ni(this,e,t),wi(e)?e===Bi||null==e||""===e?(this._$AH!==Bi&&this._$AR(),this._$AH=Bi):e!==this._$AH&&e!==Ti&&this.$(e):void 0!==e._$litType$?this.T(e):void 0!==e.nodeType?this.S(e):Ci(e)?this.A(e):this.$(e)}M(e,t=this._$AB){return this._$AA.parentNode.insertBefore(e,t)}S(e){this._$AH!==e&&(this._$AR(),this._$AH=this.M(e))}$(e){this._$AH!==Bi&&wi(this._$AH)?this._$AA.nextSibling.data=e:this.S(yi.createTextNode(e)),this._$AH=e}T(e){var t;const{values:i,_$litType$:o}=e,n="number"==typeof o?this._$AC(e):(void 0===o.el&&(o.el=Ri.createElement(o.h,this.options)),o);if((null===(t=this._$AH)||void 0===t?void 0:t._$AD)===n)this._$AH.m(i);else{const e=new Vi(n,this),t=e.p(this.options);e.m(i),this.S(t),this._$AH=e}}_$AC(e){let t=Ii.get(e.strings);return void 0===t&&Ii.set(e.strings,t=new Ri(e)),t}A(e){$i(this._$AH)||(this._$AH=[],this._$AR());const t=this._$AH;let i,o=0;for(const n of e)o===t.length?t.push(i=new Ui(this.M(xi()),this.M(xi()),this,this.options)):i=t[o],i._$AI(n),o++;o<t.length&&(this._$AR(i&&i._$AB.nextSibling,o),t.length=o)}_$AR(e=this._$AA.nextSibling,t){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,t);e&&e!==this._$AB;){const t=e.nextSibling;e.remove(),e=t}}setConnected(e){var t;void 0===this._$AM&&(this._$Cg=e,null===(t=this._$AP)||void 0===t||t.call(this,e))}}class ji{constructor(e,t,i,o,n){this.type=1,this._$AH=Bi,this._$AN=void 0,this.element=e,this.name=t,this._$AM=o,this.options=n,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=Bi}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(e,t=this,i,o){const n=this.strings;let r=!1;if(void 0===n)e=Ni(this,e,t,0),r=!wi(e)||e!==this._$AH&&e!==Ti,r&&(this._$AH=e);else{const o=e;let a,s;for(e=n[0],a=0;a<n.length-1;a++)s=Ni(this,o[i+a],t,a),s===Ti&&(s=this._$AH[a]),r||(r=!wi(s)||s!==this._$AH[a]),s===Bi?e=Bi:e!==Bi&&(e+=(null!=s?s:"")+n[a+1]),this._$AH[a]=s}r&&!o&&this.k(e)}k(e){e===Bi?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=e?e:"")}}class qi extends ji{constructor(){super(...arguments),this.type=3}k(e){this.element[this.name]=e===Bi?void 0:e}}const Hi=gi?gi.emptyScript:"";class Wi extends ji{constructor(){super(...arguments),this.type=4}k(e){e&&e!==Bi?this.element.setAttribute(this.name,Hi):this.element.removeAttribute(this.name)}}class Yi extends ji{constructor(e,t,i,o,n){super(e,t,i,o,n),this.type=5}_$AI(e,t=this){var i;if((e=null!==(i=Ni(this,e,t,0))&&void 0!==i?i:Bi)===Ti)return;const o=this._$AH,n=e===Bi&&o!==Bi||e.capture!==o.capture||e.once!==o.once||e.passive!==o.passive,r=e!==Bi&&(o===Bi||n);n&&this.element.removeEventListener(this.name,this,o),r&&this.element.addEventListener(this.name,this,e),this._$AH=e}handleEvent(e){var t,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(t=this.options)||void 0===t?void 0:t.host)&&void 0!==i?i:this.element,e):this._$AH.handleEvent(e)}}class Ki{constructor(e,t,i){this.element=e,this.type=6,this._$AN=void 0,this._$AM=t,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(e){Ni(this,e)}}const Xi=window.litHtmlPolyfillSupport;var Zi,Gi;null==Xi||Xi(Ri,Ui),(null!==(fi=globalThis.litHtmlVersions)&&void 0!==fi?fi:globalThis.litHtmlVersions=[]).push("2.1.1");class Ji extends hi{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var e,t;const i=super.createRenderRoot();return null!==(e=(t=this.renderOptions).renderBefore)&&void 0!==e||(t.renderBefore=i.firstChild),i}update(e){const t=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(e),this._$Dt=((e,t,i)=>{var o,n;const r=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:t;let a=r._$litPart$;if(void 0===a){const e=null!==(n=null==i?void 0:i.renderBefore)&&void 0!==n?n:null;r._$litPart$=a=new Ui(t.insertBefore(xi(),e),e,void 0,null!=i?i:{})}return a._$AI(e),a})(t,this.renderRoot,this.renderOptions)}connectedCallback(){var e;super.connectedCallback(),null===(e=this._$Dt)||void 0===e||e.setConnected(!0)}disconnectedCallback(){var e;super.disconnectedCallback(),null===(e=this._$Dt)||void 0===e||e.setConnected(!1)}render(){return Ti}}Ji.finalized=!0,Ji._$litElement$=!0,null===(Zi=globalThis.litElementHydrateSupport)||void 0===Zi||Zi.call(globalThis,{LitElement:Ji});const Qi=globalThis.litElementPolyfillSupport;null==Qi||Qi({LitElement:Ji});(null!==(Gi=globalThis.litElementVersions)&&void 0!==Gi?Gi:globalThis.litElementVersions=[]).push("3.1.1");const eo=e=>t=>"function"==typeof t?((e,t)=>(window.customElements.define(e,t),t))(e,t):((e,t)=>{const{kind:i,elements:o}=t;return{kind:i,elements:o,finisher(t){window.customElements.define(e,t)}}})(e,t),to=(e,t)=>"method"===t.kind&&t.descriptor&&!("value"in t.descriptor)?{...t,finisher(i){i.createProperty(t.key,e)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:t.key,initializer(){"function"==typeof t.initializer&&(this[t.key]=t.initializer.call(this))},finisher(i){i.createProperty(t.key,e)}};function io(e){return(t,i)=>void 0!==i?((e,t,i)=>{t.constructor.createProperty(i,e)})(e,t,i):to(e,t)}function oo(e){return io({...e,state:!0})}const no=({finisher:e,descriptor:t})=>(i,o)=>{var n;if(void 0===o){const o=null!==(n=i.originalKey)&&void 0!==n?n:i.key,r=null!=t?{kind:"method",placement:"prototype",key:o,descriptor:t(i.key)}:{...i,key:o};return null!=e&&(r.finisher=function(t){e(t,o)}),r}{const n=i.constructor;void 0!==t&&Object.defineProperty(i,o,t(o)),null==e||e(n,o)}};function ro(e,t){return no({descriptor:i=>{const o={get(){var t,i;return null!==(i=null===(t=this.renderRoot)||void 0===t?void 0:t.querySelector(e))&&void 0!==i?i:null},enumerable:!0,configurable:!0};if(t){const t="symbol"==typeof i?Symbol():"__"+i;o.get=function(){var i,o;return void 0===this[t]&&(this[t]=null!==(o=null===(i=this.renderRoot)||void 0===i?void 0:i.querySelector(e))&&void 0!==o?o:null),this[t]}}return o}})}var ao;const so=null!=(null===(ao=window.HTMLSlotElement)||void 0===ao?void 0:ao.prototype.assignedElements)?(e,t)=>e.assignedElements(t):(e,t)=>e.assignedNodes(t).filter((e=>e.nodeType===Node.ELEMENT_NODE));function lo(e){const{slot:t,selector:i}=null!=e?e:{};return no({descriptor:o=>({get(){var o;const n="slot"+(t?`[name=${t}]`:":not([name])"),r=null===(o=this.renderRoot)||void 0===o?void 0:o.querySelector(n),a=null!=r?so(r,e):[];return i?a.filter((e=>e.matches(i))):a},enumerable:!0,configurable:!0})})}function co(e,t){var i=Object.keys(e);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(e);t&&(o=o.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),i.push.apply(i,o)}return i}function po(e){for(var t=1;t<arguments.length;t++){var i=null!=arguments[t]?arguments[t]:{};t%2?co(Object(i),!0).forEach((function(t){fo(e,t,i[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(i)):co(Object(i)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(i,t))}))}return e}function uo(e){return uo="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},uo(e)}function ho(e,t){for(var i=0;i<t.length;i++){var o=t[i];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}}function fo(e,t,i){return t in e?Object.defineProperty(e,t,{value:i,enumerable:!0,configurable:!0,writable:!0}):e[t]=i,e}function go(e,t){return function(e){if(Array.isArray(e))return e}(e)||function(e,t){var i=null==e?null:"undefined"!=typeof Symbol&&e[Symbol.iterator]||e["@@iterator"];if(null==i)return;var o,n,r=[],a=!0,s=!1;try{for(i=i.call(e);!(a=(o=i.next()).done)&&(r.push(o.value),!t||r.length!==t);a=!0);}catch(e){s=!0,n=e}finally{try{a||null==i.return||i.return()}finally{if(s)throw n}}return r}(e,t)||vo(e,t)||function(){throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function mo(e){return function(e){if(Array.isArray(e))return bo(e)}(e)||function(e){if("undefined"!=typeof Symbol&&null!=e[Symbol.iterator]||null!=e["@@iterator"])return Array.from(e)}(e)||vo(e)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function vo(e,t){if(e){if("string"==typeof e)return bo(e,t);var i=Object.prototype.toString.call(e).slice(8,-1);return"Object"===i&&e.constructor&&(i=e.constructor.name),"Map"===i||"Set"===i?Array.from(e):"Arguments"===i||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i)?bo(e,t):void 0}}function bo(e,t){(null==t||t>e.length)&&(t=e.length);for(var i=0,o=new Array(t);i<t;i++)o[i]=e[i];return o}var ko=function(){},yo={},xo={},wo=null,$o={mark:ko,measure:ko};try{"undefined"!=typeof window&&(yo=window),"undefined"!=typeof document&&(xo=document),"undefined"!=typeof MutationObserver&&(wo=MutationObserver),"undefined"!=typeof performance&&($o=performance)}catch(e){}var Co=(yo.navigator||{}).userAgent,So=void 0===Co?"":Co,Oo=yo,Eo=xo,Do=wo,zo=$o,Ao=(Oo.document,!!Eo.documentElement&&!!Eo.head&&"function"==typeof Eo.addEventListener&&"function"==typeof Eo.createElement),Lo=~So.indexOf("MSIE")||~So.indexOf("Trident/"),_o="svg-inline--fa",Po="data-fa-i2svg",To="data-fa-pseudo-element",Bo="data-prefix",Io="data-icon",Fo="fontawesome-i2svg",Mo=["HTML","HEAD","STYLE","SCRIPT"],Ro=function(){try{return!0}catch(e){return!1}}(),No={fas:"solid","fa-solid":"solid",far:"regular","fa-regular":"regular",fal:"light","fa-light":"light",fat:"thin","fa-thin":"thin",fad:"duotone","fa-duotone":"duotone",fab:"brands","fa-brands":"brands",fak:"kit","fa-kit":"kit",fa:"solid"},Vo={solid:"fas",regular:"far",light:"fal",thin:"fat",duotone:"fad",brands:"fab",kit:"fak"},Uo={fab:"fa-brands",fad:"fa-duotone",fak:"fa-kit",fal:"fa-light",far:"fa-regular",fas:"fa-solid",fat:"fa-thin"},jo={"fa-brands":"fab","fa-duotone":"fad","fa-kit":"fak","fa-light":"fal","fa-regular":"far","fa-solid":"fas","fa-thin":"fat"},qo=/fa[srltdbk\-\ ]/,Ho="fa-layers-text",Wo=/Font ?Awesome ?([56 ]*)(Solid|Regular|Light|Thin|Duotone|Brands|Free|Pro|Kit)?.*/i,Yo={900:"fas",400:"far",normal:"far",300:"fal",100:"fat"},Ko=[1,2,3,4,5,6,7,8,9,10],Xo=Ko.concat([11,12,13,14,15,16,17,18,19,20]),Zo=["class","data-prefix","data-icon","data-fa-transform","data-fa-mask"],Go="duotone-group",Jo="swap-opacity",Qo="primary",en="secondary",tn=[].concat(mo(Object.keys(Vo)),["2xs","xs","sm","lg","xl","2xl","beat","border","fade","beat-fade","bounce","flip-both","flip-horizontal","flip-vertical","flip","fw","inverse","layers-counter","layers-text","layers","li","pull-left","pull-right","pulse","rotate-180","rotate-270","rotate-90","rotate-by","shake","spin-pulse","spin-reverse","spin","stack-1x","stack-2x","stack","ul",Go,Jo,Qo,en]).concat(Ko.map((function(e){return"".concat(e,"x")}))).concat(Xo.map((function(e){return"w-".concat(e)}))),on=Oo.FontAwesomeConfig||{};if(Eo&&"function"==typeof Eo.querySelector){[["data-family-prefix","familyPrefix"],["data-style-default","styleDefault"],["data-replacement-class","replacementClass"],["data-auto-replace-svg","autoReplaceSvg"],["data-auto-add-css","autoAddCss"],["data-auto-a11y","autoA11y"],["data-search-pseudo-elements","searchPseudoElements"],["data-observe-mutations","observeMutations"],["data-mutate-approach","mutateApproach"],["data-keep-original-source","keepOriginalSource"],["data-measure-performance","measurePerformance"],["data-show-missing-icons","showMissingIcons"]].forEach((function(e){var t=go(e,2),i=t[0],o=t[1],n=function(e){return""===e||"false"!==e&&("true"===e||e)}(function(e){var t=Eo.querySelector("script["+e+"]");if(t)return t.getAttribute(e)}(i));null!=n&&(on[o]=n)}))}var nn=po(po({},{familyPrefix:"fa",styleDefault:"solid",replacementClass:_o,autoReplaceSvg:!0,autoAddCss:!0,autoA11y:!0,searchPseudoElements:!1,observeMutations:!0,mutateApproach:"async",keepOriginalSource:!0,measurePerformance:!1,showMissingIcons:!0}),on);nn.autoReplaceSvg||(nn.observeMutations=!1);var rn={};Object.keys(nn).forEach((function(e){Object.defineProperty(rn,e,{enumerable:!0,set:function(t){nn[e]=t,an.forEach((function(e){return e(rn)}))},get:function(){return nn[e]}})})),Oo.FontAwesomeConfig=rn;var an=[];var sn=16,dn={size:16,x:0,y:0,rotate:0,flipX:!1,flipY:!1};function ln(){for(var e=12,t="";e-- >0;)t+="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"[62*Math.random()|0];return t}function cn(e){for(var t=[],i=(e||[]).length>>>0;i--;)t[i]=e[i];return t}function pn(e){return e.classList?cn(e.classList):(e.getAttribute("class")||"").split(" ").filter((function(e){return e}))}function un(e){return"".concat(e).replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}function hn(e){return Object.keys(e||{}).reduce((function(t,i){return t+"".concat(i,": ").concat(e[i].trim(),";")}),"")}function fn(e){return e.size!==dn.size||e.x!==dn.x||e.y!==dn.y||e.rotate!==dn.rotate||e.flipX||e.flipY}function gn(){var e="fa",t=_o,i=rn.familyPrefix,o=rn.replacementClass,n=':root, :host {\n  --fa-font-solid: normal 900 1em/1 "Font Awesome 6 Solid";\n  --fa-font-regular: normal 400 1em/1 "Font Awesome 6 Regular";\n  --fa-font-light: normal 300 1em/1 "Font Awesome 6 Light";\n  --fa-font-thin: normal 100 1em/1 "Font Awesome 6 Thin";\n  --fa-font-duotone: normal 900 1em/1 "Font Awesome 6 Duotone";\n  --fa-font-brands: normal 400 1em/1 "Font Awesome 6 Brands";\n}\n\nsvg:not(:root).svg-inline--fa, svg:not(:host).svg-inline--fa {\n  overflow: visible;\n  box-sizing: content-box;\n}\n\n.svg-inline--fa {\n  display: var(--fa-display, inline-block);\n  height: 1em;\n  overflow: visible;\n  vertical-align: -0.125em;\n}\n.svg-inline--fa.fa-2xs {\n  vertical-align: 0.1em;\n}\n.svg-inline--fa.fa-xs {\n  vertical-align: 0em;\n}\n.svg-inline--fa.fa-sm {\n  vertical-align: -0.0714285705em;\n}\n.svg-inline--fa.fa-lg {\n  vertical-align: -0.2em;\n}\n.svg-inline--fa.fa-xl {\n  vertical-align: -0.25em;\n}\n.svg-inline--fa.fa-2xl {\n  vertical-align: -0.3125em;\n}\n.svg-inline--fa.fa-pull-left {\n  margin-right: var(--fa-pull-margin, 0.3em);\n  width: auto;\n}\n.svg-inline--fa.fa-pull-right {\n  margin-left: var(--fa-pull-margin, 0.3em);\n  width: auto;\n}\n.svg-inline--fa.fa-li {\n  width: var(--fa-li-width, 2em);\n  top: 0.25em;\n}\n.svg-inline--fa.fa-fw {\n  width: var(--fa-fw-width, 1.25em);\n}\n\n.fa-layers svg.svg-inline--fa {\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  position: absolute;\n  right: 0;\n  top: 0;\n}\n\n.fa-layers-counter, .fa-layers-text {\n  display: inline-block;\n  position: absolute;\n  text-align: center;\n}\n\n.fa-layers {\n  display: inline-block;\n  height: 1em;\n  position: relative;\n  text-align: center;\n  vertical-align: -0.125em;\n  width: 1em;\n}\n.fa-layers svg.svg-inline--fa {\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.fa-layers-text {\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.fa-layers-counter {\n  background-color: var(--fa-counter-background-color, #ff253a);\n  border-radius: var(--fa-counter-border-radius, 1em);\n  box-sizing: border-box;\n  color: var(--fa-inverse, #fff);\n  line-height: var(--fa-counter-line-height, 1);\n  max-width: var(--fa-counter-max-width, 5em);\n  min-width: var(--fa-counter-min-width, 1.5em);\n  overflow: hidden;\n  padding: var(--fa-counter-padding, 0.25em 0.5em);\n  right: var(--fa-right, 0);\n  text-overflow: ellipsis;\n  top: var(--fa-top, 0);\n  -webkit-transform: scale(var(--fa-counter-scale, 0.25));\n          transform: scale(var(--fa-counter-scale, 0.25));\n  -webkit-transform-origin: top right;\n          transform-origin: top right;\n}\n\n.fa-layers-bottom-right {\n  bottom: var(--fa-bottom, 0);\n  right: var(--fa-right, 0);\n  top: auto;\n  -webkit-transform: scale(var(--fa-layers-scale, 0.25));\n          transform: scale(var(--fa-layers-scale, 0.25));\n  -webkit-transform-origin: bottom right;\n          transform-origin: bottom right;\n}\n\n.fa-layers-bottom-left {\n  bottom: var(--fa-bottom, 0);\n  left: var(--fa-left, 0);\n  right: auto;\n  top: auto;\n  -webkit-transform: scale(var(--fa-layers-scale, 0.25));\n          transform: scale(var(--fa-layers-scale, 0.25));\n  -webkit-transform-origin: bottom left;\n          transform-origin: bottom left;\n}\n\n.fa-layers-top-right {\n  top: var(--fa-top, 0);\n  right: var(--fa-right, 0);\n  -webkit-transform: scale(var(--fa-layers-scale, 0.25));\n          transform: scale(var(--fa-layers-scale, 0.25));\n  -webkit-transform-origin: top right;\n          transform-origin: top right;\n}\n\n.fa-layers-top-left {\n  left: var(--fa-left, 0);\n  right: auto;\n  top: var(--fa-top, 0);\n  -webkit-transform: scale(var(--fa-layers-scale, 0.25));\n          transform: scale(var(--fa-layers-scale, 0.25));\n  -webkit-transform-origin: top left;\n          transform-origin: top left;\n}\n\n.fa-1x {\n  font-size: 1em;\n}\n\n.fa-2x {\n  font-size: 2em;\n}\n\n.fa-3x {\n  font-size: 3em;\n}\n\n.fa-4x {\n  font-size: 4em;\n}\n\n.fa-5x {\n  font-size: 5em;\n}\n\n.fa-6x {\n  font-size: 6em;\n}\n\n.fa-7x {\n  font-size: 7em;\n}\n\n.fa-8x {\n  font-size: 8em;\n}\n\n.fa-9x {\n  font-size: 9em;\n}\n\n.fa-10x {\n  font-size: 10em;\n}\n\n.fa-2xs {\n  font-size: 0.625em;\n  line-height: 0.1em;\n  vertical-align: 0.225em;\n}\n\n.fa-xs {\n  font-size: 0.75em;\n  line-height: 0.0833333337em;\n  vertical-align: 0.125em;\n}\n\n.fa-sm {\n  font-size: 0.875em;\n  line-height: 0.0714285718em;\n  vertical-align: 0.0535714295em;\n}\n\n.fa-lg {\n  font-size: 1.25em;\n  line-height: 0.05em;\n  vertical-align: -0.075em;\n}\n\n.fa-xl {\n  font-size: 1.5em;\n  line-height: 0.0416666682em;\n  vertical-align: -0.125em;\n}\n\n.fa-2xl {\n  font-size: 2em;\n  line-height: 0.03125em;\n  vertical-align: -0.1875em;\n}\n\n.fa-fw {\n  text-align: center;\n  width: 1.25em;\n}\n\n.fa-ul {\n  list-style-type: none;\n  margin-left: var(--fa-li-margin, 2.5em);\n  padding-left: 0;\n}\n.fa-ul > li {\n  position: relative;\n}\n\n.fa-li {\n  left: calc(var(--fa-li-width, 2em) * -1);\n  position: absolute;\n  text-align: center;\n  width: var(--fa-li-width, 2em);\n  line-height: inherit;\n}\n\n.fa-border {\n  border-color: var(--fa-border-color, #eee);\n  border-radius: var(--fa-border-radius, 0.1em);\n  border-style: var(--fa-border-style, solid);\n  border-width: var(--fa-border-width, 0.08em);\n  padding: var(--fa-border-padding, 0.2em 0.25em 0.15em);\n}\n\n.fa-pull-left {\n  float: left;\n  margin-right: var(--fa-pull-margin, 0.3em);\n}\n\n.fa-pull-right {\n  float: right;\n  margin-left: var(--fa-pull-margin, 0.3em);\n}\n\n.fa-beat {\n  -webkit-animation-name: fa-beat;\n          animation-name: fa-beat;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, ease-in-out);\n          animation-timing-function: var(--fa-animation-timing, ease-in-out);\n}\n\n.fa-bounce {\n  -webkit-animation-name: fa-bounce;\n          animation-name: fa-bounce;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.28, 0.84, 0.42, 1));\n          animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.28, 0.84, 0.42, 1));\n}\n\n.fa-fade {\n  -webkit-animation-name: fa-fade;\n          animation-name: fa-fade;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.4, 0, 0.6, 1));\n          animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.4, 0, 0.6, 1));\n}\n\n.fa-beat-fade {\n  -webkit-animation-name: fa-beat-fade;\n          animation-name: fa-beat-fade;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.4, 0, 0.6, 1));\n          animation-timing-function: var(--fa-animation-timing, cubic-bezier(0.4, 0, 0.6, 1));\n}\n\n.fa-flip {\n  -webkit-animation-name: fa-flip;\n          animation-name: fa-flip;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, ease-in-out);\n          animation-timing-function: var(--fa-animation-timing, ease-in-out);\n}\n\n.fa-shake {\n  -webkit-animation-name: fa-shake;\n          animation-name: fa-shake;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, linear);\n          animation-timing-function: var(--fa-animation-timing, linear);\n}\n\n.fa-spin {\n  -webkit-animation-name: fa-spin;\n          animation-name: fa-spin;\n  -webkit-animation-delay: var(--fa-animation-delay, 0);\n          animation-delay: var(--fa-animation-delay, 0);\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 2s);\n          animation-duration: var(--fa-animation-duration, 2s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, linear);\n          animation-timing-function: var(--fa-animation-timing, linear);\n}\n\n.fa-spin-reverse {\n  --fa-animation-direction: reverse;\n}\n\n.fa-pulse,\n.fa-spin-pulse {\n  -webkit-animation-name: fa-spin;\n          animation-name: fa-spin;\n  -webkit-animation-direction: var(--fa-animation-direction, normal);\n          animation-direction: var(--fa-animation-direction, normal);\n  -webkit-animation-duration: var(--fa-animation-duration, 1s);\n          animation-duration: var(--fa-animation-duration, 1s);\n  -webkit-animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n          animation-iteration-count: var(--fa-animation-iteration-count, infinite);\n  -webkit-animation-timing-function: var(--fa-animation-timing, steps(8));\n          animation-timing-function: var(--fa-animation-timing, steps(8));\n}\n\n@media (prefers-reduced-motion: reduce) {\n  .fa-beat,\n.fa-bounce,\n.fa-fade,\n.fa-beat-fade,\n.fa-flip,\n.fa-pulse,\n.fa-shake,\n.fa-spin,\n.fa-spin-pulse {\n    -webkit-animation-delay: -1ms;\n            animation-delay: -1ms;\n    -webkit-animation-duration: 1ms;\n            animation-duration: 1ms;\n    -webkit-animation-iteration-count: 1;\n            animation-iteration-count: 1;\n    transition-delay: 0s;\n    transition-duration: 0s;\n  }\n}\n@-webkit-keyframes fa-beat {\n  0%, 90% {\n    -webkit-transform: scale(1);\n            transform: scale(1);\n  }\n  45% {\n    -webkit-transform: scale(var(--fa-beat-scale, 1.25));\n            transform: scale(var(--fa-beat-scale, 1.25));\n  }\n}\n@keyframes fa-beat {\n  0%, 90% {\n    -webkit-transform: scale(1);\n            transform: scale(1);\n  }\n  45% {\n    -webkit-transform: scale(var(--fa-beat-scale, 1.25));\n            transform: scale(var(--fa-beat-scale, 1.25));\n  }\n}\n@-webkit-keyframes fa-bounce {\n  0% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n  10% {\n    -webkit-transform: scale(var(--fa-bounce-start-scale-x, 1.1), var(--fa-bounce-start-scale-y, 0.9)) translateY(0);\n            transform: scale(var(--fa-bounce-start-scale-x, 1.1), var(--fa-bounce-start-scale-y, 0.9)) translateY(0);\n  }\n  30% {\n    -webkit-transform: scale(var(--fa-bounce-jump-scale-x, 0.9), var(--fa-bounce-jump-scale-y, 1.1)) translateY(var(--fa-bounce-height, -0.5em));\n            transform: scale(var(--fa-bounce-jump-scale-x, 0.9), var(--fa-bounce-jump-scale-y, 1.1)) translateY(var(--fa-bounce-height, -0.5em));\n  }\n  50% {\n    -webkit-transform: scale(var(--fa-bounce-land-scale-x, 1.05), var(--fa-bounce-land-scale-y, 0.95)) translateY(0);\n            transform: scale(var(--fa-bounce-land-scale-x, 1.05), var(--fa-bounce-land-scale-y, 0.95)) translateY(0);\n  }\n  57% {\n    -webkit-transform: scale(1, 1) translateY(var(--fa-bounce-rebound, -0.125em));\n            transform: scale(1, 1) translateY(var(--fa-bounce-rebound, -0.125em));\n  }\n  64% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n  100% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n}\n@keyframes fa-bounce {\n  0% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n  10% {\n    -webkit-transform: scale(var(--fa-bounce-start-scale-x, 1.1), var(--fa-bounce-start-scale-y, 0.9)) translateY(0);\n            transform: scale(var(--fa-bounce-start-scale-x, 1.1), var(--fa-bounce-start-scale-y, 0.9)) translateY(0);\n  }\n  30% {\n    -webkit-transform: scale(var(--fa-bounce-jump-scale-x, 0.9), var(--fa-bounce-jump-scale-y, 1.1)) translateY(var(--fa-bounce-height, -0.5em));\n            transform: scale(var(--fa-bounce-jump-scale-x, 0.9), var(--fa-bounce-jump-scale-y, 1.1)) translateY(var(--fa-bounce-height, -0.5em));\n  }\n  50% {\n    -webkit-transform: scale(var(--fa-bounce-land-scale-x, 1.05), var(--fa-bounce-land-scale-y, 0.95)) translateY(0);\n            transform: scale(var(--fa-bounce-land-scale-x, 1.05), var(--fa-bounce-land-scale-y, 0.95)) translateY(0);\n  }\n  57% {\n    -webkit-transform: scale(1, 1) translateY(var(--fa-bounce-rebound, -0.125em));\n            transform: scale(1, 1) translateY(var(--fa-bounce-rebound, -0.125em));\n  }\n  64% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n  100% {\n    -webkit-transform: scale(1, 1) translateY(0);\n            transform: scale(1, 1) translateY(0);\n  }\n}\n@-webkit-keyframes fa-fade {\n  50% {\n    opacity: var(--fa-fade-opacity, 0.4);\n  }\n}\n@keyframes fa-fade {\n  50% {\n    opacity: var(--fa-fade-opacity, 0.4);\n  }\n}\n@-webkit-keyframes fa-beat-fade {\n  0%, 100% {\n    opacity: var(--fa-beat-fade-opacity, 0.4);\n    -webkit-transform: scale(1);\n            transform: scale(1);\n  }\n  50% {\n    opacity: 1;\n    -webkit-transform: scale(var(--fa-beat-fade-scale, 1.125));\n            transform: scale(var(--fa-beat-fade-scale, 1.125));\n  }\n}\n@keyframes fa-beat-fade {\n  0%, 100% {\n    opacity: var(--fa-beat-fade-opacity, 0.4);\n    -webkit-transform: scale(1);\n            transform: scale(1);\n  }\n  50% {\n    opacity: 1;\n    -webkit-transform: scale(var(--fa-beat-fade-scale, 1.125));\n            transform: scale(var(--fa-beat-fade-scale, 1.125));\n  }\n}\n@-webkit-keyframes fa-flip {\n  50% {\n    -webkit-transform: rotate3d(var(--fa-flip-x, 0), var(--fa-flip-y, 1), var(--fa-flip-z, 0), var(--fa-flip-angle, -180deg));\n            transform: rotate3d(var(--fa-flip-x, 0), var(--fa-flip-y, 1), var(--fa-flip-z, 0), var(--fa-flip-angle, -180deg));\n  }\n}\n@keyframes fa-flip {\n  50% {\n    -webkit-transform: rotate3d(var(--fa-flip-x, 0), var(--fa-flip-y, 1), var(--fa-flip-z, 0), var(--fa-flip-angle, -180deg));\n            transform: rotate3d(var(--fa-flip-x, 0), var(--fa-flip-y, 1), var(--fa-flip-z, 0), var(--fa-flip-angle, -180deg));\n  }\n}\n@-webkit-keyframes fa-shake {\n  0% {\n    -webkit-transform: rotate(-15deg);\n            transform: rotate(-15deg);\n  }\n  4% {\n    -webkit-transform: rotate(15deg);\n            transform: rotate(15deg);\n  }\n  8%, 24% {\n    -webkit-transform: rotate(-18deg);\n            transform: rotate(-18deg);\n  }\n  12%, 28% {\n    -webkit-transform: rotate(18deg);\n            transform: rotate(18deg);\n  }\n  16% {\n    -webkit-transform: rotate(-22deg);\n            transform: rotate(-22deg);\n  }\n  20% {\n    -webkit-transform: rotate(22deg);\n            transform: rotate(22deg);\n  }\n  32% {\n    -webkit-transform: rotate(-12deg);\n            transform: rotate(-12deg);\n  }\n  36% {\n    -webkit-transform: rotate(12deg);\n            transform: rotate(12deg);\n  }\n  40%, 100% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n}\n@keyframes fa-shake {\n  0% {\n    -webkit-transform: rotate(-15deg);\n            transform: rotate(-15deg);\n  }\n  4% {\n    -webkit-transform: rotate(15deg);\n            transform: rotate(15deg);\n  }\n  8%, 24% {\n    -webkit-transform: rotate(-18deg);\n            transform: rotate(-18deg);\n  }\n  12%, 28% {\n    -webkit-transform: rotate(18deg);\n            transform: rotate(18deg);\n  }\n  16% {\n    -webkit-transform: rotate(-22deg);\n            transform: rotate(-22deg);\n  }\n  20% {\n    -webkit-transform: rotate(22deg);\n            transform: rotate(22deg);\n  }\n  32% {\n    -webkit-transform: rotate(-12deg);\n            transform: rotate(-12deg);\n  }\n  36% {\n    -webkit-transform: rotate(12deg);\n            transform: rotate(12deg);\n  }\n  40%, 100% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n}\n@-webkit-keyframes fa-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n@keyframes fa-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n.fa-rotate-90 {\n  -webkit-transform: rotate(90deg);\n          transform: rotate(90deg);\n}\n\n.fa-rotate-180 {\n  -webkit-transform: rotate(180deg);\n          transform: rotate(180deg);\n}\n\n.fa-rotate-270 {\n  -webkit-transform: rotate(270deg);\n          transform: rotate(270deg);\n}\n\n.fa-flip-horizontal {\n  -webkit-transform: scale(-1, 1);\n          transform: scale(-1, 1);\n}\n\n.fa-flip-vertical {\n  -webkit-transform: scale(1, -1);\n          transform: scale(1, -1);\n}\n\n.fa-flip-both,\n.fa-flip-horizontal.fa-flip-vertical {\n  -webkit-transform: scale(-1, -1);\n          transform: scale(-1, -1);\n}\n\n.fa-rotate-by {\n  -webkit-transform: rotate(var(--fa-rotate-angle, none));\n          transform: rotate(var(--fa-rotate-angle, none));\n}\n\n.fa-stack {\n  display: inline-block;\n  vertical-align: middle;\n  height: 2em;\n  position: relative;\n  width: 2.5em;\n}\n\n.fa-stack-1x,\n.fa-stack-2x {\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  position: absolute;\n  right: 0;\n  top: 0;\n  z-index: var(--fa-stack-z-index, auto);\n}\n\n.svg-inline--fa.fa-stack-1x {\n  height: 1em;\n  width: 1.25em;\n}\n.svg-inline--fa.fa-stack-2x {\n  height: 2em;\n  width: 2.5em;\n}\n\n.fa-inverse {\n  color: var(--fa-inverse, #fff);\n}\n\n.sr-only,\n.fa-sr-only {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  white-space: nowrap;\n  border-width: 0;\n}\n\n.sr-only-focusable:not(:focus),\n.fa-sr-only-focusable:not(:focus) {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  white-space: nowrap;\n  border-width: 0;\n}\n\n.svg-inline--fa .fa-primary {\n  fill: var(--fa-primary-color, currentColor);\n  opacity: var(--fa-primary-opacity, 1);\n}\n\n.svg-inline--fa .fa-secondary {\n  fill: var(--fa-secondary-color, currentColor);\n  opacity: var(--fa-secondary-opacity, 0.4);\n}\n\n.svg-inline--fa.fa-swap-opacity .fa-primary {\n  opacity: var(--fa-secondary-opacity, 0.4);\n}\n\n.svg-inline--fa.fa-swap-opacity .fa-secondary {\n  opacity: var(--fa-primary-opacity, 1);\n}\n\n.svg-inline--fa mask .fa-primary,\n.svg-inline--fa mask .fa-secondary {\n  fill: black;\n}\n\n.fad.fa-inverse,\n.fa-duotone.fa-inverse {\n  color: var(--fa-inverse, #fff);\n}';if(i!==e||o!==t){var r=new RegExp("\\.".concat(e,"\\-"),"g"),a=new RegExp("\\--".concat(e,"\\-"),"g"),s=new RegExp("\\.".concat(t),"g");n=n.replace(r,".".concat(i,"-")).replace(a,"--".concat(i,"-")).replace(s,".".concat(o))}return n}var mn=!1;function vn(){rn.autoAddCss&&!mn&&(!function(e){if(e&&Ao){var t=Eo.createElement("style");t.setAttribute("type","text/css"),t.innerHTML=e;for(var i=Eo.head.childNodes,o=null,n=i.length-1;n>-1;n--){var r=i[n],a=(r.tagName||"").toUpperCase();["STYLE","LINK"].indexOf(a)>-1&&(o=r)}Eo.head.insertBefore(t,o)}}(gn()),mn=!0)}var bn={mixout:function(){return{dom:{css:gn,insertCss:vn}}},hooks:function(){return{beforeDOMElementCreation:function(){vn()},beforeI2svg:function(){vn()}}}},kn=Oo||{};kn.___FONT_AWESOME___||(kn.___FONT_AWESOME___={}),kn.___FONT_AWESOME___.styles||(kn.___FONT_AWESOME___.styles={}),kn.___FONT_AWESOME___.hooks||(kn.___FONT_AWESOME___.hooks={}),kn.___FONT_AWESOME___.shims||(kn.___FONT_AWESOME___.shims=[]);var yn=kn.___FONT_AWESOME___,xn=[],wn=!1;function $n(e){Ao&&(wn?setTimeout(e,0):xn.push(e))}function Cn(e){var t=e.tag,i=e.attributes,o=void 0===i?{}:i,n=e.children,r=void 0===n?[]:n;return"string"==typeof e?un(e):"<".concat(t," ").concat(function(e){return Object.keys(e||{}).reduce((function(t,i){return t+"".concat(i,'="').concat(un(e[i]),'" ')}),"").trim()}(o),">").concat(r.map(Cn).join(""),"</").concat(t,">")}function Sn(e,t,i){if(e&&e[t]&&e[t][i])return{prefix:t,iconName:i,icon:e[t][i]}}Ao&&((wn=(Eo.documentElement.doScroll?/^loaded|^c/:/^loaded|^i|^c/).test(Eo.readyState))||Eo.addEventListener("DOMContentLoaded",(function e(){Eo.removeEventListener("DOMContentLoaded",e),wn=1,xn.map((function(e){return e()}))})));var On=function(e,t,i,o){var n,r,a,s=Object.keys(e),d=s.length,l=void 0!==o?function(e,t){return function(i,o,n,r){return e.call(t,i,o,n,r)}}(t,o):t;for(void 0===i?(n=1,a=e[s[0]]):(n=0,a=i);n<d;n++)a=l(a,e[r=s[n]],r,e);return a};function En(e){var t=function(e){for(var t=[],i=0,o=e.length;i<o;){var n=e.charCodeAt(i++);if(n>=55296&&n<=56319&&i<o){var r=e.charCodeAt(i++);56320==(64512&r)?t.push(((1023&n)<<10)+(1023&r)+65536):(t.push(n),i--)}else t.push(n)}return t}(e);return 1===t.length?t[0].toString(16):null}function Dn(e){return Object.keys(e).reduce((function(t,i){var o=e[i];return!!o.icon?t[o.iconName]=o.icon:t[i]=o,t}),{})}function zn(e,t){var i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{},o=i.skipHooks,n=void 0!==o&&o,r=Dn(t);"function"!=typeof yn.hooks.addPack||n?yn.styles[e]=po(po({},yn.styles[e]||{}),r):yn.hooks.addPack(e,Dn(t)),"fas"===e&&zn("fa",t)}var An=yn.styles,Ln=yn.shims,_n=Object.values(Uo),Pn=null,Tn={},Bn={},In={},Fn={},Mn={},Rn=Object.keys(No);function Nn(e,t){var i,o=t.split("-"),n=o[0],r=o.slice(1).join("-");return n!==e||""===r||(i=r,~tn.indexOf(i))?null:r}var Vn,Un=function(){var e=function(e){return On(An,(function(t,i,o){return t[o]=On(i,e,{}),t}),{})};Tn=e((function(e,t,i){if(t[3]&&(e[t[3]]=i),t[2]){var o=t[2].filter((function(e){return"number"==typeof e}));o.forEach((function(t){e[t.toString(16)]=i}))}return e})),Bn=e((function(e,t,i){if(e[i]=i,t[2]){var o=t[2].filter((function(e){return"string"==typeof e}));o.forEach((function(t){e[t]=i}))}return e})),Mn=e((function(e,t,i){var o=t[2];return e[i]=i,o.forEach((function(t){e[t]=i})),e}));var t="far"in An||rn.autoFetchSvg,i=On(Ln,(function(e,i){var o=i[0],n=i[1],r=i[2];return"far"!==n||t||(n="fas"),"string"==typeof o&&(e.names[o]={prefix:n,iconName:r}),"number"==typeof o&&(e.unicodes[o.toString(16)]={prefix:n,iconName:r}),e}),{names:{},unicodes:{}});In=i.names,Fn=i.unicodes,Pn=Yn(rn.styleDefault)};function jn(e,t){return(Tn[e]||{})[t]}function qn(e,t){return(Mn[e]||{})[t]}function Hn(e){return In[e]||{prefix:null,iconName:null}}function Wn(){return Pn}Vn=function(e){Pn=Yn(e.styleDefault)},an.push(Vn),Un();function Yn(e){var t=Vo[e]||Vo[No[e]],i=e in yn.styles?e:null;return t||i||null}function Kn(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.skipLookups,o=void 0!==i&&i,n=null,r=e.reduce((function(e,t){var i=Nn(rn.familyPrefix,t);if(An[t]?(t=_n.includes(t)?jo[t]:t,n=t,e.prefix=t):Rn.indexOf(t)>-1?(n=t,e.prefix=Yn(t)):i?e.iconName=i:t!==rn.replacementClass&&e.rest.push(t),!o&&e.prefix&&e.iconName){var r="fa"===n?Hn(e.iconName):{},a=qn(e.prefix,e.iconName);r.prefix&&(n=null),e.iconName=r.iconName||a||e.iconName,e.prefix=r.prefix||e.prefix,"far"!==e.prefix||An.far||!An.fas||rn.autoFetchSvg||(e.prefix="fas")}return e}),{prefix:null,iconName:null,rest:[]});return"fa"!==r.prefix&&"fa"!==n||(r.prefix=Wn()||"fas"),r}var Xn=function(){function e(){!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,e),this.definitions={}}var t,i,o;return t=e,i=[{key:"add",value:function(){for(var e=this,t=arguments.length,i=new Array(t),o=0;o<t;o++)i[o]=arguments[o];var n=i.reduce(this._pullDefinitions,{});Object.keys(n).forEach((function(t){e.definitions[t]=po(po({},e.definitions[t]||{}),n[t]),zn(t,n[t]);var i=Uo[t];i&&zn(i,n[t]),Un()}))}},{key:"reset",value:function(){this.definitions={}}},{key:"_pullDefinitions",value:function(e,t){var i=t.prefix&&t.iconName&&t.icon?{0:t}:t;return Object.keys(i).map((function(t){var o=i[t],n=o.prefix,r=o.iconName,a=o.icon,s=a[2];e[n]||(e[n]={}),s.length>0&&s.forEach((function(t){"string"==typeof t&&(e[n][t]=a)})),e[n][r]=a})),e}}],i&&ho(t.prototype,i),o&&ho(t,o),Object.defineProperty(t,"prototype",{writable:!1}),e}(),Zn=[],Gn={},Jn={},Qn=Object.keys(Jn);function er(e,t){for(var i=arguments.length,o=new Array(i>2?i-2:0),n=2;n<i;n++)o[n-2]=arguments[n];var r=Gn[e]||[];return r.forEach((function(e){t=e.apply(null,[t].concat(o))})),t}function tr(e){for(var t=arguments.length,i=new Array(t>1?t-1:0),o=1;o<t;o++)i[o-1]=arguments[o];var n=Gn[e]||[];n.forEach((function(e){e.apply(null,i)}))}function ir(){var e=arguments[0],t=Array.prototype.slice.call(arguments,1);return Jn[e]?Jn[e].apply(null,t):void 0}function or(e){"fa"===e.prefix&&(e.prefix="fas");var t=e.iconName,i=e.prefix||Wn();if(t)return t=qn(i,t)||t,Sn(nr.definitions,i,t)||Sn(yn.styles,i,t)}var nr=new Xn,rr={i2svg:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};return Ao?(tr("beforeI2svg",e),ir("pseudoElements2svg",e),ir("i2svg",e)):Promise.reject("Operation requires a DOM of some kind.")},watch:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},t=e.autoReplaceSvgRoot;!1===rn.autoReplaceSvg&&(rn.autoReplaceSvg=!0),rn.observeMutations=!0,$n((function(){dr({autoReplaceSvgRoot:t}),tr("watch",e)}))}},ar={icon:function(e){if(null===e)return null;if("object"===uo(e)&&e.prefix&&e.iconName)return{prefix:e.prefix,iconName:qn(e.prefix,e.iconName)||e.iconName};if(Array.isArray(e)&&2===e.length){var t=0===e[1].indexOf("fa-")?e[1].slice(3):e[1],i=Yn(e[0]);return{prefix:i,iconName:qn(i,t)||t}}if("string"==typeof e&&(e.indexOf("".concat(rn.familyPrefix,"-"))>-1||e.match(qo))){var o=Kn(e.split(" "),{skipLookups:!0});return{prefix:o.prefix||Wn(),iconName:qn(o.prefix,o.iconName)||o.iconName}}if("string"==typeof e){var n=Wn();return{prefix:n,iconName:qn(n,e)||e}}}},sr={noAuto:function(){rn.autoReplaceSvg=!1,rn.observeMutations=!1,tr("noAuto")},config:rn,dom:rr,parse:ar,library:nr,findIconDefinition:or,toHtml:Cn},dr=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},t=e.autoReplaceSvgRoot,i=void 0===t?Eo:t;(Object.keys(yn.styles).length>0||rn.autoFetchSvg)&&Ao&&rn.autoReplaceSvg&&sr.dom.i2svg({node:i})};function lr(e,t){return Object.defineProperty(e,"abstract",{get:t}),Object.defineProperty(e,"html",{get:function(){return e.abstract.map((function(e){return Cn(e)}))}}),Object.defineProperty(e,"node",{get:function(){if(Ao){var t=Eo.createElement("div");return t.innerHTML=e.html,t.children}}}),e}function cr(e){var t=e.icons,i=t.main,o=t.mask,n=e.prefix,r=e.iconName,a=e.transform,s=e.symbol,d=e.title,l=e.maskId,c=e.titleId,p=e.extra,u=e.watchable,h=void 0!==u&&u,f=o.found?o:i,g=f.width,m=f.height,v="fak"===n,b=[rn.replacementClass,r?"".concat(rn.familyPrefix,"-").concat(r):""].filter((function(e){return-1===p.classes.indexOf(e)})).filter((function(e){return""!==e||!!e})).concat(p.classes).join(" "),k={children:[],attributes:po(po({},p.attributes),{},{"data-prefix":n,"data-icon":r,class:b,role:p.attributes.role||"img",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 ".concat(g," ").concat(m)})},y=v&&!~p.classes.indexOf("fa-fw")?{width:"".concat(g/m*16*.0625,"em")}:{};h&&(k.attributes[Po]=""),d&&(k.children.push({tag:"title",attributes:{id:k.attributes["aria-labelledby"]||"title-".concat(c||ln())},children:[d]}),delete k.attributes.title);var x=po(po({},k),{},{prefix:n,iconName:r,main:i,mask:o,maskId:l,transform:a,symbol:s,styles:po(po({},y),p.styles)}),w=o.found&&i.found?ir("generateAbstractMask",x)||{children:[],attributes:{}}:ir("generateAbstractIcon",x)||{children:[],attributes:{}},$=w.children,C=w.attributes;return x.children=$,x.attributes=C,s?function(e){var t=e.prefix,i=e.iconName,o=e.children,n=e.attributes,r=e.symbol,a=!0===r?"".concat(t,"-").concat(rn.familyPrefix,"-").concat(i):r;return[{tag:"svg",attributes:{style:"display: none;"},children:[{tag:"symbol",attributes:po(po({},n),{},{id:a}),children:o}]}]}(x):function(e){var t=e.children,i=e.main,o=e.mask,n=e.attributes,r=e.styles,a=e.transform;if(fn(a)&&i.found&&!o.found){var s={x:i.width/i.height/2,y:.5};n.style=hn(po(po({},r),{},{"transform-origin":"".concat(s.x+a.x/16,"em ").concat(s.y+a.y/16,"em")}))}return[{tag:"svg",attributes:n,children:t}]}(x)}function pr(e){var t=e.content,i=e.width,o=e.height,n=e.transform,r=e.title,a=e.extra,s=e.watchable,d=void 0!==s&&s,l=po(po(po({},a.attributes),r?{title:r}:{}),{},{class:a.classes.join(" ")});d&&(l[Po]="");var c=po({},a.styles);fn(n)&&(c.transform=function(e){var t=e.transform,i=e.width,o=void 0===i?16:i,n=e.height,r=void 0===n?16:n,a=e.startCentered,s=void 0!==a&&a,d="";return d+=s&&Lo?"translate(".concat(t.x/sn-o/2,"em, ").concat(t.y/sn-r/2,"em) "):s?"translate(calc(-50% + ".concat(t.x/sn,"em), calc(-50% + ").concat(t.y/sn,"em)) "):"translate(".concat(t.x/sn,"em, ").concat(t.y/sn,"em) "),d+="scale(".concat(t.size/sn*(t.flipX?-1:1),", ").concat(t.size/sn*(t.flipY?-1:1),") "),d+"rotate(".concat(t.rotate,"deg) ")}({transform:n,startCentered:!0,width:i,height:o}),c["-webkit-transform"]=c.transform);var p=hn(c);p.length>0&&(l.style=p);var u=[];return u.push({tag:"span",attributes:l,children:[t]}),r&&u.push({tag:"span",attributes:{class:"sr-only"},children:[r]}),u}function ur(e){var t=e.content,i=e.title,o=e.extra,n=po(po(po({},o.attributes),i?{title:i}:{}),{},{class:o.classes.join(" ")}),r=hn(o.styles);r.length>0&&(n.style=r);var a=[];return a.push({tag:"span",attributes:n,children:[t]}),i&&a.push({tag:"span",attributes:{class:"sr-only"},children:[i]}),a}var hr=yn.styles;function fr(e){var t=e[0],i=e[1],o=go(e.slice(4),1)[0];return{found:!0,width:t,height:i,icon:Array.isArray(o)?{tag:"g",attributes:{class:"".concat(rn.familyPrefix,"-").concat(Go)},children:[{tag:"path",attributes:{class:"".concat(rn.familyPrefix,"-").concat(en),fill:"currentColor",d:o[0]}},{tag:"path",attributes:{class:"".concat(rn.familyPrefix,"-").concat(Qo),fill:"currentColor",d:o[1]}}]}:{tag:"path",attributes:{fill:"currentColor",d:o}}}}var gr={found:!1,width:512,height:512};function mr(e,t){var i=t;return"fa"===t&&null!==rn.styleDefault&&(t=Wn()),new Promise((function(o,n){ir("missingIconAbstract");if("fa"===i){var r=Hn(e)||{};e=r.iconName||e,t=r.prefix||t}if(e&&t&&hr[t]&&hr[t][e])return o(fr(hr[t][e]));!function(e,t){Ro||rn.showMissingIcons||!e||console.error('Icon with name "'.concat(e,'" and prefix "').concat(t,'" is missing.'))}(e,t),o(po(po({},gr),{},{icon:rn.showMissingIcons&&e&&ir("missingIconAbstract")||{}}))}))}var vr=function(){},br=rn.measurePerformance&&zo&&zo.mark&&zo.measure?zo:{mark:vr,measure:vr},kr='FA "6.0.0"',yr=function(e){br.mark("".concat(kr," ").concat(e," ends")),br.measure("".concat(kr," ").concat(e),"".concat(kr," ").concat(e," begins"),"".concat(kr," ").concat(e," ends"))},xr=function(e){return br.mark("".concat(kr," ").concat(e," begins")),function(){return yr(e)}},wr=function(){};function $r(e){return"string"==typeof(e.getAttribute?e.getAttribute(Po):null)}function Cr(e){return Eo.createElementNS("http://www.w3.org/2000/svg",e)}function Sr(e){return Eo.createElement(e)}function Or(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.ceFn,o=void 0===i?"svg"===e.tag?Cr:Sr:i;if("string"==typeof e)return Eo.createTextNode(e);var n=o(e.tag);Object.keys(e.attributes||[]).forEach((function(t){n.setAttribute(t,e.attributes[t])}));var r=e.children||[];return r.forEach((function(e){n.appendChild(Or(e,{ceFn:o}))})),n}var Er={replace:function(e){var t=e[0];if(t.parentNode)if(e[1].forEach((function(e){t.parentNode.insertBefore(Or(e),t)})),null===t.getAttribute(Po)&&rn.keepOriginalSource){var i=Eo.createComment(function(e){var t=" ".concat(e.outerHTML," ");return"".concat(t,"Font Awesome fontawesome.com ")}(t));t.parentNode.replaceChild(i,t)}else t.remove()},nest:function(e){var t=e[0],i=e[1];if(~pn(t).indexOf(rn.replacementClass))return Er.replace(e);var o=new RegExp("".concat(rn.familyPrefix,"-.*"));if(delete i[0].attributes.id,i[0].attributes.class){var n=i[0].attributes.class.split(" ").reduce((function(e,t){return t===rn.replacementClass||t.match(o)?e.toSvg.push(t):e.toNode.push(t),e}),{toNode:[],toSvg:[]});i[0].attributes.class=n.toSvg.join(" "),0===n.toNode.length?t.removeAttribute("class"):t.setAttribute("class",n.toNode.join(" "))}var r=i.map((function(e){return Cn(e)})).join("\n");t.setAttribute(Po,""),t.innerHTML=r}};function Dr(e){e()}function zr(e,t){var i="function"==typeof t?t:wr;if(0===e.length)i();else{var o=Dr;"async"===rn.mutateApproach&&(o=Oo.requestAnimationFrame||Dr),o((function(){var t=!0===rn.autoReplaceSvg?Er.replace:Er[rn.autoReplaceSvg]||Er.replace,o=xr("mutate");e.map(t),o(),i()}))}}var Ar=!1;function Lr(){Ar=!0}function _r(){Ar=!1}var Pr=null;function Tr(e){if(Do&&rn.observeMutations){var t=e.treeCallback,i=void 0===t?wr:t,o=e.nodeCallback,n=void 0===o?wr:o,r=e.pseudoElementsCallback,a=void 0===r?wr:r,s=e.observeMutationsRoot,d=void 0===s?Eo:s;Pr=new Do((function(e){if(!Ar){var t=Wn();cn(e).forEach((function(e){if("childList"===e.type&&e.addedNodes.length>0&&!$r(e.addedNodes[0])&&(rn.searchPseudoElements&&a(e.target),i(e.target)),"attributes"===e.type&&e.target.parentNode&&rn.searchPseudoElements&&a(e.target.parentNode),"attributes"===e.type&&$r(e.target)&&~Zo.indexOf(e.attributeName))if("class"===e.attributeName&&function(e){var t=e.getAttribute?e.getAttribute(Bo):null,i=e.getAttribute?e.getAttribute(Io):null;return t&&i}(e.target)){var o=Kn(pn(e.target)),r=o.prefix,s=o.iconName;e.target.setAttribute(Bo,r||t),s&&e.target.setAttribute(Io,s)}else(d=e.target)&&d.classList&&d.classList.contains&&d.classList.contains(rn.replacementClass)&&n(e.target);var d}))}})),Ao&&Pr.observe(d,{childList:!0,attributes:!0,characterData:!0,subtree:!0})}}function Br(e){var t=e.getAttribute("style"),i=[];return t&&(i=t.split(";").reduce((function(e,t){var i=t.split(":"),o=i[0],n=i.slice(1);return o&&n.length>0&&(e[o]=n.join(":").trim()),e}),{})),i}function Ir(e){var t=e.getAttribute("data-prefix"),i=e.getAttribute("data-icon"),o=void 0!==e.innerText?e.innerText.trim():"",n=Kn(pn(e));return n.prefix||(n.prefix=Wn()),t&&i&&(n.prefix=t,n.iconName=i),n.iconName&&n.prefix||n.prefix&&o.length>0&&(n.iconName=function(e,t){return(Bn[e]||{})[t]}(n.prefix,e.innerText)||jn(n.prefix,En(e.innerText))),n}function Fr(e){var t=cn(e.attributes).reduce((function(e,t){return"class"!==e.name&&"style"!==e.name&&(e[t.name]=t.value),e}),{}),i=e.getAttribute("title"),o=e.getAttribute("data-fa-title-id");return rn.autoA11y&&(i?t["aria-labelledby"]="".concat(rn.replacementClass,"-title-").concat(o||ln()):(t["aria-hidden"]="true",t.focusable="false")),t}function Mr(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{styleParser:!0},i=Ir(e),o=i.iconName,n=i.prefix,r=i.rest,a=Fr(e),s=er("parseNodeAttributes",{},e),d=t.styleParser?Br(e):[];return po({iconName:o,title:e.getAttribute("title"),titleId:e.getAttribute("data-fa-title-id"),prefix:n,transform:dn,mask:{iconName:null,prefix:null,rest:[]},maskId:null,symbol:!1,extra:{classes:r,styles:d,attributes:a}},s)}var Rr=yn.styles;function Nr(e){var t="nest"===rn.autoReplaceSvg?Mr(e,{styleParser:!1}):Mr(e);return~t.extra.classes.indexOf(Ho)?ir("generateLayersText",e,t):ir("generateSvgReplacementMutation",e,t)}function Vr(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null;if(!Ao)return Promise.resolve();var i=Eo.documentElement.classList,o=function(e){return i.add("".concat(Fo,"-").concat(e))},n=function(e){return i.remove("".concat(Fo,"-").concat(e))},r=rn.autoFetchSvg?Object.keys(No):Object.keys(Rr),a=[".".concat(Ho,":not([").concat(Po,"])")].concat(r.map((function(e){return".".concat(e,":not([").concat(Po,"])")}))).join(", ");if(0===a.length)return Promise.resolve();var s=[];try{s=cn(e.querySelectorAll(a))}catch(e){}if(!(s.length>0))return Promise.resolve();o("pending"),n("complete");var d=xr("onTree"),l=s.reduce((function(e,t){try{var i=Nr(t);i&&e.push(i)}catch(e){Ro||"MissingIcon"===e.name&&console.error(e)}return e}),[]);return new Promise((function(e,i){Promise.all(l).then((function(i){zr(i,(function(){o("active"),o("complete"),n("pending"),"function"==typeof t&&t(),d(),e()}))})).catch((function(e){d(),i(e)}))}))}function Ur(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null;Nr(e).then((function(e){e&&zr([e],t)}))}var jr=function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.transform,o=void 0===i?dn:i,n=t.symbol,r=void 0!==n&&n,a=t.mask,s=void 0===a?null:a,d=t.maskId,l=void 0===d?null:d,c=t.title,p=void 0===c?null:c,u=t.titleId,h=void 0===u?null:u,f=t.classes,g=void 0===f?[]:f,m=t.attributes,v=void 0===m?{}:m,b=t.styles,k=void 0===b?{}:b;if(e){var y=e.prefix,x=e.iconName,w=e.icon;return lr(po({type:"icon"},e),(function(){return tr("beforeDOMElementCreation",{iconDefinition:e,params:t}),rn.autoA11y&&(p?v["aria-labelledby"]="".concat(rn.replacementClass,"-title-").concat(h||ln()):(v["aria-hidden"]="true",v.focusable="false")),cr({icons:{main:fr(w),mask:s?fr(s.icon):{found:!1,width:null,height:null,icon:{}}},prefix:y,iconName:x,transform:po(po({},dn),o),symbol:r,title:p,maskId:l,titleId:h,extra:{attributes:v,styles:k,classes:g}})}))}},qr={mixout:function(){return{icon:(e=jr,function(t){var i=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},o=(t||{}).icon?t:or(t||{}),n=i.mask;return n&&(n=(n||{}).icon?n:or(n||{})),e(o,po(po({},i),{},{mask:n}))})};var e},hooks:function(){return{mutationObserverCallbacks:function(e){return e.treeCallback=Vr,e.nodeCallback=Ur,e}}},provides:function(e){e.i2svg=function(e){var t=e.node,i=void 0===t?Eo:t,o=e.callback;return Vr(i,void 0===o?function(){}:o)},e.generateSvgReplacementMutation=function(e,t){var i=t.iconName,o=t.title,n=t.titleId,r=t.prefix,a=t.transform,s=t.symbol,d=t.mask,l=t.maskId,c=t.extra;return new Promise((function(t,p){Promise.all([mr(i,r),d.iconName?mr(d.iconName,d.prefix):Promise.resolve({found:!1,width:512,height:512,icon:{}})]).then((function(d){var p=go(d,2),u=p[0],h=p[1];t([e,cr({icons:{main:u,mask:h},prefix:r,iconName:i,transform:a,symbol:s,maskId:l,title:o,titleId:n,extra:c,watchable:!0})])})).catch(p)}))},e.generateAbstractIcon=function(e){var t,i=e.children,o=e.attributes,n=e.main,r=e.transform,a=hn(e.styles);return a.length>0&&(o.style=a),fn(r)&&(t=ir("generateAbstractTransformGrouping",{main:n,transform:r,containerWidth:n.width,iconWidth:n.width})),i.push(t||n.icon),{children:i,attributes:o}}}},Hr={mixout:function(){return{layer:function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.classes,o=void 0===i?[]:i;return lr({type:"layer"},(function(){tr("beforeDOMElementCreation",{assembler:e,params:t});var i=[];return e((function(e){Array.isArray(e)?e.map((function(e){i=i.concat(e.abstract)})):i=i.concat(e.abstract)})),[{tag:"span",attributes:{class:["".concat(rn.familyPrefix,"-layers")].concat(mo(o)).join(" ")},children:i}]}))}}}},Wr={mixout:function(){return{counter:function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.title,o=void 0===i?null:i,n=t.classes,r=void 0===n?[]:n,a=t.attributes,s=void 0===a?{}:a,d=t.styles,l=void 0===d?{}:d;return lr({type:"counter",content:e},(function(){return tr("beforeDOMElementCreation",{content:e,params:t}),ur({content:e.toString(),title:o,extra:{attributes:s,styles:l,classes:["".concat(rn.familyPrefix,"-layers-counter")].concat(mo(r))}})}))}}}},Yr={mixout:function(){return{text:function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=t.transform,o=void 0===i?dn:i,n=t.title,r=void 0===n?null:n,a=t.classes,s=void 0===a?[]:a,d=t.attributes,l=void 0===d?{}:d,c=t.styles,p=void 0===c?{}:c;return lr({type:"text",content:e},(function(){return tr("beforeDOMElementCreation",{content:e,params:t}),pr({content:e,transform:po(po({},dn),o),title:r,extra:{attributes:l,styles:p,classes:["".concat(rn.familyPrefix,"-layers-text")].concat(mo(s))}})}))}}},provides:function(e){e.generateLayersText=function(e,t){var i=t.title,o=t.transform,n=t.extra,r=null,a=null;if(Lo){var s=parseInt(getComputedStyle(e).fontSize,10),d=e.getBoundingClientRect();r=d.width/s,a=d.height/s}return rn.autoA11y&&!i&&(n.attributes["aria-hidden"]="true"),Promise.resolve([e,pr({content:e.innerHTML,width:r,height:a,transform:o,title:i,extra:n,watchable:!0})])}}},Kr=new RegExp('"',"ug"),Xr=[1105920,1112319];function Zr(e,t){var i="".concat("data-fa-pseudo-element-pending").concat(t.replace(":","-"));return new Promise((function(o,n){if(null!==e.getAttribute(i))return o();var r,a,s,d=cn(e.children).filter((function(e){return e.getAttribute(To)===t}))[0],l=Oo.getComputedStyle(e,t),c=l.getPropertyValue("font-family").match(Wo),p=l.getPropertyValue("font-weight"),u=l.getPropertyValue("content");if(d&&!c)return e.removeChild(d),o();if(c&&"none"!==u&&""!==u){var h=l.getPropertyValue("content"),f=~["Solid","Regular","Light","Thin","Duotone","Brands","Kit"].indexOf(c[2])?Vo[c[2].toLowerCase()]:Yo[p],g=function(e){var t,i,o,n,r,a=e.replace(Kr,""),s=(i=0,n=(t=a).length,(r=t.charCodeAt(i))>=55296&&r<=56319&&n>i+1&&(o=t.charCodeAt(i+1))>=56320&&o<=57343?1024*(r-55296)+o-56320+65536:r),d=s>=Xr[0]&&s<=Xr[1],l=2===a.length&&a[0]===a[1];return{value:En(l?a[0]:a),isSecondary:d||l}}(h),m=g.value,v=g.isSecondary,b=c[0].startsWith("FontAwesome"),k=jn(f,m),y=k;if(b){var x=(a=Fn[r=m],s=jn("fas",r),a||(s?{prefix:"fas",iconName:s}:null)||{prefix:null,iconName:null});x.iconName&&x.prefix&&(k=x.iconName,f=x.prefix)}if(!k||v||d&&d.getAttribute(Bo)===f&&d.getAttribute(Io)===y)o();else{e.setAttribute(i,y),d&&e.removeChild(d);var w={iconName:null,title:null,titleId:null,prefix:null,transform:dn,symbol:!1,mask:{iconName:null,prefix:null,rest:[]},maskId:null,extra:{classes:[],styles:{},attributes:{}}},$=w.extra;$.attributes[To]=t,mr(k,f).then((function(n){var r=cr(po(po({},w),{},{icons:{main:n,mask:{prefix:null,iconName:null,rest:[]}},prefix:f,iconName:y,extra:$,watchable:!0})),a=Eo.createElement("svg");"::before"===t?e.insertBefore(a,e.firstChild):e.appendChild(a),a.outerHTML=r.map((function(e){return Cn(e)})).join("\n"),e.removeAttribute(i),o()})).catch(n)}}else o()}))}function Gr(e){return Promise.all([Zr(e,"::before"),Zr(e,"::after")])}function Jr(e){return!(e.parentNode===document.head||~Mo.indexOf(e.tagName.toUpperCase())||e.getAttribute(To)||e.parentNode&&"svg"===e.parentNode.tagName)}function Qr(e){if(Ao)return new Promise((function(t,i){var o=cn(e.querySelectorAll("*")).filter(Jr).map(Gr),n=xr("searchPseudoElements");Lr(),Promise.all(o).then((function(){n(),_r(),t()})).catch((function(){n(),_r(),i()}))}))}var ea=!1,ta=function(e){return e.toLowerCase().split(" ").reduce((function(e,t){var i=t.toLowerCase().split("-"),o=i[0],n=i.slice(1).join("-");if(o&&"h"===n)return e.flipX=!0,e;if(o&&"v"===n)return e.flipY=!0,e;if(n=parseFloat(n),isNaN(n))return e;switch(o){case"grow":e.size=e.size+n;break;case"shrink":e.size=e.size-n;break;case"left":e.x=e.x-n;break;case"right":e.x=e.x+n;break;case"up":e.y=e.y-n;break;case"down":e.y=e.y+n;break;case"rotate":e.rotate=e.rotate+n}return e}),{size:16,x:0,y:0,flipX:!1,flipY:!1,rotate:0})},ia={mixout:function(){return{parse:{transform:function(e){return ta(e)}}}},hooks:function(){return{parseNodeAttributes:function(e,t){var i=t.getAttribute("data-fa-transform");return i&&(e.transform=ta(i)),e}}},provides:function(e){e.generateAbstractTransformGrouping=function(e){var t=e.main,i=e.transform,o=e.containerWidth,n=e.iconWidth,r={transform:"translate(".concat(o/2," 256)")},a="translate(".concat(32*i.x,", ").concat(32*i.y,") "),s="scale(".concat(i.size/16*(i.flipX?-1:1),", ").concat(i.size/16*(i.flipY?-1:1),") "),d="rotate(".concat(i.rotate," 0 0)"),l={outer:r,inner:{transform:"".concat(a," ").concat(s," ").concat(d)},path:{transform:"translate(".concat(n/2*-1," -256)")}};return{tag:"g",attributes:po({},l.outer),children:[{tag:"g",attributes:po({},l.inner),children:[{tag:t.icon.tag,children:t.icon.children,attributes:po(po({},t.icon.attributes),l.path)}]}]}}}},oa={x:0,y:0,width:"100%",height:"100%"};function na(e){var t=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];return e.attributes&&(e.attributes.fill||t)&&(e.attributes.fill="black"),e}var ra={hooks:function(){return{parseNodeAttributes:function(e,t){var i=t.getAttribute("data-fa-mask"),o=i?Kn(i.split(" ").map((function(e){return e.trim()}))):{prefix:null,iconName:null,rest:[]};return o.prefix||(o.prefix=Wn()),e.mask=o,e.maskId=t.getAttribute("data-fa-mask-id"),e}}},provides:function(e){e.generateAbstractMask=function(e){var t,i=e.children,o=e.attributes,n=e.main,r=e.mask,a=e.maskId,s=e.transform,d=n.width,l=n.icon,c=r.width,p=r.icon,u=function(e){var t=e.transform,i=e.containerWidth,o=e.iconWidth,n={transform:"translate(".concat(i/2," 256)")},r="translate(".concat(32*t.x,", ").concat(32*t.y,") "),a="scale(".concat(t.size/16*(t.flipX?-1:1),", ").concat(t.size/16*(t.flipY?-1:1),") "),s="rotate(".concat(t.rotate," 0 0)");return{outer:n,inner:{transform:"".concat(r," ").concat(a," ").concat(s)},path:{transform:"translate(".concat(o/2*-1," -256)")}}}({transform:s,containerWidth:c,iconWidth:d}),h={tag:"rect",attributes:po(po({},oa),{},{fill:"white"})},f=l.children?{children:l.children.map(na)}:{},g={tag:"g",attributes:po({},u.inner),children:[na(po({tag:l.tag,attributes:po(po({},l.attributes),u.path)},f))]},m={tag:"g",attributes:po({},u.outer),children:[g]},v="mask-".concat(a||ln()),b="clip-".concat(a||ln()),k={tag:"mask",attributes:po(po({},oa),{},{id:v,maskUnits:"userSpaceOnUse",maskContentUnits:"userSpaceOnUse"}),children:[h,m]},y={tag:"defs",children:[{tag:"clipPath",attributes:{id:b},children:(t=p,"g"===t.tag?t.children:[t])},k]};return i.push(y,{tag:"rect",attributes:po({fill:"currentColor","clip-path":"url(#".concat(b,")"),mask:"url(#".concat(v,")")},oa)}),{children:i,attributes:o}}}},aa={provides:function(e){var t=!1;Oo.matchMedia&&(t=Oo.matchMedia("(prefers-reduced-motion: reduce)").matches),e.missingIconAbstract=function(){var e=[],i={fill:"currentColor"},o={attributeType:"XML",repeatCount:"indefinite",dur:"2s"};e.push({tag:"path",attributes:po(po({},i),{},{d:"M156.5,447.7l-12.6,29.5c-18.7-9.5-35.9-21.2-51.5-34.9l22.7-22.7C127.6,430.5,141.5,440,156.5,447.7z M40.6,272H8.5 c1.4,21.2,5.4,41.7,11.7,61.1L50,321.2C45.1,305.5,41.8,289,40.6,272z M40.6,240c1.4-18.8,5.2-37,11.1-54.1l-29.5-12.6 C14.7,194.3,10,216.7,8.5,240H40.6z M64.3,156.5c7.8-14.9,17.2-28.8,28.1-41.5L69.7,92.3c-13.7,15.6-25.5,32.8-34.9,51.5 L64.3,156.5z M397,419.6c-13.9,12-29.4,22.3-46.1,30.4l11.9,29.8c20.7-9.9,39.8-22.6,56.9-37.6L397,419.6z M115,92.4 c13.9-12,29.4-22.3,46.1-30.4l-11.9-29.8c-20.7,9.9-39.8,22.6-56.8,37.6L115,92.4z M447.7,355.5c-7.8,14.9-17.2,28.8-28.1,41.5 l22.7,22.7c13.7-15.6,25.5-32.9,34.9-51.5L447.7,355.5z M471.4,272c-1.4,18.8-5.2,37-11.1,54.1l29.5,12.6 c7.5-21.1,12.2-43.5,13.6-66.8H471.4z M321.2,462c-15.7,5-32.2,8.2-49.2,9.4v32.1c21.2-1.4,41.7-5.4,61.1-11.7L321.2,462z M240,471.4c-18.8-1.4-37-5.2-54.1-11.1l-12.6,29.5c21.1,7.5,43.5,12.2,66.8,13.6V471.4z M462,190.8c5,15.7,8.2,32.2,9.4,49.2h32.1 c-1.4-21.2-5.4-41.7-11.7-61.1L462,190.8z M92.4,397c-12-13.9-22.3-29.4-30.4-46.1l-29.8,11.9c9.9,20.7,22.6,39.8,37.6,56.9 L92.4,397z M272,40.6c18.8,1.4,36.9,5.2,54.1,11.1l12.6-29.5C317.7,14.7,295.3,10,272,8.5V40.6z M190.8,50 c15.7-5,32.2-8.2,49.2-9.4V8.5c-21.2,1.4-41.7,5.4-61.1,11.7L190.8,50z M442.3,92.3L419.6,115c12,13.9,22.3,29.4,30.5,46.1 l29.8-11.9C470,128.5,457.3,109.4,442.3,92.3z M397,92.4l22.7-22.7c-15.6-13.7-32.8-25.5-51.5-34.9l-12.6,29.5 C370.4,72.1,384.4,81.5,397,92.4z"})});var n=po(po({},o),{},{attributeName:"opacity"}),r={tag:"circle",attributes:po(po({},i),{},{cx:"256",cy:"364",r:"28"}),children:[]};return t||r.children.push({tag:"animate",attributes:po(po({},o),{},{attributeName:"r",values:"28;14;28;28;14;28;"})},{tag:"animate",attributes:po(po({},n),{},{values:"1;0;1;1;0;1;"})}),e.push(r),e.push({tag:"path",attributes:po(po({},i),{},{opacity:"1",d:"M263.7,312h-16c-6.6,0-12-5.4-12-12c0-71,77.4-63.9,77.4-107.8c0-20-17.8-40.2-57.4-40.2c-29.1,0-44.3,9.6-59.2,28.7 c-3.9,5-11.1,6-16.2,2.4l-13.1-9.2c-5.6-3.9-6.9-11.8-2.6-17.2c21.2-27.2,46.4-44.7,91.2-44.7c52.3,0,97.4,29.8,97.4,80.2 c0,67.6-77.4,63.5-77.4,107.8C275.7,306.6,270.3,312,263.7,312z"}),children:t?[]:[{tag:"animate",attributes:po(po({},n),{},{values:"1;0;0;0;0;1;"})}]}),t||e.push({tag:"path",attributes:po(po({},i),{},{opacity:"0",d:"M232.5,134.5l7,168c0.3,6.4,5.6,11.5,12,11.5h9c6.4,0,11.7-5.1,12-11.5l7-168c0.3-6.8-5.2-12.5-12-12.5h-23 C237.7,122,232.2,127.7,232.5,134.5z"}),children:[{tag:"animate",attributes:po(po({},n),{},{values:"0;0;1;1;0;0;"})}]}),{tag:"g",attributes:{class:"missing"},children:e}}}};!function(e,t){var i=t.mixoutsTo;Zn=e,Gn={},Object.keys(Jn).forEach((function(e){-1===Qn.indexOf(e)&&delete Jn[e]})),Zn.forEach((function(e){var t=e.mixout?e.mixout():{};if(Object.keys(t).forEach((function(e){"function"==typeof t[e]&&(i[e]=t[e]),"object"===uo(t[e])&&Object.keys(t[e]).forEach((function(o){i[e]||(i[e]={}),i[e][o]=t[e][o]}))})),e.hooks){var o=e.hooks();Object.keys(o).forEach((function(e){Gn[e]||(Gn[e]=[]),Gn[e].push(o[e])}))}e.provides&&e.provides(Jn)}))}([bn,qr,Hr,Wr,Yr,{hooks:function(){return{mutationObserverCallbacks:function(e){return e.pseudoElementsCallback=Qr,e}}},provides:function(e){e.pseudoElements2svg=function(e){var t=e.node,i=void 0===t?Eo:t;rn.searchPseudoElements&&Qr(i)}}},{mixout:function(){return{dom:{unwatch:function(){Lr(),ea=!0}}}},hooks:function(){return{bootstrap:function(){Tr(er("mutationObserverCallbacks",{}))},noAuto:function(){Pr&&Pr.disconnect()},watch:function(e){var t=e.observeMutationsRoot;ea?_r():Tr(er("mutationObserverCallbacks",{observeMutationsRoot:t}))}}}},ia,ra,aa,{hooks:function(){return{parseNodeAttributes:function(e,t){var i=t.getAttribute("data-fa-symbol"),o=null!==i&&(""===i||i);return e.symbol=o,e}}}}],{mixoutsTo:sr});var sa=sr.icon;let da=class extends Ji{firstUpdated(e){super.firstUpdated(e);const t=getComputedStyle(this,null).fontSize;this.icon?this.style.minWidth=parseInt(t.replace(/px/,""),10)/this.icon.icon[1]*this.icon.icon[0]+"px":this.style.minWidth="1.0em"}render(){var e;return this.icon?Pi`${null===(e=sa(this.icon))||void 0===e?void 0:e.node}`:Pi``}};da.styles=[ni`
            :host {
                line-height: 1.0em;
                font-size: inherit;
                display: inline-block;
                vertical-align: middle;
                color: inherit;
            }

            :host(.icon-top) {
                vertical-align: top;
            }

            :host(.icon-bottom) {
                vertical-align: bottom;
            }

            :host(.icon-baseline) {
                vertical-align: baseline;
            }

            :host(.icon-xxxl) {
                font-size: 3em;
            }

            :host(.icon-xxl) {
                font-size: 2.5em;
            }

            :host(.icon-xl) {
                font-size: 2em;
            }

            :host(.icon-l) {
                font-size: 1.5em;
            }

            :host(.icon-s) {
                font-size: .75em;
            }

            :host(.icon-xs) {
                font-size: .5em;
            }

            :host(.icon-xxs) {
                font-size: .3em;
            }
        `],Qt([io({type:String})],da.prototype,"icon",void 0),da=Qt([eo("kode4-fa-icon")],da);const la=new Map;let ca=class extends Ji{constructor(){super(...arguments),this.open=!1,this.transparent=!1,this.modal=!1,this.resizeContentObserver=new ResizeObserver((()=>{this.requestUpdate()}))}toggleCssClass(e,t){e?this.classList.add(t):this.classList.remove(t)}set left(e){this.toggleCssClass(e,"kode4-dialog-left")}get left(){return this.classList.contains("kode4-dialog-left")}set right(e){this.toggleCssClass(e,"kode4-dialog-right")}get right(){return this.classList.contains("kode4-dialog-right")}set top(e){this.toggleCssClass(e,"kode4-dialog-top")}get top(){return this.classList.contains("kode4-dialog-top")}set bottom(e){this.toggleCssClass(e,"kode4-dialog-bottom")}get bottom(){return this.classList.contains("kode4-dialog-bottom")}async show(){return this.open=!0,new Promise(((e,t)=>{this.modalResolve=e,this.modalReject=t}))}onConfirm(e){this.modalResolve?(e.stopPropagation(),this.modalResolve(!0),this.hide()):this.onError(e)}onCancel(e){this.modalResolve?(e.stopPropagation(),this.modalResolve(!1),this.hide()):this.onError(e)}onError(e){if(e.stopPropagation(),!this.modalReject)throw new Error(`No modal handlers found for Kode4Dialog: ${this.name}`);this.modalReject(),this.hide()}hide(){this.open=!1,this.modalResolve=void 0,this.modalReject=void 0,this.dispatchEvent(new CustomEvent("close",{bubbles:!0,composed:!0}))}handleBackdropClick(e){e.stopPropagation(),e.preventDefault(),this.modal||this.hide()}get contentWidth(){var e;let t=0;return null===(e=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetWidth)})),t}get contentHeight(){var e;let t=0;return null===(e=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetHeight)})),t}firstUpdated(e){var t;super.firstUpdated(e),null===(t=this.mainItems)||void 0===t||t.forEach((e=>{this.resizeContentObserver.observe(e)}))}connectedCallback(){this.name&&la.set(this.name,this),super.connectedCallback()}disconnectedCallback(){this.resizeContentObserver.disconnect(),this.name&&la.has(this.name)&&la.delete(this.name),super.disconnectedCallback()}handleSlotchange(){console.log("Whatever")}__render(){return Pi`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="dialog">
                <kode4-column class="dialog-content" part="container" style="${this.open?`top: calc(50vh - ${this.contentHeight/2}px);`:void 0} ${this.open?`left: calc(50vw - ${this.contentWidth/2}px);`:void 0} width: ${this.contentWidth}px; height: ${this.contentHeight}px;">
                    <slot name="header" @slotchange=${this.handleSlotchange}></slot>
                    <div class="body" part="body">
                        <slot @slotchange=${this.handleSlotchange}></slot>
                    </div>
                    <div>
                        <slot name="footer" @slotchange=${this.handleSlotchange}>
                            <kode4-row slot="footer">
                                <div slot="title">XXX</div>
                                <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                                <div class="kode4-stretch"></div>
                                <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                            </kode4-row>
                        </slot>
                    </div>
                </kode4-column>
            </div>
        `}render(){return Pi`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="dialog" style="${this.contentWidth>0?`left: calc(50vw - ${this.contentWidth/2}px);`:void 0} ${this.contentHeight>0?`top: calc(50vh - ${this.contentHeight/2}px);`:void 0}">
                <div class="dialog-content">
                        
                    <div class="body">
                        <slot @slotchange=${this.handleSlotchange}></slot>
                    </div>
                        
                    <div>
                        <hr>
                        Width: ${this.contentWidth}
                        <br>
                        Height: ${this.contentHeight}
                    </div>
                </div>
            </div>
        `}_render(){return Pi`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <kode4-card class="dialog-content" style="${this.open?"top: 20vh;":void 0}">
                ${this.modal?Pi`
                    <kode4-row slot="header">
                        <div slot="title">XXX</div>
                        <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                        <div class="kode4-stretch"></div>
                        <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                    </kode4-row>
                `:void 0}
                <slot @slotchange=${this.handleSlotchange}></slot>
                ${this.modal?Pi`
                    <kode4-row slot="footer">
                        <div slot="title">XXX</div>
                        <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                        <div class="kode4-stretch"></div>
                        <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                    </kode4-row>
                `:void 0}
            </kode4-card>
        `}};ca.styles=[ni`
            * {
                box-sizing: border-box;
            }
            
            :host {
                position: fixed;
                display: block;
                z-index: 10010;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                //transition: left 0s linear 0s;
                border: solid 2px #0ff;
            }

            :host(:not([open])) {
                //left: 50vw;
                //right: -50vw;
                visibility: hidden;
                //transition: left 0s linear 0.1s;
            }
            
            //:host(:not([open])) {
            //    bottom: 100vh;
            //    //left: 50vw;
            //    //right: -50vw;
            //    visibility: hidden;
            //    //transition: left 0s linear 0.1s;
            //}

            .dialog {
                position: fixed;
                border: solid 2px #00f;
                //max-width: 80vw;
                background-color: #fff;
                max-width: 90vw;
                max-height: 90vh;
                z-index: 10100;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }
            
            .dialog-content {
                flex: 1 0 1px;
                border: solid 2px #f00;
                height: 100%;
                overflow: hidden;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: stretch;
                align-items: stretch;
                //height: 100%;
            }
        
            
            .body {
                flex: 1 0 1px;
                overflow: auto;
                
            }
        `],Qt([ro("kode4-card")],ca.prototype,"body",void 0),Qt([io({type:String,attribute:"name"})],ca.prototype,"name",void 0),Qt([io({type:Boolean,attribute:"open",reflect:!0})],ca.prototype,"open",void 0),Qt([io({type:Boolean,attribute:"left"})],ca.prototype,"left",null),Qt([io({type:Boolean,attribute:"right"})],ca.prototype,"right",null),Qt([io({type:Boolean,attribute:"top"})],ca.prototype,"top",null),Qt([io({type:Boolean,attribute:"bottom"})],ca.prototype,"bottom",null),Qt([io({type:Boolean,attribute:!0})],ca.prototype,"transparent",void 0),Qt([io({type:Boolean,attribute:!0,reflect:!0})],ca.prototype,"modal",void 0),Qt([oo()],ca.prototype,"modalResolve",void 0),Qt([oo()],ca.prototype,"modalReject",void 0),Qt([lo({slot:"header"})],ca.prototype,"headerItems",void 0),Qt([lo()],ca.prototype,"mainItems",void 0),Qt([lo({slot:"footer"})],ca.prototype,"footerItems",void 0),Qt([io({type:Number})],ca.prototype,"contentWidth",null),Qt([io({type:Number})],ca.prototype,"contentHeight",null),ca=Qt([eo("kode4-dialog")],ca);class pa{constructor(){this.dropSuccessful=!1}static getInstance(){return this.instance||(this.instance=new pa),this.instance}}const ua=pa.getInstance(),ha=(e,t,i)=>{var o;ua.dataTransfer?(ua.dataTransfer.type&&(null===(o=null==t?void 0:t.accepts)||void 0===o?void 0:o.has(ua.dataTransfer.type))&&(e.target.classList.add("kode4-dragover"),e.dataTransfer&&(e.dataTransfer.dropEffect=t.accepts.get(ua.dataTransfer.type))),e.preventDefault()):((null==t?void 0:t.acceptsFiles)||(null==t?void 0:t.acceptsExternal))&&(e.target.classList.add("kode4-dragover"),e.dataTransfer&&(e.dataTransfer.dropEffect="copy"),e.preventDefault()),i&&i(e,ua.dataTransfer)},fa=(e,t)=>{e.target.classList.remove("kode4-dragover"),t&&t(e,ua.dataTransfer)},ga=(e,t,i)=>{e.preventDefault(),e.stopPropagation(),ua.dropSuccessful=!0,e.target.classList.remove("kode4-dragover"),t(e,ua.dataTransfer,i)};var ma=i(955);const va=ni`
    .kode4-text-primary {
        --main-color: var(--main-color-primary);
        --main-color-hover: var(--main-color-hover-primary);
    }

    .kode4-bg-primary {
        --main-color-filled: var(--main-color-filled-primary);
        --main-color-filled-hover: var(--main-color-filled-hover-primary);
        --background-color-filled: var(--background-color-filled-primary);
        --background-color-filled-hover: var(--background-color-filled-hover-primary);
    }

    .kode4-text-secondary {
        --main-color: var(--main-color-secondary);
        --main-color-hover: var(--main-color-hover-secondary);
    }

    .kode4-bg-secondary {
        --main-color-filled: var(--main-color-filled-secondary);
        --main-color-filled-hover: var(--main-color-filled-hover-secondary);
        --background-color-filled: var(--background-color-filled-secondary);
        --background-color-filled-hover: var(--background-color-filled-hover-secondary);
    }

    .kode4-text-success {
        --main-color: var(--main-color-success);
        --main-color-hover: var(--main-color-hover-success);
    }

    .kode4-bg-success {
        --main-color-filled: var(--main-color-filled-success);
        --main-color-filled-hover: var(--main-color-filled-hover-success);
        --background-color-filled: var(--background-color-filled-success);
        --background-color-filled-hover: var(--background-color-filled-hover-success);
    }

    .kode4-text-danger {
        --main-color: var(--main-color-danger);
        --main-color-hover: var(--main-color-hover-danger);
    }

    .kode4-bg-danger {
        --main-color-filled: var(--main-color-filled-danger);
        --main-color-filled-hover: var(--main-color-filled-hover-danger);
        --background-color-filled: var(--background-color-filled-danger);
        --background-color-filled-hover: var(--background-color-filled-hover-danger);
    }

    .kode4-text-warning {
        --main-color: var(--main-color-warning);
        --main-color-hover: var(--main-color-hover-warning);
    }

    .kode4-bg-warning {
        --main-color-filled: var(--main-color-filled-warning);
        --main-color-filled-hover: var(--main-color-filled-hover-warning);
        --background-color-filled: var(--background-color-filled-warning);
        --background-color-filled-hover: var(--background-color-filled-hover-warning);
    }

    .kode4-text-info {
        --main-color: var(--main-color-info);
        --main-color-hover: var(--main-color-hover-info);
    }

    .kode4-bg-info {
        --main-color-filled: var(--main-color-filled-info);
        --main-color-filled-hover: var(--main-color-filled-hover-info);
        --background-color-filled: var(--background-color-filled-info);
        --background-color-filled-hover: var(--background-color-filled-hover-info);
    }


    .kode4-text,
    .kode4-text-primary,
    .kode4-text-secondary,
    .kode4-text-success,
    .kode4-text-danger,
    .kode4-text-warning,
    .kode4-text-info {
        color: var(--main-color);
    }

    a.kode4-text:hover,
    a.kode4-text-primary:hover,
    a.kode4-text-secondary:hover,
    a.kode4-text-success:hover,
    a.kode4-text-danger:hover,
    a.kode4-text-warning:hover,
    a.kode4-text-info:hover,
    .kode4-text-hover:hover {
        color: var(--main-color-hover);
    }

    .kode4-bg,
    .kode4-bg-primary,
    .kode4-bg-secondary,
    .kode4-bg-success,
    .kode4-bg-danger,
    .kode4-bg-warning,
    .kode4-bg-info {
        color: var(--main-color-filled);
        background-color: var(--background-color-filled);
    }

    a.kode4-bg:hover,
    a.kode4-bg-primary:hover,
    a.kode4-bg-secondary:hover,
    a.kode4-bg-success:hover,
    a.kode4-bg-danger:hover,
    a.kode4-bg-warning:hover,
    a.kode4-bg-info:hover,
    .kode4-bg-hover:hover{
        color: var(--main-color-filled-hover);
        background-color: var(--background-color-filled-hover);
    }

    .kode4-alert {
        padding: 10px;
        color: var(--main-color-filled);
        background-color: var(--background-color-filled);
    }

    a.kode4-alert:hover,
    .kode4-alert-hover:hover {
        color: var(--main-color-filled-hover);
        background-color: var(--background-color-filled-hover);
    }



    /* HELPERS FOR CENTERING */
    .kode4-center-v {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        height: 100%;
    }

    .kode4-center-h {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        align-items: center;
    }

    .kode4-center {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: center;
    }

    .kode4-center-full {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: center;
        height: 100%;
    }

    .kode4-spacer {
        flex: 1 0 1px;
    }

`;var ba;let ka=ba=class extends Ji{constructor(){super(...arguments),this.type="linear",this.indeterminate=!1,this.inline=!1,this.showPercentage=!1,this.progress=0,this.visible=!0}firstUpdated(e){super.firstUpdated(e),this.classList.add(`kode4-progress-${this.type}`),this.inline&&this.classList.add("kode4-progress-inline")}render(){switch(this.type){case"icon":return this.renderProgessIcon();case"text":return this.renderProgessText();default:return this.renderProgressLinear()}}renderProgessIcon(){return Pi`
            <div class="progress-icon ${this.indeterminate?"progress-icon-indeterminate":"progress-icon-determinate"}">
                ${this.indeterminate?this.renderProgessBarIndeterminateIcon():this.renderProgessBarDeterminateIcon()}
            </div>
        `}renderProgessBarIndeterminateIcon(){return Pi`
            <div class="indeterminate-icon kode4-text ${this.color?`kode4-text-${this.color}`:""}">
                <kode4-fa-icon .icon="${ma.IJ}"></kode4-fa-icon>
            </div>
        `}renderProgessBarDeterminateIcon(){return Pi`
            <div class="determinate-icon kode4-text ${this.color?`kode4-text-${this.color}`:""}"
                 style="-webkit-transform:rotate(${this.getAngleForProgress()}deg); -moz-transform:rotate(${this.getAngleForProgress()}deg); transform:rotate(${this.getAngleForProgress()}deg);">
                <kode4-fa-icon .icon="${ma.IJ}"></kode4-fa-icon>
            </div>
        `}getAngleForProgress(){return Math.round(3.6*this.progress)}renderProgessText(){return Pi`
            <div class="progress-text ${this.indeterminate?"proress-text-indeterminate":""}">
                ${this.indeterminate?this.renderProgessBarIndeterminateText():this.renderProgessBarDeterminateText()}
            </div>
        `}renderProgessBarIndeterminateText(){return Pi`
            <div class="indeterminate-text kode4-text ${this.color?`kode4-text-${this.color}`:""}">
                |
            </div>
        `}renderProgessBarDeterminateText(){return Pi`
            <div class="determinate-text kode4-text ${this.color?`kode4-text-${this.color}`:""}">
                ${this.progress} %
            </div>
        `}renderProgressLinear(){return Pi`
            <div class="progress-linear">
                ${this.indeterminate?this.renderProgressBarIndeterminateLinear():this.renderProgressBarDeterminateLinear()}
            </div>
        `}renderProgressBarIndeterminateLinear(){return Pi`
            <div class="indeterminate kode4-text ${this.color?`kode4-text-${this.color}`:""}"></div>
        `}renderProgressBarDeterminateLinear(){return Pi`
            <div class="determinate kode4-text ${this.color?`kode4-text-${this.color}`:""}"
                 style="width: ${this.progress}%;"></div>
        `}static getIconStyles(){return ni`
            .progress-icon-determinate,
            .progress-icon-indeterminate {
                display: flex;
                flex-direction: row;
                min-width: 1.0em;
                height: 1.0em;
                line-height: 1.0em;
                transform-origin: center center;
            }

            .determinate-icon {
                display: inline-block;
                transition: all .3s linear;
            }

            .indeterminate-icon {
                display: inline-block;
                -webkit-animation: indeterminate-icon 1.5s linear infinite;
                -moz-animation: indeterminate-icon 1.5s linear infinite;
                animation: indeterminate-icon 1.5s linear infinite;
            }

            @-moz-keyframes indeterminate-icon {
                from {
                    -moz-transform: rotate(0deg);
                }
                to {
                    -moz-transform: rotate(360deg);
                }
            }
            @-webkit-keyframes indeterminate-icon {
                from {
                    -webkit-transform: rotate(0deg);
                }
                to {
                    -webkit-transform: rotate(360deg);
                }
            }
            @keyframes indeterminate-icon {
                from {
                    transform: rotate(0deg);
                }
                to {
                    transform: rotate(360deg);
                }
            }
        `}static getTextStyles(){return ni`
            .proress-text-indeterminate {
                display: block;
                min-width: 1.0em;
                height: 1.0em;
                line-height: 1.0em;
                display: block;
                text-align: center;
            }

            .indeterminate-text {
                display: inline-block;
                -webkit-animation: indeterminate-text 1.5s linear infinite;
                -moz-animation: indeterminate-text 1.5s linear infinite;
                animation: indeterminate-text 1.5s linear infinite;
            }

            @-moz-keyframes indeterminate-text {
                from {
                    -moz-transform: rotate(0deg);
                }
                to {
                    -moz-transform: rotate(360deg);
                }
            }
            @-webkit-keyframes indeterminate-text {
                from {
                    -webkit-transform: rotate(0deg);
                }
                to {
                    -webkit-transform: rotate(360deg);
                }
            }
            @keyframes indeterminate-text {
                from {
                    transform: rotate(0deg);
                }
                to {
                    transform: rotate(360deg);
                }
            }

        `}static getLinearStyles(){return ni`
            .progress-linear {
                position: relative;
                display: block;
                width: 100%;
                height: 100%;
                background-color: transparent;
                background-clip: padding-box;
                margin: 0;
                padding: 0;
                overflow: hidden;
            }

            .progress-linear .determinate {
                position: absolute;
                top: 0;
                bottom: 0;
                background-color: currentColor;
                transition: width .3s linear;
            }

            .progress-linear .indeterminate {
                background-color: currentColor;
            }

            .progress-linear .indeterminate:before {
                content: '';
                position: absolute;
                background-color: inherit;
                top: 0;
                left: 0;
                bottom: 0;
                will-change: left, right;
                -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
                animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
            }

            .progress-linear .indeterminate:after {
                content: '';
                position: absolute;
                background-color: inherit;
                top: 0;
                left: 0;
                bottom: 0;
                will-change: left, right;
                -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
                animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
                -webkit-animation-delay: 1.15s;
                animation-delay: 1.15s;
            }

            @-webkit-keyframes indeterminate {
                0% {
                    left: -35%;
                    right: 100%;
                }
                60% {
                    left: 100%;
                    right: -90%;
                }
                100% {
                    left: 100%;
                    right: -90%;
                }
            }

            @keyframes indeterminate {
                0% {
                    left: -35%;
                    right: 100%;
                }
                60% {
                    left: 100%;
                    right: -90%;
                }
                100% {
                    left: 100%;
                    right: -90%;
                }
            }

            @-webkit-keyframes indeterminate-short {
                0% {
                    left: -200%;
                    right: 100%;
                }
                60% {
                    left: 107%;
                    right: -8%;
                }
                100% {
                    left: 107%;
                    right: -8%;
                }
            }

            @keyframes indeterminate-short {
                0% {
                    left: -200%;
                    right: 100%;
                }
                60% {
                    left: 107%;
                    right: -8%;
                }
                100% {
                    left: 107%;
                    right: -8%;
                }
            }
        `}};ka.styles=[va,ni`

            :host {
                display: block;
            }

            :host(.kode4-progress-inline) {
                display: inline-block;
            }

            :host(.kode4-progress-linear) {
                height: 2px;
            }

            :host(.stretch-height) {
                height: 100%;
            }

            :host(.stretch-width) {
                width: 100%;
                display: block;
            }

        `,ba.getLinearStyles(),ba.getTextStyles(),ba.getIconStyles()],Qt([io({type:String})],ka.prototype,"type",void 0),Qt([io({type:String})],ka.prototype,"color",void 0),Qt([io({type:Boolean})],ka.prototype,"indeterminate",void 0),Qt([io({type:Boolean})],ka.prototype,"inline",void 0),Qt([io({type:Boolean})],ka.prototype,"showPercentage",void 0),Qt([io({type:Number})],ka.prototype,"progress",void 0),Qt([io({type:Boolean})],ka.prototype,"visible",void 0),ka=ba=Qt([eo("kode4-progress")],ka);let ya=class extends Ji{constructor(){super(...arguments),this.color="",this._open=!1,this.opening=!1,this.closing=!1}get open(){return this._open}set open(e){this._open=e}render(){return Pi`
            <div class="backdrop ${this.open?"visible":""} ${this.opening?"opening":""} ${this.closing?"closing":""}">
                ${this.open?Pi`
                    <div class="content" @click="${e=>e.stopPropagation()}">        
                        <slot>
                            <kode4-progress type="icon" color="${this.color}" indeterminate></kode4-progress>            
                        </slot>
                    </div>
                `:""}
            </div>
        `}};ya.styles=[va,ni`
            :host {
                width: 0;
                height: 0;
            }

            .backdrop {
                display: flex;
                visibility: visible;
                opacity: 0;
                position: fixed;
                top: 0;
                bottom: auto;
                left: 0;
                right: auto;
                width: 0;
                height: 0;
                overflow: hidden;
                background-color: rgba(0, 0, 0, 0.25);
                align-items: center;
                justify-items: center;
                z-index: 5000;
                transition: opacity 0.25s linear, bottom 0s linear 0.25s, right 0s linear 0.25s, width 0s linear 0.25s, height 0s linear 0.25s;

                -moz-user-select: none;
                -khtml-user-select: none;
                -webkit-user-select: none;
                user-select: none;
                /* Required to make elements draggable in old WebKit */
                -khtml-user-drag: element;
                -webkit-user-drag: element;
            }

            .content {
                background-color: transparent;
                max-width: 90vw;
                max-height: 90vh;
                z-index: 5010;
                margin-left: auto;
                margin-right: auto;
                padding: 0;
                border-radius: 2px;
                text-shadow: 2px 2px 20px #888;
                font-size: 5vmin;
            }

            .backdrop.visible {
                opacity: 1;
            }

            .backdrop.visible,
            .backdrop.opening,
            .backdrop.closing {
                transition: opacity 0.25s linear, bottom 0s linear 0.25s, right 0s linear 0.25s, width 0s linear 0.25s, height 0s linear 0.25s;
                bottom: 0;
                right: 0;
                width: auto;
                height: auto;
            }
        `],Qt([io({type:String})],ya.prototype,"color",void 0),Qt([io({type:Boolean})],ya.prototype,"_open",void 0),Qt([io({type:Boolean})],ya.prototype,"open",null),Qt([io({type:Boolean})],ya.prototype,"opening",void 0),Qt([io({type:Boolean})],ya.prototype,"closing",void 0),ya=Qt([eo("kode4-modalprogress")],ya);var xa,wa=new Uint8Array(16);function $a(){if(!xa&&!(xa="undefined"!=typeof crypto&&crypto.getRandomValues&&crypto.getRandomValues.bind(crypto)||"undefined"!=typeof msCrypto&&"function"==typeof msCrypto.getRandomValues&&msCrypto.getRandomValues.bind(msCrypto)))throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");return xa(wa)}const Ca=/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i;const Sa=function(e){return"string"==typeof e&&Ca.test(e)};for(var Oa=[],Ea=0;Ea<256;++Ea)Oa.push((Ea+256).toString(16).substr(1));const Da=function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,i=(Oa[e[t+0]]+Oa[e[t+1]]+Oa[e[t+2]]+Oa[e[t+3]]+"-"+Oa[e[t+4]]+Oa[e[t+5]]+"-"+Oa[e[t+6]]+Oa[e[t+7]]+"-"+Oa[e[t+8]]+Oa[e[t+9]]+"-"+Oa[e[t+10]]+Oa[e[t+11]]+Oa[e[t+12]]+Oa[e[t+13]]+Oa[e[t+14]]+Oa[e[t+15]]).toLowerCase();if(!Sa(i))throw TypeError("Stringified UUID is invalid");return i};const za=function(e,t,i){var o=(e=e||{}).random||(e.rng||$a)();if(o[6]=15&o[6]|64,o[8]=63&o[8]|128,t){i=i||0;for(var n=0;n<16;++n)t[i+n]=o[n];return t}return Da(o)};var Aa=i(545);const La=new class{constructor(){this.observers=[],this.fireGrowlAlert=e=>{this.observers.forEach((t=>{t.onGrowlAlert(e)}))}}observe(e){this.observers.includes(e)||this.observers.push(e)}unobserve(e){this.observers=e?[...this.observers.filter((t=>t!==e))]:[]}alert(e){this.fireGrowlAlert(e)}},_a=()=>La;let Pa=class extends Ji{constructor(){super(...arguments),this.messages=[],this.horizontalAlign="right",this.verticalAlign="bottom"}alert(e){e.id||(e.id=`kode4growlmessage_${za()}`);const t=e.id;this.messages=[...this.messages,e],e.timeout&&setTimeout((()=>{this.closeMessage(t)}),e.timeout)}closeMessage(e){e&&(this.messages=[...this.messages.filter((t=>t.id!==e))])}render(){return Pi`
            <div class="container ${this.horizontalAlign} ${this.verticalAlign}" part="container">
                ${this.messages.map((e=>Pi`
                    <div class="message kode4-alert ${e.color?`kode4-bg-${e.color}`:""}"
                         id="${e.id}">
                        <div class="closeButton kode4-text-hover" @click="${t=>{t.preventDefault(),this.closeMessage(e.id)}}">
                            <kode4-fa-icon .icon="${Aa.NB}"></kode4-fa-icon>
                        </div>
                        ${e.message}
                    </div>
                `))}
            </div>
        `}};Pa.styles=[va,ni`
            :host {
                display: inline-block;
                position: relative;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                border: none;
                z-index: 10010;
            }

            .container {
                position: absolute;
                max-height: 600px;
                bottom: 0;
                right: 0;
                z-index: 10020;
            }

            .container.bottom {
                top: auto;
                bottom: 0;
            }

            .container.top {
                top: 0;
                bottom: auto;
            }

            .container.right {
                left: auto;
                right: 0;
                width: 300px;
                max-width: 80vw;
            }

            .container.left {
                left: 0;
                right: auto;
                width: 300px;
                max-width: 80vw;
            }

            .container.stretch {
                left: 0;
                right: 0;
                width: auto;
            }

            .message {
                width: auto;
                position: relative;
            }

            .message + .message {
                margin-top: 10px;
            }

            .closeButton {
                position: absolute;
                top: 5px;
                right: 5px;
                cursor: pointer;
                opacity: 0;
                transition: all 0.15s linear;
            }

            .message:hover .closeButton {
                opacity: 1;
            }
        `],Qt([io()],Pa.prototype,"messages",void 0),Qt([io({attribute:"horizontal-align"})],Pa.prototype,"horizontalAlign",void 0),Qt([io({attribute:"vertical-align"})],Pa.prototype,"verticalAlign",void 0),Pa=Qt([eo("kode4-growl")],Pa);let Ta=class extends Ji{connectedCallback(){_a().observe(this),super.connectedCallback()}disconnectedCallback(){_a().unobserve(this),super.disconnectedCallback()}onGrowlAlert(e){this.alert(e)}alert(e){var t,i,o,n,r,a;switch(e.pos){case"t":case"top":null===(t=this.growlT)||void 0===t||t.alert(e);break;case"tl":case"top-left":null===(i=this.growlTl)||void 0===i||i.alert(e);break;case"tr":case"top-right":null===(o=this.growlTr)||void 0===o||o.alert(e);break;case"b":case"bottom":null===(n=this.growlB)||void 0===n||n.alert(e);break;case"bl":case"bottom-left":null===(r=this.growlBl)||void 0===r||r.alert(e);break;default:null===(a=this.growlBr)||void 0===a||a.alert(e)}}render(){return Pi`
            <div>
                
            <kode4-growl id="growlTop" horizontal-align="stretch" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopLeft" horizontal-align="left" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopRight" horizontal-align="right" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlBottom" horizontal-align="stretch" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomLeft" horizontal-align="left" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomRight" horizontal-align="right" vertical-align="bottom"></kode4-growl>
            </div>
        `}};Ta.styles=[va,ni`
            kode4-growl {
                position: fixed;
                display: block;
                z-index: 12000;
            }
            
            #growlTop {
                top: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlTopLeft {
                top: 10px;
                left: 10px;
            }
            
            #growlTopRight {
                top: 10px;
                right: 10px;
            }
            
            #growlBottom {
                bottom: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlBottomLeft {
                bottom: 10px;
                left: 10px;
            }
            
            #growlBottomRight {
                bottom: 10px;
                right: 10px;
            }
        `],Qt([ro("kode4-growl#growlTop")],Ta.prototype,"growlT",void 0),Qt([ro("kode4-growl#growlTopLeft")],Ta.prototype,"growlTl",void 0),Qt([ro("kode4-growl#growlTopRight")],Ta.prototype,"growlTr",void 0),Qt([ro("kode4-growl#growlBottom")],Ta.prototype,"growlB",void 0),Qt([ro("kode4-growl#growlBottomLeft")],Ta.prototype,"growlBl",void 0),Qt([ro("kode4-growl#growlBottomRight")],Ta.prototype,"growlBr",void 0),Ta=Qt([eo("kode4-growl-areas")],Ta);let Ba=class extends Ji{constructor(){super(...arguments),this.open=!1,this.closeDelay=150,this.x="right",this.y="center",this.mouseEntered=!1,this.container=null}connectedCallback(){var e,t;this.parendMouseenterEventListener=this.onParentMouseover.bind(this),this.parendMouseleaveEventListener=this.onParentMouseleave.bind(this),null===(e=this.parentElement)||void 0===e||e.addEventListener("mouseenter",this.parendMouseenterEventListener),null===(t=this.parentElement)||void 0===t||t.addEventListener("mouseleave",this.parendMouseleaveEventListener),super.connectedCallback()}disconnectedCallback(){var e,t;null===(e=this.parentElement)||void 0===e||e.removeEventListener("mouseenter",this.parendMouseenterEventListener),null===(t=this.parentElement)||void 0===t||t.removeEventListener("mouseleave",this.parendMouseleaveEventListener),super.disconnectedCallback()}firstUpdated(e){var t;super.firstUpdated(e),this.container=(null===(t=this.shadowRoot)||void 0===t?void 0:t.querySelector(".container"))||null}onParentMouseover(){this.mouseEntered=!0,setTimeout((()=>{var e;if(this.parentElement&&this.container){this.container.classList.add("opening");const t=null===(e=this.parentElement)||void 0===e?void 0:e.getBoundingClientRect(),i=this.container.getBoundingClientRect(),o=this.getBoundingClientRect();let n,r;switch(this.y){case"top":n=0-(o.y-t.y)-i.height-10;break;case"bottom":n=t.height+10;break;default:n=t.height/2-(o.y-t.y)-i.height/2}switch(this.x){case"center":r=t.width/2-(o.x-t.x)-i.width/2;break;case"left":r=0-(o.x-t.x)-i.width-10;break;default:r=t.width+10}this.container.style.top=`${n}px`,this.container.style.left=`${r}px`,this.container.classList.remove("opening")}this.open=!0}),this.closeDelay-10)}onParentMouseleave(){this.mouseEntered=!1,setTimeout((()=>{this.mouseEntered||(this.open=!1)}),this.closeDelay)}render(){return Pi`
            <div class="container ${this.open?"open":""} ${this.arrowclass}">
                <slot></slot>
            </div>
        `}get arrowclass(){return"bottom"===this.y&&"center"===this.x?"arrowtop":"top"===this.y&&"center"===this.x?"arrowbottom":"left"===this.x&&"center"===this.y?"arrowleft":"right"===this.x&&"center"===this.y?"arrowright":""}};Ba.styles=[va,ni`
            :host {
                position: relative;
                filter: none !important;
                background: transparent;
            }

            .container {
                display: none;
            }
            
            .container.open,
            .container.opening {
                display: block;
                position: absolute;
                z-index: 9000;
                top: 0;
                left: 0;
                padding-top: var(--kode4-tooltip-padding-top);
                padding-right: var(--kode4-tooltip-padding-right);
                padding-bottom: var(--kode4-tooltip-padding-bottom);
                padding-left: var(--kode4-tooltip-padding-left);
                filter: brightness(1) sepia(0) hue-rotate(0) saturate(100%);
                color: var(--kode4-tooltip-color);
                font-family: var(--kode4-tooltip-font-family);
                font-size: var(--kode4-tooltip-font-size);
                line-height: var(--kode4-tooltip-line-height);
                font-weight: var(--kode4-tooltip-font-weight);
                background: var(--kode4-tooltip-background-color);
                box-sizing: border-box;
                border-radius: var(--kode4-tooltip-border-radius);
            }

            .container.opening {
                visibility: hidden;
            }

            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowright:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                left: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0;
                border-color: transparent var(--kode4-tooltip-background-color) transparent transparent;
            }
            
            .container.open.arrowbottom:before {
                content: " ";
                background: transparent;
                position: absolute;
                bottom: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size);
                border-color: var(--kode4-tooltip-background-color) transparent transparent transparent;
            }
            
            .container.open.arrowleft:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                right: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent transparent var(--kode4-tooltip-background-color);
            }
        `],Qt([io({type:Boolean})],Ba.prototype,"open",void 0),Qt([io({type:Number})],Ba.prototype,"closeDelay",void 0),Qt([io({type:String})],Ba.prototype,"x",void 0),Qt([io({type:String})],Ba.prototype,"y",void 0),Qt([io({type:Boolean})],Ba.prototype,"mouseEntered",void 0),Qt([io()],Ba.prototype,"container",void 0),Qt([io()],Ba.prototype,"arrowclass",null),Ba=Qt([eo("kode4-tooltip")],Ba);let Ia=class extends Ji{scrollBodyTo(e){this.bodyElement.scrollTo(0,e)}onBodyScrolled(){this.dispatchEvent(new CustomEvent("kode4-body-scrolled",{bubbles:!0,composed:!0,detail:{pos:this.bodyElement.scrollTop}}))}render(){return Pi`
            <div class="kode4-applayout-header">
                <slot name="header"></slot>
            </div>
            
            <div class="kode4-applayout-progressbar-top">
                <div class="kode4-applayout-progressbar-top-content">
                    <slot name="progressbar-top"></slot>
                </div>
            </div>
            
            <div class="kode4-applayout-tray-left">
                <slot name="tray-left"></slot>
            </div>
            
            <div class="kode4-applayout-menu-left">
                <slot name="menu-left""></slot>
            </div>

            <div class="kode4-applayout-body" @scroll="${this.onBodyScrolled}">
                <slot name="body"></slot>
            </div>

            <div class="kode4-applayout-menu-right">
                <slot name="menu-right"></slot>
            </div>
            
            <div class="kode4-applayout-tray-right">
                <slot name="tray-right"></slot>
            </div>

            <div class="kode4-applayout-progressbar-bottom">
                <div class="kode4-applayout-progressbar-bottom-content">
                    <slot name="progressbar-bottom"></slot>
                </div>
            </div>

            <div class="kode4-applayout-footer">
                <slot name="footer"></slot>
            </div>
        `}};Ia.styles=[ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: grid;
                //position: relative;
                width: 100vw;
                height: 100vh;
                overflow: hidden;
                grid-template-rows: [header] auto [progressbar-top] auto [body] 1fr [progressbar-bottom] auto [footer] auto [end];
                grid-template-columns: [tray-left] auto [menu-left] auto [body] 1fr [menu-right] auto [tray-right] auto;
            }
            
            .kode4-applayout-header {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: header;
                //z-index: 90;
            }

            .kode4-applayout-tray-left {
                grid-column: tray-left;
                grid-row: body;
                writing-mode: vertical-lr;
                text-orientation: mixed;
                //z-index: 70;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            .kode4-applayout-tray-left ::slotted(:not(.kode4-applayout-tray-left-norotate)) {
                display: inline-block;
                transform: rotate(180deg);
            }

            .kode4-applayout-menu-left {
                grid-column: menu-left;
                grid-row: body;
                //z-index: 50;
            }

            div.kode4-applayout-menu-left {
                display: flex;
                flex-direction: row;
                height: 100%;
            }

            .kode4-applayout-body {
                grid-column: body;
                grid-row: body;
                overflow-x: hidden;
                overflow-y: auto;
                //z-index: 100;
            }

            .kode4-applayout-menu-right {
                grid-column: menu-right;
                grid-row: body;
                //z-index: 50;
            }

            div.kode4-applayout-menu-right {
                display: flex;
                flex-direction: row;
                height: 100%;
            }

            .kode4-applayout-tray-right {
                grid-column: tray-right;
                grid-row: body;
                writing-mode: vertical-rl;
                text-orientation: mixed;
                //z-index: 70;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            .kode4-applayout-footer {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: footer;
                //z-index: 90;
            }

            .kode4-applayout-progressbar-top {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: progressbar-top;
                position: relative;
            }
            
            .kode4-applayout-progressbar-top-content {
                position: absolute;
                left: 0;
                right: 0;
                top: 0;
                //z-index: 150;
            }

            .kode4-applayout-progressbar-bottom {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: progressbar-bottom;
                position: relative;
            }
            
            .kode4-applayout-progressbar-bottom-content {
                position: absolute;
                left: 0;
                right: 0;
                bottom: 0;
                //z-index: 150;
            }
        `],Qt([ro(".kode4-applayout-body")],Ia.prototype,"bodyElement",void 0),Ia=Qt([eo("kode4-applayout")],Ia);let Fa=class extends Ji{constructor(){super(...arguments),this.open=!1,this.alwaysopen=!1,this.absolute=!1,this.left=!0,this.right=!1,this.shadow=!1,this.animations=!0,this.animationEnabled=!1,this.resizeContentObserver=new ResizeObserver((()=>{this.requestUpdate()}))}get contentWidth(){var e;let t=100;return null===(e=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetWidth)})),`${t}px`}get cssClassLeftRight(){return this.right?"right":"left"}firstUpdated(e){var t;super.firstUpdated(e),null===(t=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===t||t.forEach((e=>{this.resizeContentObserver.observe(e)})),this.animations&&setTimeout((()=>{this.animationEnabled=!0}),100)}connectedCallback(){super.connectedCallback()}disconnectedCallback(){this.resizeContentObserver.disconnect(),super.disconnectedCallback()}render(){return Pi`
            <div class="body ${this.open||this.alwaysopen?"open":""} ${this.alwaysopen?"alwaysopen":""} ${this.absolute?"absolute":""} ${this.cssClassLeftRight} ${this.shadow?"shadow":""} ${this.animationEnabled?"animate":""}" style="width: ${this.contentWidth};">
                <kode4-column class="bodycontent" part="container">
                    <slot name="header"></slot>
                    <div style="flex: 1 0 1px; overflow: auto;">
                        <slot class="kode4-stretch"></slot>
                    </div>
                    <slot name="footer"></slot>
                </kode4-column>
            </div>
        `}};Fa.styles=[ni`
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                position: relative;
            }

            .body {
                display: block;
                position: relative;
                overflow-y: hidden;
                height: 100%;
            }
            
            .body.animate:not(.alwaysopen) {
                transition: all 0.25s linear;
            }
            
            .body.shadow.left {
                box-shadow: 2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .body.shadow.right {
                box-shadow: -2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .bodycontent {
                position: absolute;
                bottom: 0;
                top: 0;
                overflow: hidden;
            }
            
            .body.left .bodycontent {
                left: auto;
                right: 0;
            }
            
            .body.right .bodycontent {
                left: 0;
                right: auto;
            }
            
            .body.absolute {
                position: absolute;
                top: 0;
                bottom: 0;
                height: auto;
                overflow: visible;
            }

            .body.absolute.left {
                left: 0;
                right: auto;
            }

            .body.absolute.right {
                left: auto;
                right: 0;
            }

            .body:not(.open) {
                width: 0 !important;
                overflow: hidden;
            }
        `],Qt([lo({slot:"header"})],Fa.prototype,"headerItems",void 0),Qt([lo()],Fa.prototype,"mainItems",void 0),Qt([lo({slot:"footer"})],Fa.prototype,"footerItems",void 0),Qt([io({type:Boolean,reflect:!0})],Fa.prototype,"open",void 0),Qt([io({type:Boolean,reflect:!0})],Fa.prototype,"alwaysopen",void 0),Qt([io({type:Boolean})],Fa.prototype,"absolute",void 0),Qt([io({type:Boolean})],Fa.prototype,"left",void 0),Qt([io({type:Boolean})],Fa.prototype,"right",void 0),Qt([io({type:Boolean})],Fa.prototype,"shadow",void 0),Qt([io({type:Boolean})],Fa.prototype,"animations",void 0),Qt([oo()],Fa.prototype,"animationEnabled",void 0),Qt([io({type:Number})],Fa.prototype,"contentWidth",null),Qt([oo()],Fa.prototype,"cssClassLeftRight",null),Fa=Qt([eo("kode4-sidemenu")],Fa);let Ma=class extends Ji{constructor(){super(...arguments),this.spacing=!1}updateCssClass(e,t,i,o){e.has(t)&&(i?this.classList.add(o):this.classList.remove(o))}updated(e){super.updated(e),this.updateCssClass(e,"spacing",this.spacing,"kode4-spacing")}render(){return Pi`
            <div class="center-title" part="title">
                <slot name="title"></slot>
            </div>
            <slot></slot>
        `}};Ma.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                position: relative;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }
            
            ::slotted(*:not(kode4-column)) {
                position: relative;
                //align-self: center;
                //display: flex;
                //flex-direction: row;
                //flex-wrap: nowrap;
                //align-items: center;
            }

            ::slotted(kode4-column) {
                align-self: stretch;
                height: auto;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
            }
            
            .center-title {
                position: absolute;
                left: 0;
                right: 0;
                margin-left: auto;
                margin-right: auto;
                top: 0;
                bottom: 0;
                display: inline-flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
                justify-content: center;
            }
        `],Qt([io({type:Boolean,attribute:!0})],Ma.prototype,"spacing",void 0),Ma=Qt([eo("kode4-row")],Ma);let Ra=class extends Ji{constructor(){super(...arguments),this.spacing=!1}updateCssClass(e,t,i,o){e.has(t)&&(i?this.classList.add(o):this.classList.remove(o))}updated(e){super.updated(e),this.updateCssClass(e,"spacing",this.spacing,"kode4-spacing")}render(){return Pi`
            <slot></slot>
        `}};Ra.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                height: 100%;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }

            ::slotted(*:not(kode4-row)) {
                position: relative;
                //align-self: stretch;
                //display: flex;
                //flex-direction: column;
                //flex-wrap: nowrap;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
                place-self: flex-end stretch;
            }
        `],Qt([io({type:Boolean,attribute:!0})],Ra.prototype,"spacing",void 0),Ra=Qt([eo("kode4-column")],Ra);let Na=class extends Ji{render(){return Pi`
            <slot></slot>
        `}};Na.styles=[va,ni`
            :host {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }

            ::slotted(*) {
                position: relative;
                align-self: stretch;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            ::slotted(.kode4-vcenter) {
                align-self: center;
            }

            ::slotted(.kode4-vcontent) {
                position: relative;
                align-self: stretch;
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
            }

            ::slotted(.kode4-top) {
                align-items: flex-start;
            }

            ::slotted(.kode4-bottom) {
                align-items: flex-end;
            }

            ::slotted(.kode4-right) {
                justify-content: flex-end;
            }

            ::slotted(.kode4-center) {
                justify-content: center;
            }

        `],Na=Qt([eo("kode4-toolbar")],Na);let Va=class extends Ji{constructor(){super(...arguments),this.spacing=!1,this.stretch=!1,this.tabs=new Map,this.changeTo="right"}get selectedTab(){return this.selected?this.selected:this.tabs.size?this.tabs.get(this.tabs.keys().next().value):void 0}selectTab(e){var t,i,o,n;if(!(null===(t=this.tabContentElements)||void 0===t?void 0:t.length))return;const r=null===(i=this.tabContentElements)||void 0===i?void 0:i.findIndex((e=>e.name===this.selected)),a=null===(o=this.tabContentElements)||void 0===o?void 0:o.findIndex((t=>t.name===e));this.changeTo=r<a?"right":"left",this.selected=e,null===(n=this.tabContentElements)||void 0===n||n.forEach((e=>{e.name===this.selected?e.classList.add("kode4-tabs-selected"):e.classList.remove("kode4-tabs-selected")}))}handleSlotchange(){var e;null===(e=this.tabContentElements)||void 0===e||e.forEach((e=>{this.tabs.set(e.name,e.tab),e.classList.contains("kode4-tabs-selected")&&(this.selected=e.name)}))}render(){return Pi`
            <div class="tabtitles ${"left"===this.changeTo?"toLeft":"toRight"}" part="tab-container">
                ${[...this.tabs.entries()].map((([e,t])=>Pi`
                    <div class="tab ${this.selectedTab===e?"active":""}" @click=${()=>this.selectTab(e)} part="tab">
                        <div class="tab-body">
                            ${t}
                        </div>
                    </div>
                `))}
            </div>
            <div class="tabcontents ${this.stretch?"stretch-height":""}" part="container">
                <slot @slotchange=${this.handleSlotchange}></slot>
            </div>
        `}};Va.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                min-height: 100%;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }
            
            .tabtitles {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: stretch;
                align-items: stretch;
                overflow-x: auto;
            }
            
            .tab-body {
                height: calc(100% - 2px);
                padding: 5px 2px;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: stretch;
                align-items: center;
            }

            .tabtitles > .tab {
                cursor: pointer;
                text-overflow: ellipsis;
                flex: 0 1 auto;
            }
 
            .tabtitles > .tab:after {
                content: "";
                display: block;
                height: 2px;
                width: 100%;
                background-color: var(--background-color-filled-primary);
                transition: transform .25s linear;
                transform: scaleX(0);
            }

            .tabtitles.toRight > .tab:after {
                transform-origin: right;
            }

            .tabtitles.toLeft > .tab:after {
                transform-origin: left;
            }

            .tabtitles > .tab.active {
            }
 
            .tabtitles > .tab.active:after {
                transform: scaleX(1);
            }
 
            .tabtitles.toRight > .tab.active:after {
                transform-origin: left;
            }
 
            .tabtitles.toLeft > .tab.active:after {
                transform-origin: right;
            }
 
            .tabcontents {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: flex-start;
                align-items: flex-start;
                overflow: hidden;
            }
            
            .tabcontents.stretch-height {
                flex: 1 0 1px;
                justify-items: stretch;
                align-items: stretch;
                overflow: auto;
            }
            
            ::slotted(*) {
                //flex: 0 1 0px;
                width: 0;
                height: 0;
                //min-height: 100%;
                padding: 0;
                margin: 0;
                border: none;
                opacity: 0;
                //transform: transla.teX(100%) scaleX(0);
                //transform: scaleX(0);
                //transform: scaleX(0);
                //transform-origin: top left;
                transition: all .15s ease-in;
                overflow:hidden;
            }

            ::slotted(.kode4-tabs-selected) {
                flex: 1 0 1px;
                left: 0;
                width: 100%;
                height: auto;
                opacity: 1;
                overflow: auto;
                //transform: translateX(0) scaleX(1);
                //transform: scaleX(1);
                //transform: scale(1, 1);
            }

            //.tabcontents:not(.stretch-height) ::slotted(*) {
            //    //transform: scaleY(0);
            //    //transform-origin: top;
            //    //opacity: 0.5;
            //}
            //
            //.tabcontents:not(.stretch-height) ::slotted(.kode4-tabs-selected) {
            //    //transform: scaleY(1);
            //    //opacity: 1;
            //}
        `],Qt([lo()],Va.prototype,"tabContentElements",void 0),Qt([io({type:Boolean,attribute:!0})],Va.prototype,"spacing",void 0),Qt([io({type:Boolean,attribute:!0})],Va.prototype,"stretch",void 0),Qt([io({type:Array,attribute:"tabs"})],Va.prototype,"tabs",void 0),Qt([io({type:String,attribute:!0})],Va.prototype,"selected",void 0),Qt([oo()],Va.prototype,"changeTo",void 0),Qt([oo()],Va.prototype,"selectedTab",null),Va=Qt([eo("kode4-tabs")],Va);let Ua=class extends Ji{constructor(){super(...arguments),this.name=`kode4tab_${za()}`}get tab(){var e;return(null===(e=this.titleElements)||void 0===e?void 0:e.length)?Pi`${this.titleElements}`:Pi`${this.name}`}firstUpdated(e){super.firstUpdated(e)}render(){return Pi`
            <div class="title">
                <slot name="title"></slot>
            </div>
                
            <slot></slot>
        `}};Ua.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: block;
            }
            
            .title {
                display: none;
                visibility: hidden;
                width: 0;
                height: 0;
                overflow: hidden;
            }
        `],Qt([lo({slot:"title"})],Ua.prototype,"titleElements",void 0),Qt([io()],Ua.prototype,"name",void 0),Qt([oo()],Ua.prototype,"tab",null),Ua=Qt([eo("kode4-tab")],Ua);let ja=class extends Ji{constructor(){super(...arguments),this.observerFix=!1,this.listEndObserverOptions={rootMargin:"0px",threshold:1},this.observerIsRunning=!1}get empty(){return!this.listBodyElements||!!this.listBodyElements.length}async firstUpdated(e){super.firstUpdated(e),this.listEndObserverRootSelector&&(this.listEndObserverOptions.root=this.closest(this.listEndObserverRootSelector)),this.listEndObserver=new IntersectionObserver((async e=>{e[0].intersectionRatio>=this.listEndObserverOptions.threshold&&await this.triggerObserverHandler()}),this.listEndObserverOptions),this.startObserver()}async triggerObserverHandler(){this.more instanceof Function?await this.more():this.dispatchEvent(new CustomEvent("kode4-table-more",{bubbles:!0,composed:!0}))}stopObserver(){this.observerIsRunning&&this.currentObservedElement&&(this.listEndObserver.unobserve(this.currentObservedElement),this.observerIsRunning=!1,this.currentObservedElement=void 0)}startObserver(){var e;this.observerIsRunning||((null===(e=this.listBodyElements)||void 0===e?void 0:e.length)?this.currentObservedElement=this.listBodyElements[this.listBodyElements.length-1]:this.currentObservedElement=this.listEndObserverTrigger,this.listEndObserver.observe(this.currentObservedElement),this.observerIsRunning=!0)}disconnectedCallback(){this.listEndObserver.disconnect(),this.observerIsRunning=!1,super.disconnectedCallback()}handleSlotchange(){this.stopObserver(),this.changed instanceof Function&&this.changed(),this.startObserver()}render(){return Pi`
            <div class="header">
                <slot name="header"></slot>
            </div>

            <slot @slotchange=${this.handleSlotchange}></slot>

            <div class="empty ${this.empty?"hidden":""}" style="display: table-row-group !important; width:100%;">
                <slot name="empty">
                    <div class="kode4-center-h" part="empty">
                        --
                    </div>
                </slot>
            </div>

            <div id="listEndObserver" class="${this.observerFix?"absolute":void 0}" part="end-observer"></div>
        `}};ja.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                position: relative;
                display: table;
                table-layout: fixed;
                width: 100%;
                background-color: #fff;
            }
            
            .header {
                display: table-row;
                position: sticky;
                top: 0;
                background-color: inherit;
            }

            .header > ::slotted(*) {
                display: table-cell;
            }

            #listEndObserver {
                display: block;
                height: 0;
                width: 100%;
            }

            #listEndObserver.absolute {
                display: block;
                position: absolute;
                height: 0;
                width: 0;
                left: 0;
                bottom: 20px;
            }
            
            .empty:not(.hidden) {
                display: block;
                visibility: visible;
                position: absolute;
                left: 0;
                right: 0;
                top: 100%;
                width: 100%;
            }
            
            .empty.hidden {
                display: none;
                visibility: hidden;
            }
        `],Qt([ro("#listEndObserver")],ja.prototype,"listEndObserverTrigger",void 0),Qt([io({type:"String",attribute:"observer-root"})],ja.prototype,"listEndObserverRootSelector",void 0),Qt([io({type:"String"})],ja.prototype,"observerRoot",void 0),Qt([io({type:Boolean,attribute:"observer-fix"})],ja.prototype,"observerFix",void 0),Qt([io({attribute:!1})],ja.prototype,"more",void 0),Qt([io({attribute:!1})],ja.prototype,"changed",void 0),Qt([lo()],ja.prototype,"listBodyElements",void 0),Qt([oo()],ja.prototype,"empty",null),Qt([oo()],ja.prototype,"observerIsRunning",void 0),ja=Qt([eo("kode4-table")],ja);var qa=i(566);const Ha=e=>{class t extends e{constructor(){super(...arguments),this.kode4ThemeType="text"}toggleCssClass(e,t){e?this.classList.add(t):this.classList.remove(t)}set primary(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-primary`)}get primary(){return this.classList.contains(`kode4-${this.kode4ThemeType}-primary`)}set secondary(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-secondary`)}get secondary(){return this.classList.contains(`kode4-${this.kode4ThemeType}-secondary`)}set success(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-success`)}get success(){return this.classList.contains(`kode4-${this.kode4ThemeType}-success`)}set danger(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-danger`)}get danger(){return this.classList.contains(`kode4-${this.kode4ThemeType}-danger`)}set warning(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-warning`)}get warning(){return this.classList.contains(`kode4-${this.kode4ThemeType}-warning`)}set info(e){this.toggleCssClass(e,`kode4-${this.kode4ThemeType}-info`)}get info(){return this.classList.contains(`kode4-${this.kode4ThemeType}-info`)}set text(e){this.toggleCssClass(e,"kode4-text")}get text(){return this.classList.contains("kode4-text")}set textPrimary(e){this.toggleCssClass(e,"kode4-text-primary")}get textPrimary(){return this.classList.contains("kode4-text-primary")}set textSecondary(e){this.toggleCssClass(e,"kode4-text-secondary")}get textSecondary(){return this.classList.contains("kode4-text-secondary")}set textSuccess(e){this.toggleCssClass(e,"kode4-text-success")}get textSuccess(){return this.classList.contains("kode4-text-success")}set textDanger(e){this.toggleCssClass(e,"kode4-text-danger")}get textDanger(){return this.classList.contains("kode4-text-danger")}set textWarning(e){this.toggleCssClass(e,"kode4-text-warning")}get textWarning(){return this.classList.contains("kode4-text-warning")}set textInfo(e){this.toggleCssClass(e,"kode4-text-info")}get textInfo(){return this.classList.contains("kode4-text-info")}set bg(e){this.toggleCssClass(e,"kode4-bg")}get bg(){return this.classList.contains("kode4-bg")}set bgPrimary(e){this.toggleCssClass(e,"kode4-bg-primary")}get bgPrimary(){return this.classList.contains("kode4-bg-primary")}set bgSecondary(e){this.toggleCssClass(e,"kode4-bg-secondary")}get bgSecondary(){return this.classList.contains("kode4-bg-secondary")}set bgSuccess(e){this.toggleCssClass(e,"kode4-bg-success")}get bgSuccess(){return this.classList.contains("kode4-bg-success")}set bgDanger(e){this.toggleCssClass(e,"kode4-bg-danger")}get bgDanger(){return this.classList.contains("kode4-bg-danger")}set bgWarning(e){this.toggleCssClass(e,"kode4-bg-warning")}get bgWarning(){return this.classList.contains("kode4-bg-warning")}set bgInfo(e){this.toggleCssClass(e,"kode4-bg-info")}get bgInfo(){return this.classList.contains("kode4-bg-info")}}return Qt([io({type:String,attribute:"theme"})],t.prototype,"kode4ThemeType",void 0),Qt([io({type:Boolean,attribute:"primary"})],t.prototype,"primary",null),Qt([io({type:Boolean,attribute:"secondary"})],t.prototype,"secondary",null),Qt([io({type:Boolean,attribute:"success"})],t.prototype,"success",null),Qt([io({type:Boolean,attribute:"danger"})],t.prototype,"danger",null),Qt([io({type:Boolean,attribute:"warning"})],t.prototype,"warning",null),Qt([io({type:Boolean,attribute:"info"})],t.prototype,"info",null),Qt([io({type:Boolean,attribute:"text"})],t.prototype,"text",null),Qt([io({type:Boolean,attribute:"text-primary"})],t.prototype,"textPrimary",null),Qt([io({type:Boolean,attribute:"text-secondary"})],t.prototype,"textSecondary",null),Qt([io({type:Boolean,attribute:"text-success"})],t.prototype,"textSuccess",null),Qt([io({type:Boolean,attribute:"text-danger"})],t.prototype,"textDanger",null),Qt([io({type:Boolean,attribute:"text-warning"})],t.prototype,"textWarning",null),Qt([io({type:Boolean,attribute:"text-info"})],t.prototype,"textInfo",null),Qt([io({type:Boolean,attribute:"bg"})],t.prototype,"bg",null),Qt([io({type:Boolean,attribute:"bg-primary"})],t.prototype,"bgPrimary",null),Qt([io({type:Boolean,attribute:"bg-secondary"})],t.prototype,"bgSecondary",null),Qt([io({type:Boolean,attribute:"bg-success"})],t.prototype,"bgSuccess",null),Qt([io({type:Boolean,attribute:"bg-danger"})],t.prototype,"bgDanger",null),Qt([io({type:Boolean,attribute:"bg-warning"})],t.prototype,"bgWarning",null),Qt([io({type:Boolean,attribute:"bg-info"})],t.prototype,"bgInfo",null),t};let Wa=class extends(Ha(Ji)){constructor(){super(...arguments),this.sortable=!1}handleSortingChanged(e){e.preventDefault(),this.dispatchEvent(new CustomEvent("kode4-sort",{bubbles:!0,composed:!0,detail:{currentOrder:this.order}}))}render(){return Pi`
            <div class="layout ${this.sortable?"sortable":void 0}" @click="${this.handleSortingChanged}">
                <div class="body">
                    <slot></slot>
                </div>
                ${this.sortable&&this.order?Pi`
                    <div class="sorting ${this.order}">
                        <slot name="sorting">
                            <kode4-fa-icon .icon="${qa.HH}"></kode4-fa-icon>
                        </slot>
                    </div>
                `:void 0}
            </div>
        `}};Wa.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: table-cell;
                padding: 10px 1px;
                background-color: inherit;
            }
            
            :host(:first-child) {
                padding-left: 10px;
            }

            :host(:last-child) {
                padding-right: 10px;
            }
            
            .layout {
                width: 100%;
                display: inline-flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }
            
            .sortable {
                cursor: pointer;
            }
            
            .body {
                flex: 1 0 1px;
            }
            
            .sorting {
                padding-right: 5px;
                padding-left: 5px;
                transition: transform .25s linear;
            }

            .desc {
                transform: rotate(-180deg);
            }
            
            :host {
                border-bottom: solid 1px var(--main-color);
            }
        `],Qt([io({type:Boolean,attribute:!0})],Wa.prototype,"sortable",void 0),Qt([io({type:String,attribute:"order",converter:{fromAttribute:e=>{if(e&&["asc","desc"].includes(e.toLowerCase()))return e.toLowerCase()}}})],Wa.prototype,"order",void 0),Wa=Qt([eo("kode4-tableheader")],Wa);let Ya=class extends Ji{render(){return Pi`
            <slot></slot>
        `}};Ya.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: table-row;
                background-color: inherit;
            }
            
            //:host(:nth-child(even)) {
            //    background-color: #ccc;
            //}
            //
            //:host(:nth-child(odd)) {
            //    background-color: #f00;
            //}
            
            :host(:hover) {
                background-color: #f0f0f0;
            }
            
            ::slotted(*) {
                display: table-cell;
                padding: 10px 1px;
                border-bottom: solid 1px #f0f0f0;
            }
            
            ::slotted(*) {
                display: table-cell;
            }
            
            ::slotted(:first-child) {
                padding-left: 10px;
            }

            ::slotted(:last-child) {
                padding-right: 10px;
                border: none;
            }
        `],Ya=Qt([eo("kode4-tablerow")],Ya);let Ka=class extends Ji{render(){return Pi`
            <slot></slot>
        `}};Ka.styles=[va,ni`
            * {
                box-sizing: border-box;
            }

            :host {
                border: solid 1px #eee;
                display: block;
            }

            :host([inline]) {
                display: inline-block;
            }

            ::slotted(*) {
                display: list-item;
                list-style: none;
                padding: 0.5em 10px;
                cursor: pointer;
                border-bottom: solid 1px #eee;
                background-color: #fff;
            }

            ::slotted(*:hover) {
                background-color: #f0f0f0;
            }

            ::slotted(*:last-child) {
                border-bottom: none;
            }
        `],Ka=Qt([eo("kode4-list")],Ka);let Xa=class extends Ji{toggleCssClass(e,t){e?this.classList.add(t):this.classList.remove(t)}set visible(e){this.toggleCssClass(e,"kode4-backdrop-visible")}get visible(){return this.classList.contains("kode4-backdrop-visible")}set transparent(e){this.toggleCssClass(e,"kode4-backdrop-transparent")}get transparent(){return this.classList.contains("kode4-backdrop-transparent")}render(){return Pi`
            <slot></slot>
        `}};Xa.styles=[ni`
            :host {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 10000;
                overflow: hidden;
                background-color: rgba(0, 0, 0, 0.25);
                transition: background 0.25s ease-out;
            }
            
            :host(.kode4-backdrop-transparent) {
                background-color: transparent;
            }

            :host(:not(.kode4-backdrop-visible)) {
                bottom: 100%;
                background-color: transparent;
                transition: all 0s linear 0.25s, background 0.25s ease-out;
            }
        `],Qt([io({type:Boolean,attribute:"visible"})],Xa.prototype,"visible",null),Qt([io({type:Boolean,attribute:"transparent"})],Xa.prototype,"transparent",null),Xa=Qt([eo("kode4-backdrop")],Xa);const Za=new Map;let Ga=class extends Ji{constructor(){super(...arguments),this.isOpen=!1,this.transparent=!1,this._backdrop=!1,this.floating=!1,this.bound=!1,this.boundToParent=!1,this.cover=!1,this.handleParentClick=e=>{e.defaultPrevented||this.isOpen||(e.preventDefault(),this.show({x:e.pageX,y:e.pageY}))}}set open(e){e&&!this.isOpen&&this.show()}toggleCssClass(e,t){e?this.classList.add(t):this.classList.remove(t)}set visible(e){this.toggleCssClass(e,"kode4-menu-visible")}get visible(){return this.classList.contains("kode4-menu-visible")}get backdrop(){return this._backdrop}set backdrop(e){this.toggleCssClass(e,"kode4-list-backdrop"),this._backdrop=e}set parent(e){this.boundToParent=e}set left(e){this.toggleCssClass(e,"kode4-list-align-left")}get left(){return this.classList.contains("kode4-list-align-left")}set right(e){this.toggleCssClass(e,"kode4-list-align-right")}get right(){return this.classList.contains("kode4-list-align-right")}set top(e){this.toggleCssClass(e,"kode4-list-align-top")}get top(){return this.classList.contains("kode4-list-align-top")}set bottom(e){this.toggleCssClass(e,"kode4-list-align-bottom")}get bottom(){return this.classList.contains("kode4-list-align-bottom")}set stretch(e){this.toggleCssClass(e,"kode4-list-align-stretch-x")}get stretch(){return this.classList.contains("kode4-list-align-stretch-x")}getCallerParams(){return this.callerParams}onShow(e,t=!0){e.defaultPrevented||(t&&e.preventDefault(),e instanceof MouseEvent?this.show({x:e.pageX,y:e.pageY}):this.show())}show(e){(null==e?void 0:e.callerParams)&&(this.callerParams=e.callerParams),this.visible=!0,this.isOpen=!0,this.floating&&this.findFloatingPosition({x:null==e?void 0:e.x,y:null==e?void 0:e.y}),(this.bound||this.boundToParent)&&this.findBoundToParentPosition((null==e?void 0:e.alignToElement)||this.parentElement),this.requestUpdate()}findFloatingPosition(e){const t=window.innerWidth,i=window.innerHeight,o=this.menu.getBoundingClientRect();let n=(null==e?void 0:e.x)||10,r=(null==e?void 0:e.y)||10;n+o.width>t&&(n=t-o.width),r+o.height>i&&(r=i-o.height),this.x=`${Math.max(n,0)}px`,this.y=`${Math.max(r,0)}px`}findBoundToParentPosition(e){const t=window.innerWidth,i=window.innerHeight,o=e.getBoundingClientRect(),n=this.menu.getBoundingClientRect();let r,a,s=0;const d=this.cover?o.height:0;switch(!0){case this.top:a=o.y-n.height+d,a<0&&(a=o.y+o.height-d,a+n.height>i&&(a=0));break;default:case this.bottom:a=o.y+o.height-d,a+n.height>i&&(a=o.y-n.height+d,a<0&&(a=i-n.height))}switch(!0){case n.width>o.width&&this.left:r=o.x,r+n.width>t&&(r=o.x+o.width-n.width,r<0&&(r=t-n.width));break;case n.width>o.width&&this.right:r=o.x+o.width-n.width,r<0&&(r=o.x,r+n.width>t&&(r=0));break;default:case this.stretch:r=o.x,n.width<o.width&&(s=o.width)}this.x=`${Math.max(r,0)}px`,this.y=`${Math.max(a,0)}px`,s>0&&(this.minWidth=`${s}px`)}hide(){this.visible=!1,this.isOpen=!1,this.callerParams=void 0,this.requestUpdate()}handleBackdropClick(e){e.stopPropagation(),e.preventDefault(),this.hide()}handleListClick(e){e.defaultPrevented&&this.hide()}connectedCallback(){this.name&&Za.set(this.name,this),this.boundToParent&&this.parentElement.addEventListener("click",this.handleParentClick),super.connectedCallback()}disconnectedCallback(){this.boundToParent&&this.parentElement.removeEventListener("click",this.handleParentClick),this.name&&Za.has(this.name)&&Za.delete(this.name),super.disconnectedCallback()}render(){return Pi`
            ${this.bound||this.boundToParent||this.floating?Pi`
                <kode4-backdrop ?visible="${this.isOpen}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            `:void 0}
            <kode4-list style="left: ${this.x||"auto"}; top: ${this.y||"auto"}; ${this.minWidth?`min-width: ${this.minWidth};`:void 0} ${this.width?`width: ${this.width};`:void 0}" inline @click="${this.handleListClick}">
                <slot></slot>
            </kode4-list>
        `}};Ga.styles=[va,ni`
            * {
                box-sizing: border-box;
            }
            
            :host  {
                padding: 0;
                margin: 0;
                border: none 0;
            }

            :host(:not(.kode4-menu-visible)) {
                display: none;
            }
            
            :host kode4-list,
            :host([floating]) kode4-list,
            :host([bound]) kode4-list,
            :host([parent]) kode4-list {
                position: fixed;
                z-index: 10100;
            }
        `],Qt([io({type:String,attribute:"name"})],Ga.prototype,"name",void 0),Qt([oo()],Ga.prototype,"isOpen",void 0),Qt([io({type:Boolean,attribute:"open"})],Ga.prototype,"open",null),Qt([io({type:Boolean,attribute:!0})],Ga.prototype,"transparent",void 0),Qt([io({type:Boolean,attribute:"visible"})],Ga.prototype,"visible",null),Qt([ro("kode4-list")],Ga.prototype,"menu",void 0),Qt([io({type:Boolean,attribute:!0})],Ga.prototype,"_backdrop",void 0),Qt([io({type:Boolean,attribute:!0})],Ga.prototype,"backdrop",null),Qt([ro(".backdrop")],Ga.prototype,"backdropElement",void 0),Qt([io({type:Boolean,attribute:"floating"})],Ga.prototype,"floating",void 0),Qt([io({type:Boolean,attribute:"bound"})],Ga.prototype,"bound",void 0),Qt([oo()],Ga.prototype,"boundToParent",void 0),Qt([io({type:Boolean,attribute:"parent"})],Ga.prototype,"parent",null),Qt([io({type:Boolean,attribute:"left"})],Ga.prototype,"left",null),Qt([io({type:Boolean,attribute:"right"})],Ga.prototype,"right",null),Qt([io({type:Boolean,attribute:"top"})],Ga.prototype,"top",null),Qt([io({type:Boolean,attribute:"bottom"})],Ga.prototype,"bottom",null),Qt([io({type:Boolean,attribute:"stretch"})],Ga.prototype,"stretch",null),Qt([io({type:Boolean,attribute:"cover"})],Ga.prototype,"cover",void 0),Qt([oo()],Ga.prototype,"x",void 0),Qt([oo()],Ga.prototype,"y",void 0),Qt([oo()],Ga.prototype,"minWidth",void 0),Qt([io({type:String,attribute:"w"})],Ga.prototype,"width",void 0),Ga=Qt([eo("kode4-menu")],Ga);let Ja=class extends(Ha(Ji)){constructor(){super(),this.kode4ThemeType="bg"}render(){return Pi`
            <slot></slot>
        `}};Ja.styles=[va,ni`
            :host {
                display: block;
                padding: 10px;
            }
        `],Ja=Qt([eo("kode4-alert")],Ja);const Qa=new Map;let es=class extends Ji{constructor(){super(...arguments),this.open=!1,this.transparent=!1,this.resizeContentObserver=new ResizeObserver((()=>{this.requestUpdate()}))}toggleCssClass(e,t){e?this.classList.add(t):this.classList.remove(t)}set left(e){this.toggleCssClass(e,"kode4-drawer-left")}get left(){return this.classList.contains("kode4-drawer-left")}set right(e){this.toggleCssClass(e,"kode4-drawer-right")}get right(){return this.classList.contains("kode4-drawer-right")}set top(e){this.toggleCssClass(e,"kode4-drawer-top")}get top(){return this.classList.contains("kode4-drawer-top")}set bottom(e){this.toggleCssClass(e,"kode4-drawer-bottom")}get bottom(){return this.classList.contains("kode4-drawer-bottom")}doShow(){this.open=!0}show(){this.open=!0}hide(){this.open=!1,this.dispatchEvent(new CustomEvent("close",{bubbles:!0,composed:!0}))}handleBackdropClick(e){e.stopPropagation(),e.preventDefault(),this.hide()}get contentWidth(){var e;let t=100;return null===(e=this.mainItems)||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetWidth)})),t}get contentHeight(){var e;let t=100;return null===(e=this.mainItems)||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetHeight)})),t}firstUpdated(e){var t;super.firstUpdated(e),null===(t=this.mainItems)||void 0===t||t.forEach((e=>{this.resizeContentObserver.observe(e)}))}connectedCallback(){this.name&&Qa.set(this.name,this),super.connectedCallback()}disconnectedCallback(){this.resizeContentObserver.disconnect(),this.name&&Qa.has(this.name)&&Qa.delete(this.name),super.disconnectedCallback()}handleListClick(e){e.defaultPrevented&&this.hide()}render(){return Pi`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="drawer" style="${(this.left||this.right)&&this.contentWidth>0?`width: ${this.contentWidth}px;`:void 0} ${(this.top||this.bottom)&&this.contentHeight>0?`height: ${this.contentHeight}px;`:void 0}" @click="${this.handleListClick}">
                ${this.left||this.right?Pi`
                    <kode4-column class="drawer-content">
                        <slot></slot>
                    </kode4-column>
                `:Pi`
                    <kode4-row class="drawer-content">
                        <slot></slot>
                    </kode4-row>
                `}
            </div>
        `}};es.styles=[ni`
            * {
                box-sizing: border-box;
            }
            
            :host {
                position: fixed;
                display: block;
                z-index: 10010;
            }

            :host(.kode4-drawer-left) {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-right) {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-top) {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-bottom) {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-left:not([open])),
            :host(.kode4-drawer-right:not([open])) {
                top: 1000vh;
                transition: top 0s linear 0.25s;
            }

            :host(.kode4-drawer-top:not([open])),
            :host(.kode4-drawer-bottom:not([open])) {
                left: 1000vw;
                transition: left 0s linear 0.25s;
            }

            .drawer {
                position: absolute;
                display: block;
            }

            :host(.kode4-drawer-left) .drawer {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right) .drawer {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-top) .drawer {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
            }
            
            :host(.kode4-drawer-bottom) .drawer {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
            }
            
            .drawer-content {
                position: absolute;
                display: block;
                z-index: 10050;
                transition: all .25s linear;
                background-color: #fff;
            }
            
            :host(.kode4-drawer-left) .drawer-content {
                left: -100%;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-left[open]) .drawer-content {
                left: 0;
            }
            
            :host(.kode4-drawer-right) .drawer-content {
                left: auto;
                right: -100%;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right[open]) .drawer-content {
                right: 0;
            }
            
            :host(.kode4-drawer-top) .drawer-content {
                left: 0;
                right: 0;
                top: -100%;
                bottom: auto;
            }
            
            :host(.kode4-drawer-top[open]) .drawer-content {
                top: 0;
            }
             
            :host(.kode4-drawer-bottom) .drawer-content {
                left: 0;
                right: 0;
                top: auto;
                bottom: -100%;
            }
            
            :host(.kode4-drawer-bottom[open]) .drawer-content {
                bottom: 0;
            }
        `],Qt([io({type:String,attribute:"name"})],es.prototype,"name",void 0),Qt([io({type:Boolean,attribute:"open",reflect:!0})],es.prototype,"open",void 0),Qt([io({type:Boolean,attribute:"left"})],es.prototype,"left",null),Qt([io({type:Boolean,attribute:"right"})],es.prototype,"right",null),Qt([io({type:Boolean,attribute:"top"})],es.prototype,"top",null),Qt([io({type:Boolean,attribute:"bottom"})],es.prototype,"bottom",null),Qt([io({type:Boolean,attribute:!0})],es.prototype,"transparent",void 0),Qt([lo()],es.prototype,"mainItems",void 0),Qt([io({type:Number})],es.prototype,"contentWidth",null),Qt([io({type:Number})],es.prototype,"contentHeight",null),es=Qt([eo("kode4-drawer")],es);let ts=class extends Ji{constructor(){super(...arguments),this.resizeContentObserver=new ResizeObserver((()=>{this.requestUpdate()}))}get contentWidth(){var e;let t=this.offsetWidth;return null===(e=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetWidth)})),`${t}px`}get contentHeight(){var e;let t=this.offsetHeight;return null===(e=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===e||e.forEach((e=>{t=Math.max(t,e.offsetHeight)})),`${t}px`}firstUpdated(e){var t;super.firstUpdated(e),null===(t=[...this.headerItems,...this.mainItems,...this.footerItems])||void 0===t||t.forEach((e=>{this.resizeContentObserver.observe(e)}))}scrollBodyTo(e){this.bodyElement.scrollTo(0,e)}onBodyScrolled(){this.dispatchEvent(new CustomEvent("kode4-body-scrolled",{bubbles:!0,composed:!0,detail:{pos:this.bodyElement.scrollTop}}))}connectedCallback(){super.connectedCallback()}disconnectedCallback(){this.resizeContentObserver.disconnect(),super.disconnectedCallback()}render(){return Pi`
            <kode4-column part="container" style="width: ${this.contentWidth};">
                <slot name="header"></slot>
                <div class="body" part="body" @scroll="${this.onBodyScrolled}">
                    <slot></slot>
                </div>
                <div>
                    <slot name="footer"></slot>
                </div>
            </kode4-column>
        `}};ts.styles=[ni`
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                margin: 0;
            }
            
            :host([stretch]) {
                height: 100%;
            }

            .body {
                flex: 0 1 100%;
                width: 100%;
                overflow-x: hidden;
                overflow-y: auto;
                //border: solid 2px #00f;
                //background-color: #ccc;
            }
        `],Qt([ro(".body")],ts.prototype,"bodyElement",void 0),Qt([lo({slot:"header"})],ts.prototype,"headerItems",void 0),Qt([lo()],ts.prototype,"mainItems",void 0),Qt([lo({slot:"footer"})],ts.prototype,"footerItems",void 0),Qt([oo()],ts.prototype,"contentWidth",null),Qt([oo()],ts.prototype,"contentHeight",null),ts=Qt([eo("kode4-card")],ts);let is=class extends Ji{render(){return Pi`
            <slot></slot>
        `}};is.styles=[ni`
            * {
                box-sizing: border-box;
            }

            :host {
                display: block;
                cursor: pointer;
                color: var(--kode4-nav-text);
                background-color: var(--kode4-nav-background);
                border-radius: var(--kode4-nav-border-radius);
                padding-top: var(--kode4-nav-padding-top);
                padding-right: var(--kode4-nav-padding-right);
                padding-bottom: var(--kode4-nav-padding-bottom);
                padding-left: var(--kode4-nav-padding-left);
                transition: all 0.15s linear
            }
            
            :host([inline]) {
                display: inline-block;
            }

            :host(:hover) {
                color: var(--kode4-nav-text-hover);
                background-color: var(--kode4-nav-background-hover);
            }

            :host([active]) {
                color: var(--kode4-nav-active-text);
                background-color: var(--kode4-nav-active-background);
            }

            :host([active]:hover) {
                color: var(--kode4-nav-active-text-hover);
                background-color: var(--kode4-nav-active-background-hover);
            }
        `],is=Qt([eo("kode4-nav")],is);let os=class extends Ji{constructor(){super(...arguments),this.mode="toggle",this.boundToParent=!1,this.handleParentClick=e=>{if(this.menu&&Za.has(this.menu)){e.preventDefault();Za.get(this.menu).show({alignToElement:this.parentElement,callerParams:this.callerParams})}else if(this.drawer&&Qa.has(this.drawer)){e.preventDefault();Qa.get(this.drawer).show()}else if(this.dialog&&la.has(this.dialog)){e.preventDefault();la.get(this.dialog).show()}else console.log("[Kode4Trigger] unable to connect")}}connectedCallback(){this.boundToParent&&this.parentElement.addEventListener("click",this.handleParentClick),super.connectedCallback()}disconnectedCallback(){this.boundToParent&&this.parentElement.removeEventListener("click",this.handleParentClick),super.disconnectedCallback()}render(){return Pi`
        `}};Qt([io({type:String,attribute:!0})],os.prototype,"menu",void 0),Qt([io({type:String,attribute:!0})],os.prototype,"drawer",void 0),Qt([io({type:String,attribute:!0})],os.prototype,"dialog",void 0),Qt([io({type:String,attribute:!0})],os.prototype,"mode",void 0),Qt([io({type:String,attribute:"caller-params"})],os.prototype,"callerParams",void 0),Qt([io({type:Boolean,attribute:"parent"})],os.prototype,"boundToParent",void 0),os=Qt([eo("kode4-trigger")],os);const ns=function(){return y.Date.now()};var rs=/\s/;const as=function(e){for(var t=e.length;t--&&rs.test(e.charAt(t)););return t};var ss=/^\s+/;const ds=function(e){return e?e.slice(0,as(e)+1).replace(ss,""):e};const ls=function(e){return"symbol"==typeof e||Re(e)&&"[object Symbol]"==A(e)};var cs=/^[-+]0x[0-9a-f]+$/i,ps=/^0b[01]+$/i,us=/^0o[0-7]+$/i,hs=parseInt;const fs=function(e){if("number"==typeof e)return e;if(ls(e))return NaN;if(L(e)){var t="function"==typeof e.valueOf?e.valueOf():e;e=L(t)?t+"":t}if("string"!=typeof e)return 0===e?e:+e;e=ds(e);var i=ps.test(e);return i||us.test(e)?hs(e.slice(2),i?2:8):cs.test(e)?NaN:+e};var gs=Math.max,ms=Math.min;const vs=function(e,t,i){var o,n,r,a,s,d,l=0,c=!1,p=!1,u=!0;if("function"!=typeof e)throw new TypeError("Expected a function");function h(t){var i=o,r=n;return o=n=void 0,l=t,a=e.apply(r,i)}function f(e){return l=e,s=setTimeout(m,t),c?h(e):a}function g(e){var i=e-d;return void 0===d||i>=t||i<0||p&&e-l>=r}function m(){var e=ns();if(g(e))return v(e);s=setTimeout(m,function(e){var i=t-(e-d);return p?ms(i,r-(e-l)):i}(e))}function v(e){return s=void 0,u&&o?h(e):(o=n=void 0,a)}function b(){var e=ns(),i=g(e);if(o=arguments,n=this,d=e,i){if(void 0===s)return f(d);if(p)return clearTimeout(s),s=setTimeout(m,t),h(d)}return void 0===s&&(s=setTimeout(m,t)),a}return t=fs(t)||0,L(i)&&(c=!!i.leading,r=(p="maxWait"in i)?gs(fs(i.maxWait)||0,t):r,u="trailing"in i?!!i.trailing:u),b.cancel=function(){void 0!==s&&clearTimeout(s),l=0,o=d=n=s=void 0},b.flush=function(){return void 0===s?a:v(ns())},b};let bs=class extends Ji{constructor(){super(...arguments),this.action="",this.method="get",this.isValid=!1,this.formElementsInitialized=!1,this.formElements=[],this.fieldsDirty=new FormData,this.dirty=!1,this.debounce=150,this.fieldsToAdd=[],this.fieldsToRemove=[],this.onFieldsUpdatedDebounced=vs(this.onFieldsUpdated,this.debounce)}connectedCallback(){this.addEventListener("kode4-field-destroyed",(e=>{this.fieldDestroyed(e)})),super.connectedCallback()}disconnectedCallback(){super.disconnectedCallback()}checkDirty(e){const t=this.dirty;for(let e of[...this.fieldsDirty.keys()])this.formElements.find((t=>t.name===e))||this.fieldsDirty.delete(e);if(this.dirty=[...this.fieldsDirty.keys()].length>0,this.dirty?this.classList.add("kode4-dirty"):this.classList.remove("kode4-dirty"),t!==this.dirty){const t={dirty:this.dirty};e&&(t.originalEvent=e);let i=new CustomEvent("stateChanged",{detail:t});this.dispatchEvent(i)}}async onChange(e){const{name:t,dirty:i}=e.detail;i&&!this.fieldsDirty.has(t)?this.fieldsDirty.set(t,i):!i&&this.fieldsDirty.has(t)&&this.fieldsDirty.delete(t),this.checkDirty(),await this.applyConditions()}async applyConditions(){for(let e=0;e<this.formElements.length;e+=1)await this.formElements[e].applyConditions(this)}getField(e){return this.formElements.reduce(((t,i)=>(i.name===e&&(t=i),t)),null)}validate(){let e=!0;return this.formElements.forEach((t=>{try{t.canValidate&&!t.valid&&(e=!1)}catch(e){console.warn("unexpected error while validating",e)}})),e}get formData(){const e=new FormData;return this.formElements.map((({name:t,stringValue:i,hidden:o,disabled:n,hasValue:r,manageValue:a})=>{a&&r&&t&&!o&&!n&&e.append(t,i)})),e}onResetForm(e){e.preventDefault(),this.reset()}onClearForm(e){e.preventDefault(),this.clear()}onUpdateValues(e){e.preventDefault(),this.updateValues()}onRespawnForm(e){e.preventDefault(),this.respawn()}onSubmit(e){var t,i;let o;console.log("GO SUBMIT!"),e.preventDefault();const n=this.formData;e.detail.name&&e.detail.value&&n.append(e.detail.name,e.detail.value),this.validate()?(o=new CustomEvent("submit",{bubbles:!0,detail:{formData:n,form:this}}),console.log("SUBMIT:"),console.log([...null===(t=o.detail.formData)||void 0===t?void 0:t.entries()])):(o=new CustomEvent("submit-error",{bubbles:!0,detail:{formData:n,form:this}}),console.log("SUBMIT-ERROR:"),console.log([...null===(i=o.detail.formData)||void 0===i?void 0:i.entries()])),this.dispatchEvent(o)}reset(){this.formElements.forEach((e=>{e.reset()}))}clear(){this.formElements.forEach((e=>{e.clear()}))}updateValues(){this.formElements.forEach((e=>{e.updateValue()}))}respawn(){this.formElements.forEach((e=>{e.respawn()}))}fieldCreated(e){var t,i,o;(null===(i=null===(t=e.detail)||void 0===t?void 0:t.field)||void 0===i?void 0:i.name)&&(this.fieldsToAdd.push(null===(o=e.detail)||void 0===o?void 0:o.field),this.fieldsToRemove.filter((t=>{var i,o;return t!==(null===(o=null===(i=e.detail)||void 0===i?void 0:i.field)||void 0===o?void 0:o.id)})),this.onFieldsUpdatedDebounced())}fieldDestroyed(e){var t,i;(null===(t=e.detail)||void 0===t?void 0:t.fieldid)&&(this.fieldsToRemove.push(null===(i=e.detail)||void 0===i?void 0:i.fieldid),this.fieldsToAdd.filter((t=>{var i;return t.id!==(null===(i=e.detail)||void 0===i?void 0:i.fieldid)})),this.onFieldsUpdatedDebounced())}onFieldsUpdated(){const e=[...this.formElements.filter((e=>!this.fieldsToRemove.find((t=>t===e.id))))];this.fieldsToAdd.forEach((t=>{e.find((e=>e.id===t.id))||e.push(t)})),this.fieldsToAdd=[],this.fieldsToRemove=[],this.formElements=[...e],this.applyConditions(),this.checkDirty(),this.formElementsInitialized=!0}render(){return Pi`
        <form
            class="kode4-form ${this.formElementsInitialized?"initialized":""}"
            action="${this.action}"
            method="${this.method}"
            @submitForm="${this.onSubmit}"
            @change="${this.onChange}"
            @resetForm="${this.onResetForm}"
            @clearForm="${this.onClearForm}"
            @updateValues="${this.onUpdateValues}"
            @respawnForm="${this.onRespawnForm}"
            @kode4-field-created="${this.fieldCreated}"
            >
            <slot></slot>
        </form>
        `}static get styles(){return ni`
            form:not(.initialized) {
                visibility: hidden;
                opacity: 0;
            }

            form {
                transition: opacity 0.4s ease-in-out;
            }
        `}};Qt([io()],bs.prototype,"action",void 0),Qt([io()],bs.prototype,"method",void 0),Qt([io({type:Boolean})],bs.prototype,"formElementsInitialized",void 0),Qt([io()],bs.prototype,"dirty",void 0),Qt([io({type:Number})],bs.prototype,"debounce",void 0),bs=Qt([eo("kode4-form")],bs);const ks=1,ys=2,xs=e=>(...t)=>({_$litDirective$:e,values:t});class ws{constructor(e){}get _$AU(){return this._$AM._$AU}_$AT(e,t,i){this._$Ct=e,this._$AM=t,this._$Ci=i}_$AS(e,t){return this.update(e,t)}update(e,t){return this.render(...t)}}const $s=xs(class extends ws{constructor(e){var t;if(super(e),e.type!==ks||"class"!==e.name||(null===(t=e.strings)||void 0===t?void 0:t.length)>2)throw Error("`classMap()` can only be used in the `class` attribute and must be the only part in the attribute.")}render(e){return" "+Object.keys(e).filter((t=>e[t])).join(" ")+" "}update(e,[t]){var i,o;if(void 0===this.st){this.st=new Set,void 0!==e.strings&&(this.et=new Set(e.strings.join(" ").split(/\s/).filter((e=>""!==e))));for(const e in t)t[e]&&!(null===(i=this.et)||void 0===i?void 0:i.has(e))&&this.st.add(e);return this.render(t)}const n=e.element.classList;this.st.forEach((e=>{e in t||(n.remove(e),this.st.delete(e))}));for(const e in t){const i=!!t[e];i===this.st.has(e)||(null===(o=this.et)||void 0===o?void 0:o.has(e))||(i?(n.add(e),this.st.add(e)):(n.remove(e),this.st.delete(e)))}return Ti}}),Cs=xs(class extends ws{constructor(e){var t;if(super(e),e.type!==ks||"style"!==e.name||(null===(t=e.strings)||void 0===t?void 0:t.length)>2)throw Error("The `styleMap` directive must be used in the `style` attribute and must be the only part in the attribute.")}render(e){return Object.keys(e).reduce(((t,i)=>{const o=e[i];return null==o?t:t+`${i=i.replace(/(?:^(webkit|moz|ms|o)|)(?=[A-Z])/g,"-$&").toLowerCase()}:${o};`}),"")}update(e,[t]){const{style:i}=e.element;if(void 0===this.ct){this.ct=new Set;for(const e in t)this.ct.add(e);return this.render(t)}this.ct.forEach((e=>{null==t[e]&&(this.ct.delete(e),e.includes("-")?i.removeProperty(e):i[e]="")}));for(const e in t){const o=t[e];null!=o&&(this.ct.add(e),e.includes("-")?i.setProperty(e,o):i[e]=o)}return Ti}});let Ss=class extends Ji{constructor(){super(),this.block=!1,this.submitEmpty=!1,this.initialized=!1,this.initializing=!1,this.touched=!1,this.type="text",this.name="",this.required=!1,this.floatingLabel=!1,this.xxs=!1,this.xs=!1,this.s=!1,this.m=!1,this.l=!1,this.xl=!1,this.xxl=!1,this.requiredSuffix="",this._value="",this.manageValue=!0,this.slotsReady=!1,this.initialValue=void 0,this.resetValue=void 0,this.initialDisabled=!1,this._disabled=!1,this.readonly=!1,this.controlReadonly=!1,this.label="",this.hint="",this.error="",this.debounce=350,this.canValidate=!0,this.inputFocused=!1,this.busyStyle="linear",this.inputFocusEventBlocked=!1,this.preventBlur=!1,this.forceBlur=!1,this.inputBlurEventBlocked=!1,this.dirty=!1,this.lastFocusedState=!1,this.isInitialized=new Promise((e=>{this.resolveInitialized=e})),this.setAttribute("id",this.id)}get typeCssClass(){return"kode4-hidden"}get inputCssSelector(){return"input"}get value(){return this._value}set value(e){this.initialized||this.initializing?(this._value=this.sanititzeValue(e),this.initialized&&(this.dirty=this.getDirty())):this._valueCandidate=e}getDirty(){return this.disabled!==this.initialDisabled||(this.getValueIsDefined(this.initialValue)?this.initialValue!=this._value:this.getValueIsDefined(this._value))}get stringValue(){return this.getValueIsDefined(this.value)?String(this.value):""}sanititzeValue(e){return e}get disabled(){return this._disabled}set disabled(e){this._disabled=e,this.initialized&&(this.dirty=this.getDirty())}get kode4Form(){return this._kode4Form||(this._kode4Form=this.closest("kode4-form")),this._kode4Form}applyConditions(e){if(this.conditions)if(Array.isArray(this.conditions))for(let t of this.conditions)this.applyCondition(t,e);else this.applyCondition(this.conditions,e)}applyCondition(e,t){(Array.isArray(e.conditions)?e.conditions.reduce(((e,i)=>e=e&&this.evalCondition(i,t)),!0):this.evalCondition(e.conditions,t))?e.handleThen&&(e.handleThen(t,this),this.requestUpdate()):e.handleElse&&(e.handleElse(t,this),this.requestUpdate())}evalCondition(e,t){return"function"==typeof e&&Boolean(e(t,this))}get displayValue(){return this.getValueIsDefined(this.value)?this.value:""}get hasValue(){return this.submitEmpty||this.getValueIsDefined(this.value)}getValueIsDefined(e){return!(void 0===e||!e||"undefined"===e||"null"===e)}get isFocused(){return this.inputFocused||this.preventBlur}get busy(){return!1}initDom(){this.classList.add("kode4-field"),this.classList.add(this.typeCssClass)}connectedCallback(){this.id||(this.id=`kode4field_${za()}`),this.name||(this.name=`kode4field_${za()}`),this.block&&this.classList.add("kode4-block"),this.canValidate&&this.classList.add("kode4-canValidate"),this.xxs&&this.classList.add("kode4-field-xxs"),this.xs&&this.classList.add("kode4-field-xs"),this.s&&this.classList.add("kode4-field-s"),this.m&&this.classList.add("kode4-field-m"),this.l&&this.classList.add("kode4-field-l"),this.xl&&this.classList.add("kode4-field-xl"),this.xxl&&this.classList.add("kode4-field-xxl"),super.connectedCallback()}disconnectedCallback(){var e;let t=new CustomEvent("kode4-field-destroyed",{bubbles:!1,composed:!0,detail:{fieldname:this.name,fieldid:this.id}});null===(e=this.kode4Form)||void 0===e||e.dispatchEvent(t),super.disconnectedCallback()}firstUpdated(e){var t;super.firstUpdated(e),this.initDom(),this.slotsReady=!0,this.input=(null===(t=this.shadowRoot)||void 0===t?void 0:t.querySelector(this.inputCssSelector))||void 0,this.init()}updated(e){this.lastFocusedState!==this.isFocused&&(this.isFocused?this.handleOnFocus():this.handleOnBlur()),this.lastFocusedState=this.isFocused}handleOnFocus(){}handleOnBlur(){}async init(e=!1){this.beforeInit(),await this.doInit(e),this.afterInit()}beforeInit(){this.initialized=!1,this.initializing=!0,this.dirty=!1,this.initialValue=void 0,this.initialDisabled=!1,this.error=""}async doInit(e=!1){e?this.updateValue():this.value=this._valueCandidate,this.initialValue=this.getValueIsDefined(this.value)?this.value:void 0,this.initialDisabled=this.disabled}afterInit(){this.initializing=!1,this.initialized=!0,this.resolveInitialized&&this.resolveInitialized(),this.fireRegisterEvent()}reset(){this.value=this.initialValue,this.disabled=this.initialDisabled,this.requestUpdate(),this.onChange()}clear(){this.value=this.resetValue,this.requestUpdate(),this.onChange()}updateValue(){this.value=this.getAttribute("value")}async respawn(){await this.init(!0),this.onChange()}resetState(){this.touched=!1,this.error=""}get valid(){return!this.canValidate||this.validate()}validate(){return this.error="",!(this.required&&!this.disabled&&!this.hidden&&!this.hasValue)||(this.error="Dieses Feld wird benötigt",!1)}focus(){this.inputFocusEventBlocked=!0,this.focusInput(),this.inputFocusEventBlocked=!1}blur(){}focusInput(){var e;null===(e=this.input)||void 0===e||e.focus()}blurInput(){var e;null===(e=this.input)||void 0===e||e.blur()}sanitizeInputFocused(){var e;this.inputFocused=(null===(e=this.shadowRoot)||void 0===e?void 0:e.activeElement)===this.input}fireRegisterEvent(){let e=new CustomEvent("kode4-field-created",{bubbles:!0,composed:!0,detail:{field:this}});this.dispatchEvent(e)}fireUnregisterEvent(){let e=new CustomEvent("kode4-field-destroyed",{bubbles:!0,composed:!0,detail:{fieldname:this.name,fieldid:this.id}});this.dispatchEvent(e)}onFocusInput(){this.inputFocused=!this.forceBlur}onBlurInput(){this.preventBlur?(this.sanitizeInputFocused(),this.preventBlur=!1,this.focusInput()):this.inputFocused=!1}onChange(e){this.touched=!0;let t=new CustomEvent("change",{bubbles:!0,composed:!0,detail:{disabled:this.disabled,readonly:this.readonly,manageValue:this.manageValue,value:this.value,name:this.name,originalEvent:e,dirty:this.dirty}});e&&e.preventDefault(),this.dispatchEvent(t)}onSubmit(e,t=!0){const i={originalEvent:e};t&&(i.value=this.value,i.name=this.name);let o=new CustomEvent("submitForm",{bubbles:!0,composed:!0,detail:i});e&&e.preventDefault(),this.dispatchEvent(o)}onMousedownPreventBlur(){this.isFocused&&(this.preventBlur=!0)}onMousedownPreventBlurOrForceBlur(){this.isFocused?this.preventBlur=!0:this.forceBlur=!0}onClear(){this.clear()}onReset(){this.reset()}get cssClasses(){return{"kode4-focus":this.isFocused,busy:this.busy,hidden:this.hidden,disabled:this.disabled,readonly:this.readonly,"kode4-field-hasValue":this.hasValue,"kode4-field-empty":!this.hasValue}}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">
                ${this.renderInput()}
            </div>
        `}renderInput(){return Pi`
            <input
                id="field"
                type="hidden"
                class="field-control-input"
                ?name="${this.name}"
                .value="${this.displayValue}"
                />
        `}get isLabelFloating(){return this.floatingLabel&&this.label&&!this.hasValue&&!this.isFocused}get legendWidth(){var e;if(this.floatingLabel&&this.isLabelFloating)return"0px";if(!this.labelWidth){const t=(null===(e=this.shadowRoot)||void 0===e?void 0:e.querySelector(".field-label"))||null;if(!t)return"auto";this.labelWidth=t.clientWidth}return this.labelWidth?`${this.labelWidth}px`:"auto"}renderLabel(e){return Pi`
            <legend class="field-legend" style="${Cs({width:this.legendWidth})}">${e}</legend>
            <label class="field-label" for="field">${e}${this.required&&this.requiredSuffix?Pi` ${this.requiredSuffix}`:""}</label>
        `}renderMessages(){return Pi`
            <div class="kode4-field-messages">
                <div class="kode4-field-messages-container">
                    ${this.renderHint()}
                    ${this.renderError()}
                </div>

                ${this.renderMessagesAddon()}
            </div>
        `}renderHint(){return this.hint?Pi`
            <div class="kode4-field-hint">${this.hint}</div>
        `:""}renderError(){return this.error?Pi`
            <div class="kode4-field-error">${this.error}</div>
        `:""}renderMessagesAddon(){return Pi``}static get styles(){return ni`
        
            * {
                box-sizing: border-box;
                text-overflow: ellipsis;
            }

            :host {
                display: inline-block;
                font-size: 1em;
                line-height: 1.0em;
                width: auto;
                max-width: 100%;
                box-sizing: border-box;

                margin-top: var(--kode4-field-padding-top, 0px);
                margin-right: var(--kode4-field-margin-right, 0px);
                margin-bottom: var(--kode4-field-padding-bottom, 0px);
                margin-left: var(--kode4-field-margin-left, 0px);
            }

            :host(.kode4-block) {
                display: block;
                width: 100%;
            }

            .component {
                transition: opacity 0.4s ease-in-out;
                display: flex;
                flex-direction: row;
            }

            .component.hidden {
                position: absolute;
                width: 0;
                overflow: hidden;
                visibility: hidden;
                opacity: 0;
            }

            .component.disabled,
            .component.disabled > *,
            .component.disabled .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .component.readonly,
            .component.readonly > *,
            .component.readonly .field-input-display {
                filter: grayscale(100%) brightness(102%);
                opacity: 0.85;
            }

            .field-container {
                flex: 1 0 1px;
                /* flex: 0 1 100%; */
                position: relative;
            }

            .field-input-container-layout {
                display: flex;
                flex-direction: row;
                /* max-width: 100%; */
                overflow: hidden;
            }

            .field-input {
                /* display: block; */
                flex: 1 0 1px;
                width: 100%;
                min-width: 10px;
                /* flex: 0 1 100%; */
                background: transparent;
                border: none;
                outline: none;
                box-shadow: none; /* Firefox adds a red shadow to required fields */
            }

            ${this.getLabelStyles()}
            ${this.getOutlineStyles()}
            ${this.getFilledStyles()}


            .kode4-field-messages {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;

                color: var(--kode4-field-messages-color);
                font-family: var(--kode4-field-messages-font-family);
                font-size: var(--kode4-field-messages-font-size);
                line-height: var(--kode4-field-messages-line-height);
                font-weight: var(--kode4-field-messages-font-weight);

                margin-right: var(--kode4-field-messages-margin-right);
                margin-left: var(--kode4-field-messages-margin-left);
                padding-right: var(--kode4-field-messages-padding-right);
                padding-left: var(--kode4-field-messages-padding-left);
            }
            
            .kode4-field-messages-container {
                flex: 1 0 1px;
            }

            .kode4-field-messages-container > *:first-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-top: var(--kode4-field-messages-padding-top);
            }
            
            .kode4-field-messages-container > *:last-child {
                margin-top: var(--kode4-field-messages-margin-top);
                padding-bottom: var(--kode4-field-messages-padding-bottom);
            }
                
            .kode4-field-messages-container > *:not(:first-child) {
                margin-top: var(--kode4-field-messages-spacing);
            }
                
            .outline .kode4-field-messages {
                padding-top: var(--kode4-field-outline-messages-padding-top);
                padding-right: var(--kode4-field-outline-messages-padding-right);
                padding-bottom: var(--kode4-field-outline-messages-padding-bottom);
                padding-left: var(--kode4-field-outline-messages-padding-left);
            }

            .kode4-field-hint {
                font-family: var(--kode4-field-hint-font-family, inherit);
                font-size: var(--kode4-field-hint-font-size, inherit);
                line-height: var(--kode4-field-hint-line-height, inherit);
                font-weight: var(--kode4-field-hint-font-weight, inherit);
                color: var(--kode4-field-hint-color, inherit);
            }

            .kode4-field-error {
                font-family: var(--kode4-field-error-font-family, inherit);
                font-size: var(--kode4-field-error-font-size, inherit);
                line-height: var(--kode4-field-error-line-height, inherit);
                font-weight: var(--kode4-field-error-font-weight, inherit);
                color: var(--kode4-field-error-color, inherit);
            }


            /*
            .field-hint-content {
                display: inline-block;
                font-family: var(--kode4-field-hint-font-family);
                font-size: var(--kode4-field-hint-font-size);
                line-height: var(--kode4-field-hint-line-height);
                font-weight: var(--kode4-field-hint-font-weight);
                color: var(--kode4-field-hint-color);
                padding-top: var(--kode4-field-hint-padding-top);
                padding-right: var(--kode4-field-hint-padding-right);
                padding-bottom: var(--kode4-field-hint-padding-bottom);
                padding-left: var(--kode4-field-hint-padding-left);
            }

            .field-error-content {
                display: inline-block;
                font-family: var(--kode4-field-error-font-family);
                font-size: var(--kode4-field-error-font-size);
                line-height: var(--kode4-field-error-line-height);
                font-weight: var(--kode4-field-error-font-weight);
                color: var(--kode4-field-error-color);
                padding-top: var(--kode4-field-error-padding-top);
                padding-right: var(--kode4-field-error-padding-right);
                padding-bottom: var(--kode4-field-error-padding-bottom);
                padding-left: var(--kode4-field-error-padding-left);
            }

            .outline .field-hint-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-hint-padding-top);
                padding-right: var(--kode4-field-outline-hint-padding-right);
                padding-bottom: var(--kode4-field-outline-hint-padding-bottom);
                padding-left: var(--kode4-field-outline-hint-padding-left);
            }

            .outline .field-error-content {
                display: inline-block;
                padding-top: var(--kode4-field-outline-error-padding-top);
                padding-right: var(--kode4-field-outline-error-padding-right);
                padding-bottom: var(--kode4-field-outline-error-padding-bottom);
                padding-left: var(--kode4-field-outline-error-padding-left);
            }
            */
        `}static getLabelStyles(){return ni`
            .field-label {
                display: block;
                text-overflow: ellipsis;
                max-width: 75%;
                overflow: hidden;
                white-space: nowrap;
            }

            .component {
            }

            .component .field-label {
                transition: all 0.3s ease-out;
                position: absolute;
                z-index: 10;
            }

            .field-input-container {
                margin-top: var(--kode4-field-input-container-margin-top);
                margin-right: var(--kode4-field-input-container-margin-right);
                margin-bottom: var(--kode4-field-input-container-margin-bottom);
                margin-left: var(--kode4-field-input-container-margin-left);
                padding-top: var(--kode4-field-input-container-padding-top);
                padding-right: var(--kode4-field-input-container-padding-right);
                padding-bottom: var(--kode4-field-input-container-padding-bottom);
                padding-left: var(--kode4-field-input-container-padding-left);
            }

            .field-label {
                transform: translate(var(--kode4-field-label-translate-x), var(--kode4-field-label-translate-y)) scale(var(--kode4-field-label-scale));
            }

            legend.field-legend {
                width: 0;
                opacity: 0;
                height: 0;
                margin: 0;
                padding: 0;
                visibility: hidden;
                transition: width 0.05s ease-out 0.15s;
            }

            .floatinglabel .field-label {
                transform: translate(var(--kode4-field-floatinglabel-label-translate-x), var(--kode4-field-floatinglabel-label-translate-y)) ;
            }
        `}static getOutlineStyles(){return ni`
            .component:not(.outline) .field-input-container {
                border: none;
            }

            .outline .field-input-container {
                border: var(--kode4-field-outline-border-style) var(--kode4-field-outline-border-width) var(--kode4-field-outline-border-color);
                margin-top: var(--kode4-field-outline-input-container-margin-top);
                margin-right: var(--kode4-field-outline-input-container-margin-right);
                margin-bottom: var(--kode4-field-outline-input-container-margin-bottom);
                margin-left: var(--kode4-field-outline-input-container-margin-left);
                padding-top: var(--kode4-field-outline-input-container-padding-top);
                padding-right: var(--kode4-field-outline-input-container-padding-right);
                padding-bottom: var(--kode4-field-outline-input-container-padding-bottom);
                padding-left: var(--kode4-field-outline-input-container-padding-left);
                border-radius: var(--kode4-field-outline-border-radius);
            }
            
            .outline .field-label {
                transform: translate(var(--kode4-field-outline-label-translate-x), var(--kode4-field-outline-label-translate-y)) scale(var(--kode4-field-outline-label-scale));
            }

            @-moz-document url-prefix() {
                .outline.floatinglabel .field-legend {
                    /* Fix. Firefox creates an overflow without this */
                    display: none;
                }
            }

            .outline.floatinglabel .field-label {
                transform: translate(var(--kode4-field-outline-floatinglabel-label-translate-x), var(--kode4-field-outline-floatinglabel-label-translate-y));
            }
        `}static getFilledStyles(){return ni`
            .component:not(.filled) .field-input-container {
                background-color: transparent;
            }

            .filled .field-input-container {
                background-color: var(--kode4-field-filled-background);
            }
        `}};Qt([io({type:Boolean})],Ss.prototype,"block",void 0),Qt([io({type:Boolean})],Ss.prototype,"submitEmpty",void 0),Qt([io({type:Boolean})],Ss.prototype,"initialized",void 0),Qt([io({type:Boolean})],Ss.prototype,"initializing",void 0),Qt([io({type:Boolean})],Ss.prototype,"touched",void 0),Qt([io()],Ss.prototype,"type",void 0),Qt([io()],Ss.prototype,"name",void 0),Qt([io({type:Boolean})],Ss.prototype,"required",void 0),Qt([io({type:Boolean})],Ss.prototype,"floatingLabel",void 0),Qt([io({type:Boolean})],Ss.prototype,"xxs",void 0),Qt([io({type:Boolean})],Ss.prototype,"xs",void 0),Qt([io({type:Boolean})],Ss.prototype,"s",void 0),Qt([io({type:Boolean})],Ss.prototype,"m",void 0),Qt([io({type:Boolean})],Ss.prototype,"l",void 0),Qt([io({type:Boolean})],Ss.prototype,"xl",void 0),Qt([io({type:Boolean})],Ss.prototype,"xxl",void 0),Qt([io({type:String,attribute:"required-suffix"})],Ss.prototype,"requiredSuffix",void 0),Qt([io()],Ss.prototype,"_value",void 0),Qt([io()],Ss.prototype,"_valueCandidate",void 0),Qt([io()],Ss.prototype,"value",null),Qt([io()],Ss.prototype,"stringValue",null),Qt([io({type:Boolean})],Ss.prototype,"manageValue",void 0),Qt([io()],Ss.prototype,"slotsReady",void 0),Qt([io()],Ss.prototype,"field",void 0),Qt([io()],Ss.prototype,"input",void 0),Qt([io()],Ss.prototype,"initialValue",void 0),Qt([io()],Ss.prototype,"resetValue",void 0),Qt([io({type:Boolean})],Ss.prototype,"initialDisabled",void 0),Qt([io({type:Boolean})],Ss.prototype,"_disabled",void 0),Qt([io({type:Boolean})],Ss.prototype,"disabled",null),Qt([io({type:Boolean})],Ss.prototype,"readonly",void 0),Qt([io({type:Boolean})],Ss.prototype,"controlReadonly",void 0),Qt([io()],Ss.prototype,"label",void 0),Qt([io()],Ss.prototype,"hint",void 0),Qt([io()],Ss.prototype,"error",void 0),Qt([io({type:Number,attribute:"debounce",reflect:!0})],Ss.prototype,"debounce",void 0),Qt([io({type:Boolean})],Ss.prototype,"canValidate",void 0),Qt([io()],Ss.prototype,"conditions",void 0),Qt([oo()],Ss.prototype,"_kode4Form",void 0),Qt([oo()],Ss.prototype,"kode4Form",null),Qt([io()],Ss.prototype,"displayValue",null),Qt([io()],Ss.prototype,"hasValue",null),Qt([io({type:Boolean})],Ss.prototype,"inputFocused",void 0),Qt([io()],Ss.prototype,"isFocused",null),Qt([io({type:Boolean})],Ss.prototype,"busy",null),Qt([io({type:String})],Ss.prototype,"busyStyle",void 0),Qt([io({type:Boolean})],Ss.prototype,"dirty",void 0),Qt([io()],Ss.prototype,"cssClasses",null),Qt([io({type:Number})],Ss.prototype,"labelWidth",void 0),Qt([io({type:Boolean})],Ss.prototype,"isLabelFloating",null),Qt([io({type:String})],Ss.prototype,"legendWidth",null),Ss=Qt([eo("kode4-field")],Ss);const Os=Ss,Es=e=>null!=e?e:Bi;var Ds=i(421),zs=i(24);let As=class extends Os{constructor(){super(),this.outline=!1,this.filled=!1,this.placeholder="",this.prefixOutside="",this.prefix="",this.suffix="",this.suffixOutside="",this.iconReset=zs.R8,this.iconClear=Aa.NB,this.iconAction=Ds.eW,this.resettable=!1,this.action=!1,this.clearable=!1,this.changeOnType=!1,this.counter=!1,this.counterDivider="/",this.hasDialog=!1,this.dialogOpen=!1,this.openDialogOnFocus=!1,this.confirmOnEnter=!0,this.onInputChangeDebounced=vs(this.onInputChange,this.debounce),this.dialogActionBlocked=void 0}get typeCssClass(){return"kode4-textfield"}get charCount(){return this.isFocused&&!this.changeOnType&&this.input?String(this.input.value).length:this.hasValue?this.value.length:0}validate(){return!!super.validate()&&(!(this.counter&&this.counterMax&&this.charCount>this.counterMax)||(this.error=`Die maximale Zeichenanzahl beträgt ${this.counterMax}`,!1))}get isDialogOpen(){return this.dialogOpen}async doInit(e=!1){super.doInit(e),this.outline&&(this.busyStyle="icon")}reset(){super.reset(),!this.changeOnType&&this.input&&(this.input.value=this.displayValue)}clear(){super.clear(),!this.changeOnType&&this.input&&(this.input.value=this.displayValue)}handleEsc(){this.onClear()}openDialog(){this.readonly||this.disabled||(this.dialogOpen=!0)}closeDialog(){this.dialogOpen=!1}toggleDialog(){this.readonly||this.disabled||(this.dialogOpen?this.closeDialog():this.openDialog())}onFocusInput(){super.onFocusInput(),this.openDialogOnFocus&&!this.forceBlur&&this.openDialog(),this.forceBlur=!1}onInputChange(e){var t;this.value=null===(t=this.input)||void 0===t?void 0:t.value,this.onChange(e)}onKeydown(e){}onKeyup(e){"Escape"===e.key?this.handleEsc():"Enter"===e.key&&this.confirmOnEnter?this.onSubmit(e,!1):this.changeOnType?this.onInputChangeDebounced(e):this.counter&&this.requestUpdate()}onActionMousedown(){this.onMousedownPreventBlur(),this.dialogActionBlocked=!this.dialogOpen}onAction(){this.dialogActionBlocked=void 0}get isLabelFloating(){return this.floatingLabel&&this.label&&!this.hasValue&&!this.showPlaceholder&&!this.prefix&&!this.iconPrepend&&!this.isFocused}get showPlaceholder(){return!this.hasValue&&Boolean(this.placeholder)}get cssClasses(){return{...super.cssClasses,"kode4-dialog-open":this.isDialogOpen,outline:this.outline,filled:this.filled,floatinglabel:this.isLabelFloating}}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">

                ${this.iconPrependOutside?this.renderIcon(this.iconPrependOutside,"icon-prepend-outside","click-icon-prepend-outside"):""}

                ${this.prefixOutside?this.renderPrefixOutside(this.prefixOutside):""}

                <div class="field-container">
                    <fieldset class="field-input-container">
                        ${this.label?this.renderLabel(this.label):""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend?this.renderIcon(this.iconPrepend,"icon-prepend","click-icon-prepend"):""}
                            ${this.prefix?this.renderInlinePrefix(this.prefix):""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix?this.renderInlineSuffix(this.suffix):""}
                            ${this.action?this.renderActionButton():""}
                            ${this.resettable&&this.initialValue&&this.initialValue!==this.value?this.renderResetButton():""}
                            ${this.clearable&&""!==this.displayValue?this.renderClearButton():""}
                            ${this.iconAppend?this.renderIcon(this.iconAppend,"icon-append","click-icon-append"):""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}
                    
                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside?this.renderSuffixOutside(this.suffixOutside):""}

                ${this.iconAppendOutside?this.renderIcon(this.iconAppendOutside,"icon-append-outside","click-icon-append-outside"):""}

            </div>
        `}renderIcon(e,t,i){return Pi`
            <label for="field" class="${t}" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,i||"click-icon")}"><kode4-fa-icon .icon="${e}"></kode4-fa-icon></label>
        `}fireIconEvent(e,t){let i=new CustomEvent(t,{bubbles:!0,composed:!0,detail:{element:this,originalEvent:e}});this.dispatchEvent(i)}renderInlinePrefix(e){return Pi`
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix")}">${e}</label>
        `}renderPrefixOutside(e){return Pi`
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix-outside")}">${e}</label>
        `}renderSuffixOutside(e){return Pi`
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix-outside")}">${e}</label>
        `}renderInput(){return Pi`
            <input
                class="field-input"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"

                min="${Es(this.min)}"
                max="${Es(this.max)}"
                step="${Es(this.step)}"
                size="${Es(this.size)}"
                maxlength="${Es(this.maxlength)}"

                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${e=>e.stopPropagation()}"
                autocomplete="off"
                />
        `}renderDecoratorBar(){return Pi`
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `}renderProgressBar(){return this.busy&&"linear"===this.busyStyle?Pi`
            <kode4-progress indeterminate></kode4-progress>
        `:""}renderProgressIcon(){return this.busy&&"icon"===this.busyStyle?Pi`
                <kode4-progress
                    type="icon"
                    indeterminate
                    class="icon-busy"
                    @mousedown="${this.onActionMousedown}"
                    ></kode4-progress>
        `:""}renderInlineSuffix(e){return Pi`
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix")}">${e}</label>
        `}renderResetButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `}renderActionButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `}renderClearButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `}renderMessagesAddon(){return this.renderCounter()}renderCounter(){return this.counter?Pi`
            <span class="field-counter">
                <span class="field-counter-chars">
                    ${this.charCount}
                </span>
            ${this.counterMax?Pi`
                <span class="field-counter-divider">
                    ${this.counterDivider}
                </span>
                <span class="field-counter-max">
                    ${this.counterMax}
                </span>
            `:""}
            ${this.counterLabel?Pi`
                <span class="field-counter-label">
                    ${this.counterLabel}
                </span>
            `:""}
            </span>
        `:Pi``}renderDialogContainer(){return this.hasDialog?Pi`
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `:""}renderDialog(){return Pi`
        `}static get styles(){return ni`
            ${super.styles}
            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                display: none;
                visibility: hidden;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }

            .field-counter {
                margin-top: var(--kode4-field-messages-margin-top);
                margin-bottom: var(--kode4-field-messages-margin-bottom);
                padding-top: var(--kode4-field-messages-padding-top);
                padding-bottom: var(--kode4-field-messages-padding-bottom);
                padding-left: var(--kode4-field-counter-padding-left);
            }



        `}};Qt([io()],As.prototype,"min",void 0),Qt([io({type:Number})],As.prototype,"size",void 0),Qt([io({type:Number})],As.prototype,"maxlength",void 0),Qt([io()],As.prototype,"max",void 0),Qt([io({type:Number})],As.prototype,"step",void 0),Qt([io({type:Boolean})],As.prototype,"outline",void 0),Qt([io({type:Boolean})],As.prototype,"filled",void 0),Qt([io({type:String})],As.prototype,"placeholder",void 0),Qt([io({type:String})],As.prototype,"prefixOutside",void 0),Qt([io({type:String})],As.prototype,"prefix",void 0),Qt([io({type:String})],As.prototype,"suffix",void 0),Qt([io({type:String})],As.prototype,"suffixOutside",void 0),Qt([io()],As.prototype,"iconPrependOutside",void 0),Qt([io()],As.prototype,"iconPrepend",void 0),Qt([io()],As.prototype,"iconAppend",void 0),Qt([io()],As.prototype,"iconAppendOutside",void 0),Qt([io()],As.prototype,"iconReset",void 0),Qt([io()],As.prototype,"iconClear",void 0),Qt([io()],As.prototype,"iconAction",void 0),Qt([io({type:Boolean})],As.prototype,"resettable",void 0),Qt([io({type:Boolean})],As.prototype,"action",void 0),Qt([io({type:Boolean})],As.prototype,"clearable",void 0),Qt([io({type:Boolean})],As.prototype,"changeOnType",void 0),Qt([io({type:Boolean})],As.prototype,"counter",void 0),Qt([io({type:Number,attribute:"counter-max"})],As.prototype,"counterMax",void 0),Qt([io({type:String,attribute:"counter-divider"})],As.prototype,"counterDivider",void 0),Qt([io({type:String,attribute:"counter-label"})],As.prototype,"counterLabel",void 0),Qt([io()],As.prototype,"charCount",null),Qt([io({type:Boolean})],As.prototype,"hasDialog",void 0),Qt([io({type:Boolean})],As.prototype,"dialogOpen",void 0),Qt([io({type:Boolean})],As.prototype,"isDialogOpen",null),Qt([io({type:Boolean})],As.prototype,"openDialogOnFocus",void 0),Qt([io({type:Boolean})],As.prototype,"confirmOnEnter",void 0),Qt([io({type:Boolean})],As.prototype,"isLabelFloating",null),Qt([io({type:Boolean})],As.prototype,"showPlaceholder",null),Qt([io()],As.prototype,"cssClasses",null),As=Qt([eo("kode4-textfield")],As);const Ls=As;let _s=class extends Ls{constructor(){super(),this.resize="vertical",this.confirmOnEnter=!1}get typeCssClass(){return"kode4-textarea"}get inputCssSelector(){return"textarea"}renderInput(){return Pi`
            <textarea
                class="field-input ${this.resize}"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="${this.type}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${e=>e.stopPropagation()}"
                >${this.displayValue}</textarea>
        `}static get styles(){return ni`
            ${super.styles}

            textarea.field-input {
                min-height: 3em;
                height: 3em;
            }

            textarea.field-input.both {
                resize: both;
            }

            textarea.field-input.vertical {
                resize: vertical;
            }

            textarea.field-input.horizontal {
                resize: horizontal;
            }

            textarea.field-input.none {
                resize: none;
            }

        `}};Qt([io()],_s.prototype,"resize",void 0),_s=Qt([eo("kode4-textarea")],_s);class Ps extends ws{constructor(e){if(super(e),this.it=Bi,e.type!==ys)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(e){if(e===Bi||null==e)return this.vt=void 0,this.it=e;if(e===Ti)return e;if("string"!=typeof e)throw Error(this.constructor.directiveName+"() called with a non-string value");if(e===this.it)return this.vt;this.it=e;const t=[e];return t.raw=t,this.vt={_$litType$:this.constructor.resultType,strings:t,values:[]}}}Ps.directiveName="unsafeHTML",Ps.resultType=1;const Ts=xs(Ps);class Bs{constructor(e){this.busy=!1,this.list=[],this.subscribers=[],e&&this.setList(e)}subscribe(e){this.subscribers.includes(e)||this.subscribers.push(e)}unsubscribe(e){if(this.subscribers.includes(e)){const t=this.subscribers.indexOf(e);this.subscribers.splice(t,1)}}fireListChanged(){this.subscribers.map((e=>{e.handleListUpdated()}))}clearList(){this.list=[],this.fireListChanged()}setList(e){e.length&&e!==this.list&&(this.list=e,this.fireListChanged())}applyListFilters(e){return e}async getList(e){let t;return t=this.valueField||this.labelField?this.list.map((e=>(this.valueField&&(e.value=e[this.valueField]),this.labelField&&(e.label=e[this.labelField]),e))):this.list,t=e&&e.has("searchterm")?t.filter((t=>t.label===e.get("searchterm"))):t,t=this.applyListFilters(t),this.noSelection&&t.unshift({label:this.noSelection,unsafe:!1}),{error:0,list:t}}async checkItemChangedAfterListUpdate(e,t){try{const i=await this.getItem(e);return!((!i||t)&&(null==i?void 0:i.label)==(null==t?void 0:t.label))&&i}catch(e){return!1}}async getItem(e){const t=this.list.reduce(((t,i)=>(i.value==e&&(t=i),t)),void 0);if(void 0===t)throw new Error("Item not found for "+e);return this.valueField&&(t.value=t[this.valueField]),this.labelField&&(t.label=t[this.labelField]),t}}class Is{constructor(e="default"){this.httpConfigBase="default",this.baseUrl="",this.listCacheEnabled=!0,this.listCache=[],this.listUrl="",this.itemUrl="",this.busyList=!1,this.busyItem=!1,this.subscribers=[],this.httpConfigBase=e}get busy(){return this.busyList||this.busyItem}subscribe(e){this.subscribers.includes(e)||this.subscribers.push(e)}unsubscribe(e){if(this.subscribers.includes(e)){const t=this.subscribers.indexOf(e);this.subscribers.splice(t,1)}}fireListChanged(){this.subscribers.map((e=>{e.handleListUpdated()}))}applyRequestFilters(e){return e}applyListFilters(e){return e}async getList(e){var t;try{const i={searchterm:String(null==e?void 0:e.get("searchterm"))},o=JSON.stringify(i);if(this.lastRequestHash===o&&this.listCache)return{total:this.listCache.length,list:this.listCache,error:0};this.lastRequestHash=o,this.busyList=!0;const n=await jt().config(this.httpConfigBase).url(`${this.baseUrl}${this.listUrl}`).param(i).get();console.log("[Kode4RestOptionsProvider] response",n);const r={list:(null===(t=n.data)||void 0===t?void 0:t.list)||n.data,error:0};if(!Array.isArray(r.list))throw Error("Invalid list result: "+typeof r.list);return(this.valueField||this.labelField)&&(r.list=r.list.map((e=>(this.valueField&&(e.value=e[this.valueField]),this.labelField&&(e.label=e[this.labelField]),e)))),r.list=this.applyListFilters(r.list),this.listCacheEnabled&&(this.listCache=this.listCache.concat(r.list)),this.noSelection&&r.list.unshift({label:this.noSelection,unsafe:!1}),this.busyList=!1,r}finally{this.busyList=!1}}async checkItemChangedAfterListUpdate(){return!1}getItemFromListCache(e){return this.listCache.find((t=>String(t.value)===e))}clearListCache(){this.listCache=[]}async getItem(e){try{if(!e)throw new Error("primary Key is required");if(this.listCacheEnabled){const t=this.getItemFromListCache(e);if(void 0!==t)return t}this.busyItem=!0;const t=(await jt().config(this.httpConfigBase).url(`${this.baseUrl}${this.itemUrl}/${e}`).get()).data.data;return this.valueField&&(t.value=t[this.valueField]),this.labelField&&(t.label=t[this.labelField]),this.busyItem=!1,this.listCache.push(t),t}finally{this.busyList=!1}}}let Fs=class extends Ls{constructor(){super(),this._options=[],this.optionHighlighted=-1,this.searchTerm=void 0,this.filters=new FormData,this.forceSelection=!1,this.onSearchTermChangeDebounced=vs(this.onSearchTermChange,this.debounce),this.type="select",this.action=!0,this.hasDialog=!0,this.openDialogOnFocus="select"===this.type,this.changeOnType=!0,document.addEventListener("click",this.onOutsideClick.bind(this))}get typeCssClass(){return`kode4-${this.type}`}get value(){return super.value}set value(e){var t;this.initialized||this.initializing?(super.value=e,void 0===e?("autocomplete"===this.type&&(this.searchTerm=void 0),this._selectedOption=void 0,this.onChange()):(null===(t=this._selectedOption)||void 0===t?void 0:t.value)!=e&&(async()=>{var t;try{if(!e||"undefined"==e||"null"==e)throw new Error("Skip getItem on optionsProvider, value is falsy");"autocomplete"===this.type&&(this.searchTerm=void 0);let i=await(null===(t=this.optionsProvider)||void 0===t?void 0:t.getItem(e));this._selectedOption=i,this.onChange()}catch(e){super.value=void 0,this._selectedOption=void 0}})()):this._valueCandidate=e}get displayValue(){var e;return"autocomplete"===this.type&&void 0!==this.searchTerm&&this.searchTerm!==(null===(e=this.selectedOption)||void 0===e?void 0:e.value)?this.searchTerm:this.selectedOption?this.selectedOption.unsafe?Pi`${Ts(String(this.selectedOption.label))}`:this.selectedOption.label:""}get options(){return this._options}set options(e){this.initialized||this.initializing?this._options=e.map((e=>("object"==typeof e&&(this.valueField&&(e.value=e[this.valueField]),this.labelField&&(e.label=e[this.labelField])),"object"==typeof e?{...e,value:e.value||void 0,label:e.label||"",unsafe:e.unsafe||!1}:{value:e,label:e,unsafe:!1}))):this._optionsCandidate=e}get optionsProvider(){return this._optionsProvider}set optionsProvider(e){this._optionsProvider&&this._optionsProvider.unsubscribe(this),this._optionsProvider=e,this.initOptionsProvider()}get selectedOption(){if(this._selectedOption)return this._selectedOption}set selectedOption(e){this._selectedOption=e,this.value=(null==e?void 0:e.value)||void 0}getOptionByValue(e){if(e)return this.options.reduce(((t,i)=>(null==i?void 0:i.value)==e?i:t),void 0)}get busy(){return!!super.busy||!!this.optionsProvider&&this.optionsProvider.busy}async firstUpdated(e){super.firstUpdated(e),void 0===this.openDialogOnFocus&&(this.openDialogOnFocus=!0)}async doInit(e=!1){this.initOptionsProvider(),e?this.updateValue():this.value=this._valueCandidate,this.initialValue=this.value,this.outline&&(this.busyStyle="icon")}async initOptionsProvider(){if(!this.optionsProvider){const e=new Bs;e.list=this._optionsCandidate||[],this.optionsProvider=e}this.optionsProvider.subscribe(this),this.updateOptionsFromOptionsProvider()}handleListUpdated(){this.updateOptionsFromOptionsProvider()}async updateOptionsFromOptionsProvider(){var e;try{this.handleOptionsProviderListResponse(await(null===(e=this.optionsProvider)||void 0===e?void 0:e.getList(this.filters)))}catch(e){this.handleOptionsProviderListResponseError(e)}}async handleOptionsProviderListResponse(e){var t;try{if(0!==e.error)throw new Error("Errorcode");this.options=e.list,this.dialogErrorMessage=void 0,this.value||this.updateValue();const i=await(null===(t=this.optionsProvider)||void 0===t?void 0:t.checkItemChangedAfterListUpdate(String(this.value),this._selectedOption));!1!==i&&(this._selectedOption=i)}catch(t){this.handleOptionsProviderListResponseError(t,e)}}handleOptionsProviderListResponseError(e,t){throw this.options=[],this.dialogErrorMessage=(null==t?void 0:t.errorMessage)||"Beim Laden ist ein Fehler aufgetreten",e}addFocusClass(){}removeFocusClass(){}handleOnFocus(){}handleOnBlur(){this.optionHighlighted=-1,this.searchTerm=void 0}highlightPrevOption(){this.options?-1!==this.optionHighlighted?(this.optionHighlighted-=1,this.optionHighlighted<0&&(this.optionHighlighted=0)):this.optionHighlighted=this.options.length-1:this.optionHighlighted=-1}highlightNextOption(){this.options?-1!==this.optionHighlighted?(this.optionHighlighted+=1,this.optionHighlighted>this.options.length-1&&(this.optionHighlighted=this.options.length-1)):this.optionHighlighted=0:this.optionHighlighted=-1}selectHighlightedOption(){var e,t,i,o;-1!==this.optionHighlighted?(this.value=null===(e=this.options[this.optionHighlighted])||void 0===e?void 0:e.value,this.searchTerm=void 0):(null===(t=this.searchTerm)||void 0===t?void 0:t.length)&&this.options.length?(this.value=null===(i=this.options[0])||void 0===i?void 0:i.value,this.searchTerm=void 0):this.forceSelection&&this.options.length&&!this.value?(this.value=null===(o=this.options[0])||void 0===o?void 0:o.value,this.searchTerm=void 0):this.options.length||(this.value=void 0,this.searchTerm=void 0)}onChange(e){this.touched=!0;let t=new CustomEvent("change",{bubbles:!0,composed:!0,detail:{disabled:this.disabled,manageValue:this.manageValue,value:this.value,valueObject:this._selectedOption,name:this.name,originalEvent:e,dirty:this.dirty}});e&&e.preventDefault(),this.dispatchEvent(t)}onKeydown(e){"Tab"!==e.key?"autocomplete"!==this.type&&e.preventDefault():this.dialogOpen&&(this.selectHighlightedOption(),this.closeDialog())}onKeyup(e){var t;switch(e.key){case"Escape":return void(this.dialogOpen?this.closeDialog():this.handleEsc());case"ArrowDown":return void(this.dialogOpen?this.highlightNextOption():this.openDialog());case"ArrowUp":return void(this.dialogOpen?this.highlightPrevOption():this.openDialog());case"ArrowRight":return void(this.dialogOpen||this.openDialog());case"ArrowLeft":return void(this.dialogOpen&&this.closeDialog());case"Enter":return void(this.dialogOpen?(this.selectHighlightedOption(),this.closeDialog()):this.onSubmit(e));case"Tab":return;default:"autocomplete"===this.type&&(this.dialogOpen||this.openDialog(),this.onSearchTermChangeDebounced(null===(t=this.input)||void 0===t?void 0:t.value)),e.preventDefault()}}onInputChange(){}onSearchTermChange(e){this.isFocused?this.searchTerm!==e&&(this.searchTerm=e,this.optionsProvider&&(this.filters.set("searchterm",e),this.updateOptionsFromOptionsProvider())):this.searchTerm=void 0}onAction(){void 0!==this.dialogActionBlocked&&(this.dialogOpen=this.dialogActionBlocked)}onOptionClick(e){var t,i,o;e.preventDefault();const n=null===(t=e.target)||void 0===t?void 0:t.closest("[data-value]");void 0!==n&&((null===(i=this.shadowRoot)||void 0===i?void 0:i.contains(n))||this.contains(n))&&(this.value=(null===(o=n)||void 0===o?void 0:o.dataset.value)||void 0,this.searchTerm=void 0,this.closeDialog())}onOutsideClick(e){var t;(null===(t=e.target)||void 0===t?void 0:t.closest(`#${this.id}`))||this.closeDialog()}renderInput(){return Pi`
            ${super.renderInput()}
            <label
                for="field"
                class="field-input field-input-display ${this.displayValue?"":"field-input-display-placeholder"}"
                >${this.displayValue||this.placeholder||Pi`&nbsp;`}</label>
        `}renderDialog(){return Pi`
            ${this.dialogErrorMessage?Pi`
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            `:""}
            ${this.dialogMessagePrepend?Pi`
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            `:""}
            <div class="list" @click="${this.onOptionClick}">
                ${this.options.map((e=>this.renderOption(e)))}
            </div>
            ${this.dialogMessageAppend?Pi`
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            `:""}
        `}renderOption(e){return Pi`
            <div class="kode4-select-option ${-1!==this.optionHighlighted&&e===this.options[this.optionHighlighted]?"kode4-select-option-highlight":""}" data-value="${e.value}">
                ${e.unsafe?Pi`${Ts(String(e.label))}`:e.label}
            </div>
        `}static get styles(){return ni`
            ${super.styles}

            :host(.kode4-select) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            :host(.kode4-autocomplete) .field-input.field-input-display {
                display: none !important;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .kode4-select-option {
                display: block;
                padding-top: var(--kode4-select-option-padding-top);
                padding-right: var(--kode4-select-option-padding-right);
                padding-bottom: var(--kode4-select-option-padding-bottom);
                padding-left: var(--kode4-select-option-padding-left);
                margin-top: var(--kode4-select-option-margin-top);
                margin-right: var(--kode4-select-option-margin-right);
                margin-bottom: var(--kode4-select-option-margin-bottom);
                margin-left: var(--kode4-select-option-margin-left);
                cursor: pointer;

                color: var(--kode4-select-option-color);
                font-family: var(--kode4-select-option-font-family);
                font-size: var(--kode4-select-option-font-size);
                line-height: var(--kode4-select-option-line-height);
                font-weight: var(--kode4-select-option-font-weight);
                opacity: var(--kode4-select-option-opacity);
                background-color: var(--kode4-select-option-background-color);
            }

            .kode4-select-option:first-child {
                border-top-left-radius: var(--kode4-field-dialog-border-radius);
                border-top-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option:last-child {
                border-bottom-left-radius: var(--kode4-field-dialog-border-radius);
                border-bottom-right-radius: var(--kode4-field-dialog-border-radius);
            }

            .kode4-select-option.kode4-select-option-highlight,
            .kode4-select-option:hover {
                color: var(--kode4-select-option-selected-color);
                font-weight: var(--kode4-select-option-selected-font-weight);
                opacity: var(--kode4-select-option-selected-opacity);
                background-color: var(--kode4-select-option-selected-background-color);
            }

            .kode4-field-dialog {
                max-height: var(--kode4-select-dialog-max-height);
                overflow: auto;
            }
        `}};Qt([io()],Fs.prototype,"valueObject",void 0),Qt([io()],Fs.prototype,"value",null),Qt([io()],Fs.prototype,"displayValue",null),Qt([io({type:Array})],Fs.prototype,"_options",void 0),Qt([io({type:Array})],Fs.prototype,"_optionsCandidate",void 0),Qt([io()],Fs.prototype,"options",null),Qt([io({type:String})],Fs.prototype,"valueField",void 0),Qt([io({type:String})],Fs.prototype,"labelField",void 0),Qt([io()],Fs.prototype,"_optionsProvider",void 0),Qt([io()],Fs.prototype,"optionsProvider",null),Qt([io()],Fs.prototype,"_selectedOption",void 0),Qt([io()],Fs.prototype,"selectedOption",null),Qt([io()],Fs.prototype,"optionHighlighted",void 0),Qt([io({type:Boolean})],Fs.prototype,"busy",null),Qt([io()],Fs.prototype,"searchTerm",void 0),Qt([io()],Fs.prototype,"filters",void 0),Qt([io()],Fs.prototype,"dialogMessagePrepend",void 0),Qt([io()],Fs.prototype,"dialogMessageAppend",void 0),Qt([io()],Fs.prototype,"dialogErrorMessage",void 0),Qt([io({type:Boolean,attribute:"force-selection"})],Fs.prototype,"forceSelection",void 0),Fs=Qt([eo("kode4-select")],Fs);var Ms=i(723);let Rs=class extends Os{constructor(){super(),this.previewImageMode="contain",this.mimeTypes=[],this.uploading=!1,this.progress=0,this.imageDragoverOptions={acceptsFiles:!0},this.showFilename=!1,this.outline=!1,this.filled=!1,this.placeholder="",this.placeholderImage="dummy",this.prefixOutside="",this.prefix="",this.suffix="",this.suffixOutside="",this.iconReset=zs.R8,this.iconClear=Aa.NB,this.iconAction=Ds.eW,this.resettable=!1,this.action=!1,this.clearable=!1,this.hasDialog=!1,this.dialogOpen=!1,this.openDialogOnFocus=!1,this.dialogActionBlocked=void 0}get typeCssClass(){return"kode4-imagefield"}get value(){return this._value}set value(e){if(super.value=e,this.hasValue&&"string"==typeof this.value&&this.value.length)try{JSON.parse(String(this.value)),this.previewThumb=void 0}catch(e){this.previewThumb!==String(this.value)&&(this.previewThumb=String(this.value))}}get fileUploader(){return this._fileUploader}set fileUploader(e){this._fileUploader&&this._fileUploader.unsubscribe(this),this._fileUploader=e,this._fileUploader&&this._fileUploader.subscribe(this)}get isDialogOpen(){return this.dialogOpen}get isLabelFloating(){return this.floatingLabel&&this.label&&!this.hasValue&&!this.showPlaceholder&&!this.prefix&&!this.iconPrepend}get showPlaceholder(){return!this.hasValue&&(Boolean(this.placeholder)||Boolean(this.placeholderIcon))}get displayValue(){if(!this.getValueIsDefined(this.value))return"";try{const e=JSON.parse(String(this.value)),t=Array.isArray(e)?e[0]:e;return t?t.hasOwnProperty("originalFilename")?t.originalFilename:this.value:"?"}catch(e){return String(this.value).split("/").pop()}}firstUpdated(e){super.firstUpdated(e)}async doInit(e=!1){super.doInit(e)}reset(){super.reset()}clear(){super.clear()}handleEsc(){this.onClear()}openDialog(){this.dialogOpen=!0}closeDialog(){this.dialogOpen=!1}toggleDialog(){this.dialogOpen?this.closeDialog():this.openDialog()}onFocusInput(){super.onFocusInput(),this.openDialogOnFocus&&!this.forceBlur&&this.openDialog(),this.inputBlurEventBlocked=!0,this.forceBlur=!1}async uploadFiles(e){try{if(!this.fileUploader)throw new Error("FileUploader is not defined.");this.updateValueAfterUpload(await this.fileUploader.upload(e))}catch(e){this.handleOnUploadError(e)}}updateValueAfterUpload(e){const t=[];e.forEach((e=>{t.push({uploadedFilename:e.filename,originalFilename:e.file.name,originalFilesize:e.file.size})})),this.value=JSON.stringify(t),this.onChange();const i=e[0];var o=new FileReader;o.onload=e=>{this.previewThumb=String(e.target.result)},o.readAsDataURL(i.file)}handleOnBeginUpload(){this.error="",this.uploading=!0,this.progress=0}handleOnUploadProgress(e){this.progress=e}handleOnUploadComplete(){this.uploading=!1,this.progress=0}handleOnUploadError(e){this.error=e.message,this.uploading=!1,this.progress=0}onInputChange(e){this.uploadFiles(e.target.files||void 0)}onActionMousedown(e){e.preventDefault(),this.onMousedownPreventBlur(),this.dialogActionBlocked=!this.dialogOpen}onAction(){this.dialogActionBlocked=void 0}get cssClasses(){return{...super.cssClasses,"kode4-dialog-open":this.isDialogOpen,outline:this.outline,filled:this.filled,floatinglabel:this.isLabelFloating}}onDropOnAssets(e){var t;this.uploadFiles(null===(t=e.dataTransfer)||void 0===t?void 0:t.files)}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">

                ${this.iconPrependOutside?this.renderIcon(this.iconPrependOutside,"icon-prepend-outside","click-icon-prepend-outside"):""}

                ${this.prefixOutside?this.renderPrefixOutside(this.prefixOutside):""}

                <div class="field-container"
                
                    @dragover="${e=>ha(e,this.imageDragoverOptions)}"
                    @dragleave="${fa}"
                    @drop="${e=>ga(e,this.onDropOnAssets.bind(this))}"
                    >
                
                    <fieldset class="field-input-container">
                        ${this.label?this.renderLabel(this.label):""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend?this.renderIcon(this.iconPrepend,"icon-prepend","click-icon-prepend"):""}
                            ${this.prefix?this.renderInlinePrefix(this.prefix):""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix?this.renderInlineSuffix(this.suffix):""}
                            ${this.action?this.renderActionButton():""}
                            ${this.resettable&&this.initialValue&&this.initialValue!==this.value?this.renderResetButton():""}
                            ${this.clearable&&""!==this.displayValue?this.renderClearButton():""}
                            ${this.iconAppend?this.renderIcon(this.iconAppend,"icon-append","click-icon-append"):""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}

                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside?this.renderSuffixOutside(this.suffixOutside):""}

                ${this.iconAppendOutside?this.renderIcon(this.iconAppendOutside,"icon-append-outside","click-icon-append-outside"):""}

            </div>
        `}renderIcon(e,t,i){return Pi`
            <label for="field" class="${t}" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,i||"click-icon")}"><kode4-fa-icon .icon="${e}"></kode4-fa-icon></label>
        `}renderInlinePrefix(e){return Pi`
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix")}">${e}</label>
        `}renderPrefixOutside(e){return Pi`
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix-outside")}">${e}</label>
        `}renderSuffixOutside(e){return Pi`
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix-outside")}">${e}</label>
        `}fireIconEvent(e,t){let i=new CustomEvent(t,{bubbles:!0,composed:!0,detail:{element:this,originalEvent:e}});this.dispatchEvent(i)}renderPlaceholder(){if(!this.showPlaceholder&&this.hasValue)return"";return Pi`
            <div style="${Cs({display:"flex",flexDirection:"row",flexWrap:"nowrap",alignItems:"center",margin:"0",padding:"0"})}">
                <div style="${Cs({flex:"1 0 1px",textAlign:"center"})}" class="imagePreviewAspectRatio">
                    <div class="placeholder-container">
                        ${this.placeholderIcon?Pi`<div class="placeholder-icon"><kode4-fa-icon .icon="${this.placeholderIcon}"></kode4-fa-icon></div>`:""}
                        ${this.placeholder?Pi`<div class="placeholder">${this.placeholder}</div>`:Pi`<div class="placeholder">&nbsp;</div>`}
                    </div>
                </div>
            </div>
        `}renderPreviewThumb(){return Pi`
            <div>${this.displayValue}</div>
        `}renderInput(){const e=this.mimeTypes?this.mimeTypes.join(","):void 0;return Pi`
            <div
                class="field-input">
                ${this.renderPlaceholder()}
                ${this.renderPreviewThumb()}
                <div
                    style="overflow: hidden !important; width: 0 !important; height: 0 !important; padding: 0 !important; margin: 0 !important; border: none !important; display: block !important;">
                    <input 
                        id="field"
                        type="file" 
                        accept="${Es(e)}"
                        @change="${this.onInputChange}"
                        @focus="${this.onFocusInput}"
                        @blur="${this.onBlurInput}"
                        >
                </div>
            </div>
            <input
                type="hidden"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                type="text"

                .value="${this.displayValue}"
                autocomplete="off"
                />
        `}renderDecoratorBar(){return this.uploading&&"linear"===this.busyStyle?Pi`
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `:""}renderProgressBar(){return this.uploading&&"linear"===this.busyStyle?Pi`
            <kode4-progress progress="${this.progress}"></kode4-progress>
        `:""}renderProgressIcon(){return this.uploading&&"icon"===this.busyStyle?Pi`
            <kode4-progress
                type="icon"
                progress="${this.progress}"
                class="icon-busy"
                @mousedown="${this.onActionMousedown}"
                ></kode4-progress>
        `:""}renderInlineSuffix(e){return Pi`
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix")}">${e}</label>
        `}renderResetButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `}renderActionButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `}renderClearButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `}renderDialogContainer(){return this.hasDialog?Pi`
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `:""}renderDialog(){return Pi`
        `}static get styles(){return ni`
            ${super.styles}

            .field-input-container {
                overflow: hidden;
            }





            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                margin-left: -9px;
                margin-right: -9px;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }




            .placeholder-container {
                dispplay: block;
            }

            .placeholder-icon {
                dispplay: block;
                font-size: 2.0em;
            }
            
            .placeholder {
                dispplay: block;
            }

            .placeholder-icon + .placeholder {
                margin-top: 0.5em;
            }

        `}};Qt([io({type:String,attribute:"preview-image-mode",reflect:!0,converter:{fromAttribute:e=>"cover"!==(null==e?void 0:e.toLowerCase())?"contain":"cover"}})],Rs.prototype,"previewImageMode",void 0),Qt([io({type:String,attribute:"mime-types",reflect:!0,converter:{fromAttribute:e=>{if(e&&e.length)return e.split(",")},toAttribute:e=>e?e.join(","):null}})],Rs.prototype,"mimeTypes",void 0),Qt([io()],Rs.prototype,"uploading",void 0),Qt([io()],Rs.prototype,"progress",void 0),Qt([io({type:String})],Rs.prototype,"previewThumb",void 0),Qt([io()],Rs.prototype,"value",null),Qt([io()],Rs.prototype,"_fileUploader",void 0),Qt([io()],Rs.prototype,"fileUploader",null),Qt([io({type:Boolean})],Rs.prototype,"showFilename",void 0),Qt([io({type:Number})],Rs.prototype,"size",void 0),Qt([io({type:Boolean})],Rs.prototype,"outline",void 0),Qt([io({type:Boolean})],Rs.prototype,"filled",void 0),Qt([io({type:String})],Rs.prototype,"placeholder",void 0),Qt([io({type:String,attribute:"placeholder-icon",reflect:!0,converter:{fromAttribute:()=>Ms.cf}})],Rs.prototype,"placeholderIcon",void 0),Qt([io({type:Boolean,attribute:"placeholder-image",reflect:!0,converter:{fromAttribute:e=>{if(!e)return!1;switch(e.toLowerCase().trim()){case"true":case"yes":case"on":case"1":return!0;case"false":case"no":case"off":case"0":case"null":return!1;default:return Boolean(e)}},toAttribute:e=>String(e)}})],Rs.prototype,"placeholderImage",void 0),Qt([io({type:String})],Rs.prototype,"prefixOutside",void 0),Qt([io({type:String})],Rs.prototype,"prefix",void 0),Qt([io({type:String})],Rs.prototype,"suffix",void 0),Qt([io({type:String})],Rs.prototype,"suffixOutside",void 0),Qt([io()],Rs.prototype,"iconPrependOutside",void 0),Qt([io()],Rs.prototype,"iconPrepend",void 0),Qt([io()],Rs.prototype,"iconAppend",void 0),Qt([io()],Rs.prototype,"iconAppendOutside",void 0),Qt([io()],Rs.prototype,"iconReset",void 0),Qt([io()],Rs.prototype,"iconClear",void 0),Qt([io()],Rs.prototype,"iconAction",void 0),Qt([io({type:Boolean})],Rs.prototype,"resettable",void 0),Qt([io({type:Boolean})],Rs.prototype,"action",void 0),Qt([io({type:Boolean})],Rs.prototype,"clearable",void 0),Qt([io({type:Boolean})],Rs.prototype,"hasDialog",void 0),Qt([io({type:Boolean})],Rs.prototype,"dialogOpen",void 0),Qt([io({type:Boolean})],Rs.prototype,"isDialogOpen",null),Qt([io({type:Boolean})],Rs.prototype,"openDialogOnFocus",void 0),Qt([io({type:Boolean})],Rs.prototype,"isLabelFloating",null),Qt([io({type:Boolean})],Rs.prototype,"showPlaceholder",null),Qt([io()],Rs.prototype,"displayValue",null),Qt([io()],Rs.prototype,"cssClasses",null),Rs=Qt([eo("kode4-filefield")],Rs);let Ns=class extends Os{constructor(){super(),this.previewImageMode="contain",this.mimeTypes=["image/*"],this.placeholderAspectRatio=!1,this.uploading=!1,this.progress=0,this.imageDragoverOptions={acceptsFiles:!0},this.showFilename=!1,this.outline=!1,this.filled=!1,this.placeholder="",this.placeholderImage="dummy",this.prefixOutside="",this.prefix="",this.suffix="",this.suffixOutside="",this.iconReset=zs.R8,this.iconClear=Aa.NB,this.iconAction=Ds.eW,this.resettable=!1,this.action=!1,this.clearable=!1,this.hasDialog=!1,this.dialogOpen=!1,this.openDialogOnFocus=!1,this.dialogActionBlocked=void 0}get typeCssClass(){return"kode4-imagefield"}get value(){return this._value}set value(e){if(super.value=e,this.hasValue&&"string"==typeof this.value&&this.value.length)try{JSON.parse(String(this.value)),this.previewThumb=void 0}catch(e){this.previewThumb!==String(this.value)&&(this.previewThumb=String(this.value))}}get fileUploader(){return this._fileUploader}set fileUploader(e){this._fileUploader&&this._fileUploader.unsubscribe(this),this._fileUploader=e,this._fileUploader&&this._fileUploader.subscribe(this)}get isDialogOpen(){return this.dialogOpen}get isLabelFloating(){return this.floatingLabel&&this.label&&!this.hasValue&&!this.showPlaceholder&&!this.prefix&&!this.iconPrepend}get showPlaceholder(){return!this.hasValue&&(Boolean(this.placeholder)||Boolean(this.placeholderIcon))}firstUpdated(e){super.firstUpdated(e)}async doInit(e=!1){super.doInit(e)}reset(){super.reset()}clear(){super.clear()}handleEsc(){this.onClear()}openDialog(){this.dialogOpen=!0}closeDialog(){this.dialogOpen=!1}toggleDialog(){this.dialogOpen?this.closeDialog():this.openDialog()}onFocusInput(){super.onFocusInput(),this.openDialogOnFocus&&!this.forceBlur&&this.openDialog(),this.inputBlurEventBlocked=!0,this.forceBlur=!1}async uploadFiles(e){try{if(!this.fileUploader)throw new Error("FileUploader is not defined.");this.updateValueAfterUpload(await this.fileUploader.upload(e))}catch(e){this.handleOnUploadError(e)}}updateValueAfterUpload(e){const t=[];e.forEach((e=>{t.push({uploadedFilename:e.filename,originalFilename:e.file.name,originalFilesize:e.file.size})})),this.value=JSON.stringify(t),this.onChange();const i=e[0];var o=new FileReader;o.onload=e=>{this.previewThumb=String(e.target.result)},o.readAsDataURL(i.file)}handleOnBeginUpload(){this.error="",this.uploading=!0,this.progress=0}handleOnUploadProgress(e){this.progress=e}handleOnUploadComplete(){this.uploading=!1,this.progress=0}handleOnUploadError(e){this.error=e.message,this.uploading=!1,this.progress=0}onInputChange(e){this.uploadFiles(e.target.files||void 0)}onActionMousedown(e){e.preventDefault(),this.onMousedownPreventBlur(),this.dialogActionBlocked=!this.dialogOpen}onAction(){this.dialogActionBlocked=void 0}get cssClasses(){return{...super.cssClasses,"kode4-dialog-open":this.isDialogOpen,outline:this.outline,filled:this.filled,floatinglabel:this.isLabelFloating}}onDropOnAssets(e){var t;this.uploadFiles(null===(t=e.dataTransfer)||void 0===t?void 0:t.files)}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">

                ${this.iconPrependOutside?this.renderIcon(this.iconPrependOutside,"icon-prepend-outside","click-icon-prepend-outside"):""}

                ${this.prefixOutside?this.renderPrefixOutside(this.prefixOutside):""}

                <div class="field-container"
                
                    @dragover="${e=>ha(e,this.imageDragoverOptions)}"
                    @dragleave="${fa}"
                    @drop="${e=>ga(e,this.onDropOnAssets.bind(this))}"
                    >
                
                    <fieldset class="field-input-container">
                        ${this.label?this.renderLabel(this.label):""}

                        <label for="field" class="field-input-container-layout" @mousedown="${this.onMousedownPreventBlur}">
                            ${this.iconPrepend?this.renderIcon(this.iconPrepend,"icon-prepend","click-icon-prepend"):""}
                            ${this.prefix?this.renderInlinePrefix(this.prefix):""}
                            ${this.renderInput()}
                            ${this.renderProgressIcon()}
                            ${this.suffix?this.renderInlineSuffix(this.suffix):""}
                            ${this.action?this.renderActionButton():""}
                            ${this.resettable&&this.initialValue&&this.initialValue!==this.value?this.renderResetButton():""}
                            ${this.clearable&&""!==this.displayValue?this.renderClearButton():""}
                            ${this.iconAppend?this.renderIcon(this.iconAppend,"icon-append","click-icon-append"):""}
                        </label>

                        ${this.renderDecoratorBar()}

                    </fieldset>

                    ${this.renderDialogContainer()}

                    ${this.renderMessages()}

                </div>

                ${this.suffixOutside?this.renderSuffixOutside(this.suffixOutside):""}

                ${this.iconAppendOutside?this.renderIcon(this.iconAppendOutside,"icon-append-outside","click-icon-append-outside"):""}

            </div>
        `}renderIcon(e,t,i){return Pi`
            <label for="field" class="${t}" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,i||"click-icon")}"><kode4-fa-icon .icon="${e}"></kode4-fa-icon></label>
        `}renderInlinePrefix(e){return Pi`
            <label class="field-prefix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix")}">${e}</label>
        `}renderPrefixOutside(e){return Pi`
            <label class="prefix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-prefix-outside")}">${e}</label>
        `}renderSuffixOutside(e){return Pi`
            <label class="suffix-outside" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix-outside")}">${e}</label>
        `}fireIconEvent(e,t){let i=new CustomEvent(t,{bubbles:!0,composed:!0,detail:{aspectRatio:this.aspectRatio,element:this,originalEvent:e}});this.dispatchEvent(i)}renderPlaceholder(){if(!this.showPlaceholder&&this.hasValue)return"";return Pi`
            <div style="${Cs({display:"flex",flexDirection:"row",flexWrap:"nowrap",alignItems:"center",margin:"0",padding:"0"})}">
                ${this.placeholderAspectRatio&&this.aspectRatio?Pi`
                    <div style="${Cs({paddingTop:`calc(${this.aspectRatio.height} / ${this.aspectRatio.width} * 100%)`,width:"0"})}"></div>
                `:""}
                <div style="${Cs({flex:"1 0 1px",textAlign:"center"})}" class="imagePreviewAspectRatio">
                    <div class="placeholder-container">
                        ${this.placeholderIcon?Pi`<div class="placeholder-icon"><kode4-fa-icon .icon="${this.placeholderIcon}"></kode4-fa-icon></div>`:""}
                        ${this.placeholder?Pi`<div class="placeholder">${this.placeholder}</div>`:Pi`<div class="placeholder">&nbsp;</div>`}
                    </div>
                </div>
            </div>
        `}renderPreviewThumb(){if(!this.previewThumb)return"";const e={flex:"1 0 1px",textAlign:"center",background:"center center no-repeat transparent",backgroundSize:"cover"===this.previewImageMode?"cover":"contain",backgroundImage:this.previewThumb?`url("${this.previewThumb}")`:""};return Pi`
            <div style="${Cs({display:"flex",flexDirection:"row",flexWrap:"nowrap",alignItems:"stretch"})}">
                ${this.aspectRatio?Pi`
                    <div style="${Cs({paddingTop:`calc(${this.aspectRatio.height} / ${this.aspectRatio.width} * 100%)`,width:"0"})}"></div>
                    <div style="${Cs(e)}" class="imagePreviewAspectRatio"></div>
                `:Pi`
                    <img src="${this.previewThumb}" style="max-width: 100%; display: block;">
                `}
            </div>
        `}renderInput(){return Pi`
            <div
                class="field-input">
                ${this.renderPlaceholder()}
                ${this.renderPreviewThumb()}
                <div
                    style="overflow: hidden !important; width: 0 !important; height: 0 !important; padding: 0 !important; margin: 0 !important; border: none !important; display: block !important;">
                    <input 
                        id="field"
                        type="file" 
                        accept="${this.mimeTypes.join(",")}"
                        @change="${this.onInputChange}"
                        @focus="${this.onFocusInput}"
                        @blur="${this.onBlurInput}"
                        >
                </div>
            </div>
            <input
                type="hidden"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                type="text"

                .value="${this.displayValue}"
                autocomplete="off"
                />
        `}renderDecoratorBar(){return this.uploading&&"linear"===this.busyStyle?Pi`
            <div class="field-decorator-bar">
                ${this.renderProgressBar()}
            </div>
        `:""}renderProgressBar(){return this.uploading&&"linear"===this.busyStyle?Pi`
            <kode4-progress progress="${this.progress}"></kode4-progress>
        `:""}renderProgressIcon(){return this.uploading&&"icon"===this.busyStyle?Pi`
            <kode4-progress
                type="icon"
                progress="${this.progress}"
                class="icon-busy"
                @mousedown="${this.onActionMousedown}"
                ></kode4-progress>
        `:""}renderInlineSuffix(e){return Pi`
            <label class="field-suffix" for="field" @mousedown="${this.onMousedownPreventBlur}" @click="${e=>this.fireIconEvent(e,"click-suffix")}">${e}</label>
        `}renderResetButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconReset}"
                class="icon-reset"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onReset}"
                ></kode4-fa-icon>
        `}renderActionButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconAction}"
                class="icon-action"
                @mousedown="${this.onActionMousedown}"
                @click="${this.onAction}"
                ></kode4-fa-icon>
        `}renderClearButton(){return Pi`
            <kode4-fa-icon
                .icon="${this.iconClear}"
                class="icon-clear"
                @click="${this.onClear}"
                @mousedown="${this.onMousedownPreventBlurOrForceBlur}"></kode4-fa-icon>
        `}renderDialogContainer(){return this.hasDialog?Pi`
            <div class="kode4-field-dialog">
                ${this.renderDialog()}
            </div>
        `:""}renderDialog(){return Pi`
        `}static get styles(){return ni`
            ${super.styles}

            .field-input-container {
                overflow: hidden;
            }





            /* --- FIELD, PREFIXES, SUFFIXES, ICONS ---------------- */

            .field-prefix {
                color: var(--kode4-field-prefix-color);
                font-family: var(--kode4-field-prefix-font-family);
                font-size: var(--kode4-field-prefix-font-size);
                line-height: var(--kode4-field-prefix-line-height);
                font-weight: var(--kode4-field-prefix-font-weight);
                padding-top: var(--kode4-field-prefix-padding-top);
                padding-right: var(--kode4-field-prefix-padding-right);
                padding-bottom: var(--kode4-field-prefix-padding-bottom);
                padding-left: var(--kode4-field-prefix-padding-left);
                margin-top: var(--kode4-field-prefix-margin-top);
                margin-right: var(--kode4-field-prefix-margin-right);
                margin-bottom: var(--kode4-field-prefix-margin-bottom);
                margin-left: var(--kode4-field-prefix-margin-left);
                width: var(--kode4-field-prefix-width);
            }
            
            .field-suffix {
                color: var(--kode4-field-suffix-color);
                font-family: var(--kode4-field-suffix-font-family);
                font-size: var(--kode4-field-suffix-font-size);
                line-height: var(--kode4-field-suffix-line-height);
                font-weight: var(--kode4-field-suffix-font-weight);
                padding-top: var(--kode4-field-suffix-padding-top);
                padding-right: var(--kode4-field-suffix-padding-right);
                padding-bottom: var(--kode4-field-suffix-padding-bottom);
                padding-left: var(--kode4-field-suffix-padding-left);
                margin-top: var(--kode4-field-suffix-margin-top);
                margin-right: var(--kode4-field-suffix-margin-right);
                margin-bottom: var(--kode4-field-suffix-margin-bottom);
                margin-left: var(--kode4-field-suffix-margin-left);
                width: var(--kode4-field-suffix-width);
            }

            .field-input {
                /* width: 100%; */
                flex: 1 0 1px;
                color: var(--kode4-field-input-color);
                font-family: var(--kode4-field-input-font-family);
                font-size: var(--kode4-field-input-font-size);
                line-height: var(--kode4-field-input-line-height);
                font-weight: var(--kode4-field-input-font-weight);
                padding-top: var(--kode4-field-input-padding-top);
                padding-right: var(--kode4-field-input-padding-right);
                padding-bottom: var(--kode4-field-input-padding-bottom);
                padding-left: var(--kode4-field-input-padding-left);
                margin-top: var(--kode4-field-input-margin-top);
                margin-right: var(--kode4-field-input-margin-right);
                margin-bottom: var(--kode4-field-input-margin-bottom);
                margin-left: var(--kode4-field-input-margin-left);
                -moz-appearance: textfield;
            }

            .field-input::-webkit-outer-spin-button,
            .field-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .icon-prepend {
                color: var(--kode4-field-icon-prepend-color);
                font-family: var(--kode4-field-icon-prepend-font-family);
                font-size: var(--kode4-field-icon-prepend-font-size);
                line-height: var(--kode4-field-icon-prepend-line-height);
                font-weight: var(--kode4-field-icon-prepend-font-weight);
                padding-top: var(--kode4-field-icon-prepend-padding-top);
                padding-right: var(--kode4-field-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-padding-left);
                margin-top: var(--kode4-field-icon-prepend-margin-top);
                margin-right: var(--kode4-field-icon-prepend-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-margin-left);
            }

            .icon-append {
                color: var(--kode4-field-icon-append-color);
                font-family: var(--kode4-field-icon-append-font-family);
                font-size: var(--kode4-field-icon-append-font-size);
                line-height: var(--kode4-field-icon-append-line-height);
                font-weight: var(--kode4-field-icon-append-font-weight);
                padding-top: var(--kode4-field-icon-append-padding-top);
                padding-right: var(--kode4-field-icon-append-padding-right);
                padding-bottom: var(--kode4-field-icon-append-padding-bottom);
                padding-left: var(--kode4-field-icon-append-padding-left);
                margin-top: var(--kode4-field-icon-append-margin-top);
                margin-right: var(--kode4-field-icon-append-margin-right);
                margin-bottom: var(--kode4-field-icon-append-margin-bottom);
                margin-left: var(--kode4-field-icon-append-margin-left);
            }

            .icon-action {
                color: var(--kode4-field-icon-action-color);
                font-family: var(--kode4-field-icon-action-font-family);
                font-size: var(--kode4-field-icon-action-font-size);
                line-height: var(--kode4-field-icon-action-line-height);
                font-weight: var(--kode4-field-icon-action-font-weight);
                padding-top: var(--kode4-field-icon-action-padding-top);
                padding-right: var(--kode4-field-icon-action-padding-right);
                padding-bottom: var(--kode4-field-icon-action-padding-bottom);
                padding-left: var(--kode4-field-icon-action-padding-left);
                margin-top: var(--kode4-field-icon-action-margin-top);
                margin-right: var(--kode4-field-icon-action-margin-right);
                margin-bottom: var(--kode4-field-icon-action-margin-bottom);
                margin-left: var(--kode4-field-icon-action-margin-left);
            }

            .icon-reset {
                color: var(--kode4-field-icon-reset-color);
                font-family: var(--kode4-field-icon-reset-font-family);
                font-size: var(--kode4-field-icon-reset-font-size);
                line-height: var(--kode4-field-icon-reset-line-height);
                font-weight: var(--kode4-field-icon-reset-font-weight);
                padding-top: var(--kode4-field-icon-reset-padding-top);
                padding-right: var(--kode4-field-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-icon-reset-padding-left);
                margin-top: var(--kode4-field-icon-reset-margin-top);
                margin-right: var(--kode4-field-icon-reset-margin-right);
                margin-bottom: var(--kode4-field-icon-reset-margin-bottom);
                margin-left: var(--kode4-field-icon-reset-margin-left);
            }

            .icon-busy {
                color: var(--kode4-field-icon-busy-color);
                font-family: var(--kode4-field-icon-busy-font-family);
                font-size: var(--kode4-field-icon-busy-font-size);
                line-height: var(--kode4-field-icon-busy-line-height);
                font-weight: var(--kode4-field-icon-busy-font-weight);
                padding-top: var(--kode4-field-icon-busy-padding-top);
                padding-right: var(--kode4-field-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-icon-busy-padding-left);
                margin-top: var(--kode4-field-icon-busy-margin-top);
                margin-right: var(--kode4-field-icon-busy-margin-right);
                margin-bottom: var(--kode4-field-icon-busy-margin-bottom);
                margin-left: var(--kode4-field-icon-busy-margin-left);
            }

            .icon-clear {
                color: var(--kode4-field-icon-clear-color);
                font-family: var(--kode4-field-icon-clear-font-family);
                font-size: var(--kode4-field-icon-clear-font-size);
                line-height: var(--kode4-field-icon-clear-line-height);
                font-weight: var(--kode4-field-icon-clear-font-weight);
                padding-top: var(--kode4-field-icon-clear-padding-top);
                padding-right: var(--kode4-field-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-icon-clear-padding-left);
                margin-top: var(--kode4-field-icon-clear-margin-top);
                margin-right: var(--kode4-field-icon-clear-margin-right);
                margin-bottom: var(--kode4-field-icon-clear-margin-bottom);
                margin-left: var(--kode4-field-icon-clear-margin-left);
            }

            .prefix-outside {
                color: var(--kode4-field-prefix-outside-color);
                font-family: var(--kode4-field-prefix-outside-font-family);
                font-size: var(--kode4-field-prefix-outside-font-size);
                line-height: var(--kode4-field-prefix-outside-line-height);
                font-weight: var(--kode4-field-prefix-outside-font-weight);
                padding-top: var(--kode4-field-prefix-outside-padding-top);
                padding-right: var(--kode4-field-prefix-outside-padding-right);
                padding-bottom: var(--kode4-field-prefix-outside-padding-bottom);
                padding-left: var(--kode4-field-prefix-outside-padding-left);
                margin-top: var(--kode4-field-prefix-outside-margin-top);
                margin-right: var(--kode4-field-prefix-outside-margin-right);
                margin-bottom: var(--kode4-field-prefix-outside-margin-bottom);
                margin-left: var(--kode4-field-prefix-outside-margin-left);
                width: var(--kode4-field-prefix-outside-width);
            }

            .suffix-outside {
                color: var(--kode4-field-suffix-outside-color);
                font-family: var(--kode4-field-suffix-outside-font-family);
                font-size: var(--kode4-field-suffix-outside-font-size);
                line-height: var(--kode4-field-suffix-outside-line-height);
                font-weight: var(--kode4-field-suffix-outside-font-weight);
                padding-top: var(--kode4-field-suffix-outside-padding-top);
                padding-right: var(--kode4-field-suffix-outside-padding-right);
                padding-bottom: var(--kode4-field-suffix-outside-padding-bottom);
                padding-left: var(--kode4-field-suffix-outside-padding-left);
                margin-top: var(--kode4-field-suffix-outside-margin-top);
                margin-right: var(--kode4-field-suffix-outside-margin-right);
                margin-bottom: var(--kode4-field-suffix-outside-margin-bottom);
                margin-left: var(--kode4-field-suffix-outside-margin-left);
                width: var(--kode4-field-suffix-outside-width);
            }

            .icon-prepend-outside {
                color: var(--kode4-field-icon-prepend-outside-color);
                font-family: var(--kode4-field-icon-prepend-outside-font-family);
                font-size: var(--kode4-field-icon-prepend-outside-font-size);
                line-height: var(--kode4-field-icon-prepend-outside-line-height);
                font-weight: var(--kode4-field-icon-prepend-outside-font-weight);
                padding-top: var(--kode4-field-icon-prepend-outside-padding-top);
                padding-right: var(--kode4-field-icon-prepend-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-prepend-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-prepend-outside-padding-left);
                margin-top: var(--kode4-field-icon-prepend-outside-margin-top);
                margin-right: var(--kode4-field-icon-prepend-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-prepend-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-prepend-outside-margin-left);
            }

            .icon-append-outside {
                color: var(--kode4-field-icon-append-outside-color);
                font-family: var(--kode4-field-icon-append-outside-font-family);
                font-size: var(--kode4-field-icon-append-outside-font-size);
                line-height: var(--kode4-field-icon-append-outside-line-height);
                font-weight: var(--kode4-field-icon-append-outside-font-weight);
                padding-top: var(--kode4-field-icon-append-outside-padding-top);
                padding-right: var(--kode4-field-icon-append-outside-padding-right);
                padding-bottom: var(--kode4-field-icon-append-outside-padding-bottom);
                padding-left: var(--kode4-field-icon-append-outside-padding-left);
                margin-top: var(--kode4-field-icon-append-outside-margin-top);
                margin-right: var(--kode4-field-icon-append-outside-margin-right);
                margin-bottom: var(--kode4-field-icon-append-outside-margin-bottom);
                margin-left: var(--kode4-field-icon-append-outside-margin-left);
            }





            /* --- DECORATOR ---------------- */

            .field-decorator-bar {
                position: relative;
                height: var(--kode4-field-decorator-size);
                border: none;
                padding: 0;
                background-color: var(--kode4-field-decorator-background);
                oveflow: hidden;
            }

            .field-decorator-bar:after {
                content: " ";
                display: block;
                z-index: 10;
                width: 0;
                margin-left: auto;
                margin-right: auto;
                transition: all 0.15s linear;
                background-color: var(--kode4-field-decorator-color);
                height: var(--kode4-field-decorator-size);
            }

            .kode4-focus .field-decorator-bar:after {
                width: 100%;
            }

            .busy .field-decorator-bar:after {
                display: none;
            }

            /* --- FILLED ---------------- */
            .filled .field-input-container {
                padding-top: var(--kode4-field-filled-input-container-padding-top);
                padding-right: var(--kode4-field-filled-input-container-padding-right);
                padding-bottom: var(--kode4-field-filled-input-container-padding-bottom);
                padding-left: var(--kode4-field-filled-input-container-padding-left);
            }

            .filled .field-decorator-bar {
                margin-left: calc(var(--kode4-field-filled-input-container-padding-left) * -1);
                margin-right: calc(var(--kode4-field-filled-input-container-padding-right) * -1);
            }

            
            /* --- OUTLINE ---------------- */
            
            .outline .field-prefix {
                padding-top: var(--kode4-field-outline-prefix-padding-top);
                padding-right: var(--kode4-field-outline-prefix-padding-right);
                padding-bottom: var(--kode4-field-outline-prefix-padding-bottom);
                padding-left: var(--kode4-field-outline-prefix-padding-left);
            }
            
            .outline .field-suffix {
                padding-top: var(--kode4-field-outline-suffix-padding-top);
                padding-right: var(--kode4-field-outline-suffix-padding-right);
                padding-bottom: var(--kode4-field-outline-suffix-padding-bottom);
                padding-left: var(--kode4-field-outline-suffix-padding-left);
            }
            
            .outline .field-input {
                color: var(--kode4-field-outline-input-color);
                font-family: var(--kode4-field-outline-input-font-family);
                font-size: var(--kode4-field-outline-input-font-size);
                line-height: var(--kode4-field-outline-input-line-height);
                font-weight: var(--kode4-field-outline-input-font-weight);
                padding-top: var(--kode4-field-outline-input-padding-top);
                padding-right: var(--kode4-field-outline-input-padding-right);
                padding-bottom: var(--kode4-field-outline-input-padding-bottom);
                padding-left: var(--kode4-field-outline-input-padding-left);
                margin-top: var(--kode4-field-outline-input-margin-top);
                margin-right: var(--kode4-field-outline-input-margin-right);
                margin-bottom: var(--kode4-field-outline-input-margin-bottom);
                margin-left: var(--kode4-field-outline-input-margin-left);
            }

            .outline .icon-prepend {
                padding-top: var(--kode4-field-outline-icon-prepend-padding-top);
                padding-right: var(--kode4-field-outline-icon-prepend-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-prepend-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-prepend-padding-left);
            }
            
            .outline .icon-append {
                padding-top: var(--kode4-field-outline-icon-append-padding-top);
                padding-right: var(--kode4-field-outline-icon-append-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-append-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-append-padding-left);
            }
            
            .outline .icon-action {
                padding-top: var(--kode4-field-outline-icon-action-padding-top);
                padding-right: var(--kode4-field-outline-icon-action-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-action-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-action-padding-left);
            }
            
            .outline .icon-reset {
                padding-top: var(--kode4-field-outline-icon-reset-padding-top);
                padding-right: var(--kode4-field-outline-icon-reset-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-reset-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-reset-padding-left);
            }
            
            .outline .icon-busy {
                padding-top: var(--kode4-field-outline-icon-busy-padding-top);
                padding-right: var(--kode4-field-outline-icon-busy-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-busy-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-busy-padding-left);
            }
            
            .outline .icon-clear {
                padding-top: var(--kode4-field-outline-icon-clear-padding-top);
                padding-right: var(--kode4-field-outline-icon-clear-padding-right);
                padding-bottom: var(--kode4-field-outline-icon-clear-padding-bottom);
                padding-left: var(--kode4-field-outline-icon-clear-padding-left);
            }
            
            .outline .field-decorator-bar {
                margin-left: -9px;
                margin-right: -9px;
            }


            /* --- DIALOG ---------------- */

            .kode4-field-dialog {
                display: none;

                opacity: 0;
                visibility: hidden;
                transition: all 0.1s linear;
                z-index: 0;
                position: absolute;

                top: var(--kode4-field-dialog-position-top);
                right: var(--kode4-field-dialog-position-right);
                bottom: var(--kode4-field-dialog-position-bottom);
                left: var(--kode4-field-dialog-position-left);

                background-color: var(--kode4-field-dialog-background-color);

                padding-top: var(--kode4-field-dialog-padding-top);
                padding-right: var(--kode4-field-dialog-padding-right);
                padding-bottom: var(--kode4-field-dialog-padding-bottom);
                padding-left: var(--kode4-field-dialog-padding-left);

                margin-top: var(--kode4-field-dialog-margin-top);
                margin-right: var(--kode4-field-dialog-margin-right);
                margin-bottom: var(--kode4-field-dialog-margin-bottom);
                margin-left: var(--kode4-field-dialog-margin-left);

                border-width: var(--kode4-field-dialog-border-width);
                border-style: var(--kode4-field-dialog-border-style);
                border-color: var(--kode4-field-dialog-border-color);
                border-radius: var(--kode4-field-dialog-border-radius);

                width: var(--kode4-field-dialog-width);
                min-width: var(--kode4-field-dialog-min-width);
                max-width: var(--kode4-field-dialog-max-width);
            
            }

            .kode4-dialog-open .kode4-field-dialog {
                opacity: var(--kode4-field-dialog-opacity);
                z-index: 100;
                visibility: visible;
                display: block;
            }




            .placeholder-container {
                dispplay: block;
            }

            .placeholder-icon {
                dispplay: block;
                font-size: 2.0em;
            }
            
            .placeholder {
                dispplay: block;
            }

            .placeholder-icon + .placeholder {
                margin-top: 0.5em;
            }

        `}};Qt([io({type:String,attribute:"preview-image-mode",reflect:!0,converter:{fromAttribute:e=>"cover"!==(null==e?void 0:e.toLowerCase())?"contain":"cover"}})],Ns.prototype,"previewImageMode",void 0),Qt([io({type:String,attribute:"mime-types",reflect:!0,converter:{fromAttribute:e=>{if(e&&e.length)return e.split(",")},toAttribute:e=>e?e.join(","):null}})],Ns.prototype,"mimeTypes",void 0),Qt([io({type:String,attribute:"aspect-ratio",reflect:!0,converter:{fromAttribute:e=>{if(!e)return;if(!e.length)return;const t=e.split(":");return 2===t.length?{width:Number(t[0]),height:Number(t[1])}:void 0},toAttribute:e=>e?String(e.width+":"+e.height):null}})],Ns.prototype,"aspectRatio",void 0),Qt([io({type:Boolean,attribute:"placeholder-aspect-ratio"})],Ns.prototype,"placeholderAspectRatio",void 0),Qt([io()],Ns.prototype,"uploading",void 0),Qt([io()],Ns.prototype,"progress",void 0),Qt([io({type:String})],Ns.prototype,"previewThumb",void 0),Qt([io()],Ns.prototype,"value",null),Qt([io()],Ns.prototype,"_fileUploader",void 0),Qt([io()],Ns.prototype,"fileUploader",null),Qt([io({type:Boolean})],Ns.prototype,"showFilename",void 0),Qt([io({type:Number})],Ns.prototype,"size",void 0),Qt([io({type:Boolean})],Ns.prototype,"outline",void 0),Qt([io({type:Boolean})],Ns.prototype,"filled",void 0),Qt([io({type:String})],Ns.prototype,"placeholder",void 0),Qt([io({type:String,attribute:"placeholder-icon",reflect:!0,converter:{fromAttribute:()=>Ms.cf}})],Ns.prototype,"placeholderIcon",void 0),Qt([io({type:Boolean,attribute:"placeholder-image",reflect:!0,converter:{fromAttribute:e=>{if(!e)return!1;switch(e.toLowerCase().trim()){case"true":case"yes":case"on":case"1":return!0;case"false":case"no":case"off":case"0":case"null":return!1;default:return Boolean(e)}},toAttribute:e=>String(e)}})],Ns.prototype,"placeholderImage",void 0),Qt([io({type:String})],Ns.prototype,"prefixOutside",void 0),Qt([io({type:String})],Ns.prototype,"prefix",void 0),Qt([io({type:String})],Ns.prototype,"suffix",void 0),Qt([io({type:String})],Ns.prototype,"suffixOutside",void 0),Qt([io()],Ns.prototype,"iconPrependOutside",void 0),Qt([io()],Ns.prototype,"iconPrepend",void 0),Qt([io()],Ns.prototype,"iconAppend",void 0),Qt([io()],Ns.prototype,"iconAppendOutside",void 0),Qt([io()],Ns.prototype,"iconReset",void 0),Qt([io()],Ns.prototype,"iconClear",void 0),Qt([io()],Ns.prototype,"iconAction",void 0),Qt([io({type:Boolean})],Ns.prototype,"resettable",void 0),Qt([io({type:Boolean})],Ns.prototype,"action",void 0),Qt([io({type:Boolean})],Ns.prototype,"clearable",void 0),Qt([io({type:Boolean})],Ns.prototype,"hasDialog",void 0),Qt([io({type:Boolean})],Ns.prototype,"dialogOpen",void 0),Qt([io({type:Boolean})],Ns.prototype,"isDialogOpen",null),Qt([io({type:Boolean})],Ns.prototype,"openDialogOnFocus",void 0),Qt([io({type:Boolean})],Ns.prototype,"isLabelFloating",null),Qt([io({type:Boolean})],Ns.prototype,"showPlaceholder",null),Qt([io()],Ns.prototype,"cssClasses",null),Ns=Qt([eo("kode4-imagefield")],Ns);var Vs=i(801),Us=i(480);let js=class extends Os{constructor(){super(),this.trueValue=!0,this.falseValue=!1,this.type="checkbox",this.value=!1}get typeCssClass(){return"kode4-check"}firstUpdated(e){super.firstUpdated(e),this.input.autofocus=!0}get displayValue(){return this.value}get stringValue(){return String(this.value?this.trueValue:this.falseValue)}sanititzeValue(e){const t=typeof e;if("string"===t){const t=e.toLowerCase();return"false"!==t&&"off"!==t&&"null"!==t&&"undefined"!==t&&"0"!==t}return"number"===t?e>0:e}handleEsc(){this.onClear()}onInputChange(e){var t;this.value=null===(t=this.input)||void 0===t?void 0:t.checked,this.onChange(e)}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">
                ${this.renderInput()}
                <label class="icon-unchecked" for="field">
                    <kode4-fa-icon .icon="${Vs.pL}"></kode4-fa-icon>
                </label>
                <label class="icon-checked" for="field">
                    <kode4-fa-icon .icon="${Us.a3}"></kode4-fa-icon>
                </label>
                <div class="kode4-field-box">
                    ${this.label?this.renderLabel(this.label):""}



                    ${this.renderMessages()}

                </div>
            </div>
        `}renderInput(){return Pi`
            <input
                class="field-input"
                value="1"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="checkbox"
                .checked="${this.displayValue}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                />
        `}renderLabel(e){return Pi`
            <label class="field-label-block" for="field">${e}</label>
        `}static get styles(){return ni`
            ${super.styles}

            .field-input {
                display: inline-block;
                margin: 0;
                padding: 0;
                width: 0;
                min-width: 0;
                width: 0;
                min-height: 0;
                opacity: 0;
                flex: 0 1 0px;
            }

            .field-input:not(:checked) ~ .icon-checked {
                display: none;
            }

            .field-input:checked ~ .icon-unchecked {
                display: none;
            }

            .kode4-field {
                display: flex;
                flex-direction: row;
            }

            .kode4-field-box {
                flex: 1 0 1px;
                margin-left: 10px;
            }

            .kode4-focus .icon-checked,
            .kode4-focus .icon-unchecked,
            .kode4-focus .field-label {
                color: var(--kode4-field-decorator-color);
            }

            .icon-checked,
            .icon-unchecked {
                margin-top: var(--kode4-field-check-icon-spacing);
            }

            .field-label {
                transform: translate(0, 0) scale(1);
            }

            .field-label-block {
                display: block;
                line-height: var(--kode4-field-check-line-height, inherit);
            }
        `}};Qt([io()],js.prototype,"trueValue",void 0),Qt([io()],js.prototype,"falseValue",void 0),Qt([io()],js.prototype,"displayValue",null),Qt([io()],js.prototype,"stringValue",null),js=Qt([eo("kode4-check")],js);let qs=class extends Os{constructor(){super(),this.hasMenuItems=!1,this.target=void 0,this.type="button",this.primary=!1,this.secondary=!1,this.success=!1,this.danger=!1,this.warning=!1,this.info=!1,this.outline=!1,this.filled=!1,this.flat=!1,this.icon=!1,this.iconSquare=!1,this.manageValue=!1}handleSlotchange(){this.hasMenuItems=void 0!==this.menuItems&&this.menuItems.length>0}get typeCssClass(){return"kode4-button"}updateCssClass(e,t,i,o){e.has(t)&&(i?this.classList.add(o):this.classList.remove(o))}updated(e){super.updated(e),this.updateCssClass(e,"primary",this.primary,"kode4-primary"),this.updateCssClass(e,"secondary",this.secondary,"kode4-secondary"),this.updateCssClass(e,"success",this.success,"kode4-success"),this.updateCssClass(e,"danger",this.danger,"kode4-danger"),this.updateCssClass(e,"warning",this.warning,"kode4-warning"),this.updateCssClass(e,"info",this.info,"kode4-info"),this.updateCssClass(e,"outline",this.outline,"kode4-button-outline"),this.updateCssClass(e,"filled",this.filled,"kode4-button-filled"),this.updateCssClass(e,"flat",this.flat,"kode4-button-flat"),(e.has("icon")||e.has("iconSquare"))&&(this.icon||this.iconSquare?this.icon?this.classList.add("kode4-button-icon"):this.iconSquare&&(this.classList.add("kode4-button-icon"),this.classList.add("kode4-button-iconSquare")):(this.classList.remove("kode4-button-icon"),this.classList.remove("kode4-button-iconSquare")))}handleEsc(){this.onClear()}onClick(e){if(this.disabled)return e.stopPropagation(),void e.preventDefault();if(this.menu&&Za.has(this.menu)){e.preventDefault();Za.get(this.menu).show({alignToElement:this.parentElement,callerParams:this.callerParams})}else if(this.drawer&&Qa.has(this.drawer)){e.preventDefault();Qa.get(this.drawer).show()}else if(this.dialog&&la.has(this.dialog)){e.preventDefault();la.get(this.dialog).show()}else switch(this.onChange(),this.type){case"submit":this.onSubmit(e);break;case"reset":this.onFormControlEvent(e,"resetForm");break;case"clear":this.onFormControlEvent(e,"clearForm");break;case"updateFields":this.onFormControlEvent(e,"updateFields");break;case"updateValues":this.onFormControlEvent(e,"updateValues");break;case"respawn":this.onFormControlEvent(e,"respawnForm")}}onFormControlEvent(e,t){let i=new CustomEvent(t,{bubbles:!0,composed:!0,detail:{originalEvent:e}});e&&e.preventDefault(),this.dispatchEvent(i)}get cssClasses(){return{...super.cssClasses,"kode4-button":!0}}render(){return this.href?this.renderLink():this.renderButton()}renderButton(){return Pi`
            <button
                type="${this.type}"
                class="component ${$s(this.cssClasses)}"
                ?required="${this.required}"
                @click="${this.onClick}">
                <span class="kode4-button-content">
                    <slot></slot>
                </span>
                ${this.hasMenuItems?Pi`
                    <span class="kode4-button-icon-dropdown">
                        <kode4-fa-icon .icon="${Ds.eW}"></kode4-fa-icon>
                    </span>
                `:void 0}
            </button>
            <slot name="menu" @slotchange=${this.handleSlotchange}></slot>
        `}renderLink(){return Pi`
            <a
                href="${this.href}"
                target="${Es(this.target)}"
                class="component ${this.typeCssClass}"
                @click="${this.onClick}">
                <span class="kode4-button-content">
                    <slot></slot>
                </span>
            </a>
        `}static get styles(){return ni`
            ${super.styles}

            :host {
                background-color: transparent;
            }

            :host(:hover) {
                --main-color: var(--main-color-hover);
                --main-color-filled: var(--main-color-filled-hover);
                --background-color-filled: var(--background-color-filled-hover);
            }

            .kode4-button {
                color: var(--main-color);
                font-size: var(--kode4-button-font-size);
                font-family: var(--kode4-button-font-family);
                line-height: var(--kode4-button-line-height);
                font-weight: var(--kode4-button-font-weight);
                border-radius: var(--kode4-button-border-radius);
                //display: block;
                width: 100%;
                height: 100%;
                text-align: center;
                cursor: pointer;
                outline: none;
                padding: 0;
                margin: 0;
                border: none;
                text-decoration: none;
                /* color: inherit; */
                background-color: transparent;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }
          
          .component.disabled,
          .component.disabled > *,
          .component.disabled .field-input-display {
                filter: grayscale(50%) brightness(102%);
                opacity: 0.85;
          }
          
            :host(.kode4-field-xxs) .kode4-button {
                font-size: var(--kode4-button-font-size-xxs);
            }
            
            :host(.kode4-field-xs) .kode4-button {
                font-size: var(--kode4-button-font-size-xs);
            }
            
            :host(.kode4-field-s) .kode4-button {
                font-size: var(--kode4-button-font-size-s);
            }
            
            :host(.kode4-field-m) .kode4-button {
                font-size: var(--kode4-button-font-size-m);
            }
            
            :host(.kode4-field-l) .kode4-button {
                font-size: var(--kode4-button-font-size-l);
            }
            
            :host(.kode4-field-xl) .kode4-button {
                font-size: var(--kode4-button-font-size-xl);
            }
            
            :host(.kode4-field-xxl) .kode4-button {
                font-size: var(--kode4-button-font-size-xxl);
            }
            
            :host(.kode4-primary) {
                --main-color: var(--main-color-primary);
                --main-color-filled: var(--main-color-filled-primary);
                --background-color-filled: var(--background-color-filled-primary);
            }

            :host(.kode4-primary:not(.disabled)):hover {
                --main-color: var(--main-color-hover-primary);
                --main-color-filled: var(--main-color-filled-hover-primary);
                --background-color-filled: var(--background-color-filled-hover-primary);
            }

            :host(.kode4-secondary) {
                --main-color: var(--main-color-secondary);
                /* --background-color: transparent; */
                --main-color-filled: var(--main-color-filled-secondary);
                --background-color-filled: var(--background-color-filled-secondary);
            }

            :host(.kode4-secondary:not(.disabled)):hover {
                --main-color: var(--main-color-hover-secondary);
                /* --background-color: transparent; */
                --main-color-filled: var(--main-color-filled-hover-secondary);
                --background-color-filled: var(--background-color-filled-hover-secondary);
            }

            :host(.kode4-success) {
                --main-color: var(--main-color-success);
                --main-color-filled: var(--main-color-filled-success);
                --background-color-filled: var(--background-color-filled-success);
            }

            :host(.kode4-success:not(.disabled)):hover {
                --main-color: var(--main-color-hover-success);
                --main-color-filled: var(--main-color-filled-hover-success);
                --background-color-filled: var(--background-color-filled-hover-success);
            }

            :host(.kode4-danger) {
                --main-color: var(--main-color-danger);
                --main-color-filled: var(--main-color-filled-danger);
                --background-color-filled: var(--background-color-filled-danger);
            }

            :host(.kode4-danger:not(.disabled)):hover {
                --main-color: var(--main-color-hover-danger);
                --main-color-filled: var(--main-color-filled-hover-danger);
                --background-color-filled: var(--background-color-filled-hover-danger);
            }

            :host(.kode4-warning) {
                --main-color: var(--main-color-warning);
                --main-color-filled: var(--main-color-filled-warning);
                --background-color-filled: var(--background-color-filled-warning);
            }

            :host(.kode4-warning:not(.disabled)):hover {
                --main-color: var(--main-color-hover-warning);
                --main-color-filled: var(--main-color-filled-hover-warning);
                --background-color-filled: var(--background-color-filled-hover-warning);
            }

            :host(.kode4-info) {
                --main-color: var(--main-color-info);
                --main-color-filled: var(--main-color-filled-info);
                --background-color-filled: var(--background-color-filled-info);
            }

            :host(.kode4-info:not(.disabled)):hover {
                --main-color: var(--main-color-hover-info);
                --main-color-filled: var(--main-color-filled-hover-info);
                --background-color-filled: var(--background-color-filled-hover-info);
            }

            :host(.kode4-button-outline) .kode4-button {
                border: var(--kode4-button-outline-border-width) var(--kode4-button-outline-border-style) var(--main-color);
                background: transparent;
            }
            
            :host(.kode4-button-filled) .kode4-button {
                color: var(--main-color-filled);
                background: var(--background-color-filled);
            }

            :host(:not(.kode4-button-flat):not([disabled]):hover) .kode4-button {
                box-shadow: var(--main-color) 0px 0px 5px;
            }

            .kode4-button.disabled {
                cursor: default;
            }

            :host(:not(.kode4-button-icon)) .kode4-button-content {
                flex: 1 0 1px;
                padding-top: var(--kode4-button-padding-top);
                padding-bottom: var(--kode4-button-padding-bottom);
                padding-left: var(--kode4-button-padding-left);
                padding-right: var(--kode4-button-padding-right);
            }

            :host(.kode4-button-icon) .kode4-button {
                border-radius: 50%;
                line-height: 1.0em;
                width: 3.05em;
                height: 3em;
                padding: 0;
                margin: 0;
                text-align: center;
            }
            
            :host(.kode4-button-icon.kode4-button-iconSquare) .kode4-button {
                border-radius: 0;
            }
            
            :host(.kode4-button-icon) .kode4-button-content {
                display: inline-block;
                padding: 0;
                margin: 0;
                font-size: 1.5em;
            }

            :host(.kode4-block) .kode4-button {
                display: block;
                width: 100%;
            }

            :host([no-menu-icon]) .kode4-button-icon-dropdown {
                display: none;
            }
            
            .kode4-button-icon-dropdown {
                padding-left: .5em;
                padding-right: .5em;
                //border-left-style: dotted;
                //border-left-width: 1px;
            }
        `}};Qt([oo()],qs.prototype,"hasMenuItems",void 0),Qt([lo({slot:"menu"})],qs.prototype,"menuItems",void 0),Qt([io({type:String})],qs.prototype,"href",void 0),Qt([io({type:String})],qs.prototype,"target",void 0),Qt([io({type:String})],qs.prototype,"type",void 0),Qt([io({type:String})],qs.prototype,"color",void 0),Qt([io({type:Boolean})],qs.prototype,"primary",void 0),Qt([io({type:Boolean})],qs.prototype,"secondary",void 0),Qt([io({type:Boolean})],qs.prototype,"success",void 0),Qt([io({type:Boolean})],qs.prototype,"danger",void 0),Qt([io({type:Boolean})],qs.prototype,"warning",void 0),Qt([io({type:Boolean})],qs.prototype,"info",void 0),Qt([io({type:Boolean})],qs.prototype,"outline",void 0),Qt([io({type:Boolean})],qs.prototype,"filled",void 0),Qt([io({type:Boolean})],qs.prototype,"flat",void 0),Qt([io({type:Boolean})],qs.prototype,"icon",void 0),Qt([io({type:Boolean})],qs.prototype,"iconSquare",void 0),Qt([io({type:String,attribute:"menu"})],qs.prototype,"menu",void 0),Qt([io({type:String,attribute:"drawer"})],qs.prototype,"drawer",void 0),Qt([io({type:String,attribute:"dialog"})],qs.prototype,"dialog",void 0),Qt([io({type:String,attribute:"caller-params"})],qs.prototype,"callerParams",void 0),Qt([io()],qs.prototype,"cssClasses",null),qs=Qt([eo("kode4-button")],qs);var Hs,Ws=i(394),Ys=i(606);let Ks=Hs=class extends Ls{constructor(){super(),this._timezoneOffset=0,this.nodialog=!1,this.dialogonly=!1,this.noyear=!1,this.nomonth=!1,this.noday=!1,this.nohours=!1,this.nominutes=!1,this.noseconds=!1,this.noweek=!1,this.nozeropadding=!1,this.timestamp=!1,this.dateTimeValue=new Date,this.calendarDateTime=new Date,this.searchTerm=void 0,this.type="date",this.dateTimeValue=new Date,this.action=!0,this.hasDialog=!0,this.openDialogOnFocus=!0,this.changeOnType=!0,document.addEventListener("click",this.onOutsideClick.bind(this))}get typeCssClass(){return`kode4-${this.type}`}get timezoneOffset(){return`${this._timezoneOffset<=0?"+":"-"}${this.padZeros(Math.abs(this._timezoneOffset))}:00`}get value(){let e=Intl.DateTimeFormat([],{timeZone:"Europe/Berlin",timeZoneName:"short"}).format(new Date).split(" ")[1].slice(3);var t=60*parseInt(e.split(":")[0]);if(e.includes(":"))t=t+parseInt(e.split(":")[1]);if(this.getValueIsDefined(this._value)&&this.getValueIsDefined(this.dateTimeValue)){if("isodate"===this.output)return"date"===this.type?`${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth()+1)}-${this.padZeros(this.dateTimeValue.getDate())}T00:00:00${this.timezoneOffset}`:`${this.dateTimeValue.getFullYear()}-${this.padZeros(this.dateTimeValue.getMonth()+1)}-${this.padZeros(this.dateTimeValue.getDate())}T${this.padZeros(this.dateTimeValue.getHours())}:${this.padZeros(this.dateTimeValue.getMinutes())}:${this.padZeros(this.dateTimeValue.getSeconds())}${this.timezoneOffset}`;if("timestamp"===this.output)return String(Math.round(this.dateTimeValue.getTime()/1e3));throw new Error("Date is neither isodate nor timestamp")}}set value(e){if(this.initialized||this.initializing){try{this.dateTimeValue=this.convertParameterToValue(String(e),this.timestamp),this._value=this.dateTimeValue.getTime()}catch(e){this._value=void 0}this.initialized&&(this.dirty=this.getDirty())}else this._valueCandidate=e}get mind(){return this._min}set mind(e){if(e instanceof Date)this._min=e;else if("string"==typeof e)try{this._min=this.convertParameterToValue(e,Boolean(e.match(/^\d+$/)))}catch(e){this._min=void 0}else this._min=void 0}convertParameterToDateValue(e,t=!1){if(t){const t=new Date(1e3*Number(e));return new Date(`${t.getFullYear()}-${this.padZeros(t.getMonth()+1)}-${this.padZeros(t.getDate())}T00:00:00${this.timezoneOffset}`)}{const t=e.match(/^\d{4}-\d{2}-\d{2}/);if(!t)throw new Error("Type mismatch for date");return new Date(`${t[0]}T00:00:00${this.timezoneOffset}`)}}convertParameterToDateTimeValue(e,t=!1){if(t){const t=new Date(1e3*Number(e));return new Date(`${t.getFullYear()}-${this.padZeros(t.getMonth()+1)}-${this.padZeros(t.getDate())}T${this.padZeros(t.getHours())}:${this.padZeros(t.getMinutes())}:${this.padZeros(t.getSeconds())}${this.timezoneOffset}`)}{const t=e.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);if(!t)throw new Error("Type mismatch for date");return new Date(`${t[0]}:00${this.timezoneOffset}`)}}convertParameterToValue(e,t=!1){switch(this.type){case"date":return this.convertParameterToDateValue(e,t);case"datetime":return this.convertParameterToDateTimeValue(e,t);default:throw new Error("Undefined type")}}validate(){return!!super.validate()&&(this.error="",!(this.mind instanceof Date&&this.dateTimeValue instanceof Date&&this.mind.getTime()>this.dateTimeValue.getTime())||(this.error="Bitte beachten Sie Mindestdatum",!1))}get displayValue(){if(!this.getValueIsDefined(this._value)||!this.getValueIsDefined(this.dateTimeValue))return"";let e=String(this.format);return e=e.replace(/D/,this.day),e=e.replace(/M/,this.month),e=e.replace(/Y/,this.year),e=e.replace(/h/,this.hours),e=e.replace(/m/,this.minutes),e=e.replace(/s/,this.seconds),e}async firstUpdated(e){super.firstUpdated(e),this.nodialog&&this.dialogonly&&(this.nodialog=!1)}async doInit(){if(this.output||(this.output=this.timestamp?"timestamp":"isodate"),!this.format)switch(this.type){case"date":this.format="D.M.Y";break;case"datetime":this.format="D.M.Y h:m:s"}this.value=this._valueCandidate,this.getValueIsDefined(this._valueCandidate)?(this.initialValue=this._value,this.calendarDateTime=new Date(this.dateTimeValue.getTime()),this.calendarDateTime.setDate(1)):this.initialValue=void 0}reset(){this.getValueIsDefined(this.initialValue)?(this.dateTimeValue=new Date(Number(this.initialValue)),this._value=this.dateTimeValue.getTime()):(this.dateTimeValue=new Date,this._value=void 0),this.dirty=this.getDirty(),this.requestUpdate(),this.onChange()}addFocusClass(){}removeFocusClass(){}onKeydown(e){"Tab"!==e.key?"autocomplete"!==this.type&&e.preventDefault():this.dialogOpen&&this.closeDialog()}onKeyup(e){switch(e.key){case"Escape":return void(this.dialogOpen?this.closeDialog():this.handleEsc());case"ArrowDown":case"ArrowRight":return void(this.dialogOpen||this.openDialog());case"ArrowLeft":return void(this.dialogOpen&&this.closeDialog());case"Enter":return void(this.dialogOpen?this.closeDialog():this.onSubmit(e));case"Tab":return;default:"autocomplete"===this.type&&(this.dialogOpen||this.openDialog()),e.preventDefault()}}onInputChange(){}onAction(){this.readonly||this.disabled||void 0!==this.dialogActionBlocked&&(this.dialogOpen=this.dialogActionBlocked)}onOptionClick(e){e.preventDefault()}onOutsideClick(e){var t;(null===(t=e.target)||void 0===t?void 0:t.closest(`#${this.id}`))||this.closeDialog()}get year(){var e;return String(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getFullYear())}padZeros(e){return this.nozeropadding?String(e):e<10?`0${e}`:String(e)}get month(){var e;return this.padZeros((null===(e=this.dateTimeValue)||void 0===e?void 0:e.getMonth())+1)}get monthLabel(){var e;return Hs.labels.months[null===(e=this.dateTimeValue)||void 0===e?void 0:e.getMonth()]}get day(){var e;return this.padZeros(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getDate())}get weekDay(){var e,t;return 0===(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getDay())?"7":String(null===(t=this.dateTimeValue)||void 0===t?void 0:t.getDay())}get weekDayLabel(){var e;return Hs.labels.days[null===(e=this.dateTimeValue)||void 0===e?void 0:e.getDay()]}get hours(){var e;return this.padZeros(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getHours())}get minutes(){var e;return this.padZeros(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getMinutes())}get seconds(){var e;return this.padZeros(null===(e=this.dateTimeValue)||void 0===e?void 0:e.getSeconds())}renderInput(){return Pi`
            <input
                class="field-input"
                name="${this.name}"
                ?readonly="${this.readonly}"
                ?disabled="${this.disabled}"
                ?required="${this.required}"
                id="field"
                type="text"
                .value="${this.displayValue}"
                placeholder="${this.placeholder}"
                @change="${this.onInputChange}"
                @focus="${this.onFocusInput}"
                @blur="${this.onBlurInput}"
                @keyup="${this.onKeyup}"
                @keydown="${this.onKeydown}"
                @mousedown="${e=>e.stopPropagation()}"
                />
            <label
                for="field"
                class="field-input field-input-display ${this.displayValue?"":"field-input-display-placeholder"}"
                >${this.displayValue||this.placeholder||Pi`&nbsp;`}</label>
        `}renderDialog(){return this.initialized?Pi`
            ${this.dialogErrorMessage?Pi`
                <div class="list-error">
                    ${this.dialogErrorMessage}
                </div>
            `:""}
            ${this.dialogMessagePrepend?Pi`
                <div class="list-message-prepend">
                    ${this.dialogMessagePrepend}
                </div>
            `:""}
            <div class="calendar">
                ${this.renderMonthPagination()}
                ${this.renderCalendarMonthContainer()}
            </div>
            <div class="time">
                ${"datetime"===this.type?this.renderTimeEditor():""}
            </div>
            ${this.dialogMessageAppend?Pi`
                <div class="list-message-append">
                    ${this.dialogMessageAppend}
                </div>
            `:""}
        `:Pi``}renderCalendarYearContainer(){return Pi`
            <div class="calendar-year-container">
                <input
                    id="calendarYear"
                    type="number"
                    step="1"
                    value="${this.calendarDateTime.getFullYear()}"
                    class="calendar-year-input"
                    @input="${e=>{var t;return this.selectYear(Number(null===(t=e.target)||void 0===t?void 0:t.value))}}"
                    tabindex="-1" 
                    />
            </div>
        `}renderCalendarMonthContainer(){return Pi`
            <div class="calendar-month-container">
                ${this.renderCalendarWeekContainer()}
            </div>
        `}renderMonthPagination(){return Pi`
            <div class="calendar-month-pagination">
                ${this.renderCalendarMonthPaginationPrev()}
                <div class="spacer"></div>
                <div class="calendar-month-container-monthlabel">
                    ${Hs.labels.months[this.calendarDateTime.getMonth()]}
                </div>
                ${this.renderCalendarYearContainer()}
                <div class="spacer"></div>
                ${this.renderCalendarMonthPaginationNext()}
            </div>
        `}renderCalendarMonthPaginationPrev(){return Pi`
            <div class="calendar-month-container-monthPaginationButton" @click="${()=>this.selectMonth(this.calendarDateTime.getMonth()-1)}">
                <kode4-fa-icon
                    .icon="${Ys.A3}"
                ></kode4-fa-icon>
            </div>
        `}renderCalendarMonthPaginationNext(){return Pi`
            <div class="calendar-month-container-monthPaginationButton" @click="${()=>this.selectMonth(this.calendarDateTime.getMonth()+1)}">
                <kode4-fa-icon
                    .icon="${Ws._t}"
                ></kode4-fa-icon>
            </div>
        `}renderCalendarWeekContainer(){return Pi`
            <div class="calendar-week-container">
                ${this.renderCalendarWeekContent()}
            </div>
        `}renderCalendarWeekContent(){const e=[];let t=[];const i=new Date(this.calendarDateTime.getTime());i.setDate(1);const o=i.getMonth(),n=0===i.getDay()?6:i.getDay()-1;for(let e=0;e<n;e+=1)t.push(0);let r=0;do{r+=1;let n=0;do{n+=1,t.push(i.getDate()),i.setDate(i.getDate()+1)}while(n<50&&1!==i.getDay()&&i.getMonth()===o);for(let e=t.length;e<7;e+=1)t.push(0);e.push(t),t=[]}while(r<50&&i.getMonth()===o);return Pi`
            <table class="calendarTable">
                <thead>
                    <tr>
                        <th>${Hs.labels.daysShort[1]}</th>
                        <th>${Hs.labels.daysShort[2]}</th>
                        <th>${Hs.labels.daysShort[3]}</th>
                        <th>${Hs.labels.daysShort[4]}</th>
                        <th>${Hs.labels.daysShort[5]}</th>
                        <th>${Hs.labels.daysShort[6]}</th>
                        <th>${Hs.labels.daysShort[0]}</th>
                    </tr>
                </thead>
                <tbody>
                    ${e.map((e=>this.renderCalendarWeekRow(e)))}
                </tbody>
            </table>
        `}renderCalendarWeekRow(e){return Pi`
            <tr>
                ${e.map((e=>this.renderCalendarDayCell(e)))}
            </tr>
        `}renderCalendarDayCell(e){return 0===e?Pi`
                <td>
                </td>
            `:Pi`
            <td @click="${()=>this.selectDay(e)}">
                ${e}
            </td>
        `}selectYear(e){var t;const i=new Date(this.calendarDateTime.getTime());this.calendarDateTime.setFullYear(e),i.setFullYear(e),this.yearSelector&&(null===(t=this.yearSelector)||void 0===t?void 0:t.value)!==String(this.calendarDateTime.getFullYear())&&(this.yearSelector.value=String(this.calendarDateTime.getFullYear())),this.requestUpdate()}selectMonth(e){var t;const i=new Date(this.calendarDateTime.getTime());this.calendarDateTime.setMonth(e),i.setMonth(e),this.yearSelector&&(null===(t=this.yearSelector)||void 0===t?void 0:t.value)!==String(this.calendarDateTime.getFullYear())&&(this.yearSelector.value=String(this.calendarDateTime.getFullYear())),this.requestUpdate()}selectDay(e){const t=new Date(this.calendarDateTime.getTime());t.setDate(e),this.dateTimeValue=t,this._value=this.dateTimeValue.getTime(),this.initialized&&(this.dirty=this.getDirty()),this.onChange(),this.closeDialog()}renderTimeEditor(){return Pi`
            <div class="time-editor">
                Uhrzeit:
                <div class="time-editor-body">
                    <input
                        id="timeHours"
                        type="number"
                        min="0"
                        max="24"
                        step="1"
                        value="${this.calendarDateTime.getHours()}"
                        class="time-hours-input"
                        @input="${e=>{var t;return this.selectHours(Number(null===(t=e.target)||void 0===t?void 0:t.value))}}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeMinutes"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getMinutes()}"
                        class="time-minutes-input"
                        @input="${e=>{var t;return this.selectMinutes(Number(null===(t=e.target)||void 0===t?void 0:t.value))}}"
                        tabindex="-1" 
                        />
                    :
                    <input
                        id="timeSeconds"
                        type="number"
                        min="0"
                        max="59"
                        step="1"
                        value="${this.calendarDateTime.getSeconds()}"
                        class="time-seconds-input"
                        @input="${e=>{var t;return this.selectSeconds(Number(null===(t=e.target)||void 0===t?void 0:t.value))}}"
                        tabindex="-1" 
                        />
                    </div>
            </div>
        `}selectHours(e){this.calendarDateTime.setHours(e),this.dateTimeValue.setHours(e),this.requestUpdate(),this.onChange()}selectMinutes(e){this.calendarDateTime.setMinutes(e),this.dateTimeValue.setMinutes(e),this.requestUpdate(),this.onChange()}selectSeconds(e){this.calendarDateTime.setSeconds(e),this.dateTimeValue.setSeconds(e),this.requestUpdate(),this.onChange()}static get styles(){return ni`
            ${super.styles}

            :host(.kode4-date) #field.field-input,
            :host(.kode4-datetime) #field.field-input {
                display: inline;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                flex: 0 1 0px;
                position: absolute;
            }

            .calendar {
                padding-top: var(--kode4-datetime-dialog-calendar-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            .calendar-month-pagination {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: var(--kode4-datetime-dialog-calendar-month-pagination-margin-top);
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-margin-right);
                margin-bottom: var(--kode4-datetime-dialog-calendar-month-pagination-margin-bottom);
                margin-left: var(--kode4-datetime-dialog-calendar-month-pagination-margin-left);
            }

            .calendar-month-pagination .spacer {
                flex: 1 0 1px;
            }

            .calendar-month-container-monthPaginationButton {
                width: var(--kode4-datetime-dialog-calendar-month-pagination-button-width);
                text-align: center;
                cursor: pointer;
            }

            .calendar-month-container-monthlabel {
                margin-right: var(--kode4-datetime-dialog-calendar-month-pagination-label-spacer);
                vertical-align: middle;
            }

            .calendarTable {
                width: 100%;
                table-layout: fixed;
                border: none;
                border-collapse: collapse;
                border-spacing: 0;
            }

            .calendarTable th,
            .calendarTable td {
                text-align: center;
                padding-top: var(--kode4-datetime-dialog-calendar-tablecell-padding-top);
                padding-right: var(--kode4-datetime-dialog-calendar-tablecell-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-calendar-tablecell-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-calendar-tablecell-padding-left);
                cursor: pointer;
            }

            .calendarTable th {
                font-weight: bold;
            }

            .field-input-display {
                cursor: default;
                min-height: 1.0em;
            }

            .field-input-display-placeholder {
                color: var(--kode4-field-placeholder-color);
            }

            .calendar-year-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-calendar-year-input-width);
                color: var(--kode4-datetime-dialog-calendar-year-input-color);
                font-family: var(--kode4-datetime-dialog-calendar-year-input-font-family);
                font-size: var(--kode4-datetime-dialog-calendar-year-input-font-size);
                line-height: var(--kode4-datetime-dialog-calendar-year-input-line-height);
                font-weight: var(--kode4-datetime-dialog-calendar-year-input-font-weight);
                -moz-appearance: textfield;
            }

            .calendar-year-input::-webkit-outer-spin-button,
            .calendar-year-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }


            .time-editor {
                padding-top: var(--kode4-datetime-dialog-time-padding-top);
                padding-right: var(--kode4-datetime-dialog-time-padding-right);
                padding-bottom: var(--kode4-datetime-dialog-time-padding-bottom);
                padding-left: var(--kode4-datetime-dialog-time-padding-left);
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Old versions of Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome, Opera and Firefox */            
            }

            
            .time-hours-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-hours-input-width);
                color: var(--kode4-datetime-dialog-time-hours-input-color);
                font-family: var(--kode4-datetime-dialog-time-hours-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-hours-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-hours-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-hours-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-hours-input-text-align);
                -moz-appearance: textfield;
            }

            .time-hours-input::-webkit-outer-spin-button,
            .time-hours-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            .time-minutes-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-minutes-input::-webkit-outer-spin-button,
            .time-minutes-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .time-seconds-input {
                color: inherit;
                background: transparent;
                border: none;
                font-size: inherit;
                line-height: inherit;
                vertical-align: middle;
                width: var(--kode4-datetime-dialog-time-minutes-input-width);
                color: var(--kode4-datetime-dialog-time-minutes-input-color);
                font-family: var(--kode4-datetime-dialog-time-minutes-input-font-family);
                font-size: var(--kode4-datetime-dialog-time-minutes-input-font-size);
                line-height: var(--kode4-datetime-dialog-time-minutes-input-line-height);
                font-weight: var(--kode4-datetime-dialog-time-minutes-input-font-weight);
                text-align: var(--kode4-datetime-dialog-time-minutes-input-text-align);
                -moz-appearance: textfield;
            }

            .time-seconds-input::-webkit-outer-spin-button,
            .time-seconds-input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

        `}};Ks.labels={months:["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],days:["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],daysShort:["So","Mo","Di","Mi","Do","Fr","Sa"]},Qt([io()],Ks.prototype,"_timezoneOffset",void 0),Qt([io({type:String})],Ks.prototype,"timezoneOffset",null),Qt([ro("#calendarYear")],Ks.prototype,"yearSelector",void 0),Qt([io({type:String})],Ks.prototype,"format",void 0),Qt([io({type:Boolean})],Ks.prototype,"nodialog",void 0),Qt([io({type:Boolean})],Ks.prototype,"dialogonly",void 0),Qt([io({type:Boolean})],Ks.prototype,"noyear",void 0),Qt([io({type:Boolean})],Ks.prototype,"nomonth",void 0),Qt([io({type:Boolean})],Ks.prototype,"noday",void 0),Qt([io({type:Boolean})],Ks.prototype,"nohours",void 0),Qt([io({type:Boolean})],Ks.prototype,"nominutes",void 0),Qt([io({type:Boolean})],Ks.prototype,"noseconds",void 0),Qt([io({type:Boolean})],Ks.prototype,"noweek",void 0),Qt([io({type:Boolean})],Ks.prototype,"nozeropadding",void 0),Qt([io({type:Boolean})],Ks.prototype,"timestamp",void 0),Qt([io({type:String})],Ks.prototype,"output",void 0),Qt([io()],Ks.prototype,"dateTimeValue",void 0),Qt([io()],Ks.prototype,"calendarDateTime",void 0),Qt([io()],Ks.prototype,"value",null),Qt([io()],Ks.prototype,"_min",void 0),Qt([io()],Ks.prototype,"mind",null),Qt([io()],Ks.prototype,"displayValue",null),Qt([io()],Ks.prototype,"searchTerm",void 0),Qt([io()],Ks.prototype,"dialogMessagePrepend",void 0),Qt([io()],Ks.prototype,"dialogMessageAppend",void 0),Qt([io()],Ks.prototype,"dialogErrorMessage",void 0),Qt([io()],Ks.prototype,"year",null),Qt([io()],Ks.prototype,"month",null),Qt([io()],Ks.prototype,"monthLabel",null),Qt([io()],Ks.prototype,"day",null),Qt([io()],Ks.prototype,"weekDay",null),Qt([io()],Ks.prototype,"weekDayLabel",null),Qt([io()],Ks.prototype,"hours",null),Qt([io()],Ks.prototype,"minutes",null),Qt([io()],Ks.prototype,"seconds",null),Ks=Hs=Qt([eo("kode4-datetime-picker")],Ks);let Xs=class extends Ji{constructor(){super(),this.selected=!1,this.setAttribute("id",this.id)}connectedCallback(){this.id||(this.id=`kode4field_${za()}`),super.connectedCallback()}firstUpdated(e){super.firstUpdated(e),this.kode4CustomSelect=this.closest("kode4-custom-select"),this.addEventListener("click",this.onSelect.bind(this)),this.classList.remove("kode4-custom-option-selected"),this.fireRegisterEvent()}fireRegisterEvent(){let e=new CustomEvent("kode4-custom-select-option-created",{bubbles:!0,composed:!0,detail:{field:this}});this.dispatchEvent(e)}disconnectedCallback(){var e;let t=new CustomEvent("kode4-custom-select-option-destroyed",{bubbles:!1,composed:!0,detail:{fieldid:this.id}});null===(e=this.kode4CustomSelect)||void 0===e||e.dispatchEvent(t),super.disconnectedCallback()}onSelect(){this.selected=!0,this.classList.add("kode4-custom-option-selected"),this.dispatchEvent(new CustomEvent("customOptionSelected",{bubbles:!0,composed:!0,detail:{value:this.value}}))}onOptionSelected(e){const t=this.selected;this.selected=this.value==e,t!==this.selected&&(this.selected?this.classList.add("kode4-custom-option-selected"):this.classList.remove("kode4-custom-option-selected"))}render(){return Pi`
            <slot></slot>
        `}static get styles(){return ni`
            :host {
                flex: 1 0 1px;
                overflow: hidden;
                cursor: pointer;
                transition: all 0.3s ease-in-out;
                box-sizing: border-box;
            }
            
            :host(.kode4-custom-option-grayscale:not(.kode4-custom-option-selected)) {
                filter: grayscale(100%);
            }

            :host(.kode4-custom-option-grayscale:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-grayscale.kode4-custom-option-selected) {
                filter: grayscale(0);
            }

            :host(.kode4-custom-option-opacity:not(.kode4-custom-option-selected)) {                
                opacity: var(--kode4-customselect-option-deselected-opacity);
            }
            
            :host(.kode4-custom-option-opacity:hover:not(.kode4-custom-option-selected)),
            :host(.kode4-custom-option-opacity.kode4-custom-option-selected) {
                opacity: var(--kode4-customselect-option-selected-opacity);
            }
        `}};Qt([io({type:String})],Xs.prototype,"value",void 0),Qt([io({type:Boolean})],Xs.prototype,"selected",void 0),Xs=Qt([eo("kode4-custom-select-option")],Xs);const Zs=Xs;let Gs=class extends Os{constructor(){super(...arguments),this.formElements=[],this.column=!1,this.formElementsToAdd=[],this.formElementsToRemove=[],this.debounce=150,this.onFieldsUpdatedDebounced=vs(this.onFieldsUpdated,this.debounce)}get value(){return super.value}set value(e){super.value=e,this.updateCustomOptionsValue()}get typeCssClass(){return"kode4-custom-select"}connectedCallback(){this.addEventListener("kode4-custom-select-option-destroyed",(e=>{this.formElementDestroyed(e)})),super.connectedCallback()}formElementCreated(e){var t,i;(null===(t=e.detail)||void 0===t?void 0:t.field)&&(this.formElementsToAdd.push(null===(i=e.detail)||void 0===i?void 0:i.field),this.formElementsToRemove.filter((t=>{var i;return t!==(null===(i=e.detail)||void 0===i?void 0:i.field.id)})),this.onFieldsUpdatedDebounced())}formElementDestroyed(e){var t,i;(null===(t=e.detail)||void 0===t?void 0:t.fieldid)&&(this.formElementsToRemove.push(null===(i=e.detail)||void 0===i?void 0:i.fieldid),this.formElementsToAdd.filter((t=>{var i;return t.id!==(null===(i=e.detail)||void 0===i?void 0:i.fieldid)})),this.onFieldsUpdatedDebounced())}onFieldsUpdated(){const e=[...this.formElements.filter((e=>!this.formElementsToRemove.find((t=>t===e.id))))];this.formElementsToAdd.forEach((t=>{e.find((e=>e.id===t.id))||e.push(t)})),this.formElementsToAdd=[],this.formElementsToRemove=[],this.formElements=[...e],this.updateCustomOptionsValue()}initFormElements(){var e;const t=null===(e=this.shadowRoot)||void 0===e?void 0:e.querySelector("slot#options");this.formElements=[],Object.values(null==t?void 0:t.assignedElements()).forEach((e=>{e instanceof Zs?this.formElements.push(e):this.formElements=[...this.formElements,...e.querySelectorAll("kode4-custom-select-option")]}))}async doInit(e=!1){await super.doInit(e),this.initFormElements(),this.updateCustomOptionsValue()}onCustomOptionSelected(e){this.value=e.detail.value,this.onChange()}updateCustomOptionsValue(){this.formElements.map((e=>{e.onOptionSelected(this.value)}))}onChange(e){this.updateCustomOptionsValue(),super.onChange(e)}render(){return Pi`
            <div 
                class="component ${$s(this.cssClasses)}"
                @kode4-custom-select-option-created="${this.formElementCreated}"
                >
                <slot name="label">
                    <div>
                        ${this.label}
                    </div>
                </slot>
                ${this.renderInput()}
                <div class="optionsContainer ${$s(this.cssClassesOptionsContainer)}" @customOptionSelected="${this.onCustomOptionSelected}">
                    <slot id="options"></slot>
                </div>
                <div class="kode4-field-messages">
                    <div class="kode4-field-hint">
                        <div class="kode4-field-hint-left">
                            ${this.hint?this.renderHint():""}
                        </div>
                    </div>
                    <div class="kode4-field-error">
                        ${this.error?this.renderError():""}
                    </div>
                </div>
            </div>
        `}renderInput(){return Pi`
            <input
                class="field-input"
                name="${this.name}"
                ?required="${this.required}"
                id="field"
                type="hidden"
                value="${this.value}"
                />
        `}get cssClassesOptionsContainer(){return{"layout-row":!this.column,"layout-column":this.column,"kode4-focus":this.isFocused,"kode4-field-hasValue":this.hasValue,"kode4-field-empty":!this.hasValue}}static get styles(){return ni`
            ${super.styles}

            :host {
                display: block;
            }

            .component {
                display: block;
            }

            .layout-row {
                display: flex;
                flex-direction: row;
                justify-items: stretch;
            }
            
            .layout-column {
                display: flex;
                flex-direction: column;
                justify-items: stretch;
            }
        `}};Qt([io({type:Boolean})],Gs.prototype,"column",void 0),Qt([io()],Gs.prototype,"value",null),Qt([io()],Gs.prototype,"debounce",void 0),Qt([io()],Gs.prototype,"cssClassesOptionsContainer",null),Gs=Qt([eo("kode4-custom-select")],Gs);let Js=class extends Os{get typeCssClass(){return"kode4-formblock"}constructor(){super(),this.canValidate=!1}get value(){}set value(e){}render(){return Pi`
            <div class="component ${$s(this.cssClasses)}">
                <slot style="border: solid 2px #ccc;"></slot>
            </div>
        `}};Qt([io()],Js.prototype,"value",null),Js=Qt([eo("kode4-formblock")],Js);const Qs=(e,t)=>i=>{const o=i.getField(e);return Boolean((null==o?void 0:o.value)==t)},ed=(e,t)=>i=>{const o=i.getField(e);return Boolean((null==o?void 0:o.value)!=t)},td=(e=["hidden","disabled"])=>(t,i)=>{e.includes("hidden")&&(i.hidden=!1),e.includes("disabled")&&(i.disabled=!1)},id=(e=["hidden","disabled"])=>(t,i)=>{e.includes("hidden")&&(i.hidden=!0),e.includes("disabled")&&(i.disabled=!0)},od=(e,t)=>({conditions:Qs(e,t),handleThen:td(),handleElse:id()});let nd=class extends Ji{constructor(){super(),this.mode=window.localStorage.getItem("KODE4_FORMS_DEMO_MODE")||"buttons"}setMode(e){window.localStorage.setItem("KODE4_FORMS_DEMO_MODE",e),this.mode=e}createRenderRoot(){return this}render(){return Pi`
            <div class="kode4demo-section">
                <h2>
                    KODE4 Form Demo
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    <small>${this.mode}</small>
                </h2>
                ${["valuetests","fileupload","required","dynamicForms","conditions","customselects","datetimepickers","buttons","formlogic","textfields","dirtychecking","check","select","textarea","slider"].map((e=>Pi`<span @click="${()=>{this.setMode(e)}}" class="kode4demo-menuitem">${e}</span>`))}
            </div>

            ${"formlogic"===this.mode?this.renderFormlogic():""}
            ${"textfields"===this.mode?this.renderTextfields():""}
            ${"dirtychecking"===this.mode?this.renderDirtychecking():""}
            ${"buttons"===this.mode?this.renderButtons():""}
            ${"datetimepickers"===this.mode?this.renderDatetimepickers():""}
            ${"customselects"===this.mode?this.renderCustomselects():""}
            ${"conditions"===this.mode?this.renderConditions():""}
            ${"dynamicForms"===this.mode?this.renderDynamicForms():""}
            ${"select"===this.mode?this.renderSelect():""}
            ${"check"===this.mode?this.renderCheck():""}
            ${"required"===this.mode?this.renderRequired():""}
            ${"fileupload"===this.mode?this.renderFileUpload():""}
            ${"valuetests"===this.mode?this.renderValueTests():""}
        `}renderFormlogic(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-formlogic></kode4form-demo-formlogic>
            </div>
        `}renderTextfields(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-textfields></kode4form-demo-textfields>
            </div>
        `}renderDirtychecking(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-dirtychecking></kode4form-demo-dirtychecking>
            </div>
        `}renderButtons(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-buttons></kode4form-demo-buttons>
            </div>
        `}renderDatetimepickers(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-datetimepickers></kode4form-demo-datetimepickers>
            </div>
        `}renderCustomselects(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-cutomselects></kode4form-demo-cutomselects>
            </div>
        `}renderConditions(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-conditions></kode4form-demo-conditions>
            </div>
        `}renderDynamicForms(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-dynamic-forms></kode4form-demo-dynamic-forms>
            </div>
        `}renderSelect(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-select></kode4form-demo-select>
            </div>
        `}renderCheck(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-check></kode4form-demo-check>
            </div>
        `}renderRequired(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-required></kode4form-demo-required>
            </div>
        `}renderFileUpload(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-fileupload></kode4form-demo-fileupload>
            </div>
        `}renderValueTests(){return Pi`
            <div class="kode4demo-section">
                <kode4form-demo-valuetests></kode4form-demo-valuetests>
            </div>
        `}};Qt([io({type:String})],nd.prototype,"mode",void 0),nd=Qt([eo("kode4form-demo-page")],nd);var rd=i(609),ad=i(176),sd=i(879),dd=i(337);const ld=new Is;ld.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",ld.noSelection="Bitte wählen...";let cd=class extends Ji{constructor(){super(),this.formDirty=!1}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            
                            block

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            name="floatinglabel"
                            label="Floating Label (Number)"
                            type="number"

                            min="5"
                            max="15"
                            step="1"
                            value="10"
                            
                            block
                            floatingLabel

                            counter

                            ></kode4-textfield>

                        <kode4-textarea
                            name="floatinglabel2"
                            label="Floating Label (Number)"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block
                            floatingLabel

                            counter
                            required

                            ></kode4-textarea>

                        <kode4-textfield
                            name="outline"
                            label="Outline"
                            value="312312321313123123123"

                            block
                            outline

                            counter
                            counter-max="10"
                            counter-label="Zeichen"

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outlinefloatinglabel"
                            label="Outline"
                            suffix="mm"
                            hint="Das hier ist einfach nur ein hint."

                            block
                            floatingLabel
                            outline

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            name="filled"
                            label="Filled"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"
                            value="312312321313123123123"

                            block
                            filled

                            counter
                            counter-max="10"
                            counter-label="Chars"
                            counter-divider="of"

                            ></kode4-textfield>

                        <kode4-check
                            name="cb1"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb2"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb3"
                            label="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb4"
                            label="Dies ist eine Checkbox"
                            suffix="mm"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt!"

                            block
                            filled
                            required

                            ></kode4-check>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="searchsimple"
                            placeholder="Search"
                            .iconPrepend="${dd.wn}"
                            hint="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block

                            counter
                            counter-max="10"
                            counter-label="Chars"
                            counter-divider="of"

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchfilled"
                            placeholder="Search"
                            .iconAppend="${dd.wn}"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchoutline"
                            label="Search"
                            .iconAppendOutside="${dd.wn}"

                            block
                            outline
                            floatingLabel

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="username"
                            .iconPrependOutside="${sd.IL}"
                            prefix="Username:"

                            block

                            counter

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="password"
                            type="password"
                            .iconPrependOutside="${ad.DD}"
                            prefix="Password:"

                            block

                            ></kode4-textfield>

                        <kode4-check
                            name="cb5"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb6"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb7"
                            label="Achtung! Die Zeichenanzahl ist begrenzt! Hier steht jetzt aber so viel text, dass man niemals weiss, ob das nicht irgendwie umbricht oder nicht. Der Lorem-Shortcode geht nicht, deshalb muss ich mir selbst etwas ausdenken!"

                            block
                            filled
                            required

                            ></kode4-check>

                        <kode4-check
                            name="cb8"
                            label="Dies ist eine Checkbox"

                            block
                            filled
                            required

                            ></kode4-check>


                        </div>
                </div>

                <kode4-textfield
                        name="allin"
                        label="All in"

                        value="abc"
                        placeholder="Type something..."
                        hint="All features. Default value is 'abc'"

                        .iconPrependOutside="${sd.IL}"
                        .iconPrepend="${ad.DD}"
                        .iconAppend="${dd.wn}"
                        .iconAppendOutside="${rd.by}"
                        prefixOutside="Outside"
                        prefix="Inside"
                        suffix="Inside"
                        suffixOutside="Outside"

                        block
                        filled
                        outline
                        foatingLabel

                        clearable
                        resettable
                        changeOnType

                        required

                        ></kode4-textfield>


                <!--
                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>
                -->

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],cd.prototype,"formDirty",void 0),cd=Qt([eo("kode4form-demo-textfields")],cd);const pd=new Is;pd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",pd.noSelection="Bitte wählen...";let ud=class extends Ji{constructor(){super(),this.formDirty=!1,this.demoOptions=[{label:"Bitte wählen"},{value:"1111",label:"a one"},{value:"2222",label:"and a two"},{value:"3333",labelX:Pi`Ich bin ein mächtiges<br>mehrzeiliges Monstelement!<br><span style="color: #c3c; font-weight: bold;">KLICK MICH!!!!</span>`,label:'Ich bin ein mächtiges<br>mehrzeiliges Monstelement!<br><span style="color: #c3c; font-weight: bold;">KLICK MICH!!!!</span>',unsafe:!0},{value:"4444",label:"and a four"},{value:"5555",label:"and a five"}],this.fields=[]}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}onChangeFields(){this.fields=[...this.fields,"extra-field-3","extra-field-4","extra-field-5"]}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-select
                            name="select1"
                            label="Floating North"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            hint="Type something"
                            outline

                            .options="${this.demoOptions}"
                            value="2222"

                            .iconAppend="${rd.by}"
                            .iconPrependOutside="${sd.IL}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            filled
                            floatingLabel
                            clearable

                            placeholder="Wähle weise"

                            required
                            ></kode4-select>

                        <kode4-select
                            name="select2"
                            type="autocomplete"
                            label="Autocomplete"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            .options="${this.demoOptions}"

                            .iconAppend="${rd.by}"
                            .iconPrependOutside="${sd.IL}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            required

                            ></kode4-select>

                        <kode4-select
                            name="select-jedi"
                            type="autocomplete"
                            label="Choose Jedi"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            value="5"
                            .optionsProvider="${pd}"

                            .iconAppend="${rd.by}"
                            .iconPrependOutside="${sd.IL}"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel
                            outline

                            ></kode4-select>

                        <kode4-textfield
                            name="text1"
                            label="Floating North"
                            .iconAppend="${rd.by}"
                            .iconPrependOutside="${sd.IL}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            floatingLabel
                            outline

                            ></kode4-textfield>
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text2"
                            label="Floating North"
                            .iconAppend="${rd.by}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel
                            filled

                            clearable

                            ></kode4-textfield>

                        <kode4-textfield
                            name="text3"
                            label="Floating North"
                            .iconAppend="${rd.by}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="text4"
                            label="Floating North"
                            .iconAppend="${rd.by}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-textarea
                            name="text5"
                            label="Floating North"
                            .iconAppend="${rd.by}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-textarea>

                            
                        <kode4-check
                            name="check1"
                            label="Checkbox"
                            .iconAppend="${rd.by}"
                            prefixOutside="Outside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            block
                            floatingLabel

                            ></kode4-check>
                        </div>
                </div>

                <hr>
                ${this.fields.map((e=>Pi`
                    <kode4-textfield
                        name="${e}"
                        label="${e}"
                        value="${e}"

                        block
                        floatingLabel

                        ></kode4-textfield>

                `))}
                <hr>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="submit" name="submitButton" value="submitButton-value" filled primary>Submit (width value)</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button @click="${this.onChangeFields}" filled secondary>Change Fields</kode4-button>
                <kode4-button type="updateFields" filled secondary>Update Fields</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],ud.prototype,"formDirty",void 0),Qt([io({reflect:!0})],ud.prototype,"demoOptions",void 0),Qt([io()],ud.prototype,"fields",void 0),ud=Qt([eo("kode4form-demo-formlogic")],ud);const hd=new Is;hd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",hd.noSelection="Bitte wählen...";const fd=new Is;fd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",fd.noSelection="Bitte wählen...";const gd=new Is;gd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",gd.noSelection="Bitte wählen...";let md=class extends Ji{constructor(){super(),this.staticOptions=[{value:1,label:"Option 1"},{value:2,label:"Option 2"},{value:3,label:"Option 3"},{value:4,label:"Option 4"},{value:5,label:"Option 5"}],this.text2="123",this.check1="on",this.check2=!0,this.formDirty=!1,this.select2=2,this.selectrest2=5,this.autocompleterest1=5}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}changeValues(){this.text2="456",this.text2="456",this.check1="on",this.check2=!1,this.select1=4,this.select2=void 0,this.selectrest1=5,this.selectrest2=2,this.autocompleterest1=7}render(){var e;return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${()=>this.requestUpdate()}">
                <h1>Form Dirty-checking</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Text 1"
                            value="${this.text1}"
                            
                            floatingLabel
                            
                            clearable
                            resettable
                            
                            ></kode4-textfield>
                            
                            <kode4-textfield
                            name="text2"
                            label="Text 2"
                            
                            value="${this.text2}"
                            hint="Default value is '${this.text2}'"

                            floatingLabel

                            clearable
                            resettable

                            ></kode4-textfield>

                        <kode4-check
                            name="check1"
                            label="Check 1"
                            value="${this.check1}"
                            required
                            ></kode4-check>

                        <kode4-check
                            name="check2"
                            label="Check 2"
                            value="${this.check2}"
                            ></kode4-check>

                        <kode4-select
                            name="select1"
                            label="Select 1"
                            value="${this.select1}"
                            .options="${this.staticOptions}"
                            
                            clearable
                            resettable
                            
                            ></kode4-select>
                            
                        <kode4-select
                            name="select2"
                            label="Select 2"
                            value="${this.select2}"
                            .options="${this.staticOptions}"
                            hint="Default value is '2' ('Option 2')"
                            
                            clearable
                            resettable

                            ></kode4-select>
                            
                        <kode4-select
                            name="selectrest1"
                            label="Jedi Select"
                            value="${this.selectrest1}"
                            .optionsProvider="${hd}"

                            clearable
                            resettable

                            ></kode4-select>

                        <kode4-select
                            name="selectrest2"
                            label="Jedi Select 2"
                            value="${this.selectrest2}"
                            value="5"
                            .optionsProvider="${fd}"
                            hint="Default value is '2' ('Option 2')"

                            clearable
                            resettable

                            ></kode4-select>

                            <kode4-select
                            label="Jedi Autocomplete"
                            name="autocompleterest1"
                            type="autocomplete"
                            value="${this.autocompleterest1}"
                            .optionsProvider="${gd}"
                            hint="Default value is '2' ('Option 2')"

                            clearable
                            resettable

                            required

                            ></kode4-select>
                    </div>
                </div>

                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="button" @click="${this.changeValues}">Change Values</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>

                <br><br>
                <div class="form-dirty">Form is dirty</div>
                <div class="form-clean">Form is not dirty</div>
                <hr>
                ${(null===(e=this.k4form)||void 0===e?void 0:e.dirty)?"DIRRY":"KLEEN"}
                <hr>
            </kode4-form>

            ${this.formDirty?"Form is dirty":"Form is not dirty"}
        `}};Qt([ro("kode4-form")],md.prototype,"k4form",void 0),Qt([io({type:String})],md.prototype,"text1",void 0),Qt([io({type:String})],md.prototype,"text2",void 0),Qt([io({type:String})],md.prototype,"check1",void 0),Qt([io({type:Boolean})],md.prototype,"check2",void 0),Qt([io({type:Boolean})],md.prototype,"formDirty",void 0),Qt([io({type:Number})],md.prototype,"select1",void 0),Qt([io({type:Number})],md.prototype,"select2",void 0),Qt([io({type:Number})],md.prototype,"selectrest1",void 0),Qt([io({type:Number})],md.prototype,"selectrest2",void 0),Qt([io({type:Number})],md.prototype,"autocompleterest1",void 0),md=Qt([eo("kode4form-demo-dirtychecking")],md);var vd={prefix:"fas",iconName:"chevron-left",icon:[320,512,[9001],"f053","M224 480c-8.188 0-16.38-3.125-22.62-9.375l-192-192c-12.5-12.5-12.5-32.75 0-45.25l192-192c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L77.25 256l169.4 169.4c12.5 12.5 12.5 32.75 0 45.25C240.4 476.9 232.2 480 224 480z"]},bd={prefix:"fas",iconName:"chevron-right",icon:[320,512,[9002],"f054","M96 480c-8.188 0-16.38-3.125-22.62-9.375c-12.5-12.5-12.5-32.75 0-45.25L242.8 256L73.38 86.63c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25l-192 192C112.4 476.9 104.2 480 96 480z"]},kd={prefix:"fas",iconName:"key",icon:[512,512,[128273],"f084","M282.3 343.7L248.1 376.1C244.5 381.5 238.4 384 232 384H192V424C192 437.3 181.3 448 168 448H128V488C128 501.3 117.3 512 104 512H24C10.75 512 0 501.3 0 488V408C0 401.6 2.529 395.5 7.029 391L168.3 229.7C162.9 212.8 160 194.7 160 176C160 78.8 238.8 0 336 0C433.2 0 512 78.8 512 176C512 273.2 433.2 352 336 352C317.3 352 299.2 349.1 282.3 343.7zM376 176C398.1 176 416 158.1 416 136C416 113.9 398.1 96 376 96C353.9 96 336 113.9 336 136C336 158.1 353.9 176 376 176z"]},yd={prefix:"fas",iconName:"lock",icon:[448,512,[128274],"f023","M80 192V144C80 64.47 144.5 0 224 0C303.5 0 368 64.47 368 144V192H384C419.3 192 448 220.7 448 256V448C448 483.3 419.3 512 384 512H64C28.65 512 0 483.3 0 448V256C0 220.7 28.65 192 64 192H80zM144 192H304V144C304 99.82 268.2 64 224 64C179.8 64 144 99.82 144 144V192z"]},xd={prefix:"fas",iconName:"magnifying-glass",icon:[512,512,[128269,"search"],"f002","M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z"]},wd=xd,$d={prefix:"fas",iconName:"user",icon:[448,512,[62144,128100],"f007","M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"]},Cd=i(864);let Sd=class extends Ji{constructor(){super(),this.bgColor="#ccc"}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}cycleBgColor(){const e=["transparent","#ccc","#f00","#0f0","#00f","#ff0","#f0f","#0ff"],t=e.findIndex((e=>e===this.bgColor))+1;this.bgColor=t>e.length-1?e[0]:e[t]}render(){return Pi`
            <div>
                <kode4-button flat .icon>
                    Button!
                    <br>
                    with Menu
                    <kode4-menu slot="menu" parent>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
                <kode4-button flat no-menu-icon secondary>
                    <kode4-fa-icon .icon="${Cd.iV}"></kode4-fa-icon>
                    <kode4-menu slot="menu" parent right cover>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
                <kode4-button outline filled .icon>
                    Button!
                    <br>
                    without  Menu
                </kode4-button>
                <kode4-button outline filled xs no-menu-icon>
                    Small Button with Menu
                    <br>
                    but no Icon
                    <kode4-menu slot="menu" parent right>
                        <div>
                            Element 1
                        </div>
                        <div>
                            Element 2
                        </div>
                        <div>
                            Element 3
                        </div>
                        <div>
                            Element 4
                        </div>
                    </kode4-menu>
                </kode4-button>
            </div>
            <div style="background-color: ${this.bgColor};" @click="${this.cycleBgColor}">
                <div>
                    <kode4-button>
                        Submit
                    </kode4-button>
                    <kode4-button filled>
                        Submit
                    </kode4-button>
                    <kode4-button outline>
                        Submit
                    </kode4-button>
                    <kode4-button flat>
                        Submit
                    </kode4-button>
                    <kode4-button filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary flat filled xxs>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xs>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled s>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled l>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xl>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat filled xxl>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary>
                        Submit
                    </kode4-button>
                    <kode4-button primary filled>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline>
                        Submit
                    </kode4-button>
                    <kode4-button primary flat>
                        Submit
                    </kode4-button>
                    <kode4-button primary filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button secondary>
                        Submit
                    </kode4-button>
                    <kode4-button secondary filled>
                        Submit
                    </kode4-button>
                    <kode4-button secondary outline>
                        Submit
                    </kode4-button>
                    <kode4-button secondary flat>
                        Submit
                    </kode4-button>
                    <kode4-button secondary filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button secondary outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button success>
                        Submit
                    </kode4-button>
                    <kode4-button success filled>
                        Submit
                    </kode4-button>
                    <kode4-button success outline>
                        Submit
                    </kode4-button>
                    <kode4-button success flat>
                        Submit
                    </kode4-button>
                    <kode4-button success filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button success outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button danger>
                        Submit
                    </kode4-button>
                    <kode4-button danger filled>
                        Submit
                    </kode4-button>
                    <kode4-button danger outline>
                        Submit
                    </kode4-button>
                    <kode4-button danger flat>
                        Submit
                    </kode4-button>
                    <kode4-button danger filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button danger outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button warning>
                        Submit
                    </kode4-button>
                    <kode4-button warning filled>
                        Submit
                    </kode4-button>
                    <kode4-button warning outline>
                        Submit
                    </kode4-button>
                    <kode4-button warning flat>
                        Submit
                    </kode4-button>
                    <kode4-button warning filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button warning outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button info>
                        Submit
                    </kode4-button>
                    <kode4-button info filled>
                        Submit
                    </kode4-button>
                    <kode4-button info outline>
                        Submit
                    </kode4-button>
                    <kode4-button info flat>
                        Submit
                    </kode4-button>
                    <kode4-button info filled flat>
                        Submit
                    </kode4-button>
                    <kode4-button info outline flat>
                        Submit
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button icon info outline>
                        <kode4-fa-icon .icon="${vd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon info outline>
                        <kode4-fa-icon .icon="${bd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon danger filled>
                        <kode4-fa-icon .icon="${vd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon success filled>
                        <kode4-fa-icon .icon="${bd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div style="font-size: 25px;">
                    <kode4-button icon primary>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary outline flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div style="font-size: 25px;">
                    <kode4-button icon primary filled xxs>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xs>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled s>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled m>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled l>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xl>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button icon primary filled xxl>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button iconSquare primary>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary filled>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary outline>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary filled flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                    <kode4-button iconSquare primary outline flat>
                        <kode4-fa-icon .icon="${wd}"></kode4-fa-icon>
                    </kode4-button>
                </div>

                <div class="kode4demo-spacer-v"></div>

                <div>
                    <kode4-button primary filled block>
                        Submit
                    </kode4-button>
                    <kode4-button primary outline block>
                        Submit
                    </kode4-button>
                </div>
            </div>
        `}};Qt([io({type:String})],Sd.prototype,"bgColor",void 0),Sd=Qt([eo("kode4form-demo-buttons")],Sd);var Od=i(322);const Ed=new Is;Ed.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Ed.noSelection="Bitte wählen...";let Dd=class extends Ji{constructor(){super(),this.format1="dd.mm.yyyy",this.formDirty=!1,this.value1="",this.geburtsdatum="1980-03-27"}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Date-Time-Pickers</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <!-- <kode4-datetime-picker
                            name="date1"

                            label="Date"
                            hint="Format is: ${this.format1}"

                            value="2018-07-02T19:29:00+0200"
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime1"

                            label="Date and Time"
                            hint="Format is: ${this.format1}"

                            value="2018-09-02T19:29:00+0000"
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            name="date2"

                            label="Date from Timestamp"

                            value="1535909340"
                            timestamp
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime2"

                            label="Date and Time from Timestamp"

                            value="1590135676813"
                            timestamp
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <kode4-datetime-picker
                            type="datetime"
                            name="datetime3"

                            label="Date and Time from Datestring"

                            value="2018-09-02T19:29:00+0000"
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-datetime-picker
                            name="dateMin1"

                            label="Date"
                            hint="Format is: ${this.format1}"

                            value="2018-07-02T19:29:00+0200"
                            mind="${Math.round((new Date).getTime()/1e3)+1209600}"
                            minX="2020-07-02T19:29:00+0200"
                            
                            .iconPrependOutside="${Od.fT}"

                            filled
                            clearable

                            required
                            ></kode4-datetime-picker> -->


                            <br>
                            Effects to: ${this.value1}
                            <br>
                            <kode4-datetime-picker
                                type="datetime"
                                name="datetime1"

                                label="Date and Time from Datestring"

                                value="2018-09-02T19:29:00+0200"
                                @change="${e=>this.value1=e.detail.value}"
                                
                                .iconPrependOutside="${Od.fT}"

                                filled
                                clearable

                                required
                                ></kode4-datetime-picker>

                            <kode4-datetime-picker
                                type="datetime"
                                name="datetime2"

                                label="Date and Time from Datestring"

                                value="${this.value2}"
                                @change="${e=>this.value2=e.detail.value}"
                                
                                .iconPrependOutside="${Od.fT}"

                                filled
                                clearable

                                required
                                ></kode4-datetime-picker>
                        
                            <div>

                            </div>
                                <kode4-datetime-picker
                                        type="date"
                                        name="geburtsdatum"
                                        label="Geburtsdatum"
                                        type="date"
                                        value="${this.geburtsdatum}"
                                        block
                                        outline
                                        floatingLabel
                                        ></kode4-datetime-picker>
                                
                                <kode4-datetime-picker
                                        type="date"
                                        name="geburtsdatum2"
                                        label="Geburtsdatum"
                                        hint="${this.geburtsdatum?"Falls Sie Ihr Geburtsdatum falsch eingegeben haben, kontaktieren Sie uns bitte um es zu ändern.":""}"
                                        type="date"
                                        ?readonly="${this.geburtsdatum}"
                                        value="${this.geburtsdatum}"
                                        block
                                        outline
                                        floatingLabel
                                        ></kode4-datetime-picker>
                                

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>

                </div>

                <div class="kode4demo-spacer-v"></div>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="kode4demo-spacer-v"></div>

                <div class="form-dirty">Dirty</div>
                <div class="form-clean">Clean</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty?Pi`<div class="form-dirty">Dirty</div>`:Pi`<div class="form-clean">Clean</div>`}
        `}};Qt([io({type:String})],Dd.prototype,"format1",void 0),Qt([io({type:Boolean})],Dd.prototype,"formDirty",void 0),Qt([io({type:String})],Dd.prototype,"value1",void 0),Qt([io({type:String})],Dd.prototype,"value2",void 0),Qt([io({type:String})],Dd.prototype,"geburtsdatum",void 0),Dd=Qt([eo("kode4form-demo-datetimepickers")],Dd);const zd=new Is;zd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",zd.noSelection="Bitte wählen...";const Ad={conditions:[(e,t)=>{const i=e.getField("showFields");return Boolean(null==i?void 0:i.value)}],handleThen(e,t){t.hidden=!1},handleElse(e,t){t.hidden=!0}},Ld={conditions:[(e,t)=>{const i=e.getField("setValue");return Boolean(null==i?void 0:i.value)}],handleThen(e,t){t.value="option3"}};let _d=class extends Ji{constructor(){super(),this.formDirty=!1}createRenderRoot(){return this}onSubmit(){}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Custom Selects</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-check
                            name="showFields"
                            label="Show Fields"
                            block
                            value="true"
                            >
                            </kode4-check>

                        <kode4-check
                            name="setValue"
                            label="Set Value"
                            block
                            >
                            </kode4-check>

                        <kode4-textfield
                            name="dummy1"
                            label="Dummy"
                            outline
                            floatingLabel
                            ></kode4-textfield>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-custom-select
                            name="customSelect1"
                            label="Please click an image"

                            label="Image Type"
                            hint="Click an Image to select"

                            
                            filled
                            clearable

                            required
                            .conditions="${[Ad,Ld]}">
                                
                            <kode4-custom-select-option 
                                value="option1" 
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 1</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option 
                                value="option2"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 2</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option
                                value="option3"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 3</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option
                                value="option4"
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 4</div>
                            </kode4-custom-select-option>
                            
                            <kode4-custom-select-option 
                                value="option5" 
                                class="kode4-custom-option-opacity kode4-custom-option-grayscale"
                                style="margin-left: 10px;">
                                <img src="https://loremflickr.com/640/360">    
                                <div>Option 5</div>
                            </kode4-custom-select-option>
                        
                            
                        </kode4-custom-select>

                        <div class="kode4demo-spacer-v"></div>

                        <kode4-textfield
                            name="dummy2"
                            label="Dummy"
                            outline
                            floatingLabel
                            ></kode4-textfield>
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>

                </div>

                <div class="kode4demo-spacer-v"></div>

                <kode4-button type="submit" filled primary>Submit</kode4-button>
                <kode4-button type="reset" filled warning>Reset</kode4-button>
                <kode4-button type="clear" filled danger>Clear</kode4-button>
                <kode4-button type="updateValues" filled secondary>Update Values</kode4-button>
                <kode4-button type="respawn" filled success>Respawn</kode4-button>

                <div class="kode4demo-spacer-v"></div>

                <div class="form-dirty">Dirty</div>
                <div class="form-clean">Clean</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty?Pi`<div class="form-dirty">Dirty</div>`:Pi`<div class="form-clean">Clean</div>`}
        `}};Qt([io({type:Boolean})],_d.prototype,"formDirty",void 0),_d=Qt([eo("kode4form-demo-cutomselects")],_d);const Pd=new Is;Pd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Pd.noSelection="Bitte wählen...";const Td={conditions:[(e,t)=>{const i=e.getField("showLogin");return Boolean(null==i?void 0:i.value)}],handleThen(e,t){t.hidden=!1},handleElse(e,t){t.hidden=!0}},Bd={conditions:[(e,t)=>{const i=e.getField("enableLogin");return Boolean(null==i?void 0:i.value)}],handleThen(e,t){t.disabled=!1},handleElse(e,t){t.disabled=!0}};let Id=class extends Ji{constructor(){super(),this.formDirty=!1}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Conditions</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            
                            block

                            ></kode4-textfield>

                        <kode4-textfield
                            name="floatinglabel"
                            label="Floating Label (Number)"
                            type="number"
                            
                            block
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outline"
                            label="Outline"

                            block
                            outline

                            ></kode4-textfield>

                        <kode4-textfield
                            name="outlinefloatinglabel"
                            label="Outline"
                            suffix="mm"

                            block
                            floatingLabel
                            outline

                            ></kode4-textfield>

                        <kode4-textfield
                            name="filled"
                            label="Filled"
                            suffix="mm"

                            block
                            filled

                            ></kode4-textfield>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="searchsimple"
                            placeholder="Search"
                            .iconPrepend="${wd}"

                            block

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchfilled"
                            placeholder="Search"
                            .iconAppend="${wd}"

                            block
                            filled

                            ></kode4-textfield>

                        <kode4-textfield
                            name="searchoutline"
                            label="Search"
                            .iconAppendOutside="${wd}"

                            block
                            outline
                            floatingLabel

                            ></kode4-textfield>

                        <kode4-check
                            name="showLogin"
                            label="Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login Show Login "
                            block
                            >
                        </kode4-check>

                        <kode4-check
                            name="enableLogin"
                            label="Enable Login"
                            block
                            value="true"
                            >
                        </kode4-check>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="username"
                            .iconPrependOutside="${$d}"
                            prefix="Username:"

                            block

                            .conditions="${[Td,Bd]}"

                            ></kode4-textfield>

                        <kode4-textfield
                            class="field-prefix-fixed"
                            name="password"
                            type="password"
                            .iconPrependOutside="${kd}"
                            prefix="Password:"

                            block

                            .conditions="${[Td,Bd]}"
                            ></kode4-textfield>

                        </div>
                </div>

                <kode4-textfield
                        name="allin"
                        label="All in"

                        value="abc"
                        placeholder="Type something..."
                        hint="All features. Default value is 'abc'"

                        .iconPrependOutside="${$d}"
                        .iconPrepend="${kd}"
                        .iconAppend="${wd}"
                        .iconAppendOutside="${yd}"
                        prefixOutside="Outside"
                        prefix="Inside"
                        suffix="Inside"
                        suffixOutside="Outside"

                        block
                        filled
                        outline
                        foatingLabel

                        clearable
                        resettable
                        changeOnType

                        required

                        ></kode4-textfield>


                <!--
                <kode4-button type="submit">Submit</kode4-button>
                <kode4-button type="reset">Reset</kode4-button>
                <kode4-button type="clear">Clear</kode4-button>
                <kode4-button type="updateValues">Update Values</kode4-button>
                <kode4-button type="respawn">Respawn</kode4-button>
                -->

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],Id.prototype,"formDirty",void 0),Id=Qt([eo("kode4form-demo-conditions")],Id);const Fd=new Is;Fd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Fd.noSelection="Bitte wählen...";let Md=class extends Ji{constructor(){super(),this.formDirty=!1,this.showFields=!0,this.condition123=(e,t)=>{const i=e.getField("text1");return console.log("== 123?",null==i?void 0:i.value),Boolean("123"==(null==i?void 0:i.value))},this.conditionNot123=(e,t)=>{const i=e.getField("text1");return console.log("!= 123?",null==i?void 0:i.value),Boolean("123"!=(null==i?void 0:i.value))},this.showIf123={conditions:this.condition123,handleThen(e,t){console.log("showIf123 then"),t.hidden=!1,t.disabled=!1},handleElse(e,t){console.log("showIf123 else"),t.hidden=!0,t.disabled=!0}},this.showIfNot123={conditions:this.conditionNot123,handleThen(e,t){console.log("showIfNot123 then"),t.hidden=!1,t.disabled=!1},handleElse(e,t){console.log("showIfNot123 else"),t.hidden=!0,t.disabled=!0}}}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}render(){return Pi`
            <kode4-form action="/" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Form-Style</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>

                        ${this.showFields?Pi`
                            <div>
                            <div>
                            <kode4-textfield
                                name="text0"
                                label="Text 0"
                                hint="This field is included in submit even if it is empty"

                                submitEmpty

                                floatingLabel
                                outline
                                block
                                changeOnType

                                ></kode4-textfield>

                            <kode4-textfield
                                name="text1"
                                label="Text 1"
                                hint="try '123'"

                                floatingLabel
                                outline
                                block
                                changeOnType

                                ></kode4-textfield>

                            <kode4-formblock
                                block
                                .conditions="${od("text1","123")}"
                                >
                                <div style="width: 100%; border: dotted 2px #f00; padding: 10px; margin: 10px;">
                                    Text 1 is now 123 !!!!
                                </div>
                            </kode4-formblock>
                                
                            <kode4-textfield
                                name="text2"
                                label="Text 2"
                                hint="Visible if Text 1 is 123"
                                .conditions="${od("text1","123")}"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>
                                
                            <kode4-textfield
                                name="text3"
                                label="Text 3"
                                hint="Visible if Text 1 is not 123"
                                .conditions="${e="text1",t="123",{conditions:ed(e,t),handleThen:td(),handleElse:id()}}"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>
                                
                            <kode4-textfield
                                name="text4"
                                label="Text 4"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <hr>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 1"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 2"

                                floatingLabel
                                outline
                                block

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 3"
                                hint="This Field is required"

                                floatingLabel
                                outline
                                block

                                .conditions="${od("text1","123")}"
                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="arrayfield[]"
                                label="Array-Field 4"

                                floatingLabel
                                outline
                                block

                                .conditions="${od("text1","123")}"

                                ></kode4-textfield>
                            </div>
                        </div>

                        `:""}
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>

                        ${this.showFields?Pi`
                            <kode4-button
                                filled 
                                danger 
                                flat
                                block 
                                @click="${()=>this.showFields=!1}">
                            
                                 Hide all fields
                            </kode4-button>
                        `:""}
                        ${this.showFields?"":Pi`
                            <kode4-button
                            filled 
                            success 
                            block 
                            flat
                            @click="${()=>this.showFields=!0}">Show all fields</kode4-button>
                        `}
                        
                        <br><br>
                        <div class="kode4-button-group">
                            <kode4-button
                                filled 
                                secondary
                                manageValue
                                name="visibleSelected"
                                value="visible"
                                ?disabled="${!this.showFields}"
                                @click="${e=>{console.log("You should see this only if the button is not disabled",e)}}">
                                Fields are VISIBLE ${this.showFields?"":"(DISABLED)"}
                            </kode4-button>
                            <kode4-button
                                filled 
                                secondary 
                                manageValue
                                name="invisibleSelected"
                                value="invisible"
                                ?disabled="${this.showFields}"
                                @click="${e=>{console.log("You should see this only if the button is not disabled",e)}}">
                                Fields are INVISIBLE ${this.showFields?"(DISABLED)":""}
                            </kode4-button>
                        </div>

                    </div>
                </div>

                ${""}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `;var e,t}};Qt([io({type:Boolean})],Md.prototype,"formDirty",void 0),Qt([io({type:Boolean})],Md.prototype,"showFields",void 0),Md=Qt([eo("kode4form-demo-dynamic-forms")],Md);let Rd=class extends Ji{constructor(){super(),this.k4form=null,this.formDirty=!1,this.staticOp=new Bs,this.formenList=[]}firstUpdated(e){super.firstUpdated(e),this.jediOptionsProvider=new Is,this.jediOptionsProvider.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",this.jediOptionsProvider.valueField="value",this.jediOptionsProvider.labelField="label",this.k4form=this.querySelector("kode4-form"),this.staticOp.setList([{value:1,label:"ARR"},{value:2,label:"ARR ARR"},{value:3,label:"ARR ARR ARR"}]),this.staticOpValue=2,setTimeout((()=>{this.formenList=[{id:1,label:"Stab"},{id:2,label:"Vierkantstab"},{id:3,label:"Vierkantrohr"},{id:4,label:"Rohr"},{id:5,label:"Träger"}]}),5e3)}useStaticOpUserValue(e){e.preventDefault(),this.staticOpValue=this.userOpValue,console.log("Now using value",this.staticOpValue)}useStaticOpUserValues(e){e.preventDefault(),this.staticOp.setList(this.userOpList),this.formenList=this.userOpList,console.log("Now using list",this.staticOp.list)}createRenderRoot(){return this}render(){var e;return Pi`
      <kode4-form
        action="/"
        @stateChanged="${()=>console.log("FORM STATE CHANGD")}"
      >
        <h1>Form-Style</h1>

        <div style="display: flex; flex-direction: row; width: 100%;;">
          <div
            style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;"
          >
            <kode4-select
              id="staticOp"
              name="leistungId"
              label="Static Options Provider"
              hint="Value = ${this.staticOpValue}"
              .optionsProvider="${this.staticOp}"
              value="${this.staticOpValue}"
              @change="${e=>{this.staticOpValue=e.detail.value}}"
              block
              outline
              floatingLabel
            ></kode4-select>

            <kode4-select
              name="select-jedi"
              type="autocomplete"
              label="Choose Jedi"
              value="${this.staticOpValue}"
              .optionsProvider="${this.jediOptionsProvider}"
              .iconAppend="${rd.by}"
              .iconPrependOutside="${sd.IL}"
              block
              floatingLabel
              outline
              force-selection
            ></kode4-select>


            <kode4-custom-select
                name="customSelect1"
                label="Please click an image"

                label="Image Type"
                hint="Click an Image to select"

                value="${this.staticOpValue}"
                @change="${e=>{this.staticOpValue=e.detail.value}}"

                filled
                clearable

                required
                >

                ${this.formenList.map((e=>Pi`
                        <kode4-custom-select-option
                            value="${e.id}"
                            class="kode4-custom-option-opacity kode4-custom-option-grayscale">
                            <div>${e.id}: ${e.label}</div>
                        </kode4-custom-select-option>
                    `))}

            </kode4-custom-select>

          </div>

          <div
            style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;"
          >
            <input
              style="display: block; width: 100%;"
              type="number"
              @change="${e=>{e.stopPropagation(),e.preventDefault(),this.userOpValue=Number(e.target.value)}}"
            />

            <textarea
              style="display: block; width: 100%; height: 300px; margin-top: 10px;"
              @change="${e=>{e.stopPropagation(),e.preventDefault(),this.userOpList=JSON.parse(e.target.value.trim())}}"
            >
[
    {
        "id": 1,
        "bezeichnung": "Hohe Standzeit"
    },
    {
        "id": 2,
        "bezeichnung": "Normal"
    },
    {
        "id": 3,
        "bezeichnung": "Hohe Leistung"
    }
]
                            </textarea
            >

            <div style="margin-top: 10px;">
              <kode4-button
                type="button"
                filled
                primary
                @click="${this.useStaticOpUserValues}"
                >Update List</kode4-button
              >
              <kode4-button
                type="button"
                filled
                primary
                @click="${this.useStaticOpUserValue}"
                >Update Value</kode4-button
              >
            </div>
          </div>
        </div>

        <kode4-button type="submit" filled primary>Submit</kode4-button>
        <kode4-button
          type="submit"
          name="submitButton"
          value="submitButton-value"
          filled
          primary
          >Submit (with value)</kode4-button
        >
        <kode4-button
          type="button"
          filled
          primary
          @click="${()=>{console.log("UPDATE OPTIONS PROVIDER"),this.jediOptionsProvider=new Is,this.jediOptionsProvider.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",this.jediOptionsProvider.valueField="value",this.jediOptionsProvider.labelField="label",console.log("> UPDATE OPTIONS PROVIDER DONE")}}"
          >Update Options Provider</kode4-button
        >
        <kode4-button type="reset" filled warning>Reset</kode4-button>
        <kode4-button type="clear" filled danger>Clear</kode4-button>
        <kode4-button type="updateFields" filled secondary
          >Update Fields</kode4-button
        >
        <kode4-button type="updateValues" filled secondary
          >Update Values</kode4-button
        >
        <kode4-button type="respawn" filled success>Respawn</kode4-button>
        <hr />
        <div class="form-dirty">DIRTY!!!</div>
        <div class="form-clean">CLEAN!!!</div>
        <hr>
        ${(null===(e=this.k4form)||void 0===e?void 0:e.dirty)?"DIRRY":"KLEEN"}
        <hr />
        Dirty? ${this.formDirty}
      </kode4-form>
    `}};Qt([io()],Rd.prototype,"k4form",void 0),Qt([io({type:Boolean})],Rd.prototype,"formDirty",void 0),Qt([io()],Rd.prototype,"staticOp",void 0),Qt([io()],Rd.prototype,"staticOpValue",void 0),Qt([io()],Rd.prototype,"userOpValue",void 0),Qt([io()],Rd.prototype,"userOpList",void 0),Qt([io()],Rd.prototype,"formenList",void 0),Qt([io()],Rd.prototype,"jediOptionsProvider",void 0),Rd=Qt([eo("kode4form-demo-select")],Rd);const Nd=new Is;Nd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Nd.noSelection="Bitte wählen...";let Vd=class extends Ji{constructor(){super(),this.formDirty=!1,this.field1=!1,this.field2=!0,this.field3=!1}firstUpdated(e){super.firstUpdated(e)}submitForm(e){this.formDirty||console.log("FORM IS NOT DIRTY"),console.log("SUBMIT FORM"),console.log([...e.detail.formData.entries()])}createRenderRoot(){return this}render(){return Pi`
            <kode4-form action="/" @stateChanged="${e=>this.formDirty=e.detail.dirty}" @submit="${this.submitForm}">
                <h1>Checkboxes</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h2>Default Fields</h2>
                        <em>These fields will only be submitted if checked</em>

                        <kode4-check
                            name="accountConfirmed"
                            label="Registrierung bestätigt"

                            value="${this.field1}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountRejected"
                            label="Registrierung abgelehnt"

                            value="${this.field2}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountEnabled"
                            label="Benutzerkonto aktiviert"

                            value="${this.field3}"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 2</h2>
                        <em>These fields will always be submitted</em>

                        <kode4-check
                            name="accountConfirmedAlways"
                            label="Registrierung bestätigt"

                            value="${this.field1}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountRejectedAlways"
                            label="Registrierung abgelehnt"

                            value="${this.field2}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="accountEnabledAlways"
                            label="Benutzerkonto aktiviert"

                            value="${this.field3}"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 3</h2>
                        <em>These fields will only be submitted if checked and with a custom value</em>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 1"

                            value="${this.field1}"
                            trueValue="1"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 2"

                            value="${this.field2}"
                            trueValue="2"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups[]"
                            label="Benutzergruppe 3"

                            value="${this.field3}"
                            trueValue="3"

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <h2>Fields 4</h2>
                        <em>These fields will always be submitted and with a custom value</em>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 1"

                            value="${this.field1}"
                            trueValue="1"
                            falseValue="-1"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 2"

                            value="${this.field2}"
                            trueValue="2"
                            falseValue="-2"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                        <kode4-check
                            name="groups2[]"
                            label="Benutzergruppe 3"

                            value="${this.field3}"
                            trueValue="3"
                            falseValue="-3"
                            submitEmpty

                            block
                            outline
                            floatingLabel
                            ></kode4-check>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                    </div>
                </div>

            </kode4-form>
        `}};Qt([io({type:Boolean})],Vd.prototype,"formDirty",void 0),Qt([io({type:Boolean})],Vd.prototype,"field1",void 0),Qt([io({type:Boolean})],Vd.prototype,"field2",void 0),Qt([io({type:Boolean})],Vd.prototype,"field3",void 0),Vd=Qt([eo("kode4form-demo-check")],Vd);const Ud=new Is;Ud.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Ud.noSelection="Bitte wählen...";let jd=class extends Ji{constructor(){super(),this.formDirty=!1,this.valueObject={strg1:null,strg3:"",strg5:void 0,strg6:"",strg7:null}}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}render(){return Pi`
            <kode4-form action="/" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Required-Check</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>


                            <kode4-select
                                name="int1"
                                label="Int 1"
                                value="${this.valueObject.int1}"
                                .options="${[{label:"Choose..."},{label:"Value 0",value:"0"},{label:"Value 1",value:"1"},{label:"Value 2",value:"2"}]}"
                                openDialogOnFocus
                                block
                                required
                                outline
                                floatingLabel
                                ></kode4-select>

                            <kode4-textfield
                                name="string1"
                                label="String 1"
                                value="${this.valueObject.strg1}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string2"
                                label="String 2"
                                value="${this.valueObject.strg2}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string3"
                                label="String 3"
                                value="${this.valueObject.strg3}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string4"
                                label="String 4"
                                value="${this.valueObject.strg4}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string5"
                                label="String 5"
                                value="${this.valueObject.strg5}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string6"
                                label="String 6"
                                value="${this.valueObject.strg6}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                            <kode4-textfield
                                name="string7"
                                label="String 7"
                                value="${this.valueObject.strg7}"

                                floatingLabel
                                outline
                                block

                                required

                                ></kode4-textfield>

                        <br><br>
                        <h1>Fields with submitEmpty</h1>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                    </div>
                </div>

                ${""}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],jd.prototype,"formDirty",void 0),Qt([io({type:Boolean})],jd.prototype,"valueObject",void 0),jd=Qt([eo("kode4form-demo-required")],jd);var qd=i(124);class Hd{constructor(){this.subscribers=[]}subscribe(e){this.subscribers.includes(e)||this.subscribers.push(e)}unsubscribe(e){if(this.subscribers.includes(e)){const t=this.subscribers.indexOf(e);this.subscribers.splice(t,1)}}}class Wd extends Hd{constructor(){super(...arguments),this.fileCount=0,this.uploading=!1,this.progress=0}upload(e){return new Promise((t=>{if(this.uploading)throw new Error("An upload is already in progress. Please wait fopr this Upload to finish.");if(!e)throw new Error("Filelist is empty, nothing.");this.progress=0,this.uploading=!0,this.subscribers.map((e=>{e.handleOnBeginUpload()}));const i=setInterval((()=>{if(this.progress=Math.min(this.progress+Math.floor(10*Math.random()),250),this.progress<100)this.subscribers.map((e=>{e.handleOnUploadProgress(this.progress)}));else{clearInterval(i),this.progress=0,this.uploading=!1,this.subscribers.map((e=>{e.handleOnUploadComplete()}));const o=[];Array.from(e).forEach((e=>{o.push({file:e,filename:`uploaded_filename_${this.fileCount}`}),this.fileCount+=1})),t(o)}}),200)}))}cancel(){}}const Yd=new Is;Yd.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",Yd.noSelection="Bitte wählen...";const Kd=new Wd,Xd=new Wd,Zd=new Wd,Gd=new Wd,Jd=new Wd,Qd=new Wd,el=new Wd,tl=new Wd,il=new Wd;let ol=class extends Ji{constructor(){super(),this.formDirty=!1,this.aspectRatio="16:9",this.uploadLabel="Upload an Image",this.uploadPlaceholder="Drag and drop an image here or click to select",this.showFilename=!0,this.upload1="/demo/assets/fat_cat.jpg",this.strg1="test"}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}render(){return Pi`
            <kode4-form action="/" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>File-Upload</h1>

                <div style="display: flex; flex-direction: row; width: 100%;;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Fields</h1>

                        <kode4-filefield
                            name="upload"
                            label="${Es(this.uploadLabel)}"
                            placeholder="${Es(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            .fileUploader="${Kd}"

                            @change="${e=>{console.warn("...CHANGE",e)}}"
                            @changed="${e=>{console.warn("...CHANGED",e)}}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${sd.IL}"
                            .iconPrepend="${ad.DD}"
                            .iconAppend="${dd.wn}"
                            .iconAppendOutside="${rd.by}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${e=>{e.detail.originalEvent.preventDefault(),console.log("click-icon-prepend-outside")}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-filefield>

                        <kode4-filefield
                            name="upload"
                            label="${Es(this.uploadLabel)}"
                            placeholder="${Es(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            value="${this.upload1}"
                            .fileUploader="${Kd}"

                            @change="${e=>{console.warn("...CHANGE",e)}}"
                            @changed="${e=>{console.warn("...CHANGED",e)}}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${sd.IL}"
                            .iconPrepend="${ad.DD}"
                            .iconAppend="${dd.wn}"
                            .iconAppendOutside="${rd.by}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${e=>{e.detail.originalEvent.preventDefault(),console.log("click-icon-prepend-outside")}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-filefield>

                        <kode4-imagefield
                            name="upload"
                            label="${Es(this.uploadLabel)}"
                            placeholder="${Es(this.uploadPlaceholder)}"
                            ?showFilename="${this.showFilename}"
                            aspect-ratio="${this.aspectRatio}"
                            placeholder-aspect-ratio="true"
                            value="${this.upload1}"
                            .fileUploader="${Kd}"

                            @change="${e=>{console.warn("...CHANGE",e)}}"
                            @changed="${e=>{console.warn("...CHANGED",e)}}"
                            @aspect-ratio-changed="${e=>{console.warn("ASPECT RATIO CHANGED",e.detail.element.getAttribute("aspect-ratio"),e.detail.aspectRatio),this.aspectRatio=e.detail.element.getAttribute("aspect-ratio"),console.log(this.aspectRatio),this.requestUpdate()}}"
                            hint="${this.aspectRatio}"

                            placeholder-icon

                            floatingLabel
                            outline
                            block

                            .iconPrependOutside="${sd.IL}"
                            .iconPrepend="${ad.DD}"
                            .iconAppend="${dd.wn}"
                            .iconAppendOutside="${rd.by}"
                            prefixOutside="Outside"
                            prefix="Inside"
                            suffix="Inside"
                            suffixOutside="Outside"

                            @click-icon-prepend-outside="${e=>{e.detail.originalEvent.preventDefault(),console.log("click-icon-prepend-outside")}}"

                            required

                            style="margin-top: 10px;"

                            ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload1"
                                label="Upload 1"
                                ?showFilename="${this.showFilename}"
                                value="${this.upload1}"
                                .fileUploader="${Xd}"
                                
                                aspect-ratio="${this.aspectRatio}"
                                preview-image-mode="cover"
                                placeholder="Upload image"
                                .placeholderIcon="${qd.Y9}"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload2"
                                label="Upload 2"
                                value="${this.upload2}"
                                .fileUploader="${Zd}"
                                busyStyle="icon"
                                preview-thumb="true"
                                placeholder="You can upload an image here"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload3"
                                label="Upload 3"
                                value="${this.upload3}"
                                .fileUploader="${Gd}"
                                placeholder-icon

                                floatingLabel
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="string4"
                                label="String 4"
                                value="${this.upload4}"
                                .fileUploader="${Jd}"

                                floatingLabel
                                filled
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload5"
                                label="Upload 5"
                                value="${this.upload5}"
                                .fileUploader="${Qd}"

                                filled
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload6"
                                label="Upload 6"
                                value="${this.upload6}"
                                .fileUploader="${el}"

                                filled
                                floatingLabel
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload7"
                                label="Upload 7"
                                value="${this.upload7}"
                                .fileUploader="${tl}"

                                filled
                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <kode4-imagefield
                                name="upload8"
                                label="Upload 8"
                                value="${this.upload8}"
                                .fileUploader="${il}"

                                block

                                style="margin-top: 10px;"

                                ></kode4-imagefield>

                            <div style="border-top: solid 4px #f00; margin-top: 35px;"></div>

                            <kode4-textfield
                                name="string2"
                                label="String 2"
                                value="${this.strg1}"

                                floatingLabel
                                outline
                                block

                                style="margin-top: 10px;"

                                ></kode4-textfield>

                        <br><br>
                        <h1>Fields with submitEmpty</h1>

                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">
                        <h1>Actions</h1>
                        <kode4-button type="submit" filled primary block>Submit</kode4-button>
                        <br>
                        <br>
                        <kode4-button
                            type="button" filled secondary block
                            @click="${()=>{this.aspectRatio="16:9"===this.aspectRatio?"4:3":"16:9"}}"
                            >Toggle Aspect Ratio</kode4-button>
                    </div>
                </div>

                ${""}
                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>

            <div class="kode4demo-spacer-v"></div>
            
            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],ol.prototype,"formDirty",void 0),Qt([io({type:String})],ol.prototype,"aspectRatio",void 0),Qt([io({type:String})],ol.prototype,"uploadLabel",void 0),Qt([io({type:String})],ol.prototype,"uploadPlaceholder",void 0),Qt([io({type:Boolean})],ol.prototype,"showFilename",void 0),Qt([io({type:String})],ol.prototype,"upload1",void 0),Qt([io({type:String})],ol.prototype,"upload2",void 0),Qt([io({type:String})],ol.prototype,"upload3",void 0),Qt([io({type:String})],ol.prototype,"upload4",void 0),Qt([io({type:String})],ol.prototype,"upload5",void 0),Qt([io({type:String})],ol.prototype,"upload6",void 0),Qt([io({type:String})],ol.prototype,"upload7",void 0),Qt([io({type:String})],ol.prototype,"upload8",void 0),Qt([io({type:String})],ol.prototype,"strg1",void 0),ol=Qt([eo("kode4form-demo-fileupload")],ol);const nl=new Is;nl.baseUrl="https://lab.kode4.de/npm/api/1.0/jedi",nl.noSelection="Bitte wählen...";let rl=class extends Ji{constructor(){super(),this.formDirty=!1}firstUpdated(e){super.firstUpdated(e)}createRenderRoot(){return this}onSubmit(){}render(){return Pi`
            <kode4-form action="/" @submit="${this.onSubmit}" @stateChanged="${e=>this.formDirty=e.detail.dirty}">
                <h1>Value-Tests</h1>

                <div style="display: flex; flex-direction: row; width: 100%;">
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                        <kode4-textfield
                            name="text1"
                            label="Simple"
                            block
                            counter
                            ></kode4-textfield>

                        <kode4-check
                            name="cb6"
                            label="Dies ist eine Checkbox"
                            block
                            filled
                            ></kode4-check>
                        
                    </div>
                    
                    <div style="flex: 0 0 50%; width: 50%; padding: 10px; box-sizing: border-box;">

                    </div>
                </div>

                <div class="form-dirty">DIRTY!!!</div>
                <div class="form-clean">CLEAN!!!</div>

                <kode4-button filled primary type="submit">Submit</kode4-button>
            </kode4-form>

            <div class="kode4demo-spacer-v"></div>
            ${this.formDirty}
        `}};Qt([io({type:Boolean})],rl.prototype,"formDirty",void 0),rl=Qt([eo("kode4form-demo-valuetests")],rl);i(445),i(631),i(386),i(375),jt=()=>new Jt,((e,t)=>{Kt.set(e,t)})("default",(new Zt).baseUrl(""))}},e=>{var t;t=902,e(e.s=t)}]);